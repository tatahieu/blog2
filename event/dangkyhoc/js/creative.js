/*!
 * Start Bootstrap - Creative Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        var $width = $(window).outerWidth();
        if($width<=480){
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top - 63)
            }, 1250, 'easeInOutExpo');
            event.preventDefault();
        }else if($width<=768 && $width >480){
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top - 52)
            }, 1250, 'easeInOutExpo');
            event.preventDefault();
        }else if($width<=1024 && $width >768){
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top - 69)
            }, 1250, 'easeInOutExpo');
            event.preventDefault();
        } else{
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top - 94)
            }, 1250, 'easeInOutExpo');
            event.preventDefault();
        }

    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    })

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Fit Text Plugin for Main Header
    $("h1 img").fitText(
        1.2, {
            width: '70%'
        }
    );
    $(".affix ul li a").css(
         {
            'padding-top': '15px',
           'padding-bottom': '15px'
        }
    );

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    // Initialize WOW.js Scrolling Animations
    new WOW().init();

})(jQuery); // End of use strict




$(document).ready(function() {

    var owl = $("#owl-demo");

    owl.owlCarousel({
        itemsCustom : [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 3],
            [1000, 3],
            [1200, 3],
            [1400, 3],
            [1600, 3]
        ],
        pagination:false
    });

    // Custom Navigation Events
    $(".next").click(function(){
        owl.trigger('owl.next');
    })
    $(".prev").click(function(){
        owl.trigger('owl.prev');
    })
    $(".play").click(function(){
        owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
    })
    $(".stop").click(function(){
        owl.trigger('owl.stop');
    })



    var owl1 = $("#owl-demo3");

    owl1.owlCarousel({

        itemsCustom : [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
        navigation : false


    });


});

$(document).ready(function(){
    var $window =$(window).outerWidth();
    if($window<=480){

        var owl1 = $("#owl-demo2");

        owl1.owlCarousel({

            itemsCustom : [
                [0, 1],
                [480, 1],
            ],
            navigation : false,
            pagination:false

        });
        var owl1 = $("#owl-demo4");

        owl1.owlCarousel({

            itemsCustom : [
                [0, 1],
                [480, 1],
            ],
            navigation : false,
            pagination:true

        });
        var owl1 = $("#owl-demo5");

        owl1.owlCarousel({

            itemsCustom : [
                [0, 1],
                [480, 1],
            ],
            navigation : false,
            pagination:true

        });
        var owl1 = $("#owl-demo6");

        owl1.owlCarousel({

            itemsCustom : [
                [0, 1],
                [480, 1],
            ],
            navigation : false,
            pagination:true

        });
        var owl1 = $("#owl-demo1");

        owl1.owlCarousel({

            itemsCustom : [
                [0, 1],
                [450, 1],
                [600, 2],
                [700, 2],
                [1000, 2],
                [1200, 2],
                [1400, 2],
                [1600, 2]
            ],
            navigation : false

        });
    }else{
        var owl1 = $("#owl-demo1");

        owl1.owlCarousel({

            itemsCustom : [
                [0, 1],
                [450, 1],
                [600, 2],
                [700, 2],
                [1000, 2],
                [1200, 2],
                [1400, 2],
                [1600, 2]
            ],
            navigation : false,
            touchDrag:false,
            mouseDrag:false

        });
    }
    $('#hide_float_right').click(function(){
        $(this).find(".x1").toggleClass("y");
        //$(this).find(".y1").toggleClass("x");
        $('#float_content_right').slideToggle();
        $('.mnv').slideToggle();
    })
    $('.z').click(function(){
        $(this).css("display","none");
        $('.z1').css("display","inline-block");
        $('.float-ck').css("display","none");

    })
    $('.z1').click(function(){
        $(this).css("display","none");
        $('.z').css("display","inline-block");
        $('.float-ck').css("display","block");
    })
    $('#float_content_right').click(function(){
        $(this).slideToggle();
        $('.mnv').slideToggle();
        $(".x1").toggleClass("y");
    })
    //if (content.style.display == "none")
    //{content.style.display = "block"; hide.innerHTML = '<a href="javascript:hide_float_right()">[X]</a>'; }
    //else { content.style.display = "none"; hide.innerHTML = '<a href="javascript:hide_float_right()">Hi?n th? Hotline...</a>';
    //}
    jQuery("#cnhv ul li").each(function(){
        $(this).hover(function(){
            jQuery("#cnhv ul li").removeClass("active");
            jQuery(this).addClass("active");
            //$(this).find(".megacontent").addClass("active");
            var $att = $(this).attr("data-toggle");
            jQuery(".mnv-tooltip .item").removeClass("active wow animated fadeInUp");
            jQuery(".mnv-tooltip "+$att).addClass("active wow animated wow animated fadeInUp");
            //alert(".wrapper-megacontent .megacontent "+$att);
        })
    }) ;
})






