  
/**  DropDown */	

	function DropDown(el) {
				this.dd = el;
				this.placeholder = this.dd.children('span');
				this.opts = this.dd.find('ul.dropdown > li');
				this.val = '';
				this.index = -1;
				this.initEvents();
			}
			DropDown.prototype = {
				initEvents : function() {
					var obj = this;

					obj.dd.on('click', function(event){
						$(this).toggleClass('active');
						return false;
					});

					obj.opts.on('click',function(){
						var opt = $(this);
						obj.val = opt.text();
						obj.index = opt.index();
						obj.placeholder.text(obj.val);
					});
				},
				getValue : function() {
					return this.val;
				},
				getIndex : function() {
					return this.index;
				}
			}

			$(function() {

				var dd = new DropDown( $('#dd') );

				$(document).click(function() {
					// all dropdowns
					$('.wrapper-dropdown-3').removeClass('active');
				});

			});


			
/**  ve top  */

jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

});	 
   
// js slide-project  

jQuery(document).ready(function($) {

	'use strict';

		$(".slide_advs").owlCarousel({
			slideSpeed : 200,
			items : 5,
			itemsCustom : false,
			itemsDesktop : [1199, 4],
			itemsDesktopSmall : [979, 4],
			itemsTablet : [768, 3],
			itemsTabletSmall : false,
			itemsMobile : [479, 2],
			autoPlay: true,
			stopOnHover: true,
			responsive: true,
			navigation: false,
			pagination : false,
			navigationText: ["",""], 
		});
 
});


// js slide-project  

jQuery(document).ready(function($) {

	'use strict';

		$(".slide_tailieu").owlCarousel({
			slideSpeed : 200,
			items : 2,
			itemsCustom : false,
			itemsDesktop : [1199, 2],
			itemsDesktopSmall : [979, 2],
			itemsTablet : [768, 2],
			itemsTabletSmall : false,
			itemsMobile : [479, 1],
			autoPlay: true,
			stopOnHover: true,
			responsive: true,
			navigation: false,
			pagination : false,
			navigationText: ["",""], 
		});
 
});


 
/*! Call slide-news */

jQuery(document).ready(function($) {

	'use strict';

		$(".slide-news").owlCarousel({
			slideSpeed : 200,
			items : 4,
			itemsCustom : false,
			itemsDesktop : [1199, 4],
			itemsDesktopSmall : [979, 2],
			itemsTablet : [768, 3],
			itemsTabletSmall : false,
			itemsMobile : [479, 2],
			autoPlay: true,
			stopOnHover: true,
			addClassActive: true,
			autoHeight: true,
			responsive: true,
			navigation: true,
			pagination : false,
			navigationText: ["",""],
		});
 
});

/*! Call list-video */

jQuery(document).ready(function($) {

	'use strict';

		$(".list-video").owlCarousel({
			slideSpeed : 200,
			items : 3,
			itemsCustom : false,
			itemsDesktop : [1199, 3],
			itemsDesktopSmall : [979, 2],
			itemsTablet : [768, 2],
			itemsTabletSmall : false,
			itemsMobile : [479, 1],
			autoPlay: true,
			stopOnHover: true,
			addClassActive: true,
			autoHeight: true,
			responsive: true,
			navigation: true,
			pagination : false,
			navigationText: ["",""],
		});
 
});
 
 // js expand
$(function() {
  $(".expand").on( "click", function() {
    // $(this).next().slideToggle(200);
    $expand = $(this).find(">:first-child");
    
    if($expand.text() == "+") {
      $expand.text("-");
    } else {
      $expand.text("+");
    }
  });
}); 
	
// js Tool tip 
$(document).ready(function () {
  $('.tooltip-right').tooltip({
    placement: 'right',
    viewport: {selector: 'body', padding: 3}
  })
   $('.tooltip-left').tooltip({
    placement: 'left',
    viewport: {selector: 'body', padding: 3}
  })
  $('.tooltip-top').tooltip({
    placement: 'top',
    viewport: {selector: 'body', padding: 3}
  })
  $('.tooltip-bottom').tooltip({
    placement: 'bottom',
    viewport: {selector: 'body', padding: 3}
  })
 
})
			  


 