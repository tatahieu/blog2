<?php
return  array( 
	
	'original_path' => $_SERVER['DOCUMENT_ROOT'].'/uploads/images/userfiles/',
	'resize' => array( 'path' => $_SERVER['DOCUMENT_ROOT'].'/uploads/images/resize/',
						'size' => array(50,100,200,300,550,750)
					),
	'crop' => array( 'path' => $_SERVER['DOCUMENT_ROOT'].'/uploads/images/crop/',
						'size' => array('500x500','200x200','207x207','364x182', '500x300','800x480')
				),
);

?>