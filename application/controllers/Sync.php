<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sync extends CI_Controller
{
    // See note in Sync controller of Thien.tv
    private $local_root = '';
    private $remote_root = '';

    public function index()
    {
        error_reporting(E_ERROR | E_PARSE);
        ob_start();
        // ============= CONFIG ==============================
        // Local folder:  /var/www/html/waterbook_core/uploads
        // Remote folder:  https://www.anhngumshoa.com/uploads
        // File php này được chạy từ /var/www/html/waterbook_core

        $this->local_root = '/var/www/html/waterbook_core';
        $this->remote_root = 'https://www.anhngumshoa.com';
        $url_api = 'https://www.anhngumshoa.com/sync/api';

        // ===================================================
        $res = $this->get_diffence_all_LIST_dir('', 'uploads', $url_api);
        $output = ob_get_clean();

        // B3: Viết file bash
        echo "#!/bin/bash\r\n";
        foreach ($res as $mono) {
            echo "\r\n" . $mono . ";";
        }
    }

    //  COMPARE
    public function get_diffence_all_LIST_dir($local_dir_root, $remote_dir, $url_api)
    {
        $start_time = time();

        $list_folder_target = $this->get_list_dir_child_remote($url_api, $remote_dir);

        $number = 100; // Số lượng folder mono cho 1 lần gọi đến API của remote Server
        // =====
        $array_list = array();

        while (true) {
            $number_element = count($list_folder_target);
            if ($number_element > $number) {
                $mono = array_slice($list_folder_target, 0, $number);
                array_push($array_list, $mono);
                $list_folder_target = array_slice($list_folder_target, $number);
            } else {
                array_push($array_list, $list_folder_target);
                break;
            }
        }

        $list_command_total = array(); // Danh sách command Kết quả

        for ($i = 0; $i < count($array_list); $i++) {
            $now = time();
            $deltatime = $now - $start_time;
            $start_time = $now;
            echo "\n" . 'No ' . $i . ' - ' . $deltatime . ' s ';
            $mono_LIST_folder_target = $array_list[$i];

            // echo '<pre>';        print_r($mono_LIST_folder_target); echo '</pre>';

            $remote_folder_target_LIST = array();
            $local_folder_target_LIST = array();

            foreach ($mono_LIST_folder_target as $mono_folder_target) {
                if ($local_dir_root != '') {
                    $mono_local_folder_target = $local_dir_root . '/' . $mono_folder_target;
                } else {
                    $mono_local_folder_target = $mono_folder_target;
                }
                array_push($local_folder_target_LIST, $mono_local_folder_target);
                array_push($remote_folder_target_LIST, $mono_folder_target);
            }
            $list_command = $this->get_diffence_LIST_dir($remote_folder_target_LIST, $local_folder_target_LIST, $url_api, 1);
            $list_command_total = array_merge($list_command_total, $list_command);
        }

        return $list_command_total;
    }

    public function get_diffence_LIST_dir($local_dir_arr, $remote_dir_arr, $url_api, $type)
    {
        $local_list_arr = $this->get_dir_LIST_local($local_dir_arr, $type);
        $remote_list_arr = $this->get_dir_LIST_remote($url_api, $remote_dir_arr, $type);

        $command_arr = array();

        for ($i = 0; $i < count($local_list_arr); $i++) {
            // For fix bug
//            $folder_remote = $remote_dir_arr[$i];
//            $folder_local = $local_dir_arr[$i];

            // echo "\n ________________________________ \n Inside: ".$folder_local." | ". $folder_remote ."\n";

            $local_list = $local_list_arr[$i];
            $remote_list = $remote_list_arr[$i];
            if ((is_array($local_list) && (is_array($remote_list)))) {
                $list_file_need_get_mono = array_diff($remote_list, $local_list);

                // $list_file_need_get = array_merge($list_file_need_get,$list_file_need_get_mono);
                if (count($list_file_need_get_mono) > 0) {
                    // echo '++++++++++ The diff: '."\n";
                    // var_dump($list_file_need_get_mono);

                    if (count($list_file_need_get_mono) > 0) {
                        foreach ($list_file_need_get_mono as $mono__file) {
                            if (trim($mono__file) === '') continue;
                            $mono_folder_target_str = str_replace(' ', '%20', $remote_dir_arr[$i]);
                            $mono__file_str = str_replace(' ', '%20', $mono__file);

                            $command = 'wget --no-check-certificate -nc -P ' . $this->local_root . '/' . $mono_folder_target_str . ' ' . $this->remote_root . '/' . $mono_folder_target_str . '/' . $mono__file_str;
                            // echo "\n RES: ".$command ."\n";
                            array_push($command_arr, $command);
                        }
                    }
                }
            } else {
                if ($remote_list != null) {
                    // Local có nhưng remote ko có
                    echo "\n" . 'Something Wrong' . "\n";
                    var_dump($local_list);
                    var_dump($remote_list);
                }
            }


        }
        return $command_arr;
    }

    public function get_diffence_all_dir($local_dir_root, $remote_dir, $url_api)
    {
        $command_arr = array();
//        $start_time = time();

        $list_folder_target = $this->get_list_dir_child_remote($url_api, $remote_dir);

        // echo "\n".'Total: '.count($list_folder_target);
        // echo '<br> All folder Target: ';
        // Dump_Arr_preTag($list_folder_target);
        // ob_start();

        for ($i = 0; $i < count($list_folder_target); $i++) {
            try {
//                $now = time();
//                $deltatime = $now - $start_time;
//                $start_time = $now;

                // echo "\n".'No '.$i.' - '.$deltatime.' s '.$list_folder_target[$i];
                $mono_folder_target = $list_folder_target[$i];
                if ($local_dir_root != '') {
                    $mono_folder_target2 = $local_dir_root . '/' . $mono_folder_target;
                } else {
                    $mono_folder_target2 = $mono_folder_target;
                }

                $list_file = $this->get_diffence_one_dir($mono_folder_target2, $mono_folder_target, $url_api, 1);

                // Them command wget
                if (count($list_file) > 0 ){
                    foreach ($list_file as $mono__file){
                        $mono_folder_target_str = str_replace(' ', '%20', $mono_folder_target);
                        $mono__file_str = str_replace(' ', '%20', $mono__file);
                        $command = 'wget --no-check-certificate -P ' . $this->local_root . '/' . $mono_folder_target_str . ' ' . $this->remote_root . '/' . $mono_folder_target_str . '/' . $mono__file_str;
                        array_push($command_arr, $command);
                    }
                }

            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
            }

            // ob_flush();
            // flush();
        }

        return $command_arr;
    }

    public function get_diffence_one_dir($local_dir, $remote_dir, $url_api, $type)
    {
        $local_list = $this->get_dir_local($local_dir, $type);
        $remote_list = $this->get_dir_remote($url_api, $remote_dir, $type);
        if ((is_array($local_list) && (is_array($remote_list)))) {
            $list_file_need_get = array_diff($remote_list, $local_list);
            $list_file_need_get = array_values($list_file_need_get);

            return $list_file_need_get;
        } else {
            // echo '<br>Something Wrong';
            // var_dump($local_list);
            // var_dump($remote_list);
            // echo '<hr><hr>';
        }

    }

    // GET INFO REMOTE

    private function get_dir_remote($url, $dir, $type = 0)
    {
        $param = array(
            'type' => $type,
            'dir' => $dir,
            'code' => 'file',
        );

//        $url = 'https://thor.daybreak.icu/api/list_file';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }

    private function get_dir_LIST_remote($url, $remote_dir_arr, $type = 0)
    {
        $param = array(
            'code' => 'list_file',
            'type' => $type,
            'dir' => json_encode($remote_dir_arr),

        );
//        $url = 'https://thor.daybreak.icu/api/list_file';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        $result = curl_exec($ch);
        curl_close($ch);

        $res_live = json_decode($result, true);

        return $res_live;
    }

    public function get_list_dir_child_remote($url, $dir)
    {
        $param = array(
            'dir' => $dir,
            'code' => 'dir',
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }

    // =============================

    // GET INFO LOCAL
    /**
     * @param $dir
     * @param int $type = 0: file and folder, 1 la file , 2 la folder
     * @return array
     */
    private function get_dir_local($dir, $type = 0)
    {
        $arr_file = array();
        $arr_folder = array();
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        $list = scandir($dir);
        foreach ($list as $item) {
            if (is_file($dir . '/' . $item)) {
                array_push($arr_file, $item);
            } else {
                if (($item != '.') && ($item != '..')) {
                    array_push($arr_folder, $item);
                }
            }
        }
        switch ($type) {
            case 1:
                return $arr_file;
                break;
            case 2:
                return $arr_folder;
                break;
            default:
                return array($arr_folder, $arr_file);
        }
    }

    private function get_dir_LIST_local($local_dir_arr, $type = 0)
    {
        $list_res = array();
        for ($i = 0; $i < count($local_dir_arr); $i++) {
            $dir = $local_dir_arr[$i];
            $mono_res = $this->get_dir_local($dir, $type);
            array_push($list_res, $mono_res);
        }
        return $list_res;
    }

    private function get_list_dir_child_local($dir_folder)
    {
        $arr_res = array($dir_folder);
        $arr_waiting = array($dir_folder);
        $start = true;
        $stt = 1;
        while ($start) {
            $arr_res_mono = $this->scan($arr_waiting);

            if (count($arr_res_mono) > 0) {
                foreach ($arr_res_mono as $mono_res) {
                    array_push($arr_res, $mono_res);
                }
                $arr_waiting = $arr_res_mono;
            } else {
                $arr_waiting = array();
                $start = false;
            }
            $stt++;
        }
        return $arr_res;
    }

    private function scan($arr_waiting)
    {
        $arr_res = array();

        foreach ($arr_waiting as $dir_folder) {
            $list_child = $this->get_dir_local($dir_folder, 2);
            if (count($list_child) > 0) {
                foreach ($list_child as $mono_child) {
                    $dir_folder_new = $dir_folder . '/' . $mono_child;
                    array_push($arr_res, $dir_folder_new);
                }
            }
        }
        return $arr_res;
    }

    // API để nói chuyện vs Server khác
    public function api()
    {
        if (isset($_POST['type'])) {
            $type = $_POST['type'];
        } else {
            $type = 0;
        }
        if (isset($_POST['dir'])) {
            $dir = $_POST['dir'];
        } else {
            exit;
        }

        if (!isset($_POST['code'])) {
            exit;
        } else {
            $code = $_POST['code'];
        }
        switch ($code) {
            case 'dir': // Lay danh sach cac folder child ben trong
                if (!is_dir($dir)) {
                    echo 'Dir is Not good ';
                    exit;
                }
                $res = $this->get_list_dir_child_local($dir);
                echo json_encode($res);
                break;
            case 'file': // Lay danh sach cac file, folder con cap 1 trong 1 folder - dua vao thuoc tinh type
                if (!is_dir($dir)) {
                    echo 'Dir is Not good ';
                    exit;
                }
                $res = $this->get_dir_local($dir, $type);
                echo json_encode($res);
                break;
            case 'list_file': // Câu lệnh gộp của case 'file'
                $list_dir = json_decode($dir, true);
                $res = array();
                for ($i = 0; $i < count($list_dir); $i++) {
                    $mono_dir = $list_dir[$i];
                    if (!is_dir($mono_dir)) {
//                        echo 'Dir is Not good ';
                        array_push($res, array()); // Để giữ đúng thứ tự
                    } else {
                        $mono_res = $this->get_dir_local($mono_dir, $type);
                        array_push($res, $mono_res);
                    }
                }

                echo json_encode($res);
                break;
        }

    }

}
