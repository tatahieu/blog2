<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Controller {
    public $module = 'users';
	public function __construct(){
		parent::__construct();
		$this->lang->load('backend/users');
		$this->load->setData('title',$this->lang->line('users_title'));
	}
	public function dashboard() {
		$this->load->layout('users/dashboard');
	}
	public function index(){
		if ($this->input->post('delete'))
		{
			return $this->_action('delete');
		}
		$this->load->setArray(array("isLists" => 1));
		$data = $this->_index();
		// render view
		$this->load->layout('users/list',$data);
	}
	private function _index(){
		$limit = $this->config->item("limit_item");
		$this->load->model('admin/users_model','users');
		// get level of user 
		$page = (int) $this->input->get('page');
		$offset = ($page > 1) ? ($page - 1) * $limit : 0;
		$params = array('limit' => $limit + 1,'offset' => $offset);
		////////////////// FITER /////////
		$params_filter = array_filter(array(
			'email' => $this->input->get('email')
		),'filter_item');
		$params = array_merge($params,$params_filter);
		// get data
		$rows = $this->users->lists($params);
		/** PAGING **/
		$config['total_rows'] = count($rows);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$paging = $this->paging->create_links();
		unset($rows[$limit]);
		// set limit
		$this->load->setArray(array("isLists" => 1));
		// set data
		return array('rows' => $rows, 'paging' => $paging,'filter' => $params_filter);
	}
	public function edit($id = 0){
		$id = (int) $id;
        if ($this->input->post('submit')){
			return $this->_action('edit',array('id' => $id));
		}
		$this->load->setArray(array("isForm" => 1));
		$data = $this->_edit($id);
		if (!$data) {
			show_404();
		}
		$this->load->layout('users/form',$data);
	}
	public function _edit($id = 0) {
		$this->load->model('admin/users_model','users');
		$row = $this->users->detail($id);
		if (!$row) {
			return array();
		}
		return  array(
			'row' => $row
		);
	}
	public function login(){
		$configOauth = $this->config->item("web4u_oauth");
		$url = $configOauth['site'].'/oauth/token';
        $this->load->library('Curl');
        $postData = array(
            'client_id' => $configOauth['client_id'],
            'client_secret' => $configOauth['client_secret'],
            'redirect_uri' => urlencode(SITE_URL.'/users/login'),
            'grant_type' => 'authorization_code',
            'code' => $this->input->get('code')
        );
        $data = $this->curl->simple_post($url,$postData,array(CURLOPT_FAILONERROR => FALSE));
        $data = json_decode($data,TRUE);
        if ($this->curl->info['http_code'] === 200 && $access_token = $data['access_token']) {
            $url = $configOauth['site'] . '/oauth/resource';
            $data = $this->curl->simple_get($url,array('access_token' => $access_token),array(CURLOPT_FAILONERROR => FALSE));
            $data = json_decode($data,TRUE);
            if ($this->curl->info['http_code'] === 200 && $data) {
            	// site root
            	$data['access_token'] = $access_token;
            	if ($data['site_role'] == 'root') {
            		$this->permission->setIdentity($data);
            		redirect(SITE_URL);
            	}
            	else {
            		$this->load->model("admin/roles_model","roles");
            		$permission = $this->roles->getRoleByUser($data['user_id']);
	            	if ($permission) {
	            		$data['permission'] = $permission['permission'];
	            		$data['role'] = $permission['name'];
	            		// set session
		            	$this->permission->setIdentity($data);
		            	redirect(SITE_URL);
	            	}
	            	else {
	            		$data['error_description'] = 'Access Denied';
	            	}
            	}
            }
        }
        // if false oauth2
        $session_id = session_id();
        $data['configOauth'] = $configOauth;
        $data['redirect_uri'] = urlencode(SITE_URL);
        $this->load->view('users/login',$data,FALSE);
		
	}
    public function logout(){
    	$configOauth = $this->config->item("web4u_oauth");
        $this->permission->clearIdentity();
        redirect($configOauth['site'].'/users/logout?redirect_uri='.urlencode(BASE_URL));
    }
	public function _action($action, $params = array()) {
		$this->load->model('admin/users_model','users');
		switch ($action) {
			case 'profile':
			case 'edit':
					$input = array(
                        'ho_va_ten' => $this->input->post('ho_va_ten'),
                        'ngay_sinh' => $this->input->post('ngay_sinh'),
                        'que_quan' => $this->input->post('que_quan'),
                        'dia_chi' => $this->input->post('dia_chi'),
                        'quoc_tich' => $this->input->post('quoc_tich'),
                        'ton_giao' => $this->input->post('ton_giao'),
                        'gioi_tinh' => $this->input->post('gioi_tinh'),
                        'trinh_do' => $this->input->post('trinh_do'),
                        'dien_thoai' => $this->input->post('dien_thoai'),
                        'email' => $this->input->post('email'),
                        'ngay_vao_cong_ty' => $this->input->post('ngay_vao_cong_ty'),
                        'cmtnd' => $this->input->post('cmtnd')
					);
					$result = $this->users->update($params['id'],$input);

					if ($result) {
                        // return result
						$html =$this->load->view('users/form',$this->_edit($params['id'])); 
						return $this->output->set_output(json_encode(array('status' => 'success', 'html' => $html, 'result' => $result, 'message' => $this->lang->line("common_update_success"))));
					}

				break;
			case 'delete':
				$arrId = $this->input->post('cid');
				$arrId = (is_array($arrId)) ? array_map('intval', $arrId) : (int) $arrId;
				if (!$arrId) {
					return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_delete_min_select"))));
				}
				if (!$this->permission->check_permission_backend('delete')){
					return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_access_denied"))));
				}
				$result = $this->users->delete($arrId);
				
				if ($result) {
                    // return result
					$html = $this->load->view('users/list',$this->_index()); 
					return $this->output->set_output(json_encode(array('status' => 'success','html' => $html, 'result' => $result, 'message' => $this->lang->line("common_delete_success"))));
				}
			break;
            case 'edit_certificate':
                $input = array(
                    'ten_chuyen_nganh' => $this->input->post('ten_chuyen_nganh'),
                    'ten_bang_cap' => $this->input->post('ten_bang_cap'),
                    'loai_bang' => $this->input->post('loai_bang'),
                    'ten_truong' => $this->input->post('ten_truong'),
                    'nam_tot_nghiep' => $this->input->post('nam_tot_nghiep'),
                    'hinh_thuc_dao_tao' => $this->input->post('hinh_thuc_dao_tao'),
                );
                $result = $this->users->update_certificate($params['id'],$input);

                if ($result) {
                    // return result
                    $html =$this->load->view('users/certificate_form',$this->edit_certificate($params['id']));
                    return $this->output->set_output(json_encode(array('status' => 'success', 'html' => $html, 'result' => $result, 'message' => $this->lang->line("common_update_success"))));
                }

            break;
            case 'del_certificate':
                $arrId = $this->input->post('cid');
                $arrId = (is_array($arrId)) ? array_map('intval', $arrId) : (int) $arrId;
                if (!$arrId) {
                    return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_delete_min_select"))));
                }
                if (!$this->permission->check_permission_backend('delete')){
                    return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_access_denied"))));
                }
                $result = $this->users->delete($arrId);

                if ($result) {
                    // return result
                    $html = $this->load->view('users/list',$this->_index());
                    return $this->output->set_output(json_encode(array('status' => 'success','html' => $html, 'result' => $result, 'message' => $this->lang->line("common_delete_success"))));
                }
            break;
			default:
				# code...
				break;
		}	
		return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_no_row_update"))));
	}
    public function add(){
        if ($this->input->post()){
            $input = $this->input->post();
            ///////
            $emailUser = $input['email'];
            $this->load->model('users_model','users');
            // check email existed
            if ($profile = $this->users->getProfileByEmail($emailUser)){
                if ($profile) {
                    return $this->output->set_output(json_encode(array('status' => 'error','message' => 'Email đã tồn tại')));
                }
            }
            else {
                // insert user
                $user_id = $this->users->register($input);
            }
            // send mail with token
            if ($user_id > 0) {
                $url = SITE_URL .'/users/index';
                return $this->output->set_output(json_encode(array('status' => 'success','message' => 'Đăng ký tài khoản thành công','redirect_uri' => $url)));
            }
            return $this->output->set_output(json_encode(array('status' => 'error','message' => array('email' => 'Có lỗi hệ thống vui lòng thử lại'))));
        }
        $this->load->layout('users/form',[]);
    }

    public function certificate($id = 0)
    {
        $id = (int) $id;
        if ($this->input->post('delete')){
            return $this->_action('del_certificate',array('id' => $id));
        }
        $this->load->setArray(array("isForm" => 1));
        $data = $this->_certificate($id);
        if (!$data) {
            show_404();
        }
        $this->load->layout('users/certificate',$data);
    }
    public function _certificate($id = 0) {
        $this->load->model('admin/users_model','users');
        $rows = $this->users->list_certificate($id);
        if (!$rows) {
            return array();
        }
        $row = $this->users->detail($id);
        return  array(
            'rows' => $rows,
            'row' => $row
        );
    }

    public function edit_certificate($id = 0)
    {
        $id = (int) $id;
        if ($this->input->post('submit')){
            return $this->_action('edit_certificate',array('id' => $id));
        }
        $this->load->setArray(array("isForm" => 1));
        $data = $this->_edit_certificate($id);
        if (!$data) {
            show_404();
        }
        $this->load->layout('users/certificate_form',$data);
    }
    public function _edit_certificate($id = 0) {
        $this->load->model('admin/users_model','users');
        $detail = $this->users->certificate($id);
        if (!$detail) {
            return array();
        }
        $row = $this->users->detail($detail['ma_nhan_vien']);
        return  array(
            'row' => $detail,
            'user' => $row
        );
    }

    public function add_certificate($id = 0){
        if ($this->input->post()){
            if($id == 0) {
                return true;
            }
            $input = $this->input->post();
            $this->load->model('users_model','users');
            $input = array_merge(array('ma_nhan_vien' => $id), $input);
            // insert user
            $certificate_id = $this->users->insert_certificate($input);

            // send mail with token
            if ($certificate_id > 0) {
                $url = SITE_URL .'/users/certificate/'.$id;
                return $this->output->set_output(json_encode(array('status' => 'success','message' => 'Thêm bằng cấp thành công','redirect_uri' => $url)));
            }
            return $this->output->set_output(json_encode(array('status' => 'error','message' => array('email' => 'Có lỗi hệ thống vui lòng thử lại'))));
        }
        $data = $this->_add_certificate($id);
        if (!$data) {
            show_404();
        }
        $this->load->layout('users/certificate_form',[]);
    }
    public function _add_certificate($id = 0) {
        $this->load->model('admin/users_model','users');

        $row = $this->users->detail($id);
        return  array(
            'user' => $row
        );
    }
}
?>