<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Script extends CI_Controller
{

    public function send_mail_register()
    {
        ///// GỬI EMAIL VÀ ĐĂNG KÝ VÀO GG SHEET
        // email cơ cở
        $email_manager = array('anhngumshoa@imap.edu.vn');

        $arr_location_to_email = array(
            1 => '41tayson@imap.edu.vn',
            2 => '461hqv@imap.edu.vn',
            3 => '141bachmai@imap.edu.vn',
            4 => '20ndc@imap.edu.vn',
            5 => 'hadong@imap.edu.vn',
            6 => '18nvc@imap.edu.vn',
            7 => '569svh@imap.edu.vn',
            8 => '49pdl@imap.edu.vn',
            9 => '82levanliet@imap.edu.vn',
            10 => '427conghoa@imap.edu.vn',
            11 => '224khanhhoi@imap.edu.vn',
            12 => '18pvt@imap.edu.vn',
        );

        $this->db->where('cron_email', 1);
        $query = $this->db->get('contact');
        $result = $query->result_array();

        // log working
        $this->db->set('note_new', 'Cron Send Mail Time: '.time());
        $this->db->where('site_id', 1);
        $this->db->update('setting');

        if (count($result) > 0) {
            foreach ($result as $mono_res) {
                $id_contact = $mono_res['contact_id'];
                $fullname = $mono_res['fullname'];
                $phone = $mono_res['phone'];
                $email = $mono_res['email'];
                $content = $mono_res['content'];
                $create_time = date('d/m/y - H:i:s', $mono_res['create_time']);
                $type = (int)$mono_res['type'];
                $branch = $mono_res['branch']; // id
                $offline_place = $mono_res['offline_place']; // id
                $dateofbirth = $mono_res['dateofbirth'];
                $live_area = $mono_res['live_area']; //
                $url = $mono_res['url'];

                // STEP 1: Gửi email xác nhận cho học viên
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $body_this = '<div>
                            <img style="width: 400px; max-width: 100%" src="https://www.anhngumshoa.com/theme/frontend/default/images/congratulation.png"/>
                            <h3>Chúc mừng bạn đã đăng ký thành công</h3>
                            <p>Thông tin của bạn đang được Anh ngữ Ms Hoa xử lý. Trung tâm sẽ sớm liên hệ với bạn qua email hoặc điện thoại để tư vấn trực tiếp cho bạn trong thời gian sớm nhất (thông thường từ 1-2 ngày làm việc). Bạn vui lòng thường xuyên kiểm tra email (trong mục Inbox hoặc Spam) để cập nhật các thông tin liên quan đến khoá học.
                                Trường hợp không nhận được/chậm nhận được email phản hồi (do lỗi mạng hoặc lỗi hệ thống), bạn vui lòng liên hệ trực tiếp với chúng tôi theo đường dây nóng: 0969 264 966 hoặc đến trực tiếp 14 cơ sở của Anh ngữ Ms Hoa nha!</p>
                            <h2>ANH NGỮ MS HOA – ĐÀO TẠO TOEIC SỐ 1 VIỆT NAM</h2>
                        
                            <p>&bull; Website:  <a target="_blank" href="https://www.anhngumshoa.com">https://www.anhngumshoa.com</a></p>
                            <p>&bull; Fanpage:  <a target="_blank" href="https://www.facebook.com/mshoatoeic/">https://www.facebook.com/mshoatoeic/</a></p>
                            <p>&bull; Email: <a>mshoa@imap.edu.vn</a></p>
                        </div>';
                    $this->sendEmail_from_IMAP(array($email), 'Mshoatoeic đã nhận được đăng ký của bạn',
                        $body_this);
                }

                $this->load->model('admin/setting_model', 'setting');
                $arrSetting = $this->setting->detail();
                $arrBranch = json_decode($arrSetting['branch'], TRUE);
                foreach ($arrBranch as $key => $branch) {
                    $arrBranchData[$branch['id']] = $branch['name'];
                }
                $arrOfflinePlace = json_decode($arrSetting['offline_place'], TRUE);
                foreach ($arrOfflinePlace as $key => $offline_place) {
                    $arrOfflinePlaceData[$offline_place['id']] = $offline_place['name'];
                }

                $branch_text = '';
                if (trim($arrBranch[$branch]) != '') {
                    $branch_text = $arrBranch[$branch];
                }
                $offline_place_text = '';
                if (trim($arrOfflinePlace[$offline_place]) != '') {
                    $offline_place_text = $arrOfflinePlace[$offline_place];
                }
                switch ($type) {
                    case 2:
                        $type_text = "Đăng ký tư vấn";
                        break;
                    case 3:
                        $type_text = "Đăng ký làm test";
                        break;
                    case 4:
                        $type_text = "Đăng ký offline";
                        break;
                    case 5:
                        $type_text = "Đăng ký nhận tài liệu";
                        break;
                    default:
                        $type_text = "Liên hệ"; // 1
                }

                $basic_info =  '<p>Kiểu đăng ký: '.$type_text;
                $basic_info .=  '<p>Thời gian đăng ký: '.$create_time;
                $basic_info .=  '<p>Họ tên: '.$fullname;
                $basic_info .=  '<p>Phone: '.$phone.'  <a href="tel:'.$phone.'" ><h2 style="color: greenyellow">Click to Call</h2></a>';
                $basic_info .=  '<p>Email: '.$email;
                $basic_info .=  '<p>Chi nhánh: '.$email;
                $basic_info .=  '<p>Nội dung: '.$content;
                $basic_info .=  '<p>Ngày sinh: '.$dateofbirth;
                $basic_info .=  '<p>Khu vực sống: '.$live_area;
                $basic_info .=  '<p>Chi nhánh: '.$branch_text;
                $basic_info .=  '<p>Địa điểm Offline: '.$offline_place_text;
                $basic_info .=  '<p>URL: '.$url;

                switch ($type) {
                    case 1:
                        // STEP 2: Gửi email cho quản lý chung và tư vấn
                        $title_email_co_so = $fullname . ' đăng ký liên hệ - ' . $branch_text;
                        $body_co_so = '<h5>Bạn nhận được một yêu cầu tư vấn khoá học từ website anhngumshoa. Thông tin khách hàng như sau:</h5>' .$basic_info.
                            '<p>Yêu cầu cơ sở liên lạc để tư vấn với khách hàng trong thời gian sớm nhất!</p>'
                            . '<hr><p><b>PHÒNG MARKETING</b></p>';

                        $title_email_manager = 'Anhngumshoa.com - '.$fullname . ' đăng ký liên hệ - ' . $branch_text;
                        $body_manager = '<h5>Thông tin khách hàng đăng ký:</h5>' .$basic_info
                            . '<hr><p><b>Angngumshoa.com</b></p>';

                        $this->sendEmail_from_IMAP($email_manager, $title_email_manager, $body_manager, $name_sender = "Anhngumshoa.com");

                        if (isset($arr_location_to_email[$branch])) {
                            $email_co_so = $arr_location_to_email[$branch];
                            if (is_array($email_co_so)) $email_co_so = array($email_co_so);
                            $this->sendEmail_from_IMAP($email_co_so, $title_email_co_so, $body_co_so, $name_sender = "Anhngumshoa.com");
                        }
                        break;
                    case 2:
                        // STEP 2: Gửi email cho quản lý chung và tư vấn
                        $title_email_co_so = $fullname . ' đăng ký nhận tư vấn - ' . $branch_text;
                        $body_co_so = '<h5>Bạn nhận được một yêu cầu tư vấn khoá học từ website anhngumshoa. Thông tin khách hàng như sau:</h5>' .$basic_info.
                            '<p>Yêu cầu cơ sở liên lạc để tư vấn với khách hàng trong thời gian sớm nhất!</p>'
                            . '<hr><p><b>PHÒNG MARKETING</b></p>';

                        $title_email_manager = 'Anhngumshoa.com - '.$fullname . ' đăng ký nhận tư vấn - ' . $branch_text;
                        $body_manager = '<h5>Thông tin khách hàng đăng ký:</h5>' .$basic_info
                            . '<hr><p><b>Angngumshoa.com</b></p>';

                        $this->sendEmail_from_IMAP($email_manager, $title_email_manager, $body_manager, $name_sender = "Anhngumshoa.com");

                        if (isset($arr_location_to_email[$branch])) {
                            $email_co_so = $arr_location_to_email[$branch];
                            if (is_array($email_co_so)) $email_co_so = array($email_co_so);
                            $this->sendEmail_from_IMAP($email_co_so, $title_email_co_so, $body_co_so, $name_sender = "Anhngumshoa.com");
                        }
                        break;
                    case 3:
//                        $type_text = "Đăng ký làm test";
                        // STEP 2: Gửi email cho quản lý chung và tư vấn
                        $title_email_co_so = $fullname . ' đăng ký làm bài test - ' . $branch_text;
                        $body_co_so = '<h5>Bạn nhận được một yêu cầu đăng ký làm bài test từ website anhngumshoa. Thông tin khách hàng như sau:</h5>' .$basic_info.
                            '<p>Yêu cầu cơ sở liên lạc để tư vấn với khách hàng trong thời gian sớm nhất!</p>'
                            . '<hr><p><b>PHÒNG MARKETING</b></p>';

                        $title_email_manager = 'Anhngumshoa.com - '.$fullname . ' đăng ký làm bài test - ' . $branch_text;
                        $body_manager = '<h5>Thông tin khách hàng đăng ký:</h5>' .$basic_info
                            . '<hr><p><b>Angngumshoa.com</b></p>';

                        $this->sendEmail_from_IMAP($email_manager, $title_email_manager, $body_manager, $name_sender = "Anhngumshoa.com");

                        if (isset($arr_location_to_email[$branch])) {
                            $email_co_so = $arr_location_to_email[$branch];
                            if (is_array($email_co_so)) $email_co_so = array($email_co_so);
                            $this->sendEmail_from_IMAP($email_co_so, $title_email_co_so, $body_co_so, $name_sender = "Anhngumshoa.com");
                        }
                        break;
                    case 4:
//                        $type_text = "Đăng ký offline";
                        // STEP 2: Gửi email cho quản lý chung
                        $title_email_manager = 'Anhngumshoa.com - '.$fullname . ' đăng ký tham gia sự kiện Offline';
                        $body_manager = '<h5>Thông tin khách hàng đăng ký:</h5>' .$basic_info
                            . '<hr><p><b>Angngumshoa.com</b></p>';
                        $this->sendEmail_from_IMAP($email_manager, $title_email_manager, $body_manager, $name_sender = "Anhngumshoa.com");
                        break;
                    case 5:
//                        $type_text = "Đăng ký nhận tài liệu";
                        // STEP 2: Gửi email cho quản lý chung
                        $title_email_manager = 'Anhngumshoa.com - '.$fullname . ' đăng ký nhận tài liệu' ;
                        $body_manager = '<h5>Thông tin khách hàng đăng ký:</h5>' .$basic_info
                            . '<hr><p><b>Angngumshoa.com</b></p>';
                        $this->sendEmail_from_IMAP($email_manager, $title_email_manager, $body_manager, $name_sender = "Anhngumshoa.com");
                        break;
                }

                // Mark Done
                $this->db->set('cron_email', 2);
                $this->db->where('contact_id', $id_contact);
                $this->db->update('contact');
            }
        }

    }

    public function _getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Google Sheets API PHP Quickstart');
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
        $client->setAuthConfig(BASEDIR . '/vendor/credentials.json');
        $client->setAccessType('online');
        $client->setPrompt('select_account consent');
// Load previously authorized token from a file, if it exists.
        $tokenPath = 'token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }
// If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
// Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
// Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf('Có lỗi xảy ra');
                printf('Truy cập link sau: ' . ":\n%s\n", $authUrl);
                print 'Get mã code và set vào config.php: ';
// $authCode = trim(fgets(STDIN));
                $authCode = $this->config->item("authCode");
// Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);
// Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
// Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    private function pushDataTo_IMAP($array_insert)
    {
        return null;
//        require FCPATH . 'vendor2019/vendor_api2019/autoload.php'; // api 2019

//        $client = new \Google_Client();
//        $client->setApplicationName('Google Sheets and PHP');
//        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
//        $client->setAccessType('offline');
////        $client->setAuthConfig(__DIR__.'/credentials.json');
//        $client->setAuthConfig( FCPATH . 'vendor2019/credentials.json');
//        $service = new Google_Service_Sheets($client);
//        $spreadsheetId = '19w4gEoCuR_43SH3xUOe-Ikdwxq_TsKDlIsX6dRbhuqA';
//        $values_insert = [
//            $array_insert
//        ];
//        $range_insert= "Sheet1";
//        $body = new Google_Service_Sheets_ValueRange([
//            'values' => $values_insert
//        ]);
//        $params = [
//            'valueInputOption' => 'RAW'
//        ];
//
//        $insert = [
//            'insertDataOption' => 'INSERT_ROWS'
//        ];
//        $result = $service->spreadsheets_values->append($spreadsheetId,$range_insert,$body,$params,$insert);
    }

    public function sendEmail_from_IMAP($arr_receiver, $email_title, $email_body, $name_sender = "Mshoatoeic")
    {
        $CI = &get_instance();
// load library
        $config = array(
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'thanhdat.imap@gmail.com',
            'smtp_pass' => 'cacc842679315',
            'smtp_port' => 465,
            'mailtype' => 'html',
            'protocol' => 'smtp',
            'newline' => "\r\n",
        );
        $CI->load->library('email', $config);
        $CI->email->clear();
        $CI->email->from($config['smtp_user'], $name_sender);
        $CI->email->to($arr_receiver);
        $CI->email->subject($email_title);
        $CI->email->message($email_body);
        $CI->email->send(TRUE);
//        var_dump($this->email->print_debugger(array('headers')));
//echo $CI->email->print_debugger();
        /* if ($CI->email->send(TRUE)) {
        return TRUE;
        } else {
        if (ENVIRONMENT == 'development') {
        $CI->email->print_debugger(array('headers'));
        }
        return FALSE;
        }*/

    }

    public function cron_test_first_times(){
        $this->db->set('note_new', 'Cron 1 Time: '.date('d/m/y - H:i:s',time()) );
        $this->db->where('site_id', 1);
        $this->db->update('setting');
    }

    public function script_send_email()
    {
        while(true){
            $this->send_mail_register();
            $this->send_mail_register();
            sleep(180); // 3 mins
        }

    }


}
