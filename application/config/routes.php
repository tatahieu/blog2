<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
/** ADMIN **/
$route['admin'] = "admin/users/dashboard";
// tags news

/** OLD DATA **/

// REDIRECT
$route['tin-tuc/ngu-phap-toeic-tat-tan-tat-ngu-phap-am-tron-990-diem-toeic-37651.html'] = "rss/redirect/out_link/37651";
$route['tin-tuc/tron-bo-chua-de-new-economy-toeic-format-moi-2018-chi-tiet-nhat-37627.html'] = "rss/redirect/out_link/37627";
$route['tin-tuc/top-7-sach-ngu-phap-toeic-hay-nhat-36971.html'] = "rss/redirect/out_link/36971";
$route['tin-tuc/toeic-listening-cau-truc-chien-luoc-am-tron-diem-37628.html'] = "rss/redirect/out_link/37628";
$route['tin-tuc/toeic-listening-chien-luoc-am-tron-diem-37628.html'] = "rss/redirect/out_link/37628";
$route['tin-tuc/tat-tan-tat-meo-thi-toeic-7-phan-am-tron-990-diem-toeic-37389.html'] = "rss/redirect/out_link/37389";







$route['(:any)-nl(:num)'] = "rss/redirect/news_cate/$2";
$route['(:any)-nd(:num)'] = "rss/redirect/news/$2";
$route['(:any)-nd(:num)-(:num)'] = "rss/redirect/news/$2";
$route['question/test/(:num)'] = "rss/redirect/test/$1";
$route['question/catetest/(:num)'] = "rss/redirect/test_cate/$1";
$route['question/minitest/(:num)'] = "rss/redirect/fulltest/$1";
$route['question/fulltest/(:num)'] = "rss/redirect/fulltest/$1";


http://www.mshoatoeic.com/question/catetest/78


$route['tag/(:any)-(:num).html'] = "news/tag/$2/$1";
/** NEWS **/
$route['tin-tuc/(:any)-c(:num).html'] = "news/lists/$2/$1";
$route['tin-tuc/(:any)-(:num).html'] = "news/detail/$2/$1";
$route['tu-van.html'] = "news/support";
//Video
$route['video/cate/(:any)-(:num).html'] = "news/lists/$2/$1";
$route['video/detail/(:any)-(:num).html'] = "news/detail/$2/$1";


/** TEST **/
$route['test.html'] = "test/index";
$route['test/(:any)-c(:num).html'] = "test/lists/$2/$1";
$route['test/(:any)-(:num).html'] = "test/detail/$2/$1";
/**KHOA HỌC**/
$route['khoa-hoc/(:any)/(:any)-(:num).html'] = "course/detail/$3/$2/$1";
$route['bai-hoc/(:any)-(:num).html'] = "course/class_detail/$2/$1";
/** API **/

/****Users****/
$route['bai-tap-lop.html'] = "test/class_home";
$route['bai-tap-lop/(:any).html'] = "test/class_test/$1";
$route['chia-se-lop-hoc.html'] = "news/byclass";
$route['khoa-hoc.html'] = "news/course";
$route['test-online.html'] = "test/test_online";


$route['dang-ky.html'] = "users/register";
$route['dang-nhap.html'] = "users/login";
$route['thong-tin-ca-nhan.html'] = "users/profile";
$route['doi-mat-khau.html'] = "users/updatepassword";
$route['quen-mat-khau.html'] = "users/forgotpass";

$route['writing/fulltest.html'] = "test/lists/3465";
$route['writing/(:any)/write-a-sentence.html'] = "test/lists/3467";
$route['writing/(:any)/respond-a-request.html'] = "test/lists/3468";
$route['writing/(:any)/write-an-opinion-essay.html'] = "test/lists/3469";

////////// SITEMAP ////////
$route['sitemap.xml'] = "rss/sitemap_index";
$route['sitemap/(:any).xml'] = "rss/sitemap/$1";
/** COMMON **/
//$route['(:any)'] = "frontend/$1";
$route['default_controller'] = "news/index";

// CONTACT
$route['lien-he.html'] = "contact";
$route['nhan-tai-lieu.html'] = "contact/form_tai_lieu";

//Tìm kiếm
$route['tim-kiem.html'] = "news/search";

$route['thi-thu-toeic-online-tuan-1-8.html'] = "test/thithuonline";

// Error
$route['404_override'] = '';
$route['404.html'] = 'news/error_404';

$route['translate_uri_dashes'] = FALSE;


/* End of file routes.php */
/* Location: ./application/config/routes.php */