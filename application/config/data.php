<?php
$config['test_type'] = array(
	1 => array('name' => '[Toeic] Photo'), // trac nghiem ko hien cau tra loi 
	2 => array('name' => '[Toeic] Question-Response'),
	3 => array('name' => '[Toeic] Short conversation'), // trac nghiem ko hien detail
	4 => array('name' => '[Toeic] Short talk'),
	5 => array('name' => '[Toeic] Incomplete Sentence'),
	6 => array('name' => '[Toeic] Text completion'),
	7 => array('name' => '[Toeic] Single passage'),
	10 => array('name' => '[Toeic] Writing - Write a Sentence'),
	11 => array('name' => '[Toeic] Writing - Respond a Request'),
	12 => array('name' => '[Toeic] Writing - Write an Opinion Essay'),
	20 => array('name' => '[Toeic] Speaking - Đọc một đoạn văn có sẵn'),
	21 => array('name' => '[Toeic] Speaking - Miêu tả một bức tranh'),
	22 => array('name' => '[Toeic] Speaking - Trả lời câu hỏi'),
	23 => array('name' => '[Toeic] Speaking - Trả lời câu hỏi có cho trước thông tin'),
	24 => array('name' => '[Toeic] Speaking - Đưa ra giải pháp cho một vấn đề'),
	25 => array('name' => '[Toeic] Speaking - Trình bày quan điểm')

	//20 => array('name' => 'Bài tập viết'),
	//30 => array('name' => 'Bài tập ghi âm'),
);
$config['course_class_type'] = array(
	1 => 'Bài học',
);
$config['cate_type'] = array(
	1 => array('name' => 'Tin tức', 'code' => 'tin-tuc'),
	2 => array('name' => 'Test', 'code' => 'test'),
	3 => array('name' => 'Quảng cáo', 'code' => 'advertise'),
	4 => array('name' => 'Khóa học', 'code' => 'course')
);

$config['cate_test_type'] = array(
	1 => 'Luyện tập',
	2 => 'Skill test',
	3 => 'Mini test & Full test',
	4 => 'Speaking',
	5 => 'Writing',
);
$config['cate_news_type'] = array(
	1 => 'News',
	2 => 'Khóa học',
	3 => 'Video'
);
$config['news_theme'] = array(
	1 => 'Nomal',
	2 => 'Photo',
	3 => 'Khóa học'
);
$config['device'] = array(
    0 => 'All device',
    1 => 'Mobile',
    4 => 'PC'
);
$config['location'] = array(
    1 => 'Hà Nội',
	2 => 'Hồ Chí Minh',
	3 => 'Đà Nẵng',
	4 => 'Hải Phòng',
	9 => 'Khác'
);
