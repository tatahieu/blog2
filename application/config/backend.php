<?php
$CI = &get_instance();

$config['limit_item'] = 30;
/////////////////////////////////// MENU ////////////////////////////////////
$config['menu']['position'] = array('main' => 'Menu chính','footer' => 'Menu footer','right' => 'Liên kết nhanh');
$config['menu']['module'] = array(
    "news" => array(
        "name" => $CI->lang->line('common_mod_news'),
        "action" => array(
            'news_cate' => array(
                "name"  => $CI->lang->line('common_mod_news_cate'),
                "type" => 'select'
            ),
            'news' => array(
                "name"  => $CI->lang->line('common_mod_static'),
                "type" => 'suggest'
            ),
        )
    ),
    "other" => array(
        "name" => $CI->lang->line('common_mod_fastlink'),
        "action" => array(
            'home' => array(
                "name"  => $CI->lang->line('common_home'),
                "type" => 'fix',
                "link" => '/'
            ),
            "contact" => array(
                "name"  => $CI->lang->line('common_mod_contact'),
                "type" => 'fix',
                "link"  => "/contact"
            ),
            "link" => array(
                "name"  => $CI->lang->line('common_mod_link'),
                "type" => 'text',
            ),
            "class_test" => array(
                "name"  => "Bài tập lớp",
                "type" => 'fix',
                "link" => '/bai-tap-lop.html',
            ),
            "class_news" => array(
                "name"  => "Chia sẻ lớp học",
                "type" => 'fix',
                "link" => '/chia-se-lop-hoc.html',
            ),
        )
    )
);
//////////////////// BLOCK //////////////////////
$config['block'] = array(
    'news_cate' => array(
        "name" => $CI->lang->line('common_mod_news_cate'),
        "position" => array('content' => 'Content','right' => 'Phải'),
        "params" => array("nums_item","template" => array(NULL => 'Tin tức','sugia' => 'Đội ngũ sứ giả','right_tieudiem' => 'Right - Tiêu điểm Toeic', 'vinhdanh' => 'Vinh danh học viên'),'title'),
    ),
    'news_cate_show_home' => array(
        "name" => 'Cate show home',
        "position" => array('content' => 'Content'),
        "params" => array("nums_item",'title'),
    ),
    'news_sub_cate' => array(
        "name" => 'Tin tức nhóm cấp 2',
        "position" => array('content' => 'Content'),
        "params" => array("nums_item","template" => array(NULL => 'Trang chủ - Tài liệu luyện thị Toeic','toeic' => 'Trang chủ - Tin tức Toeic','test' => 'Trang chủ - Toeic Online'),'title'),
    ),
    'static' => array(
        "name" => $CI->lang->line('common_mod_static'),
        "position" => array('content' => 'Content','right' => 'Right'),
        "params" => array("content","title","template" => array(NULL => 'Normal', 'tuvan' => 'Đăng ký tư vấn','lotrinh' => 'Lộ trình học'))
    ),
    'menu' => array(
        "name" => $CI->lang->line("common_mod_menu"),
        "position" => array('top' => 'Menu chính','footer' => 'Menu footer','right' => 'Right'),
        "params" => array("menu" => $config['menu']['position'],"template" => array(NULL => 'Menu chính','footer' => 'Menu footer','right' => 'Liên kết nhanh')),
    ),
    'advertise_cate' => array(
        "name" => $CI->lang->line("common_mod_advertise_cate"),
        "position" => array("content" => 'Content','footer' => 'Footer','right' => 'Right','top_ads' => 'Top Ads','news_ads' => 'News Ads', 'pin' => 'Pin Ads', 'class_banner' => 'Banner lớp học'),
        "params" => array("nums_item","thumb","template" => array(NULL => 'Slide home','home_lkg' => 'Trang chủ - Lịch khai giảng','partner' => 'Đối tác','right_1' => 'Right - 3 banner','top_ads' => 'Top ads', 'news_ads' => 'News ads', 'pin' => 'Pin Ads','right_2' => 'Right - Banner lớp học'))
    ),
    'news_special' => array(
        'name' => $CI->lang->line('common_mod_news_buildtop'), 
        'position' => array('top_story' => 'Top Story','hot_news' => 'Hot news')
    ),
    'news_tags' => array(
        'name' => $CI->lang->line('common_mod_news_buildtag'), 
        'position' => array('top' => 'Tag special')
    ),
);
/////////////////////////////////// PERMISSION ////////////////////////////////////
$config['admin_role'] = array(
    'category' => array(
        'name' => $CI->lang->line("common_mod_category"),
        'permission' => array(
            1 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('index','edit','add','delete')
            )
        )
    ),
    'news' => array(
        'name' => $CI->lang->line("common_mod_news"),
        'permission' => array(
            1 => array( // tao bai viet, xem dc danh sach bai viet cua minh, edit bai viet cua minh
                'name' => 'Writer',
                'permission' => array('index','edit','add','copy','suggest_tag')
            ),
            2 => array( // edit bai viet nguoi khac, public bai viet
                'name' => 'Manager', 
                'permission' => array('publish','buildtop','suggest_news','buildtag','suggest_tags')
            ),
            3 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('delete','cate_add','cate_edit','cate_index','cate_delete')
            )
        )
    ),
    'test' => array(
        'name' => 'Bài test',
        'permission' => array(
            1 => array( // tao bai viet, xem dc danh sach bai viet cua minh, edit bai viet cua minh
                'name' => 'Writer',
                'permission' => array('index','edit','add','question_add','question_edit','question_index','question_delete','suggest_test','question_sort','log_lists','log_export')
            ),
            2 => array( // edit bai viet nguoi khac, public bai viet
                'name' => 'Manager', 
                'permission' => array('publish')
            ),
            3 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('delete','cate_add','cate_edit','cate_index','cate_delete')
            )
        )
    ),
    'course' => array(
        'name' => 'Khóa học',
        'permission' => array(
            1 => array( // tao bai viet, xem dc danh sach bai viet cua minh, edit bai viet cua minh
                'name' => 'Manager',
                'permission' => array('index','add','edit','copy', 'publish', 'topic_index', 'topic_add', 'topic_edit', 'topic_delete', 'class_add', 'class_edit','class_point','class_score','users_index', 'users_add','users_delete')
            ),
            2 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('delete')
            )
        )
    ),
    'group' => array(
        'name' => 'Lớp học',
        'permission' => array(
            1 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('delete','add','edit','index','suggest_group')
            )
        )
    ),
    'menu' => array(
        'name' => $CI->lang->line("common_mod_menu"),
        'permission' => array(
            1 => array(// Xoa, sua, copy, danh sach
                'name' => 'Manager',
                'permission' => array('add','index','edit','copy','option')
            ),
            2 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('delete')
            )
        )
    ),
    'users' => array(
        'name' => $CI->lang->line("common_mod_users"),
        'permission' => array(
            1 => array( // xem dc danh sach thanh vien
                'name' => 'Viewer',
                'permission' => 'index'
            ),
            2 => array( // Edit profile member
                'name' => 'Manager', 
                'permission' => 'edit'
            ),
            3 => array(// xoa user, phan quyen user, tao role
                'name' => 'Administrator',
                'permission' => array('delete')
            )
        )
    ),
    'setting' => array(
        'name' => $CI->lang->line("common_mod_setting"),
        'permission' => array(
            1 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('index')
            )
        )
    ),
    'block' => array(
        'name' => $CI->lang->line("common_mod_block"),
        'permission' => array(
            1 => array(// Xoa, sua, copy, danh sach
                'name' => 'Manager',
                'permission' => array('option','add','index','edit','copy')
            ),
            2 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('delete')
            )
        )
    ),
    'sms' => array(
        'name' => 'sms',
        'permission' => array(
            1 => array(// Xoa, sua, copy, danh sach
                'name' => 'Manager',
                'permission' => array('add','list_')
            ),
            2 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('add','list_','delete')
            )
        )
    ),
    'advertise' => array(
        'name' => $CI->lang->line("common_mod_advertise"),
        'permission' => array(
            1 => array(// Xoa, sua, copy, danh sach
                'name' => 'Manager',
                'permission' => array('index','add','edit','copy')
            ),
            2 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('delete','cate_add','cate_edit','cate_index','cate_delete')
            )
        )
    ),
    'filemanager' => array(
        'name' => $CI->lang->line("common_mod_filemanager"),
        'permission' => array(
            1 => array(// Xoa, sua, copy, danh sach
                'name' => 'Writer',
                'permission' => array('index','files','directory','upload','folders','copy','move','create','delete','recursiveCopy','recursiveFolders','rename','fast_upload')
            ),
            2 => array(// Xoa, sua, copy, danh sach
                'name' => 'Manager',
                'permission' => array()
            ),
            3 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('recursiveDelete')
            )
        )
    ),
    'contact' => array(
        'name' => $CI->lang->line("common_mod_contact"),
        'permission' => array(
            1 => array(// Xoa, sua, copy, danh sach
                'name' => 'Manager',
                'permission' => array('index','edit','subscribe','export','sendsms','list_sms')
            ),
            2 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('delete')
            )
        )
    ),
    'roles' => array(
        'name' => $CI->lang->line("common_mod_role"),
        'permission' => array(
            1 => array(// Xoa, sua, copy, danh sach
                'name' => 'Manager',
                'permission' => array('member_index','member_add','member_edit','member_delete')
            ),
            2 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('index','add','copy','edit','delete')
            )
        )
    ),
    'convert' => array(
        'name' => 'Tool',
        'permission' => array(
            1 => array(// Xoa, sua, copy, danh sach
                'name' => 'Manager',
                'permission' => array('convert_to_link_new','fix_tag_a_html','fix_tag_img_html','fix_rel_follow_a_html','change_all_type_1_to2','update_new_branch_for_form_inside_new','list_news_have_form')
            ),
            2 => array(// xoa bai, tao folder
                'name' => 'Administrator',
                'permission' => array('delete')
            )
        )
    ),

);