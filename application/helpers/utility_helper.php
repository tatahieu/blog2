<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function get_array($array)
{
	if (is_array($array)){
      foreach ($array as $a){
         get_array($a);
	  }
	}
    else{
        echo $array;
    }
}
/**
 * type=1: datetime to timestamp
        2: timestamp to datetime
 **/
function convert_datetime($str,$type = 1)
{
    switch ($type){
        case 1:
        list($date, $time) = explode(' ', $str);
        list($day, $month, $year) = explode('/', $date);
        list($hour, $minute, $second) = explode(':', $time);
        $timestamp = mktime($hour, $minute, $second, $month, $day, $year);
        break;
        case 2:
        $timestamp = date('Y-m-d H:i:s', $str);
        break;
        case 3:

        list($year , $month , $day) = explode('-', $str);
        $timestamp = mktime(0, 0, 0, $month, $day, $year);
        break;
        default:
        $timestamp = time();
    }
    return $timestamp;
}
function set_array()
{
	$arr = func_get_args();
	foreach ($arr as $arr){
		if (is_array($arr)){
			foreach ($arr as $key => $value){
				$row[$key] = $value;
			}
		}
		else{
			$row[$arr] = null;
		}
	}
	return $row;
}
function price_format($number,$type = 1)
{
    if ($number == '')
	{
		return 0;
	}

	// Remove anything that isn't a number or decimal point.
	$number = trim(preg_replace('/([^0-9\.])/i', '', $number));
    if ($type == 1){
        return number_format($number, 0, ',', '.');
    }
}
function filter_vn($str)
{
    $marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề"
    ,"ế","ệ","ể","ễ",
    "ì","í","ị","ỉ","ĩ",
    "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
    ,"ờ","ớ","ợ","ở","ỡ",
    "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
    "ỳ","ý","ỵ","ỷ","ỹ",
    "đ",
    "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
    ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
    "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
    "Ì","Í","Ị","Ỉ","Ĩ",
    "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
    ,"Ờ","Ớ","Ợ","Ở","Ỡ",
    "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
    "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
    "Đ");

    $marKoDau=array("a","a","a","a","a","a","a","a","a","a","a"
    ,"a","a","a","a","a","a",
    "e","e","e","e","e","e","e","e","e","e","e",
    "i","i","i","i","i",
    "o","o","o","o","o","o","o","o","o","o","o","o"
    ,"o","o","o","o","o",
    "u","u","u","u","u","u","u","u","u","u","u",
    "y","y","y","y","y",
    "d",
    "A","A","A","A","A","A","A","A","A","A","A","A"
    ,"A","A","A","A","A",
    "E","E","E","E","E","E","E","E","E","E","E",
    "I","I","I","I","I",
    "O","O","O","O","O","O","O","O","O","O","O","O"
    ,"O","O","O","O","O",
    "U","U","U","U","U","U","U","U","U","U","U",
    "Y","Y","Y","Y","Y",
    "D");
    return str_replace($marTViet,$marKoDau,$str);
}
function clear_file_name($str) {
    $str = trim(filter_vn($str));
    $str = str_replace(" ","_",$str);
    $str = preg_replace("/[^a-z0-9\.\_\-]/", "", strtolower($str));
    return $str;
}   
function set_alias_link($str){
	$string = trim(filter_vn($str));
    $string = cut_text($string,170);
    $string = str_replace(array("\"",'”',"'","!","@","#","$","%","^","&","*","(",")"),"",$string);
	$string = preg_replace("`\[.*\]`U","",$string);
	$string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
	$string = htmlentities($string, ENT_COMPAT, 'utf-8');
	$string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
	$string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);

	return strtolower(trim($string, '-'));
}
function clear_cache($fd = array()){
    $CI = &get_instance();
    $CI->load->helper('file');
    $path =  APPPATH.'cache/';
    if (!empty($fd)){
        foreach ($fd as $fd){
            delete_files($path.$fd,TRUE,2);
        }
    }
    else{
        delete_files($path,TRUE,2);
    }
}
function cut_text($text,$chars = 100){
    if (strlen($text) < $chars){
        return $text;
    }
    $text = trim($text);
    $text = substr( $text, 0, $chars );
	$text = substr( $text, 0, strrpos( $text, ' ' ) );
    return $text.' ...';
}
function send_mail($to,$subject,$message){
    $CI = &get_instance();
    // 
    // get config email server
    $emailServer = $CI->config->item('email_server');
    $count = count($emailServer);
    $randInt = rand(0,$count-1);
    $emailServer = $emailServer[$randInt];
    // load library
    $config = array(
        'smtp_host' => $emailServer['host'],
        'smtp_user' => $emailServer['email'],
        'smtp_pass' => $emailServer['password'],
        'smtp_port' => $emailServer['port'],
        'mailtype' => 'html',
        'protocol' => 'smtp',
        'newline'   =>"\r\n",
    );
    $CI->load->library('email',$config);
    $CI->email->clear();
    $CI->email->from($config['smtp_user'], 'Mshoatoeic');
    $CI->email->to($to);
    $CI->email->subject($subject);
    $CI->email->message($message);
    $CI->email->send(TRUE);
    //echo $CI->email->print_debugger();
    /* if ($CI->email->send(TRUE)) {
        return TRUE; 
    } else {
        if (ENVIRONMENT == 'development') {
            $CI->email->print_debugger(array('headers'));
        }
        return FALSE;
    }*/
}
function media_show($filename,$size = ''){
    $CI = &get_instance();
    $x = explode('.', $filename);
    $type =  end($x);
    $filename = ltrim($filename,'/');
    switch ($type){
        case 'swf':
            $out = '<embed ';
            if ($height > 0) {$out.= " height=\"$height\" ";}
            if ($width > 0) {$out.= ' width="'.$width.'" ';}
            $out.=  'type="application/x-shockwave-flash" src="'.base_url().'uploads/images/'.$filename.'" style="" quality="high" wmode="transparent" allowscriptaccess="always">';
        break;
        default:
        $out = '<img src="'. getimglink($filename,$size) . '">';
    }
    return $out;
}
function debug($data){
    $out = '<div class="debug" style="position: fixed; right: 0; bottom: 0; width: 500px; height: 200px; z-index: 99999; overflow: scroll; background: #000000; color: #FFF">';
    if (is_array($data)){
        $out .= '<pre>'. htmlspecialchars(print_r($data,true)). "</pre>\n";
    }
    else{
        $out .= var_dump($data);
    }
    $out .= '</div>';
    echo $out;
}
/**
 * @param: type: 1 (widthx0); 2 (0xheight); 3: crop
 */
function getimglink($path,$size = '',$type = 0){
    $CI = &get_instance();
    
    // if(ip2long($_SERVER['SERVER_ADDR']) % 2 == 0){
    //     $path = 'https://media.anhngumshoa.com/uploads/images/userfiles/'.trim($path,'/');
    // }else{
    //     $path = 'https://media1.anhngumshoa.com/uploads/images/userfiles/'.trim($path,'/');
    // }

    $path = UPLOAD_URL.'images/userfiles/'.trim($path,'/');
    if (!$size) {
        return $path;
    }

    $strsize = ($type == 3) ? $CI->config->item("crop") : $CI->config->item("resize");
    $size = $strsize[$size];
    if (!$size) {
        return $path;
    }
    $rep = '';
    $command = 'resize';
    switch($type){
        case 1:
        $size[1] = 0;
        break;
        case 2:
        $size[0] = 0;
        break;
        case 3:
        $command = 'crop';
    }
    return str_replace('/userfiles/', '/'.$command.'/'.$size[0].'x'.$size[1].'/', $path) ;
}
/**
* @param: array('lang' => int, url => str)
* @author: namtq
*/
function redirect_seo($params,$location,$code) {
    $CI = &get_instance();
    if ($params['lang']) {
        $arrLang = $CI->config->item("multilang");
        foreach ($arrLang as $key => $value) {
            if ($value == $params['lang'])  {

                if ($CI->config->item("default_lang") == $key) {
                    redirect(BASE_URL.'/'. trim($params['url'],'/'),$location, $code); 
                }
                else {
                    redirect(BASE_URL.'/'.$key.'/'. trim($params['url'],'/'),$location, $code); 
                }
                
            }
        }
        
    }
}
function getFileLink($path,$type = 'sound') {
    switch ($type) {
        case 'sound':
            /* $arr = array('2017','2016','2018');
            foreach ($arr as $arr) {
                if (strpos($path, $arr)) {
                    return 'https://cdn.anhngumshoa.com/uploads/audio/'.trim($path,'/');
                }
            }*/
            // return (strpos($path, '://') === TRUE) ? $path : 'https://media.anhngumshoa.com/uploads/sound/'.trim($path,'/');
            // if(ip2long($_SERVER['SERVER_ADDR']) % 2 == 0){
            //     $path = 'https://media.anhngumshoa.com/uploads/sound/'.trim($path,'/');
            // }else{
            //     $path = 'https://media1.anhngumshoa.com/uploads/sound/'.trim($path,'/');
            // }
            // return $path;

            return (strpos($path, '://') === TRUE) ? $path : UPLOAD_URL.'sound/'.trim($path,'/');
            break;
        
        default:
            # code...
            break;
    }
    
}
function filter_item($var){
  return ($var !== NULL && $var !== FALSE && $var !== '');
}
function translate_answer($int){
    $alphabet = range('A', 'Z');

    return $alphabet[$int - 1];
}

function DumpArrPreTag($var){
    echo '<pre>'; print_r($var); echo '</pre>';
}