<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<form class="form-horizontal form-label-left ajax-submit-form" action="" method="POST">
<div class="form-group">	
	<button class="btn btn-primary" onclick="goBack();" type="button">Back</button>
	<?php if ($this->permission->check_permission_backend('index') && $this->router->method !='profile') {?>
	<a class="btn btn-primary" href="<?php echo SITE_URL; ?>/users/certificate/<?php echo $user['ma_nhan_vien']?>">Danh sách bằng cấp</a>
	<?php } ?>
	<button class="btn btn-success ajax-submit-button" type="submit"><?php echo $this->lang->line("common_save"); ?></button>
	<?php if ($row['ma_bang_cap']) { ?>
	<input  type="hidden" name="token" value="<?php echo $this->security->generate_token_post($row['ma_bang_cap']) ?>"/>
	<?php } ?>
</div>
<div class="row">
	<div class="col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Quản lý bằng cấp nhân viên - <?php echo $user['ho_va_ten']?></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content row">
				<div class="form-group validation_title">
					<label class="control-label col-sm-3 col-xs-12">Tên chuyên ngành</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input required type="text" name="ten_chuyen_nganh" value="<?php echo $row['ten_chuyen_nganh']; ?>" placeholder="Tên chuyên ngành" class="form-control">
					</div>
                </div>
            	<div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Tên bằng cấp</label>
					<div class="col-sm-9 col-xs-12">
						<input type="text" name="ten_bang_cap" value="<?php echo $row['ten_bang_cap']; ?>" placeholder="Tên bằng cấp" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Loại bằng</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="loai_bang" value="<?php echo $row['loai_bang']; ?>" placeholder="Loại bằng" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Tên trường</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="ten_truong" value="<?php echo $row['ten_truong']; ?>" placeholder="Tên trường" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Năm tốt nghiệp</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="nam_tot_nghiep" value="<?php echo $row['nam_tot_nghiep']; ?>" placeholder="Năm tốt nghiệp" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Hình thức đào tạo</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="hinh_thuc_dao_tao" value="<?php echo $row['hinh_thuc_dao_tao']; ?>" placeholder="Hình thức đào tạo" class="form-control">
					</div>
                </div>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        /** DATETIME **/
        // $('#nam_tot_nghiep').datetimepicker({
        //     format: 'd/m/Y H:i:s',
        //     step: 5
        // });
    });
</script>