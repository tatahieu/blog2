<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<form class="form-horizontal form-label-left ajax-submit-form" action="" method="POST">
<div class="form-group">	
	<button class="btn btn-primary" onclick="goBack();" type="button">Back</button>
	<?php if ($this->permission->check_permission_backend('index') && $this->router->method !='profile') {?>
	<a class="btn btn-primary" href="<?php echo SITE_URL; ?>/users/index"><?php echo $this->lang->line("common_mod_users_index"); ?></a>
	<?php } ?>
	<button class="btn btn-success ajax-submit-button" type="submit"><?php echo $this->lang->line("common_save"); ?></button>
	<?php if ($row['ma_nhan_vien']) { ?>
	<input  type="hidden" name="token" value="<?php echo $this->security->generate_token_post($row['ma_nhan_vien']) ?>"/>
	<?php } ?>
</div>
<div class="row">
	<div class="col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?php echo $this->lang->line("users_title"); ?></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content row">
				<div class="form-group validation_title">
					<label class="control-label col-sm-3 col-xs-12"><?php echo $this->lang->line("users_fullname"); ?></label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input required type="text" name="ho_va_ten" value="<?php echo $row['ho_va_ten']; ?>" placeholder="<?php echo $this->lang->line("users_fullname"); ?>" class="form-control">
					</div>
                </div>
            	<div class="form-group">
					<label class="control-label col-sm-3 col-xs-12"><?php echo $this->lang->line("users_email"); ?></label>
					<div class="col-sm-9 col-xs-12">
						<input type="text" name="email" value="<?php echo $row['email']; ?>" placeholder="<?php echo $this->lang->line("users_email"); ?>" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Ngày sinh</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" id="ngay_sinh" name="ngay_sinh" value="<?php echo ($row['ngay_sinh']) ? date('d/m/Y H:i:s',$row['ngay_sinh']) : ''; ?>" placeholder="Ngày sinh" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Quê quán</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="que_quan" value="<?php echo $row['que_quan']; ?>" placeholder="Quê quán" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Địa chỉ</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="dia_chi" value="<?php echo $row['dia_chi']; ?>" placeholder="Địa chỉ" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Quốc tịch</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="quoc_tich" value="<?php echo $row['quoc_tich']; ?>" placeholder="Quốc tịch" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Tôn giáo</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="ton_giao" value="<?php echo $row['ton_giao']; ?>" placeholder="Tôn giáo" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Giới tính</label>
					<div class="col-sm-9 col-xs-12 validation_form">
                        <select name="gioi_tinh" class="form-control" placeholder="Chọn giới tính" tabindex="-1">
                            <option <?php if ($row['gioi_tinh'] == 1) echo 'selected'; ?> value="1">Nam</option>
                            <option <?php if ($row['gioi_tinh'] == 2) echo 'selected'; ?> value="2">Nữ</option>
                            <option <?php if ($row['gioi_tinh'] == 3) echo 'selected'; ?> value="3">Khác</option>
                        </select>
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Trình độ</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="trinh_do" value="<?php echo $row['trinh_do']; ?>" placeholder="Trình độ" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Điện thoại</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" name="dien_thoai" value="<?php echo $row['dien_thoai']; ?>" placeholder="Điện thoại" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Ngày vào công ty</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="text" id="ngay_vao_cong_ty" name="ngay_vao_cong_ty" value="<?php echo ($row['ngay_vao_cong_ty']) ? date('d/m/Y H:i:s',$row['ngay_vao_cong_ty']) : date('d/m/Y H:i:s'); ?>" placeholder="Ngày vào công ty" class="form-control">
					</div>
                </div>
                <div class="form-group">
					<label class="control-label col-sm-3 col-xs-12">Số chứng minh thư nhân dân</label>
					<div class="col-sm-9 col-xs-12 validation_form">
						<input type="number" name="cmtnd" value="<?php echo $row['cmtnd']; ?>" placeholder="Số chứng minh thư nhân dân" class="form-control">
					</div>
                </div>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        /** DATETIME **/
        $('#ngay_sinh').datetimepicker({
            format: 'd/m/Y H:i:s',
            step: 5
        });
        $('#ngay_vao_cong_ty').datetimepicker({
            format: 'd/m/Y H:i:s',
            step: 5
        });
    });
</script>