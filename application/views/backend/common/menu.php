<?php 
$profile = $this->permission->getIdentity();
?>
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>F6 Permission</h3>
        <ul class="nav side-menu">
            <?php if ($this->permission->has_level_user('news')) { ?>
            <li><a><i class="fa fa-newspaper-o"></i> <?php echo $this->lang->line("common_mod_news"); ?> <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <?php if ($this->permission->check_permission_backend(array('function' => 'add','class' => 'news'))) { ?>
                    <li><a href="<?php echo SITE_URL; ?>/news/add"><?php echo $this->lang->line("common_mod_news_add"); ?></a>
                    <?php } ?>
                    </li>
                    <?php if ($this->permission->check_permission_backend(array('function' => 'index','class' => 'news'))) { ?>
                    <li>
                        <a href="<?php echo SITE_URL; ?>/news/index"><?php echo $this->lang->line("common_mod_news_index"); ?></a>
                    </li>
                    <?php } ?>
                    <?php if ($this->permission->check_permission_backend(array('function' => 'buildtop','class' => 'news'))) { ?>
                    <li>
                        <a href="<?php echo SITE_URL; ?>/news/buildtop"><?php echo $this->lang->line("common_mod_news_buildtop"); ?></a>
                    </li>
                    <?php } ?>
                    <?php if ($this->permission->check_permission_backend(array('function' => 'buildtag','class' => 'news'))) { ?>
                    <li>
                        <a href="<?php echo SITE_URL; ?>/news/buildtag"><?php echo $this->lang->line("common_mod_news_buildtag"); ?></a>
                    </li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
            <?php if ($this->permission->has_level_user('users')) { ?>
                <li><a><i class="fa fa-users"></i> <?php echo $this->lang->line("common_mod_users"); ?> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <?php if ($this->permission->check_permission_backend(array('function' => 'index','class' => 'users'))) { ?>
                            <li>
                                <a href="<?php echo SITE_URL; ?>/users/index"><?php echo $this->lang->line("common_mod_users_index"); ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>