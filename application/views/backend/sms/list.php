<?php

?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h3>Nhật ký gửi SMS</h3>
        </div>
        <div class="col col-sm-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">SĐT</th>
                    <th scope="col">Nội dung</th>
                    <th scope="col">Thời gian</th>
                    <th scope="col">Trạng thái</th>
                    <th scope="col">Người gửi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($list_sms as $mono_info_sms){?>
                    <tr>
                        <td><?php echo $mono_info_sms['id']; ?></td>
                        <td><?php echo $mono_info_sms['phone']; ?></td>
                        <td><?php echo $mono_info_sms['content']; ?></td>
                        <td><?php echo date('d/m/Y -H:i', $mono_info_sms['timestamp']); ?></td>
                        <td><?php
                            if ($mono_info_sms['status'] == 0 ) echo 'Đang gửi';
                            if ($mono_info_sms['status'] == 1 ) echo 'Đã gửi';
                            if ($mono_info_sms['status'] == 2 ) echo 'Lỗi';
                            ?></td>
                        <td><?php
                            echo $info_user[$mono_info_sms['user_id']].' (id - '.$mono_info_sms['user_id'].')';
                            ?></td>
                    </tr>
                <?php }
                ?>

                </tbody>
            </table>
        </div>
    </div>



</div>
