<?php

?>

<div class="container-fluid">
    <h3>Gửi tin nhắn từ IMAP</h3>
    <div class="row">
        <div class="col col-sm-12 col-md-8">
            <div class="form-group">
                <label for="comment">Nhập danh sách số điện thoại (cách nhau bởi dấu ;)</label>
                <textarea class="form-control" rows="5" id="sms_list" style="resize: vertical;" onkeyup="confirmPhoneNumberList()" onchange="confirmPhoneNumberList()"></textarea>
            </div>
            <div class="form-group">
                <label for="comment">Nội dung tin nhắn</label>
                <textarea class="form-control" rows="5" id="contentsms"></textarea>
            </div>
            <p id="user_id" style="display: none"><?php echo $user_id; ?></p>
            <p id="token_user_id" style="display: none"><?php echo $token_user_id; ?></p>
            <div>
                <button class="btn btn-success" onclick="sendsms()">Send</button>
            </div>
        </div>
        <div class="col col-sm-12 col-md-4">
            <div class="form-group">
                <label for="comment">Danh sách số điện thoại được nhận dạng</label>
                <div id="phone_see"></div>
            </div>
        </div>
    </div>


</div>

<script>
    function sendsms() {
        var phone_list = [];
        $("span.phone_number_right").each(function () {
            var phonenumber = $(this).html();
            phone_list.push(phonenumber);
        });
        if (phone_list.length === 0){
            alert('Danh sách số điện thoại đang trống');
            return null;
        }
        var contentsms = $('#contentsms').val();
        console.log("contentsms");
        console.log(contentsms);

        if (contentsms.trim() === ''){
            alert('Vui lòng nhập nội dung tin nhắn');
            return null;
        }

        $.post("/contact/apisms",
            {
                opt_cod: "sendsms",// mã creat
                token: 'WeloveIMAP',
                phone_list: JSON.stringify(phone_list),
                contentsms: contentsms,
                user_id: $('#user_id').html(),
                token_user_id: $('#token_user_id').html(),
            },
            function (data, status) {
                console.log("data");
                console.log(data);
                alert('Success');
            });
    }

    function confirmPhoneNumberList() {
        var smslist = $('#sms_list').val();
        var res = smslist.split(";");

        var express = '';
        var stt = 1;
        for (var i = 0; i < res.length; i++) {

            var mono = res[i].trim();
            console.log("mono");
            console.log(mono);
            if (checkPhoneNumber(mono)){
                express += '<p title="Số điện thoại đúng định dạng" class="phone_number_right">' + stt + '. <span class="phone_number_right">' + mono + '</span></p>';
            }else{
                express +=  '<p  title="Số điện thoại KHÔNG đúng định dạng" class="phone_number wrong" style="color: red; font-weight: bold">' + stt + '. ' + mono + '</p>';
            }
            stt ++;
        }
        $('#phone_see').html(express);
    }

    function checkPhoneNumber(phone) {
        var flag = false;
        phone = phone.replace('(+84)', '0');
        phone = phone.replace('+84', '0');
        phone = phone.replace('0084', '0');
        phone = phone.replace(/ /g, '');
        if (phone != '') {
            var firstNumber = phone.substring(0, 1);
            if ((firstNumber == '0') && phone.length == 10) {
                if (phone.match(/^\d{10}/)) {
                    flag = true;
                }
            }
        }
        return flag;
    }

</script>

