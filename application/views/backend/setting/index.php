<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
$this->load->config('data');
$location = $this->config->item('location');
?>
<form class="form-horizontal form-label-left ajax-submit-form" action="" method="POST">
<div class="form-group">	
	<button class="btn btn-primary" onclick="goBack();" type="button">Back</button>
	<button class="btn btn-success ajax-submit-button" type="submit"><?php echo $this->lang->line("common_save"); ?></button>
	<input  type="hidden" name="token" value="<?php echo $this->security->generate_token_post(SITE_ID) ?>"/>
</div>
<div class="row">
	<ul class="nav nav-tabs col-xs-12" role="tablist" style="margin-bottom: 20px;">
		<li class="active"><a href="#step1" role="tab" data-toggle="tab">Cấu hình hệ thống</a></li>
		<li><a href="#step2" role="tab" data-toggle="tab">Hệ thống</a></li>
		<li><a href="#step3" role="tab" data-toggle="tab">Mã nhúng</a></li>
	</ul>
	<div class="tab-content">
		<div id="step1" class="tab-pane active" role="tabpanel">
			<div class="col-sm-7 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Form Design <small>different form elements</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content row">
						<div class="form-group validation_email_admin">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $this->lang->line("setting_email_admin"); ?> *</label>
							<div class="col-md-9 col-sm-9 col-xs-12 validation_form">
								<input required type="text" name="email_admin" value="<?php echo $row['email_admin']; ?>" placeholder="<?php echo $this->lang->line("setting_email_admin"); ?>" class="form-control">
							</div>
		                </div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $this->lang->line("setting_email_host"); ?> *</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="email_host" value="<?php echo $row['email_host']; ?>" placeholder="<?php echo $this->lang->line("setting_email_host"); ?>" class="form-control">
							</div>
		                </div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $this->lang->line("setting_email_username"); ?> *</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="email_username" value="<?php echo $row['email_username']; ?>" placeholder="<?php echo $this->lang->line("setting_email_username"); ?>" class="form-control">
							</div>
		                </div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $this->lang->line("setting_email_password"); ?> *</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="password" name="email_password" value="<?php echo $row['email_password']; ?>" placeholder="<?php echo $this->lang->line("setting_email_password"); ?>" class="form-control">
							</div>
		                </div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Hotline</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="hotline" value="<?php echo $row['hotline']; ?>" placeholder="Hotline" class="form-control">
							</div>
		                </div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Email support</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="email_support" value="<?php echo $row['email_support']; ?>" placeholder="Email support" class="form-control">
							</div>
		                </div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Facebook</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="facebook" value="<?php echo $row['facebook']; ?>" placeholder="Facebook" class="form-control">
							</div>
		                </div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Twitter</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="twitter" value="<?php echo $row['twitter']; ?>" placeholder="Twitter" class="form-control">
							</div>
		                </div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Google plus</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="google" value="<?php echo $row['google']; ?>" placeholder="Google plus" class="form-control">
							</div>
		                </div>
					</div>
				</div>
			</div>
			<div class="col-sm-5 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>SEO</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $this->lang->line("setting_seo_title"); ?></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="seo_title" value="<?php echo $row['seo_title']; ?>" placeholder="<?php echo $this->lang->line("setting_seo_title"); ?>" class="form-control">
							</div>
		                </div>
		                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $this->lang->line("setting_seo_keyword"); ?></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="seo_keyword" value="<?php echo $row['seo_keyword']; ?>" placeholder="<?php echo $this->lang->line("setting_seo_keyword"); ?>" class="form-control">
							</div>
		                </div>
		                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $this->lang->line("setting_seo_description"); ?></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="seo_description" class="form-control" placeholder="<?php echo $this->lang->line("setting_seo_description"); ?>" value="" rows="3"><?php echo $row['seo_description']; ?></textarea>
						
							</div>
		                </div>
					</div>
				</div>
			</div>
		</div>
		<div id="step2" class="tab-pane" role="tabpanel">
			<div class="col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Các cơ sở</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><?php //echo $this->lang->line("setting_seo_title"); ?></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<?php 
								$arrBranch = json_decode($row['branch'],TRUE);
								$j = 0;
								if($arrBranch) {
								foreach ($arrBranch as $key => $branch) { ?>
									<div class="item form-inline" style="margin-bottom: 10px;">
										<input type="text" name="branch[<?php echo $j; ?>][id]" value="<?php echo $branch['id']; ?>" placeholder="Id" class="form-control">
										<input type="text" name="branch[<?php echo $j; ?>][label]" value="<?php echo $branch['label']; ?>" placeholder="Nhãn" class="form-control">
										<input type="text" name="branch[<?php echo $j; ?>][name]" value="<?php echo $branch['name']; ?>" placeholder="Tên" class="form-control">
										<input type="text" name="branch[<?php echo $j; ?>][phone]" value="<?php echo $branch['phone']; ?>" placeholder="Điện thoại" class="form-control">
										<select name="branch[<?php echo $j; ?>][loc]" class="form-control">
											<?php foreach($location as $local_id => $local_name) { ?>
												<option value="<?php echo $local_id?>" <?php if ($branch['loc'] == $local_id) echo 'selected'; ?>><?php echo $local_name?></option>
											<?php } ?>
										</select>
									</div>
								<?php $j ++; } }
								for ($i = $j; $i < 20; $i ++) { ?>
								<div class="item form-inline" style="margin-bottom: 10px;">
									<input type="text" name="branch[<?php echo $i; ?>][id]" value="" placeholder="Id" class="form-control">
									<input type="text" name="branch[<?php echo $i; ?>][label]" value="" placeholder="Nhãn" class="form-control">
									<input type="text" name="branch[<?php echo $i; ?>][name]" value="" placeholder="Tên" class="form-control">
									<input type="text" name="branch[<?php echo $i; ?>][phone]" value="" placeholder="Điện thoại" class="form-control">
									<select name="branch[<?php echo $i; ?>][loc]" class="form-control">
										<?php foreach($location as $local_id => $local_name) { ?>
											<option value="<?php echo $local_id?>"><?php echo $local_name?></option>
										<?php } ?>
									</select>
								</div>
								<?php } ?>
							</div>
		                </div>
					</div>
				</div>
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Các địa điểm Offline</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content row">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php //echo $this->lang->line("setting_seo_title"); ?></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $arrOffline = json_decode($row['offline_place'],TRUE);
                                $j = 0;
                                if($arrOffline) {
                                    foreach ($arrOffline as $key => $place) { ?>
                                        <div class="item form-inline" style="margin-bottom: 10px;">
                                            <input type="text" name="offlineplace[<?php echo $j; ?>][id]" value="<?php echo $place['id']; ?>" placeholder="Id" class="form-control">
                                            <input type="text" name="offlineplace[<?php echo $j; ?>][label]" value="<?php echo $place['label']; ?>" placeholder="Nhãn" class="form-control">
                                            <input type="text" name="offlineplace[<?php echo $j; ?>][name]" value="<?php echo $place['name']; ?>" placeholder="Tên" class="form-control">
                                            <input type="text" name="offlineplace[<?php echo $j; ?>][phone]" value="<?php echo $place['phone']; ?>" placeholder="Điện thoại" class="form-control">
                                            <select name="offlineplace[<?php echo $j; ?>][loc]" class="form-control">
                                                <?php foreach($location as $local_id => $local_name) { ?>
													<option value="<?php echo $local_id?>" <?php if ($branch['loc'] == $local_id) echo 'selected'; ?>><?php echo $local_name?></option>
												<?php } ?>
                                            </select>
                                        </div>
                                        <?php $j ++; } }
                                for ($i = $j; $i < 20; $i ++) { ?>
                                    <div class="item form-inline" style="margin-bottom: 10px;">
                                        <input type="text" name="offlineplace[<?php echo $i; ?>][id]" value="" placeholder="Id" class="form-control">
                                        <input type="text" name="offlineplace[<?php echo $i; ?>][label]" value="" placeholder="Nhãn" class="form-control">
                                        <input type="text" name="offlineplace[<?php echo $i; ?>][name]" value="" placeholder="Tên" class="form-control">
                                        <input type="text" name="offlineplace[<?php echo $i; ?>][phone]" value="" placeholder="Điện thoại" class="form-control">
                                        <?php foreach($location as $local_id => $local_name) { ?>
											<option value="<?php echo $local_id?>"><?php echo $local_name?></option>
										<?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<div id="step3" class="tab-pane" role="tabpanel">
			<div class="col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Mã nhúng</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Mã nhúng trên header</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea class="form-control" rows="10" name="tracking_code_header"><?php echo $row['tracking_code_header']; ?></textarea>
							</div>
		                </div>
		                <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Mã nhúng dưới body</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea class="form-control" rows="10" name="tracking_code_footer"><?php echo $row['tracking_code_footer']; ?></textarea>
							</div>
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>