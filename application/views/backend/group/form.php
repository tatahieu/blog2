<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
$this->load->config('data');
?>
<form class="form-horizontal form-label-left ajax-submit-form" action="" method="POST">
<div class="form-group">	
	<button class="btn btn-primary" onclick="goBack();" type="button">Back</button>
	<?php if ($this->permission->check_permission_backend('index')) {?>
	<a class="btn btn-primary" href="<?php echo SITE_URL; ?>/group/index">Danh sách lớp học</a>
	<?php } ?>
	<button class="btn btn-success ajax-submit-button" type="submit"><?php echo $this->lang->line("common_save"); ?></button>
	<?php if ($row['class_id']) { ?>
	<input  type="hidden" name="token" value="<?php echo $this->security->generate_token_post($row['class_id']) ?>"/>
	<?php } ?>
</div>
<div class="row">
	<div class="col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Lớp học</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content row">
				<div class="form-group validation_name">
					<label class="control-label col-sm-2 col-xs-12">Tên lớp học *</label>
					<div class="col-sm-10 col-xs-12 validation_form">
						<input required type="text" name="name" value="<?php echo $row['name']; ?>" placeholder="Tên lớp học" class="form-control">
					</div>
                </div>
                <div class="form-group validation_name">
					<label class="control-label col-sm-2 col-xs-12">Mục tiêu</label>
					<div class="col-sm-10 col-xs-12 validation_form">
						<input required type="text" name="target" value="<?php echo $row['target']; ?>" placeholder="Mục tiêu khóa học" class="form-control">
					</div>
                </div>
                <div class="form-group validation_username">
					<label class="control-label col-sm-2 col-xs-12">Tên đăng nhập *</label>
					<div class="col-sm-10 col-xs-12 validation_form">
						<input required type="text" name="username" value="<?php echo $row['username']; ?>" placeholder="Tên đăng nhập" class="form-control">
					</div>
                </div>
				<div class="form-group validation_password">
					<label class="control-label col-sm-2 col-xs-12">Mật khẩu *</label>
					<div class="col-sm-10 col-xs-12 validation_form">
						<input type="text" name="password" value="" placeholder="Mật khẩu" class="form-control">
					</div>
                </div>
			</div>
		</div>
	</div>
</div>
</form>