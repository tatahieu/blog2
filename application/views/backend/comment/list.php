<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?php echo temp_delete_icon()?>
	<?php echo temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="30px">
			<?php echo $this->lang->line('common_no'); ?>
		</th>
		<?php echo temp_del_head(); ?>
		<th>User id</th>
		<th>Nội dung</th>
		<th>Tình trạng</th>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){?>
	<tr>
		<td align="center"><?php echo $i?></td>
		<?php echo temp_del_check($row['comment_id']) ?>
        <td align="center" width="60px"><?php echo $row['user_id']?></td>
		<td><?php echo $row['content']?></td>
		<td align="center" width="60px"><a id="status_<?php echo $row['comment_id'] ?>" href="javascript:update_status('comment/edit',<?php echo $row['comment_id'] ?>);"><?php echo temp_status($row['status'])?></a></td>
	</tr>
	<?php
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?php echo $paging?>
<?php echo $filter; ?>