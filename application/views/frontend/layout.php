<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="vi">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php // SEO support
    if ((trim(strtolower($this->uri->segment(1))) == 'users') || (trim(strtolower($this->uri->segment(1))) == 'tim-kiem.html')) {
        echo '<META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">';
    } else {
        echo '<META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">';
    } ?>
    <meta property="og:locale" content="vi_VN"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="fb:app_id" content="<?php echo $this->config->item("facebook_app_id")?>" />
    <meta property="fb:admins" content="100000950111622"/>
    <meta property="fb:admins" content="1109703477"/>
    <title><?php echo $seo_title; ?> <?php echo $this->uri->segment(1) ? ' | Anhngumshoa.com' : ''  ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->config->item("img"); ?>favicon.ico">
    <link rel="canonical" href="https://www.anhngumshoa.com<?php echo str_replace('/index.php', '', $_SERVER['REQUEST_URI']); ?>"/>


    <meta property="og:site_name" content="Anh Ngữ Ms Hoa"/>
    <?php if ($meta) { 
        foreach ($meta as $key => $value) { ?>
            <meta name="<?php echo $key; ?>" content="<?php echo $value; ?>" />
        <?php }
    } ?>
    <?php if ($ogMeta) { 
        foreach ($ogMeta as $key => $value) { ?>
            <meta property="<?php echo $key; ?>" content="<?php echo $value; ?>" />
        <?php }
    } ?>
    <meta http-equiv="Cache-control" content="private, max-age=60, stale-while-revalidate=6, stale-if-error=864000">
    <script type="text/javascript" src="<?php echo $this->config->item("js"); ?>jquery.min.js"></script>

    <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>style.css?t=3" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>style-bem.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>menu-clean.css" type="text/css" media="all"> 
    <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>font-awesome.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>carousel.css" type="text/css" media="all" >
    <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>login.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $this->config->item("lib"); ?>bootstrap/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>../skins/style.css" type="text/css" media="all">
    <script type="text/javascript">
        SITE_URL = '<?php echo SITE_URL; ?>';
    </script>





    <?php echo $this->config->item("tracking_code_header"); ?>
    <!-- <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>tqn_portal.css" type="text/css" media="all">        -->
    <style type="text/css">
        .fb_iframe_widget_fluid_desktop, .fb_iframe_widget_fluid_desktop span, .fb_iframe_widget_fluid_desktop iframe {
            max-width: 100% !important;
            width: 100% !important;
        }
    </style>
</head>
<body>
    <?php echo $this->load->view('common/header');?>
    <div id="main" class="full_test">
        <div class="container">
            <div class="row">
                <?php echo $content_for_layout; ?>
            </div>
        </div>
        <?php echo $this->load->get_block('pin'); ?> 
    </div>
    
    <!-- phone
    <div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon">
        <div class="phonering-alo-ph-circle"></div>
        <div class="phonering-alo-ph-circle-fill"></div>
        <a href="tel:<?php echo $this->config->item('hotline')?>"></a>
        <div class="phonering-alo-ph-img-circle">
            <a href="tel:<?php echo $this->config->item('hotline')?>"></a>
            <a href="tel:<?php echo $this->config->item('hotline')?>" class="pps-btn-img " title="Liên hệ">
                <img src="https://i.imgur.com/v8TniL3.png" alt="Liên hệ" width="50" onmouseover="this.src='https://i.imgur.com/v8TniL3.png';" onmouseout="this.src='https://i.imgur.com/v8TniL3.png';">
            </a>
        </div>
    </div>
    <a href="tel:+84123456789">
    </a>
    end phone -->

    <div class="phonering-alo-phone phonering-alo-green phonering-alo-show" id="phonering-alo-phoneIcon" style="left: -50px;
        bottom: 150px;
        position: fixed;
        z-index: 999;">
        <div class="phonering-alo-ph-circle"></div>
        <div class="phonering-alo-ph-circle-fill"></div>
        <a href="tel:+84123456789"></a>
        <div class="phonering-alo-ph-img-circle">
            <a href="tel:+84123456789"></a>
            <a href="tel:+84123456789" class="pps-btn-img " title="Liên hệ">
                <img src="<?php echo $this->config->item("img"); ?>hotline_new.png" alt="Liên hệ" width="75"
                     onmouseover="this.src='<?php echo $this->config->item("img"); ?>hotline_new.png';"
                     onmouseout="this.src='<?php echo $this->config->item("img"); ?>hotline_new.png';">
            </a>
        </div>
    </div>

    <style>
        .phonering-alo-phone.phonering-alo-static {
            opacity: .6
        }

        .phonering-alo-phone.phonering-alo-hover,
        .phonering-alo-phone:hover {
            opacity: 1
        }

        .phonering-alo-ph-circle {
            width: 150px;
            height: 150px;
            top: 15px;
            left: 45px;
            position: absolute;
            background-color: transparent;
            border-radius: 100% !important;
            border: 2px solid rgba(30, 30, 30, 0.4);
            border: 2px solid #bfebfc 9;
            opacity: .1;
            -webkit-animation: phonering-alo-circle-anim 1.2s infinite ease-in-out;
            animation: phonering-alo-circle-anim 1.2s infinite ease-in-out;
            transition: all .5s;
            -webkit-transform-origin: 50% 50%;
            -ms-transform-origin: 50% 50%;
            transform-origin: 50% 50%
        }

        .phonering-alo-phone.phonering-alo-active .phonering-alo-ph-circle {
            -webkit-animation: phonering-alo-circle-anim 1.1s infinite ease-in-out !important;
            animation: phonering-alo-circle-anim 1.1s infinite ease-in-out !important
        }

        .phonering-alo-phone.phonering-alo-static .phonering-alo-ph-circle {
            -webkit-animation: phonering-alo-circle-anim 2.2s infinite ease-in-out !important;
            animation: phonering-alo-circle-anim 2.2s infinite ease-in-out !important
        }

        .phonering-alo-phone.phonering-alo-hover .phonering-alo-ph-circle,
        .phonering-alo-phone:hover .phonering-alo-ph-circle {
            border-color: red;
            opacity: .5
        }

        .phonering-alo-phone.phonering-alo-green.phonering-alo-hover .phonering-alo-ph-circle,
        .phonering-alo-phone.phonering-alo-green:hover .phonering-alo-ph-circle {
            border-color: red;
            border-color: red;
            opacity: .5
        }

        .phonering-alo-phone.phonering-alo-green .phonering-alo-ph-circle {
            border-color: red;
            border-color: red;
            opacity: .5
        }

        .phonering-alo-phone.phonering-alo-gray.phonering-alo-hover .phonering-alo-ph-circle,
        .phonering-alo-phone.phonering-alo-gray:hover .phonering-alo-ph-circle {
            border-color: #ccc;
            opacity: .5
        }

        .phonering-alo-phone.phonering-alo-gray .phonering-alo-ph-circle {
            border-color: red;
            opacity: .5
        }

        .phonering-alo-ph-circle-fill {
            width: 125px;
            height: 125px;
            top: 25px;
            left: 55px;
            position: absolute;
            background-color: #000;
            border-radius: 100% !important;
            border: 2px solid transparent;
            -webkit-animation: phonering-alo-circle-fill-anim 2.3s infinite ease-in-out;
            animation: phonering-alo-circle-fill-anim 2.3s infinite ease-in-out;
            transition: all .5s;
            -webkit-transform-origin: 50% 50%;
            -ms-transform-origin: 50% 50%;
            transform-origin: 50% 50%
        }

        .phonering-alo-phone.phonering-alo-active .phonering-alo-ph-circle-fill {
            -webkit-animation: phonering-alo-circle-fill-anim 1.7s infinite ease-in-out !important;
            animation: phonering-alo-circle-fill-anim 1.7s infinite ease-in-out !important
        }

        .phonering-alo-phone.phonering-alo-static .phonering-alo-ph-circle-fill {
            -webkit-animation: phonering-alo-circle-fill-anim 2.3s infinite ease-in-out !important;
            animation: phonering-alo-circle-fill-anim 2.3s infinite ease-in-out !important;
            opacity: 0 !important
        }

        .phonering-alo-phone.phonering-alo-hover .phonering-alo-ph-circle-fill,
        .phonering-alo-phone:hover .phonering-alo-ph-circle-fill {
            background-color: red;
            background-color: red 9;
            opacity: .75 !important
        }

        .phonering-alo-phone.phonering-alo-green.phonering-alo-hover .phonering-alo-ph-circle-fill,
        .phonering-alo-phone.phonering-alo-green:hover .phonering-alo-ph-circle-fill {
            background-color: red;
            background-color: red;
            opacity: .75 !important
        }

        .phonering-alo-phone.phonering-alo-green .phonering-alo-ph-circle-fill {
            background-color: red;
            background-color: red
        }

        .phonering-alo-phone.phonering-alo-gray.phonering-alo-hover .phonering-alo-ph-circle-fill,
        .phonering-alo-phone.phonering-alo-gray:hover .phonering-alo-ph-circle-fill {
            background-color: rgba(204, 204, 204, 0.5);
            background-color: #ccc 9;
            opacity: .75 !important
        }

        .phonering-alo-phone.phonering-alo-gray .phonering-alo-ph-circle-fill {
            background-color: red;
            opacity: .75 !important
        }

        .phonering-alo-ph-img-circle {
            width: 75px;
            height: 75px;
            top: 50px;
            left: 80px;
            position: absolute;
            /*background: rgba(30, 30, 30, 0.1) url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==) no-repeat center center;*/
            border-radius: 100% !important;
            border: 2px solid transparent;
            -webkit-animation: phonering-alo-circle-img-anim 1s infinite ease-in-out;
            animation: phonering-alo-circle-img-anim 1s infinite ease-in-out;
            -webkit-transform-origin: 50% 50%;
            -ms-transform-origin: 50% 50%;
            transform-origin: 50% 50%
        }

        .phonering-alo-phone.phonering-alo-active .phonering-alo-ph-img-circle {
            -webkit-animation: phonering-alo-circle-img-anim 1s infinite ease-in-out !important;
            animation: phonering-alo-circle-img-anim 1s infinite ease-in-out !important
        }

        .phonering-alo-phone.phonering-alo-static .phonering-alo-ph-img-circle {
            -webkit-animation: phonering-alo-circle-img-anim 0 infinite ease-in-out !important;
            animation: phonering-alo-circle-img-anim 0 infinite ease-in-out !important
        }

        .phonering-alo-phone.phonering-alo-hover .phonering-alo-ph-img-circle,
        .phonering-alo-phone:hover .phonering-alo-ph-img-circle {
            background-color: red
        }

        .phonering-alo-phone.phonering-alo-green.phonering-alo-hover .phonering-alo-ph-img-circle,
        .phonering-alo-phone.phonering-alo-green:hover .phonering-alo-ph-img-circle {
            background-color: red;
            background-color: red
        }

        .phonering-alo-phone.phonering-alo-green .phonering-alo-ph-img-circle {
            background-color: red;
            background-color: red 9
        }

        .phonering-alo-phone.phonering-alo-gray.phonering-alo-hover .phonering-alo-ph-img-circle,
        .phonering-alo-phone.phonering-alo-gray:hover .phonering-alo-ph-img-circle {
            background-color: #ccc
        }

        .phonering-alo-phone.phonering-alo-gray .phonering-alo-ph-img-circle {
            background-color: red
        }

        @-webkit-keyframes phonering-alo-circle-anim {
            0% {
                -webkit-transform: rotate(0) scale(.5) skew(1deg);
                -webkit-opacity: .1
            }

            30% {
                -webkit-transform: rotate(0) scale(.7) skew(1deg);
                -webkit-opacity: .5
            }

            100% {
                -webkit-transform: rotate(0) scale(1) skew(1deg);
                -webkit-opacity: .1
            }
        }

        @-webkit-keyframes phonering-alo-circle-fill-anim {
            0% {
                -webkit-transform: rotate(0) scale(.7) skew(1deg);
                opacity: .2
            }

            50% {
                -webkit-transform: rotate(0) scale(1) skew(1deg);
                opacity: .2
            }

            100% {
                -webkit-transform: rotate(0) scale(.7) skew(1deg);
                opacity: .2
            }
        }

        @-webkit-keyframes fadeInRight {
            0% {
                opacity: 0;
                -webkit-transform: translate3d(100%, 0, 0);
                -ms-transform: translate3d(100%, 0, 0);
                transform: translate3d(100%, 0, 0)
            }

            100% {
                opacity: 1;
                -webkit-transform: none;
                -ms-transform: none;
                transform: none
            }
        }

        @keyframes fadeInRight {
            0% {
                opacity: 0;
                -webkit-transform: translate3d(100%, 0, 0);
                -ms-transform: translate3d(100%, 0, 0);
                transform: translate3d(100%, 0, 0)
            }

            100% {
                opacity: 1;
                -webkit-transform: none;
                -ms-transform: none;
                transform: none
            }
        }

        @-webkit-keyframes fadeOutRight {
            0% {
                opacity: 1
            }

            100% {
                opacity: 0;
                -webkit-transform: translate3d(100%, 0, 0);
                -ms-transform: translate3d(100%, 0, 0);
                transform: translate3d(100%, 0, 0)
            }
        }

        @keyframes fadeOutRight {
            0% {
                opacity: 1
            }

            100% {
                opacity: 0;
                -webkit-transform: translate3d(100%, 0, 0);
                -ms-transform: translate3d(100%, 0, 0);
                transform: translate3d(100%, 0, 0)
            }
        }

        @-webkit-keyframes phonering-alo-circle-anim {
            0% {
                -webkit-transform: rotate(0) scale(.5) skew(1deg);
                transform: rotate(0) scale(.5) skew(1deg);
                opacity: .1
            }

            30% {
                -webkit-transform: rotate(0) scale(.7) skew(1deg);
                transform: rotate(0) scale(.7) skew(1deg);
                opacity: .5
            }

            100% {
                -webkit-transform: rotate(0) scale(1) skew(1deg);
                transform: rotate(0) scale(1) skew(1deg);
                opacity: .1
            }
        }

        @keyframes phonering-alo-circle-anim {
            0% {
                -webkit-transform: rotate(0) scale(.5) skew(1deg);
                transform: rotate(0) scale(.5) skew(1deg);
                opacity: .1
            }

            30% {
                -webkit-transform: rotate(0) scale(.7) skew(1deg);
                transform: rotate(0) scale(.7) skew(1deg);
                opacity: .5
            }

            100% {
                -webkit-transform: rotate(0) scale(1) skew(1deg);
                transform: rotate(0) scale(1) skew(1deg);
                opacity: .1
            }
        }

        @-webkit-keyframes phonering-alo-circle-fill-anim {
            0% {
                -webkit-transform: rotate(0) scale(.7) skew(1deg);
                transform: rotate(0) scale(.7) skew(1deg);
                opacity: .2
            }

            50% {
                -webkit-transform: rotate(0) scale(1) skew(1deg);
                transform: rotate(0) scale(1) skew(1deg);
                opacity: .2
            }

            100% {
                -webkit-transform: rotate(0) scale(.7) skew(1deg);
                transform: rotate(0) scale(.7) skew(1deg);
                opacity: .2
            }
        }

        @keyframes phonering-alo-circle-fill-anim {
            0% {
                -webkit-transform: rotate(0) scale(.7) skew(1deg);
                transform: rotate(0) scale(.7) skew(1deg);
                opacity: .2
            }

            50% {
                -webkit-transform: rotate(0) scale(1) skew(1deg);
                transform: rotate(0) scale(1) skew(1deg);
                opacity: .2
            }

            100% {
                -webkit-transform: rotate(0) scale(.7) skew(1deg);
                transform: rotate(0) scale(.7) skew(1deg);
                opacity: .2
            }
        }
    </style>


    <div id="layout_footer">
        <?php echo $this->load->view('common/footer');?>
    </div>
    <div id="support_online" style="display: none !important;"></div>

    <?php if($this->router->fetch_class() != 'test') { ?>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
            (function () {
                var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                s1.async = true;
                s1.src = 'https://embed.tawk.to/5cd004dd2846b90c57ad12c8/default';
                s1.charset = 'UTF-8';
                s1.setAttribute('crossorigin', '*');
                s0.parentNode.insertBefore(s1, s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
    <?php } ?>

    <div id="support_facebook" style="overflow-y: auto;"> 
        <div class="support_close">
            <a class="viewallsupport" href="/tu-van.html">Xem tất cả</a>
            <img src="<?php echo $this->config->item("img"); ?>close-lb.gif">
        </div>
        <div class="fb-comments" data-height="auto" data-order-by="reverse_time" data-width="510px" data-href="http://mshoatoeic.com/tu-van.html"  data-num-posts="7"></div>
    </div>
    <div class="modal fade" id="modal_message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Thông báo</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div id="fb-root"></div>
        <!--<script type="text/javascript">
            $( document ).ready(function() {
                $("img").error(function () {
                    $(this).unbind("error").attr("src", "/theme/frontend/default/images/default_image.jpg");
                });
            });
        </script>-->
        <!-- COMMON SCRIPT -->
        <!-- <script type="text/javascript" src="<?php echo $this->config->item("lib"); ?>owl-carousel/owl.carousel.min.js"></script> -->
        <script type="text/javascript" src="<?php echo $this->config->item("js"); ?>bootstrap.min.js"></script>     
        <script type="text/javascript" src="<?php echo $this->config->item("js"); ?>jquery.smartmenus.js"></script>   
        <script type="text/javascript" src="<?php echo $this->config->item("js"); ?>call-option.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item("js"); ?>owl.carousel.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item("lib"); ?>jquery/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item("js"); ?>common.js"></script>

        <?php echo $this->load->get_section('script'); ?>
        <?php echo $this->config->item("tracking_code_footer"); ?>

        <?php
        if (isset($schema_mr_Luat)) echo $schema_mr_Luat;
        ?>
        <script>
            document.addEventListener('play', function(e){
                var audios = document.getElementsByTagName('audio');
                for(var i = 0, len = audios.length; i < len;i++){
                    if(audios[i] != e.target){
                        audios[i].pause();
                    }
                }
            }, true);
        </script>

    <div class="p_list-icon">
        <ul class="list-icon">
            <li>
                <div class="item">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <a href="https://www.anhngumshoa.com/lich-khai-giang/ha-noi" title="" class="item-link">Lịch Khai Giảng</a>
                </div>
            </li>
            <li>
                <div class="item">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSemm8cReUsYP5kafsR7TE3FeTHAkzHLr_x70gheBaQ0EyYWFQ/viewform" title="" class="item-link text-red">Đăng Ký Tư Vấn</a>
                </div>
            </li>
            <li>
                <div class="item">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSdLxGNjV7lRnRRlN1Z6v5mAtDYdJhVfjBXAKFbqs6tck33eTg/viewform" title="" class="item-link">Test Trình Độ</a>
                </div>
            </li>
        </ul>
    </div>


    <script>
    $(".p_list-icon").click(function () {
		$(".p_list-icon .list-icon li .item .item-link").toggle();
	});
    </script>
    </body>
    </html>