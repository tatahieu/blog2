<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta property="og:type" content="blog">
</head>
<body>

<div class="container">
    <?php

    $this->db->select('news_id, seo_title, share_url');
    $query = $this->db->get('news');

    $arr_duplicate = []; // seo_title => list news_id
    // check trùng
    $number_duplicate = 0;
    $list_arr_key_dup = [];
    $share_url_arr = [];

    $number_no_SEO_title = 0;
    $arr_no_SEO_title = [];

    foreach ($query->result() as $row) {
        $news_id = $row->news_id;
        if (trim($row->seo_title) != '')
        {
            $seo_title = $row->seo_title;
            if (array_key_exists($seo_title, $arr_duplicate))
            {
                // đã tồn tại
                $number_duplicate ++;
                $arr_duplicate[$seo_title] = $arr_duplicate[$seo_title].'|'.$news_id;
                array_push($list_arr_key_dup,$seo_title);
                $share_url_arr[$news_id] = $row->share_url;

            }else{
                // chưa tồn tại
                $arr_duplicate[$seo_title] = $news_id;
                $share_url_arr[$news_id] = $row->share_url;
            }
        }else{
            // Chưa có SEO title
            $number_no_SEO_title ++ ;
            array_push($arr_no_SEO_title,$news_id);
        }
    }

    echo '<h2><br>Result quét SEO Title hiện tại: ';
    echo '<br>Tổng số lượng bài viết: '.count($query->result());
    echo '<br>Số lượng bài viết chưa có tiêu đề SEO: '.$number_no_SEO_title;

    echo '<hr>';
    echo '<br>Số lượng tiêu đề SEO bị trùng: '.$number_duplicate.'</h2>';
    //        echo '<br>Số lượng nhóm trùng: '.count($list_arr_key_dup);
    echo '<br>Chi tiết: ';
    for ($i = 0; $i < count($list_arr_key_dup); $i++) {
        $mono_key_arr = $list_arr_key_dup[$i];
        echo '<h3>&bull; SEO title: ' .$mono_key_arr.'</h3>';
        $arr_id = explode('|',$arr_duplicate[$mono_key_arr]);
        echo 'Số lượng trùng: '.count($arr_id);

        for ($k = 0; $k < count($arr_id); $k++) {
            $news_id_this = $arr_id[$k];
            $share_url = $share_url_arr[$news_id_this];
            echo '<br>- <a href ="'.$share_url.'" title="news_id '.$news_id_this.'" target="_blank" >'.$share_url.'</a> (Id bài viết '.$news_id_this.' )';
        }
        echo '<br>';
    }

    ?>



</div>
</body>
