<?php echo $this->load->get_block('footer'); ?> 
<?php 
    $this->load->config('data');
    $arr_loc = $this->config->item('location');
    $arr_branch = json_decode($this->config->item('branch'), true);
    if(!empty($arr_branch)){
        $arr_data = array();
        foreach ($arr_branch as $item) {
            $arr_data[$item['loc']][] = $item;
        }
        ksort($arr_data);
    }
?>
<footer>
    <?php if($arr_data) { ?>
    <section id="footer-top"> 
	    <h2><img alt="Anh ngữ Ms Hoa - Đào tạo TOEIC số 1 Việt Nam" title="Anh ngữ Ms Hoa - Đào tạo TOEIC số 1 Việt Nam" src="<?php echo $this->config->item("css"); ?>images/ico-map.png"> Anh ngữ Ms Hoa - Đào tạo TOEIC số 1 Việt Nam</h2>
        <div class="container">
             <div class="row">
                <?php foreach($arr_data as $key => $data) { ?>
                <div class="col-md-4 col-sm-12">
				    <ul class="quick-links">
                        <li><h3><span><?php echo $arr_loc[$key]?></span></h3></li>
                        <?php foreach($data as $item) { ?>
                            <li><span><?php echo $item['label']?></span>: <?php echo $item['name']?><div class="fr"><span> - Số ĐT:</span> <a href="tel:<?php echo $item['phone']?>" ><?php echo $item['phone']?></a></div></li>
                        <?php } ?>
				    </ul>
                </div>
                <?php } ?>
             
            </div>
             
          </div>
       </section>
       <?php } ?>
       
       <section id="footer-bottom">
          <div class="container">
             <div class="row">
                 <p class="content">Copyright ® AnhnguMsHoa2018. All Rights Reserved.</p>
             </div>
          </div>
    </section>
</footer>
<a href="#" class="cd-top">Top</a> 