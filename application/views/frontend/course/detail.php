<div class="-custom-row">
	<!-- content -->
	<div class="col-md-8 col-sm-8 col-xs-12 video-learning -custom-card ">
        <div class="video-wrapper">
            <iframe width="560" height="315" src="<?php echo $classDetail['video']?>" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
        </div>
        <div class="detail">
            <?php echo $classDetail['detail']?>
        </div>
	</div>

	<!-- sidebar -->
	<div class="col-md-4 col-sm-4 col-xs-12 -custom-card ">
        <div class="info-class-video">
                <div class="info-class-video__background">
                    <div class="info-class-video__title">
                        <div class="text-title">Thông tin khóa học</div>
                    </div>                    
                </div>
                <div class="user-information">
                    <div class="user-information__title text-bold">
                            Chào <span class="username">Nguyễn Văn An</span>
                    </div>
                    <div class="user-information__content">
                            Tên khóa học: <span class="text-bold"><?php echo $courseDetail['title']?></span>
                    </div>
                    <div class="user-information__content">
                            Số giờ học: <span class="text-bold"><?php echo $courseDetail['number_lesson']?></span>
                    </div>
                    <div class="user-information__content">
                            Đầu vào: <span class="text-bold"><?php echo $courseDetail['input']?></span>
                    </div>
                    <div class="user-information__content">
                            Đầu ra: <span class="text-bold"><?php echo $courseDetail['output']?></span>
                    </div>
                </div>
        </div>

        <div class="course-list">
            <div class="course-list__background -margin-top">
                <div class="course-list__title">
                    <div class="text-title">Nội dung khóa học</div>
                   <!--  <div class="button-filter">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <i class="fa fa-filter" aria-hidden="true"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-filter">
                            <li class="dropdown-filter__items"><a href="#">Khóa đã học</a></li>
                            <li class="dropdown-filter__items"><a href="#">Khóa chưa học</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-filter__items"><a href="#">Xem tất cả</a></li>
                        </ul>
                    </div> -->
                </div>                    
            </div>
            <div class="single-course">
                <?php foreach($arrClass as $topic) { ?>
                    <?php foreach($topic as $class_item) { ?>
                        <div href="" class="panel-body lesstion">
                            <div class="lesstion__checkbox">
                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                            </div>
                            <div class="lesstion-wrapper">
                                <div class="lesstion__name">
                                    <a href="<?php echo $class_item['share_url']?>">
                                        <?php echo $class_item['title']?>
                                    </a>
                                </div>
                                <div class="information-wrapper">
                                    <div class="lesstion__time">
                                        <span class="custom-icon-color"><i class="fa fa-play-circle"
                                                aria-hidden="true"></i></span> <span
                                            class="custom-text-color"><?php echo $class_item['duration']?></span>
                                    </div>

                                    <div class="lesstion__date">
                                        <span><i class="fa fa-calendar-o" aria-hidden="true"></i></span>
                                        <?php echo date('d/m/Y', $class_item['publish_time'])?>
                                    </div>

                                    <!-- <a href="" class="lesstion__document">
                                        <span><i class="fa fa-file-text-o"
                                                aria-hidden="true"></i></span> Document
                                    </a> -->
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
	</div>
	<!-- End sidebar -->
</div>