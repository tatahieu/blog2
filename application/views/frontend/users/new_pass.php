<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<form method="POST" action="">
    <div id="users_new_pass">
        <?php echo validation_errors()?>
        password: <?php echo $password?>
        password confirm: <?php echo $password_confirm?>

        <?php echo $this->lang->line('users_captcha')?>: <?php echo $input_captcha?><?php echo $captcha?>
        <?php echo $submit?>
    </div>
</form>