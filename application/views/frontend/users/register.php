<?php
$state = ($this->input->get("redirect_uri")) ? '&state='.$this->input->get('redirect_uri') : '';
$this->load->config('data');
$arrLocation = $this->config->item("location");
?>
<div class="col-md-12">
    <?php echo $this->load->view('common/breadcrumb');?>
    
    <div class="box-login"> 

        <div class="col-md-4">
            <div class="head">
                <h2 class="heading-title">Đăng ký bằng</h2>
            </div> 
            <div class="socialconnect">
                <a href="https://www.facebook.com/dialog/oauth?client_id=<?php echo $this->config->item("facebook_app_id"); ?>&scope=email,public_profile&redirect_uri=<?php echo urlencode(BASE_URL.'/users/loginsocial/facebook') ; ?><?php echo  $state; ?>" class="facebook mrb_20">Sign up with Facebook</a>
                <a href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo $this->config->item("google_app_id"); ?>&response_type=code&scope=email&redirect_uri=<?php echo urlencode(BASE_URL.'/users/loginsocial/google') ?><?php echo  $state; ?>" class="google">Sign up with Google</a>
            </div>
        </div>
        
        <div class="col-md-1 txt_login_or">OR</div>
        
        <div class="col-md-7">
            <div class="head">
                <h2 class="heading-title">Đăng ký tài khoản mới</h2>
            </div> 
        
            <form class="cd-form" id="register_form">
                <p class="fieldset">
                    <i class="fa fa-user"></i>
                    <input class="full-width has-padding has-border" id="input_fullname" name="fullname" type="text" placeholder="Họ và tên">
                </p>

                <p class="fieldset">
                    <i class="fa fa-phone"></i>
                    <input class="full-width has-padding has-border" id="input_phone" name="phone" type="text" placeholder="Số điện thoại">
                </p>
                
                <p class="fieldset">
                    <i class="fa fa-envelope"></i>
                    <input class="full-width has-padding has-border" id="input_email" name="email" type="email" placeholder="Email đăng ký">
                </p>
                <p class="fieldset">
                    <select name="city_id" class="form-control">
                        <option value="0">Nơi bạn sống</option>
                        <?php foreach ($arrLocation as $key => $value) { ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </p>
                <p class="fieldset">
                    <i class="fa fa-key"></i>
                    <input class="full-width has-padding has-border" id="input_password" name="password"  type="password"  placeholder="Mật khẩu">
                </p>
                <p class="fieldset">
                    <i class="fa fa-key"></i>
                    <input class="full-width has-padding has-border" id="input_repassword" name="repassword"  type="password"  placeholder="Nhập lại mật khẩu">
                </p> 
                
                <p class="fieldset">
                    <i class="fa fa-lock"></i>
                    <input style="width:200px" class="full-width has-padding has-border" id="input_captcha" name="captcha" type="text"  placeholder="Mã bảo mật">
                    <div id="captcha">
                        <?php echo $captcha;?>
                    </div>
                </p>

                <p class="fieldset">
                    <input type="radio" id="accept-terms" value="1" name="check_confirm">
                    <label for="accept-terms">Tôi đồng ý với các <a href="#">Điều khoản</a> trên website</label>
                </p>
                <p class="fieldset">
                    <input class="btn-edit" type="submit" value="Đăng ký">
                    <input class="btn-edit" type="reset" value="Làm lại">
                </p> 
            </form>
        </div>
                       
    </div> 

</div> 
<script type="text/javascript">
    $(document).ready(function(){
        //Liên hệ
        $('#register_form').submit(function(){
            $('#register_form').validate({
                rules: {
                    fullname: {
                        required: true,
                    },
                    phone: {
                        required: true,
                        number: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                    },
                    repassword: {
                        required: true,
                    },
                    check_confirm: {
                        required: true,
                    },
                    captcha: {
                        required: true
                    }
                },
                messages: {
                    fullname: {
                        required: "Họ và tên không được để trống",
                    },
                    phone: {
                        required: "Số điện thoại không được để trống",
                        number: "Số điện thoại không đúng định dạng",
                    },
                    email: {
                        required: "Email không được để trống",
                        email: "Email không đúng định dạng"
                    },
                    password: {
                        required: "Mật khẩu không được để trống",
                    },
                    repassword: {
                        required: "Nhập lại mật khẩu không được để trống",
                    },
                    captcha: {
                        required: "Mã bảo mật không được để trống",
                    },
                    check_confirm:{
                        required: "Bạn chưa đồng ý với các điều khoản trên webiste",
                    },
                },
                highlight: function(element) {
                    $(element).addClass("has-error");
                },
                unhighlight: function(element) {
                    $(element).removeClass("has-error");
                }
            });
            if ($('#register_form').valid()) // check if form is valid
            {
                $.ajax({
                    type: 'post',
                    dataType : 'json',
                    url: '/dang-ky.html',
                    data: $("#register_form").serializeArray(),
                    success: function (respon) {
                        if(respon.status == "success"){
                            redirect(respon.redirect_uri);
                            $("#register_form")[0].reset();
                        } else {

                            $.each( respon.message, function( key, msg ) {
                                $('#input_'+key).addClass('has-error');
                                $('#input_'+key).after('<label id="'+key+'-error" class="error" for="'+key+'">'+msg+'</label>');
                            });
                        }
                        $('#captcha').empty().append(respon.captcha);
                    },
                    error: function(respon,code) {
                        
                    }
                });   
            }
            return false;
        });
    });
</script>