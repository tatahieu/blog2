<div>
	<div class="course-item m-b-2">
		<h2 class="course-head text-center">
			<?php if($row) { ?>
				Lịch sử làm bài test
			<?php }else { ?>
				Bạn chưa làm bài test nào
			<?php } ?>
		</h2>
		<?php if($row) { ?>
			<div class="table-responsive" style="margin: 15px">
				<table class="course-table table table-bordered text-middle m-b-0">
					<thead>
						<tr>
							<th>STT</th>
							<th>Tên bài test</th>
							<th>Điểm số</th>
							<th style="text-align: center;">Thời gian làm bài</th>
							<th style="text-align: center;">Ngày làm bài</th>
							<th style="text-align: center;">Làm lại</th>
						</tr>
					</thead>

					<tbody>
						<?php foreach($row as $key => $item) { ?>
						<tr>
							<th>
								<?php echo $key + 1?>
							</th>
							<td><span class="font14"><?php echo $item['title']?></span></td>
							<td>
								<span class="font14"><?php echo $item['score']?></span>
							</td>
							<td style="text-align: center;">
								<span class="font14"><?php echo gmdate("H:i:s", $item['end_time'] - $item['start_time']) ?></span>
							</td>
							<td style="text-align: center;">
								<?php echo date('H:i d/m/Y', $item['start_time'])?>
							</td>
							<td style="text-align: center;">
								<a class="btn btn-sm btn-primary btn-reg link-reg" href="<?php echo $item['share_url']?>">Làm lại</a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php echo $paging?>
		<?php }	?>
	</div>
</div>
