<?php
$state = ($this->input->get("redirect_uri")) ? '&state='.$this->input->get('redirect_uri') : '';
?>
<div class="col-md-12">
    <?php echo $this->load->view('common/breadcrumb');?>
	
	<div class="box-login box_sign_in"> 
		<div class="head">
			<h2 class="heading-title">Đăng nhập tài khoản</h2>
		</div> 
        <?php if ($error > 0) { ?>
            <div class="alert alert-danger" role="alert">Đăng nhập không thành công</div>
        <?php } ?>
		<form class="cd-form" id="login_form" method="POST" action="">
			<p class="fieldset">
				<i class="fa fa-envelope"></i>
				<input class="full-width has-padding has-border" name="email" id="input_email" type="email" placeholder="Email đăng nhập">
                <?php echo $this->form_validation->error('email'); ?>
			</p>

			<p class="fieldset">
				<i class="fa fa-key"></i>
				<input class="full-width has-padding has-border" type="password" id="input_password" name="password" placeholder="Mật khẩu">
                <?php echo $this->form_validation->error('password'); ?>
			</p>
			<p class="fieldset">
				<input class="full-width" type="submit" value="Đăng nhập">
			</p> 
			
			<p><a href="<?php echo SITE_URL; ?>/users/forgotpass">Quên mật khẩu?</a></p>
		</form>
		<div class="login_with"><em>Hoặc đăng nhập qua</em></div>
		<div class="socialconnect">
			<a href="https://www.facebook.com/dialog/oauth?client_id=<?php echo $this->config->item("facebook_app_id"); ?>&scope=email,public_profile&redirect_uri=<?php echo urlencode(SITE_URL.'/users/loginsocial/facebook') ; ?><?php echo  $state; ?>" class="facebook">Login with Facebook</a>
			<a href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo $this->config->item("google_app_id"); ?>&response_type=code&scope=email&redirect_uri=<?php echo urlencode(BASE_URL.'/users/loginsocial/google') ?><?php echo  $state; ?>" class="google">Login with Google</a>
		</div>
	 		   
    </div> 

</div>