<div class="col-md-12">
    <?php echo $this->load->view('common/breadcrumb');?>
	
	<div class="box-login box_sign_in"> 
		<div class="head">
			<h2 class="heading-title">Quên mật khẩu</h2>
		</div> 
 
		<form class="cd-form" id="forgotpass_form">
			<p class="fieldset">
				<i class="fa fa-envelope"></i>
				<input class="full-width has-padding has-border" type="email" name="email" id="input_email" placeholder="Email">
			</p>
			<p class="fieldset">
				<i class="fa fa-lock"></i>
				<input style="width:200px" class="full-width has-padding has-border" id="input_captcha" type="text"  placeholder="Mã bảo mật" name="captcha">
				<div id="captcha">
                    <?php echo $captcha;?>
                </div>
			</p>
			<p class="fieldset">
				<input class="full-width has-padding" type="submit" value="Gửi mật khẩu">
			</p>
		</form>  
					   
    </div> 

</div> 

<script type="text/javascript">
    $(document).ready(function(){
        //Liên hệ
        $('#forgotpass_form').submit(function(){
            $('#forgotpass_form').validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    captcha: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: "Email không được để trống",
                        email: "Email không đúng định dạng"
                    },
                    captcha: {
                        required: "Mã bảo mật không được để trống",
                    },
                },
                highlight: function(element) {
                    $(element).addClass("has-error");
                },
                unhighlight: function(element) {
                    $(element).removeClass("has-error");
                }
            });
            if ($('#forgotpass_form').valid()) // check if form is valid
            {
                $.ajax({
                    type: 'post',
                    dataType : 'json',
                    url: '/quen-mat-khau.html',
                    data: $("#forgotpass_form").serializeArray(),
                    success: function (respon) {
                        if(respon.status == "success"){
                            message(respon.message);
                            $("#forgotpass_form")[0].reset();
                        } else {
                            $.each( respon.message, function( key, msg ) {
                                $('#input_'+key).addClass('has-error');
                                $('#input_'+key).after('<label id="'+key+'-error" class="error" for="'+key+'">'+msg+'</label>');
                            });
                        }
                        $('#captcha').empty().append(respon.captcha);
                    },
                    error: function(respon,code) {
                        
                    }
                });   
            }
            return false;
        });
    });
</script>