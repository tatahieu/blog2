<?php 
$url = ($newsDetail['news_id'] < 37536) ? 'http://mshoatoeic.com' . $newsDetail['old_share_url'] : SITE_URL.$newsDetail['share_url'];
$arr_loc = array(1 => 'Tp. Hà Nội', 2 => 'Tp. Hồ Chí Minh', 3 => 'Tp. Đà Nẵng');
$arr_branch = json_decode($this->config->item('branch'), true);
if(!empty($arr_branch)){
    $arr_data = array();
    foreach ($arr_branch as $item) {
        $arr_data[$item['loc']][] = $item;
    }
}
?>
<div class="col-md-8 col-sm-8 col-xs-12">
    <?php echo $this->load->view('common/breadcrumb');?>
    <div class="col_news_cm">
                 
        <article class="thumbnail-news-view">
            <h1><?php echo $newsDetail['title'];?></h1>
            <div class="block_timer_share">
                <div class="block_timer pull-left"><i class="fa fa-clock-o"></i> <?php // echo  date('d/m/Y',$newsDetail['publish_time']);?></div>
                <div class="block_share pull-right">
                    <!-- <a class="tooltip-top" href="https://www.facebook.com/sharer.php?u=<?php echo urlencode(SITE_URL.$newsDetail['share_url']);?>" title="Share Facebook" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=400')" data-original-title="Share Facebook"><img alt="" src="<?php echo $this->config->item("css"); ?>images/ico-facebook.png"></a> -->
                    <!-- <a class="tooltip-top" href="https://twitter.com/intent/tweet?url=<?php echo urlencode(SITE_URL.$newsDetail['share_url'])."&text=".$newsDetail['title']."-";?>" target="_blank" onclick="return !window.open(this.href, 'Twitter', 'width=640,height=400')" title="Share Twitter" data-original-title="Share Twitter"><img alt="" src="<?php echo $this->config->item("css"); ?>images/ico-twitter.png"></a>
                    <a class="tooltip-top" href="https://plus.google.com/share?url=<?php echo urlencode(SITE_URL.$newsDetail['share_url']);?>" title="Share Google Plus" target="_blank" onclick="return !window.open(this.href, 'Google Plus', 'width=640,height=400')" data-original-title="Share G+"><img alt="" src="<?php echo $this->config->item("css"); ?>images/ico-g.png"></a> -->
                    <!-- <a class="tooltip-top" href="mailto:username@example.com?subject=<?php echo $newsDetail['title'];?>&body=<?php echo SITE_URL.$newsDetail['share_url'];?>" title="" data-original-title="Email"><img alt="" src="<?php echo $this->config->item("css"); ?>images/ico-email.png"></a> 
                    <a class="tooltip-top" href="javascript:;" onclick="window.print();" title="Print" data-original-title="Print"><img alt="" src="<?php echo $this->config->item("css"); ?>images/ico-print.png"></a>  -->
                    <div class="fb-share-button" data-href="<?php echo SITE_URL.$newsDetail['share_url']?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url?>" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                    <div class="fb-like" data-href="<?php echo SITE_URL.$newsDetail['share_url']?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
                </div>
            </div>
            
            <div id="news_detail_content" class="post_content"> 
                <?php if($newsDetail['description']) { ?>
                    <p><strong><?php echo $newsDetail['description'];?></strong></p> 
                <?php } ?>
                <?php echo html_entity_decode($newsDetail['detail']);?>   
            </div>
            <?php if($tags){?>
            <ul class="tags">
               <li><strong>Tags:</strong></li>
               <?php foreach($tags as $tag){?>
               <li><a href="<?php echo SITE_URL.$tag['share_url'];?>"><?php echo $tag['name'];?></a></li>

               <?php }?>
            </ul>
            <?php }?>
            <?php if($relate){?>
            <div class="title-detail-cl">Tin tức khác</div>
            <ul class="other-news-other slide-news list-video">
                <?php foreach($relate as $row) { ?>
                 <li>
                      <figure><a href="<?php echo $row['share_url'];?>"><img src="<?php echo getimglink($row['images'],'size4');?>" alt="<?php echo $row['title'];?>"/></a></figure>
                      <figcaption><a href="<?php echo $row['share_url'];?>" title="<?php echo $row['title'];?>"><?php echo $row['title'];?></a></figcaption>
                 </li>
                 <?php } ?>
            </ul>
            <?php }?>
            <div class="block-comment-face"> 
                <div class="fb-comments" data-order-by="reverse_time" data-href="<?php echo $url?>" data-numposts="5" width="100%"></div>
                <div class="fb-share-button" data-href="<?php echo $url?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo SITE_URL.$newsDetail['share_url']?>" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                <div class="fb-like" data-href="<?php echo SITE_URL.$newsDetail['share_url']?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
            </div><!-- End -->

            <!-- Quảng cáo -->
            <?php echo $this->load->get_block('news_ads'); ?>
            
        </article>
     
    </div> <!-- End -->

    
    <?php echo $this->load->get_block('content'); ?> 
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
    <?php echo $this->load->get_block('right'); ?>
    <div class="sidebar-block"> 
        <div class="title-header-bl"><h2>FANPAGE</h2><span><i class="fa fa-angle-right arow"></i></span></div>
        <div class="fb-page" data-href="<?php echo $this->config->item('facebook');?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo $this->config->item('facebook');?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $this->config->item('facebook');?>">Facebook</a></blockquote></div>
    </div><!--End-->
</div>
<script type="text/javascript">
    $("iframe").wrap('<div class="embed-responsive embed-responsive-16by9"/>');
    $("iframe").addClass('embed-responsive-item');
    $("#news_detail_content").find("table").each(function(){
        //console.log($(this).css("width"));
        //if (parseInt($(this).css("width")) == 0) {
            $(this).css("width","auto");
        //}
    })
    $(document).ready(function(){
        if ($(".ck_detail_form_contact").length) {
            var html = '<div class="box_dktv">\
                            <h2>Đăng ký tư vấn</h2>\
                            <form id="tuvan_form_detail" method="POST" action="<?php echo SITE_URL; ?>/contact/tuvan">\
                            <div class="form-group">\
                                <input class="form-control" type="text" required name="fullname" placeholder="Họ và tên">\
                            </div>\
                            <div class="form-group">\
                                <input class="form-control" type="text" required name="phone" placeholder="Số điện thoại">\
                            </div>\
                            <div class="form-group">\
                                <input class="form-control" type="text" required name="email" placeholder="Email">\
                            </div>\
                            <div class="form-group">\
                                <select class="form-control" required placeholder="Cơ sở bạn muốn tư vấn" name="coso">\
                                    <option value="">Cơ sở bạn muốn tư vấn?</option>';

                                    <?php if($arr_data) { ?>
                                    <?php foreach($arr_data as $key => $data) { ?>
                                        html += '<option disabled style="font-weight: bold;" >'
                                        html += 'Hệ thống cơ sở <?php echo $arr_loc[$key]?></option>';
                                        <?php foreach($data as $item) { ?>
                                            html+= '<option value="<?php echo $item['id']?>">\
                                                <?php echo $item['label']?>: <?php echo $item['name']?> - SĐT: <?php echo $item['phone']?>\
                                            </option>';
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                html+= '</select>\
                            </div>\
                            <div style="text-align:center"><button class="btn_send btn_send_form_sidebar" type="submit" id="tuvan_submit">Gửi</button></div>\
                        </form>\
                        <div class="clearfix"></div>\
                        </div>';
            $(".ck_detail_form_contact").first().html(html).removeClass("ck_detail_form_contact");      
        }
        $("#news_detail_content").find("table").wrap('<div class="table-responsive2"/>').addClass('table table-bordered table-striped');
    })
</script>