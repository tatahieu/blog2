<div class="col-md-8 col-sm-8 col-xs-12">
    <?php echo $this->load->view('common/breadcrumb');?>
    
    <div class="col_news_cm">
        <?php foreach($rows as $row){?>
        
            <a href="<?php echo $row['share_url'];?>"><div class="thumbnail-news">
            <figure><img src="<?php echo getimglink($row['images'],'size4');?>" alt="<?php echo $row['title'];?>"></figure>
            <div class="caption">
                <h3><?php echo $row['title'];?></h3>
                <!--<span class="date"><i class="fa fa-clock-o"></i> < ?php echo date('d/m/Y',$row['publish_time']);?></span>-->
                <p><?php echo $row['description'];?> </p> 
            </div>
        </div></a> 
        <?php }?>
        <?php echo $paging;?>
    </div>
    <?php echo $this->load->get_block('content'); ?> 
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
    <?php echo $this->load->get_block('right'); ?>
    <div class="sidebar-block"> 
        <div class="title-header-bl"><h2>FANPAGE</h2><span><i class="fa fa-angle-right arow"></i></span></div>
        <div class="fb-page" data-href="<?php echo $this->config->item('facebook');?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo $this->config->item('facebook');?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $this->config->item('facebook');?>">Facebook</a></blockquote></div>
    </div><!--End-->
</div>