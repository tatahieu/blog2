<?php
	$top1 = array_shift($rows);	
	$top2 = array_shift($rows);
?>
<div class="col-md-8 col-sm-8 col-xs-12">
    <div class="td_content_news" style="border: none;">
		<div class="col-md-6 col-sm-6 col-xs-12">		 
		   <ul>
				<li class="big">
					<a href="<?php echo $top1['share_url'];?>" title="<?php echo $top1['title'];?>"><img src="<?php echo getimglink($top1['images'],'size6');?>" alt="<?php echo $top1['title'];?>"></a>
					<h3><a href="<?php echo $top1['share_url'];?>" title="<?php echo $top1['title'];?>"><?php echo $top1['title'];?></a></h3>
					<p class="sapo"><?php echo $top1['description'];?></p>
				</li>
			</ul>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">		 
		   <ul>
				<li class="big">
					<a href="<?php echo $top1['share_url'];?>" title="<?php echo $top1['title'];?>"><img src="<?php echo getimglink($top1['images'],'size6');?>" alt="<?php echo $top1['title'];?>"></a>
					<h3><a href="<?php echo $top1['share_url'];?>" title="<?php echo $top1['title'];?>"><?php echo $top1['title'];?></a></h3>
					
					<p class="sapo"><?php echo $top1['description'];?></p>
				</li>
			</ul>
		</div>
	</div><!-- /.End -->

    <?php echo $this->load->get_block('content'); ?> 
</div>
<div class="col-md-4 col-sm-4 col-xs-12">
    <?php echo $this->load->get_block('right'); ?>
    <div class="sidebar-block"> 
        <div class="title-header-bl"><h2>FANPAGE</h2><span><i class="fa fa-angle-right arow"></i></span></div>
        <div class="fb-page" data-href="<?php echo $this->config->item('facebook');?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo $this->config->item('facebook');?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $this->config->item('facebook');?>">Facebook</a></blockquote></div>
    </div><!--End-->
</div>