<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model {
	private $_table_users = 'nhan_vien';
    private $_key = 'ma_nhan_vien';
	function __construct()
    {
		parent::__construct();
    }
    public function lists($params = array()){
    	$params = array_merge(array('limit' => 30,'offset' => 0),$params);
		if (isset($params['email'])){
			$this->db->where("email",$params['email']);
		}
		$this->db->order_by($this->_key,'DESC');
		$this->db->limit($params['limit'],$params['offset']);
		$query = $this->db->get($this->_table_users);
		return $query->result_array();
    }
    
	function detail($id){
		$this->db->where($this->_key,$id);
		$query = $this->db->get($this->_table_users);
		return $query->row_array();
	}
	function delete($cid){
		$cid = (is_array($cid)) ? $cid : (int) $cid;
		$this->db->where_in($this->_key,$cid);
		$this->db->delete($this->_table_users);
		return $this->db->affected_rows();
	}

    /**
     * @param string $input
     * @return  : $user_id
     * @author: namtq
     * @todo: register user and send token
     */
    public function register($input = ''){
        $input = array(
            'ho_va_ten' => $input['ho_va_ten'],
            'ngay_sinh' => (int) convert_datetime($input['ngay_sinh']),
            'que_quan' => $input['que_quan'],
            'dia_chi' => $input['dia_chi'],
//            'password' => hash('sha256',$input['password']),
            'quoc_tich' => $input['quoc_tich'],
            'ton_giao' => $input['ton_giao'],
            'gioi_tinh' => $input['gioi_tinh'],
            'trinh_do' => $input['trinh_do'],
            'dien_thoai' => $input['dien_thoai'],
            'email' => $input['email'],
            'ngay_vao_cong_ty' => (int) convert_datetime($input['ngay_vao_cong_ty']),
            'cmtnd' => $input['cmtnd']
        );
        $this->db->insert($this->_table_users,$input);
        return $this->db->insert_id();
    }

    /**
     * @author: namtq
     * @param: user_id int, profile array()
     * @todo: Update profile data
     * @return: 0 or 1
     */
    public function update($user_id, $profile = array()){
        if (!$user_id) {
            return false;
        }
        $input = array(
            'ho_va_ten' => $profile['ho_va_ten'],
            'ngay_sinh' => (int) convert_datetime($profile['ngay_sinh']),
            'que_quan' => $profile['que_quan'],
            'dia_chi' => $profile['dia_chi'],
            'quoc_tich' => $profile['quoc_tich'],
            'ton_giao' => $profile['ton_giao'],
            'gioi_tinh' => $profile['gioi_tinh'],
            'trinh_do' => $profile['trinh_do'],
            'dien_thoai' => $profile['dien_thoai'],
            'email' => $profile['email'],
            'ngay_vao_cong_ty' => (int) convert_datetime($profile['ngay_vao_cong_ty']),
            'cmtnd' => $profile['cmtnd']
        );
        $this->db->where($this->_key,$user_id);
        $this->db->update($this->_table_users,$input);
        return $this->db->affected_rows();
    }

    public function list_certificate($id)
    {
        $this->db->where("ma_nhan_vien",$id);
        $this->db->order_by('ma_bang_cap','DESC');
        $query = $this->db->get('bang_cap_nhan_vien');
        return $query->result_array();
    }

    public function certificate($id)
    {
        $this->db->where("ma_bang_cap",$id);
        $query = $this->db->get('bang_cap_nhan_vien');
        return $query->row_array();
    }

    public function insert_certificate($input){
        // insert data
        $this->db->insert('bang_cap_nhan_vien',$input);
        $ma_bang_cap = (int)$this->db->insert_id();
        return $ma_bang_cap;
    }

    public function update_certificate($id, $data = array()){
        if (!$id) {
            return false;
        }
        $input = array(
            'ten_chuyen_nganh' => $data['ten_chuyen_nganh'],
            'ten_bang_cap' => $data['ten_bang_cap'],
            'loai_bang' => $data['loai_bang'],
            'ten_truong' => $data['ten_truong'],
            'nam_tot_nghiep' => $data['nam_tot_nghiep'],
            'hinh_thuc_dao_tao' => $data['hinh_thuc_dao_tao']
        );
        $this->db->where('ma_bang_cap',$id);
        $this->db->update('bang_cap_nhan_vien',$input);
        return $this->db->affected_rows();
    }
}