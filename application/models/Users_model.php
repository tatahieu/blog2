<?php use mysql_xdevapi\RowResult;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model {
    private $_table_member = 'nhan_vien';
    private $_key = 'ma_nhan_vien';
    function __construct()
    {
        parent::__construct();
    }
/** =================================== LOGIN ========================================= **/
    /**
     * @param: array(email)
     * @return
     * @author: namtq
     * @todo: Get profile by email
     */
    public function getProfileByEmail($email){
        $this->db->where('email',$email);
        $query =  $this->db->get($this->_table_member);
        return $query->row_array();
    }

    /**
     * @param: user_id int
     * @param array $option
     * @return RowResult
     * @author: namtq
     * @todo: Get profile by id
     */
    public function getUserById($user_id){
        $this->db->where($this->_key,$user_id);
        $query =  $this->db->get($this->_table_member);
        return $query->row_array();
    }

    /**
     * @param string $input
     * @return  : $user_id
     * @author: namtq
     * @todo: register user and send token
     */
    public function register($input = ''){
        $input = array(
            'ho_va_ten' => $input['ho_va_ten'],
            'ngay_sinh' => $input['ngay_sinh'],
            'que_quan' => $input['que_quan'],
            'dia_chi' => $input['dia_chi'],
//            'password' => hash('sha256',$input['password']),
            'quoc_tich' => $input['quoc_tich'],
            'ton_giao' => $input['ton_giao'],
            'gioi_tinh' => $input['gioi_tinh'],
            'trinh_do' => $input['trinh_do'],
            'dien_thoai' => $input['dien_thoai'],
            'email' => $input['email'],
            'ngay_vao_cong_ty' => $input['ngay_vao_cong_ty'],
            'cmtnd' => $input['cmtnd']
        );
        $this->db->insert($this->_table_member,$input);
        return $this->db->insert_id();
    }

    // kiem tra user da dang nhap lop hoc chua
    public function check_class_login($username,$password){
        $this->db->where("username",$username);
        $this->db->where("password",base64_encode($password));
        $query = $this->db->get("class",1);
        return $query->row_array();
    }
    /**
    * @author: namtq
    * @param: user_id int, profile array()
    * @todo: Update profile data
    * @return: 0 or 1
    */
    public function updateProfile($user_id, $profile = array()){
        if (!$user_id) {
            return false;
        }
        $input = array(
            'ho_va_ten' => $profile['ho_va_ten'],
            'ngay_sinh' => $profile['ngay_sinh'],
            'que_quan' => $profile['que_quan'],
            'dia_chi' => $profile['dia_chi'],
            'quoc_tich' => $profile['quoc_tich'],
            'ton_giao' => $profile['ton_giao'],
            'gioi_tinh' => $profile['gioi_tinh'],
            'trinh_do' => $profile['trinh_do'],
            'dien_thoai' => $profile['dien_thoai'],
            'email' => $profile['email'],
            'ngay_vao_cong_ty' => $profile['ngay_vao_cong_ty'],
            'cmtnd' => $profile['cmtnd']
        );
        $this->db->where($this->_key,$user_id);
        $this->db->update($this->_table_member,$input);
        return $this->db->affected_rows();
    }

    public function get_user_by_email($email) {
        //$this->db->select();
        $this->db->where('email',$email);
        $query = $this->db->get($this->_table_member);
        return $query->row_array();
    }
}