var writing = {
    pharse: 1,
    param: [],
    run: function(){
        // check editor ready
        //CKEDITOR.on("instanceReady",function() {
        //count down time

        // next & prev button
        var max_page = $("#change_question").attr("max-page");
        $(".prev_button").bind("click",function(){
            var current_page = parseInt($("#change_question").attr("current-page"));
            var page = (current_page == 1) ? max_page : current_page - 1;
            writing.showquestion(page);
        });
        $(".next_button").bind("click",function(){
            var current_page = parseInt($("#change_question").attr("current-page"));
            if (current_page == max_page){
                $("#dialog-message").html("<p>Bạn phải sử dụng hết thời gian của mình mới có thể chuyển sang các câu hỏi tiếp theo</p>");
                 $( "#dialog-message" ).dialog({
                    modal: true,
                    buttons: {
                        Return: function() {
                            $(this).dialog("close");
                        }
                    }
                 });
            }
            else{
                var page = current_page + 1;
                writing.showquestion(page);
            }
        });
        $("#writing_countdown").bind("click",function(){
            var countid = $("#writing_count_down");
            if (countid.is(':visible')){ // neu thoi gian hien
                $("#hidetime").text("Show Time");
                $("#writing_count_down").hide();
            }
            else{
                $("#hidetime").text("Hide Time");
                $("#writing_count_down").show();
            }
        });
        $("#show_count_word").bind("click",function(){
            var txt = $(this).find(".showtxt").text();
            if (txt == 'Hide word count'){
                $(this).find(".showtxt").text("Show word count");
                $(this).find(".show_word").css("width","100");
            }
            else{
                $(this).find(".showtxt").text("Hide word count");
                $(this).find(".show_word").css("width","131");
            }
        });
        //////////// count word /////////////
        $("#textarea_1").bind("keypress",function(){
            writing.count_word(1);
        });
        ///////////////////////////
        $("#writing").fadeIn(500,function(){
            writing.userguide();
        });
        $("#first_question").bind("click",function(){
            $(this).remove();
        });
        $("#writing_exit").bind("click",function(){
            writing.exit();
        });
        //////// keeplife after 2minute /////////
         setInterval(function(){
            $.get( "/users/keeplife", function( data ) {
            });
         },120000);
    },
    userguide: function() {
        var img = $("#writing_userguide").find("img");
        // xem co bao nhieu cai anh trong day
        var size = img.size();
        // cau hoi binh thuong
        if (size <= 0) {
            $("#continue_button").remove();
            this.start();
        }
        else{
            var first = img.first().show();
            $("#continue_button").show(0,function(){
                first.show();
                $(this).bind("click",function(){
                    first.remove();
                    writing.userguide();
                })
            });
        }
    },
    start: function(){
        $("#writing_userguide").hide();
        $("#question_1").show();
        $("#change_question").show();
        $("#writing_countdown").show();
        $("#show_count_word").show();
        function liftOff() {
            $('#writing_count_down').countdown('remove');
            writing.send_answer();
        }

        var fiveSeconds = new Date().getTime() + this.param.answer_time*60*1000;
        $('#writing_count_down').countdown(fiveSeconds, {elapse: true})
        .on('update.countdown', function(event) {
            var $this = $('#writing_count_down');
            if (event.elapsed) {
                $this.html('Hết thời gian làm bài');
                return liftOff();
            } else {
                $this.html(event.strftime('%H : %M : %S'));
            }
        });

        // drag the countdown
        // $( "#writing_count_down" ).draggable({ cursor: "move"});
    },
    count_word: function(id){
        id = (!id) ? 1 : id;
        var value = $("#textarea_"+id).val();
        console.log(value);
        var strText = $.trim(value);
        var number= strText.split(/\s+/).length;
        $("#show_count_word").find(".txt").text(number);
    },
    showquestion: function(page){
        $(".question_writing").hide();
        $("#question_"+page).show();
        $("#change_question").attr("current-page",page);
        //////////// count word /////////////
        this.count_word(page);
        $("#textarea_"+page).bind("keypress",function(){
            writing.count_word(page);
        });
    },
    // tai file len mang
    send_answer: function(){
        //update instances for ckeditor
        //for ( instance in CKEDITOR.instances )
        //    CKEDITOR.instances[instance].updateElement();

        $.post('/test/writing_sendanswer', $('#writing_answer_fr').serialize(),function(data){
            if (data.success == 1){
                if (data.next_test == 0) {
                    alert("<p>Hệ thống đã lưu câu trả lời");
                }
                else{
                    //Chuyển câu hỏi tiếp theo

                }
            }else{
                alert('Không thành công');
            }
        },'JSON');
    },
    point_form: function(id){
        id = parseInt(id);
        if (id <= 0) {console.log("ID < 0"); return false };
        var default_point = $("#point_view_"+id).text();
        var default_comment = $("#point_comment_"+id).text();
        $("#point_teacher_form").html('<table width="100%"><tr><td width="60px">Điểm:</td><td><input id="teacher_point" type="text" name="point" value="'+default_point+'"></td></tr><tr><td>Nhận xét:</td><td><textarea style="width: 100%; height: 80px" id="teacher_comment">'+default_comment+'</textarea></td></tr></table>');
        $("#point_teacher_form" ).dialog({
            modal: true,
            buttons: {
                'Chấm điểm': function() {
                    var point = parseInt($("#teacher_point").val());
                    var teacher_comment = $("#teacher_comment").val();
                    $.post($base_url + 'admin/writing/point_detail/', {point: point, answer_id: id, comment: teacher_comment }, function(data) {
                        alert(data.data);
                        if (data.result == 'success'){
                            $("#point_view_"+id).text(point);
                            $("#point_comment_"+id).text(teacher_comment);
                            $("#point_teacher_form" ).dialog("close");
                        }
                    },"JSON");
                },
                'Đóng': function(){
                    $("#point_teacher_form" ).dialog("close");
                }
            },
         });
    },
    exit: function(){
         $("#dialog-message").html("<p>If you log out, the system will NOT save your unfinished answers on file. Are you sure to log out? </p>");
         if (writing.param.type == 0) {
             $( "#dialog-message" ).dialog({
                modal: true,
                buttons: {
                    Ok: function() {
                        redirect(writing.param.link_exit);
                    },
                    Cancel: function(){
                        $("#dialog-message" ).dialog("close");
                    }
                },
                open: function( event, ui ) {
                    setTimeout(function() {
                        $("#dialog-message" ).dialog("close");
                    }, 15000);
                }
             });
         }
         else {
            var r = confirm('Bạn có chắc chắn muốn thoát?');
            if(r){
                return window.history.go(-1);
            }
        }
    }
};
var writing_preview = {
    run: function() {
        $("#writing").fadeIn("500",function(){
            $(".prev_button").bind("click",function(){
                writing_preview.prev_question();
            });
            $(".next_button").bind("click",function(){
                writing_preview.next_question();
            });
        })
    },
    next_question: function() {
        var max_page = $("#change_question").attr("max-page");
        var current_page = parseInt($("#change_question").attr("current-page"));
        var page = (current_page == max_page) ? 1 : current_page + 1;
        this.showquestion(current_page,page);
    },
    prev_question: function(){
        var max_page = $("#change_question").attr("max-page")
        var current_page = parseInt($("#change_question").attr("current-page"));
        var page = (current_page == 1) ? max_page : current_page - 1;
        this.showquestion(current_page,page);
    },
    showquestion: function(curpage, page){
        // chuyen tab
        $("#question_"+curpage).hide();
        $("#question_"+page).show();
        $("#change_question").attr("current-page",page);
    }
}
function change_answer(id){
    // add style selected
    $(".answer_number").removeClass("selected");
    $("#answer_number_"+id).addClass("selected");
    //show content
    $(".speaking_tab").hide();
    $("#speaking_tab_"+id).fadeIn(300);
}
function show_teacher_comment(id){
    var value = $("#point_comment_"+id).text();
    $("#comment_teacher_view").html("<p>"+value+"</p>");
    $("#comment_teacher_view" ).dialog({
        modal: true
    });
}