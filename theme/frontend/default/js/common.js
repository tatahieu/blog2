/** HAVE TO REQUIRE **/
/* function message(text){
    $('#modal_message .modal-body').html(text);
    $('#modal_message').modal()
}*/
function createCookie(name,value,time) {
	if (time) {
		var date = new Date();
		date.setTime(date.getTime()+(time*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function delCookie(name) {
     createCookie(name,"",-1);
}
function resizeImage(img,nsize)
{
    var nWidth=img.width;
    var    nHeight=img.height;
    nScale = nWidth/nHeight;
    //Check:
    if (nScale > 1) {
        nWidth = nsize;
        nHeight = (nWidth*img.height)/img.width;
        //fix vertical-align:
        nMargin = (nWidth - nHeight)/2;
        //Set style to image:
        img.width = nWidth;
        img.height = nHeight;
        img.style.margin = nMargin + 'px 0px';
        }
    else {
        //Return size:
        nHeight = nsize;
        nWidth = (nHeight*img.width)/img.height;
        //fix align:
        nMargin = (nHeight - nWidth)/2;
        //Set style to image:
        img.width = nWidth;
        img.height = nHeight;
        img.style.margin = '0px ' + nMargin + 'px';
    }
}
function redirect($url){
    window.location.href = $url;
}

function message(text,option){
    $('#modal_message .modal-body').html(text);
    $("#modal_message").find(".modal-footer").empty().html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
    
    if (typeof(option) != 'undefined') {
        if (typeof(option.button) != 'undefined') {

            $.each(option.button, function( index, value ) {
                $("#modal_message").find(".modal-footer").prepend(value);
            });
        }
        if (typeof(option.title) != 'undefined') {
            $("#modal_message").find(".modal-title").text(option.title);
        }
    }
    $('#modal_message').modal();
}
function social(){
    // facebook: docs https://developers.facebook.com/docs/reference/plugins/like/
    //<div class="fb-like" data-href="http://hanoicdc.com" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true"></div> //
    // g+ : Docs https://developers.google.com/+/plugins/+1button/?hl=vi
    //<g:plusone></g:plusone>
    (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();
    // tweet: docs https://dev.twitter.com/docs/tweet-button
    //<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
}
//////////// FACEBOOK //////////
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
/** UTIL **/
$(document).ready(function() {
    // UP TO TOP
    $('body').append('<div class="up_to_top"></div>');
    $(window).scroll(function() {
        if($(window).scrollTop() != 0) {
            $('.up_to_top').fadeIn();
        } else {
            $('.up_to_top').fadeOut();
        }
    });
    $('.up_to_top').click(function() {
        $('html, body').animate({scrollTop:0},500);
    });
    // show template
    $("#support_online").bind("click",function() {
        //$(this).find(".fb-comments").empty();
        $("#support_facebook").fadeIn();
    })
    $("#support_facebook").find(".support_close").bind("click",function() {
        $("#support_facebook").hide();
    });

    //////////// USER MENU //////////////
    $("#users_menu_shortcut").bind('click', function(e){
        
        $(this).toggleClass('active');
        e.stopPropagation();
        //return false;
    });
    // all dropdowns
    $(document).bind("click",function(e){
        $("#users_menu_shortcut").removeClass('active');
        ////////// TU VAN ////////
    });
    console.log("sth");
    $('#tuvan_form').submit(function(e){
        var self = $(this);
        var self_submit = $("input[type=submit]",self);
        self_submit.css('background-color: red');

        self.find(".has-error").removeClass("has-error");
        self.find(".error").remove();
        e.preventDefault(); 

        $('#tuvan_submit').attr('disabled', true);
        $.ajax({
            type: 'post',
            dataType : 'json',
            url: $(this).attr("action"),
            data: $(this).serializeArray(),
            success: function (respon) {
                if(respon.status == "success"){
                    // redirect('/contact/success');
                    $('#tuvan_form')[0].reset();
                    alert('Đăng ký thành công');
                } else {
                    $.each( respon.message, function( key, value ) {
                        var dom = self.find("input[name=\"" + key + "\"]").parent().addClass("has-error").append('<p class="error">' + value + '</p>');
                    });
                }
                $('#tuvan_submit').prop('disabled', false);
            },
            error: function(respon,code) {
                
            }
        });   
    });

    $('#tuvan_form_detail').submit(function(e){
        var self = $(this);
        self.find(".has-error").removeClass("has-error");
        self.find(".error").remove();
        e.preventDefault();
        $.ajax({
            type: 'post',
            dataType : 'json',
            url: $(this).attr("action"),
            data: $(this).serializeArray(),
            success: function (respon) {
                if(respon.status == "success"){
                    // redirect('/contact/success');
                    alert('Đăng ký thành công');
                } else {
                    $.each( respon.message, function( key, value ) {
                        var dom = self.find("input[name=\"" + key + "\"]").parent().addClass("has-error").append('<p class="error">' + value + '</p>');
                    });
                }
            },
            error: function(respon,code) {
                
            }
        });   
    });


    /* Add Submit Event for Form */
    // tuvan_form_new test_contact_form document_earn_form event_offline_form

    var arr_id_form_need_to_add_event_submit = ['tuvan_form_new','test_contact_form','document_earn_form','event_offline_form'];
    var pathname = window.location.pathname;
    var url      = window.location.href;

    $("[id]").each(function(){
        var thiss_id = $(this).attr("id");
        if (arr_id_form_need_to_add_event_submit.includes(thiss_id)){
            $(this).submit(function(e){
                var self = $(this);
                $('#url_form_target').val(url);
                self.find(".has-error").removeClass("has-error");
                self.find(".error").remove();
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    dataType : 'json',
                    url: $(this).attr("action"),
                    data: $(this).serializeArray(),
                    success: function (respon) {
                        if(respon.status == "success"){
                            // redirect('/contact/success?type='+mono_id_form+'&url='+pathname);
                            alert('Đăng ký thành công !');
                            $('.modal').modal('hide');
                        } else {
                            $.each( respon.message, function( key, value ) {
                                var dom = self.find("input[name=\"" + key + "\"]").parent().addClass("has-error").append('<p class="error">' + value + '</p>');
                            });
                        }
                    },
                    error: function(respon,code) {

                    }
                });
            });
        }
    });


    for (var zign = 0; zign < arr_id_form_need_to_add_event_submit.length; zign++) {
        var mono_id_form = arr_id_form_need_to_add_event_submit[zign];

        // $('#'+mono_id_form).submit(function(e){
        //     var self = $(this);
        //     $('#url_form_target').val(url);
        //     self.find(".has-error").removeClass("has-error");
        //     self.find(".error").remove();
        //     e.preventDefault();
        //     $.ajax({
        //         type: 'post',
        //         dataType : 'json',
        //         url: $(this).attr("action"),
        //         data: $(this).serializeArray(),
        //         success: function (respon) {
        //             if(respon.status == "success"){
        //                 // redirect('/contact/success?type='+mono_id_form+'&url='+pathname);
        //                 alert('Đăng ký thành công !');
        //                 $('.modal').modal('hide');
        //             } else {
        //                 $.each( respon.message, function( key, value ) {
        //                     var dom = self.find("input[name=\"" + key + "\"]").parent().addClass("has-error").append('<p class="error">' + value + '</p>');
        //                 });
        //             }
        //         },
        //         error: function(respon,code) {
        //         }
        //     });
        // });
    }
    //document.body.innerHTML = document.body.innerHTML.replace(/http:\/\/anhngumshoa.test\/uploads/g, 'https://www.anhngumshoa.com/uploads');
    //document.body.innerHTML = document.body.innerHTML.replace(/http:\/\/anhngumshoa.test\/theme/g, 'https://www.anhngumshoa.com/theme');
});


//////////// function ////////
