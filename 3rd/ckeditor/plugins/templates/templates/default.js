﻿/*
 Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
*/
// CKEDITOR.addTemplates("default", {
//     imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/"),
//     templates: [
//         {
//         title: "Lộ trình học",
//         image: "template1.gif",
//         description: "sdfgsdfgsdfgsdfg",
//         html: '<div class="col-md-4 w_27 pdr_none"><ul class="lt_toeic"><li class="top">Lộ trình 1</li><li><div class="box_active"><h4>PRE TOEIC<br>300-350+</h4><p class="txt-center">Listening & Reading</p><p class="imgbg">Nắm chắc kiến thức cơ bản.</p><p class="more-link"><a href="#">Xem thêm...</a></p></div></li><li class="bot">Lộ trình 2<span>TOEIC 4 Kỹ năng</span></li></ul></div><div class="col-md-8 w_73 pdl_none"><div class="row"><div class="col-md-4"><div class="box_next_ti"><h5>TOEIC A 500 - 550+</h5><div><p class="imgbg">Nắm chắc ngữ pháp từ vựng.</p><p class="imgbg">Bước đầu của giao tiếp</p><p class="more-link"><a href="#">Xem thêm...</a></p></div></div></div><div class="col-md-4"><div class="box_next_ti"><h5>TOEIC B 650 - 750+</h5><div><p class="imgbg">Nghe đọc thành thạo.</p><p class="imgbg">Tự tin giao tiếp công việc ở mức cơ bản.</p><p class="more-link"><a href="#">Xem thêm...</a></p></div></div></div><div class="col-md-4"><div class="box_next_ti"><h5>KHÓA LUYỆN ĐỀ</h5><div><p class="imgbg">Tăng <strong>100 - 150</strong> điểm sau 1 khóa học</p><p class="more-link"><a href="#">Xem thêm...</a></p></div></div></div></div><div class="row"><div class="col-md-4"><div class="box_next_ti"><h5>TOEIC A PUS</h5><div> <span>400 - 450+</span><p class="txt-center">Listening & Reading</p><p class="imgbg">Tăng <strong>70 - 110/200</strong> Speaking & Writing</p><p class="more-link"><a href="#">Xem thêm...</a></p></div></div></div><div class="col-md-4"><div class="box_next_ti"><h5>TOEIC B PUS</h5><div> <span>550 - 600+</span><p class="txt-center">Listening & Reading</p><p class="imgbg">Tăng <strong>110 - 150/200</strong> Speaking & Writing</p><p class="more-link"><a href="#">Xem thêm...</a></p></div></div></div><div class="col-md-4"><div class="box_next_ti"><h5>TOEIC C PUS</h5><div> <span>650 - 750+</span><p class="txt-center">Listening & Reading</p><p class="imgbg">Tăng <strong>110 - 180/200</strong> Speaking & Writing</p><p class="more-link"><a href="#">Xem thêm...</a></p></div></div></div></div></div>',
//     },
//     ]
// });


