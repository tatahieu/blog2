<?php
// output_buffering = Off; php.ini
define('api','images');
$fd = dirname(__FILE__);
require("$fd/lib/config.php");
$url = strip_tags($_GET['url']);

if(!preg_match('/(.+)\/thumb_(\d+)x(\d+)_(.+)/is', $url, $match)){
    return false;
}
$width = (int)$match[2];
$height = (int)$match[3];
$cw = ($width == 0) ? $height : $width;
$ch = ($height == 0) ? $width : $height;
if (!in_array(array($cw,$ch),$size)){
    exit("Size resize not found");
}
$path = strip_tags($match[1].'/'.$match[4]);
$path = str_replace(array('\'','"'),'',$path);
$type = strip_tags($_GET['type']);


$file = media_path.$path;
if (!file_exists($file)){
    exit('File org not exits');
}
////////////////////////////////////
// xac dinh phan mo rong cua file //
////////////////////////////////////
$mimes = array('bmp'=>'image/bmp', 'gif'=>'image/gif', 'jpg'=>'image/jpeg','jpeg'=>'image/jpeg', 'png'=>'image/png');
$ext = strtolower(substr(strrchr($file, "."), 1));
if (!array_key_exists($ext,$mimes)){
    exit();
}
$mime = $mimes[$ext];
$filename =  substr(strrchr($file, "/"), 1);
if (strpos($filename,'thumb_') !== FALSE){
    exit('File org not exits');
}
else{
    switch ($type){
    case 'resize':
        if ($width <= 0 AND $height <= 0){
            $file_new_name = $filename;
        }
        else{
            $file_new_name = 'thumb_'.$width.'x'.$height.'_'.$filename;
        }

        break;
        default:
        exit();
    }
}

$file_new = substr($file,0,strrpos  ($file,'/') + 1).$file_new_name;
/////// CACHE HTTP

$key = md5($file_new);
if (array_key_exists('HTTP_IF_NONE_MATCH',$_SERVER) AND $_SERVER['HTTP_IF_NONE_MATCH'] == $key) {
    header('HTTP/1.1 304 Not Modified');
}
else{
    // neu ko ton tai cache
    if (!file_exists($file_new)){

        require_once ('lib/images.php');
        $config = array(
                'create_thumb' => TRUE,
                'thumb_marker' => '',
                'new_image' => $file_new_name,
                'quality' => 100,
                'source_image' => $file,
                'maintain_ratio' => true
        );
        if ($width == 0){
            $config['master_dim'] = 'height';
            $config['height'] = $height;
            $config['width'] = $height;
        }
        elseif($height == 0){
            $config['master_dim'] = 'width';
            $config['width'] = $width;
            $config['height'] = $width;
        }
        else{
            $config['width'] = $width;
            $config['height'] = $height;
            $config['master_dim'] = 'auto';
        }
        //print_r($config);
        //exit();
        $lib = new Images($config);
        $lib->resize();
        if (!empty($lib->error_msg)){
            exit('have some error');
        }
    }
    header("Cache-Control: max-age=72000, must-revalidate");
    header('Etag: '.$key);
    header('Content-Type: '.$mime);
    header("Content-Disposition: filename=hao-anh-".time().";");
    readfile($file_new);
}