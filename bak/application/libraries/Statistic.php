<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class statistic {
	var $timeout = 600;
	function user_online(){
        $CI = get_instance();

        // pass for
        $status = (int)$CI->session->userdata("is_count_online");
        $uid = (int)$CI->session->userdata("userid");
        $timestamp = time();
        // khi thanh vien dang nhap
        if ($status == 1){
            // neu thanh vien
            if ($uid > 0){
                $CI->db->where("session_id",$CI->session->userdata("session_id"));
                $CI->db->update("user_online",array('is_user'=>$uid));
                $CI->session->set_userdata("is_count_online",2);
            }
        }
        elseif ($status == 0){
            $CI->db->delete("user_online","timestamp < ($timestamp - $this->timeout)");
            $data = array(
                "is_user" => $uid,
                "ip" => $CI->session->userdata("ip_address"),
                "session_id" => $CI->session->userdata("session_id"),
                "timestamp" => time(),
                );
                if ($uid > 0){
                    $CI->session->set_userdata("is_count_online",2);
                }
                else{
                    $CI->session->set_userdata("is_count_online",1);
                }
            $CI->db->insert('user_online',$data);
        }
        return $CI->db->count_all_results("user_online");
	}
    function hit_counter(){
        $CI = &get_instance();
        $CI->load->helper("file");
        $data = read_file('statistic.txt');
        $data = @unserialize($data);
        if ($CI->session->userdata("is_hit_counter") !== 1 OR $data === FALSE){
            if (!is_array($data) OR !array_key_exists('hit_counter',$data)){
                $data['hit_counter'] = 1000;
            }
            else{
                $data['hit_counter'] = $data['hit_counter'] + 1;
            }
            write_file("statistic.txt",serialize($data));
            $CI->session->set_userdata("is_hit_counter",1);
        }
        return $data['hit_counter'];
    }
}

?>