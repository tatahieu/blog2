<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bootstrap

{

    var $obj;

    var $lang = array(1 => 'vietnam', 2=> 'english');

    function __construct()

    {

		$this->obj =& get_instance();

        $this->setdefine();

	   	$this->setconfig();

    }

    function setconfig(){

        $cp = $this->obj->router->directory;

		$cp = str_replace('/','',$cp);

        if ($this->obj->input->get("lang")){

            $lang = $this->obj->input->get("lang");

            if (array_key_exists($lang,$this->lang)){

                $this->obj->session->set_userdata("lang",$lang);

            }

            else{

                $this->obj->session->set_userdata("lang",1);

            }

        }

        if (!$this->obj->session->userdata("lang")){

            $this->obj->session->set_userdata("lang",1);

        }

        $this->obj->load->helper('cookie');

        $this->obj->load->library("permission");

        $this->obj->config->set_item('panel',$cp);

        switch ($cp){

			case 'backend':

			$this->backend();

			break;

			default:

            $this->frontend();

		}

        /* if (DEVICE_ENV == 1) {

            $this->obj->config->set_item('layout','m_layout');

        }

        else { */

            $this->obj->config->set_item('layout','layout');

        /* } */

		

        $theme = $cp.'/'.$this->obj->config->item("theme");

		$this->obj->config->set_item('img',STATIC_URL.'theme/'.$theme.'/images/');

        $this->obj->config->set_item('js',STATIC_URL.'theme/'.$theme.'/js/');

        $this->obj->config->set_item('css',STATIC_URL.'theme/'.$theme.'/css/');

        $this->obj->config->set_item('audio',STATIC_URL.'theme/'.$theme.'/audio/');

        $this->obj->config->set_item('theme',STATIC_URL.'theme/'.$theme.'/');

        $this->obj->config->set_item('themeorg',BASE_URL.'theme/'.$theme.'/');

    }

    function frontend(){

        // detect device ///
        if (!$this->obj->session->userdata("device_env")) {

            $this->obj->load->library('user_agent');
            if ($this->obj->agent->is_mobile()) {

                $this->obj->session->set_userdata('device_env',1);
                $device_env = 1;

            }

            else {
                $device_env = 4;
                $this->obj->session->set_userdata('device_env',4);

            }

        }

        //define('DEVICE_ENV', 1);

        define('DEVICE_ENV', $this->obj->session->userdata("device_env"));

        // check user miss profile

        if ($this->obj->session->userdata('userid')) {

            $arrMissing = array();

            if (!$this->obj->session->userdata('fullname')) {

                $arrMissing['fullname'] = 1;

            }

            if (!$this->obj->session->userdata('user_birthday')) {

                $arrMissing['birthday'] = 1;

            }

            if (!$this->obj->session->userdata('user_live')) {

                $arrMissing['live'] = 1;

            }

            if (!$this->obj->session->userdata('user_phone')) {

                $arrMissing['phone'] = 1;

            }

            if ($arrMissing) {

                $this->obj->config->set_item('missprofile',$arrMissing);

            }

        }

        // end detect ///

        $check = $this->obj->permission->check_permission_frontend();

        $this->obj->config->set_item("language",$this->lang[$this->obj->session->userdata("lang")]);

        $this->obj->load->driver('cache',array('adapter' => 'file', 'backup'=>'file'));

        $this->setting();

        $this->obj->config->set_item("menu_selected",array());

    	$this->obj->load->setData('title',$this->obj->config->item("site_title"));

        $this->obj->load->setData("metakey",$this->obj->config->item("metakey"));

        $this->obj->load->setData("metades",$this->obj->config->item("metadesc"));

        $this->obj->lang->load('frontend/common');

    }

    /** EDIT & DELETE can view

     *

     */

    function backend(){

        $this->obj->config->set_item('index_page',$this->obj->uri->segment(1));

        $check = $this->obj->permission->check_permission_backend();

        if ($check == false){

            redirect("users/denied");

        }

        $this->obj->load->helper('admin_temp');

    	$this->obj->load->setData('title','MshoaToeic');

        $this->obj->lang->load('backend/common');

    }

    /*function setTheme($theme){

    	$this->obj->config->set_item('theme','theme/'.$this->cp.'/'.$theme.'/');

    }

    function setLayout($layout)

    {

      	$this->obj->config->set_item('layout',$layout);

    }*/

	function setting(){

		$CI = &get_instance();



        $CI->load->model('common_model','common');

        $row = $CI->common->set_setting();


        if (!empty($row)){

            foreach ($row as $key => $value){

			$CI->config->set_item($key,$value);

            }

        }

        //$CI->load->library("statistic");

        //$CI->config->set_item("user_online",$CI->statistic->user_online());

        //$CI->config->set_item("hit_counter",$CI->statistic->hit_counter());

    }

    function setdefine(){

        define("BASE_URL",base_url());

        define("UPLOAD_URL",BASE_URL.'/uploads/');

        define("UPLOAD_CDN1",BASE_URL.'/uploads/');

        //if (ENVIRONMENT == 'development') {

            define("STATIC_URL",BASE_URL);

        //}

        //else {

            //define("STATIC_URL",'http://mshoa.hanoicdc.net/');

        //}

    }

}