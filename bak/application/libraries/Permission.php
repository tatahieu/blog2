<?php class Permission {
	var $timeout = 600;
    var $permission = array();
    var $class_data = array();
    function __construct(){
        $CI = &get_instance();
        $this->permission = $CI->session->userdata('permission');
    }
    function checkrememberpassword(){
        $CI = &get_instance();
        $isremember = get_cookie(USER_REMEMBER_PASSWORD);
        if ($isremember) {
            $isremember = base64_decode($isremember);

            parse_str($isremember,$arrInfo);
            if ($arrInfo['username'] && $arrInfo['password']){
                $CI->load->model('users_model','users');
                $data = $CI->users->check_login($arrInfo);
                if ($data) {
                    $this->set_user_session($data);
                }
            }
            //delete_cookie(USER_REMEMBER_PASSWORD);
        }

    }
    function check_permission_frontend(){
        $CI = &get_instance();
        if (!$CI->session->userdata("userid")){
            $this->checkrememberpassword();
        }
    }
    function set_user_session($row){
        if (!$row){
            return false;
        }
        $CI = &get_instance();
        $CI->session->set_userdata('username',$row['username']);
        $CI->session->set_userdata('fullname',$row['fullname']);
        $CI->session->set_userdata('user_phone',$row['phone']);
        $CI->session->set_userdata('user_birthday',$row['birthday']);
        $CI->session->set_userdata('user_live',$row['where_live']);
		$CI->session->set_userdata('userid',$row['user_id']);
		$CI->session->set_userdata('roles',$row['name']);
        $CI->session->set_userdata('user_email',$row['email']);
		$CI->session->set_userdata('permission',unserialize($row['permission']));
    }
	function backend(){
	   $CI = &get_instance();
       $per = array(
            'news' => array(
                    "name" => $CI->lang->line("common_mod_news"),
                    "func" => array('add','edit','index','delete','cate_add','cate_edit','cate_index','cate_delete'),

                    ),
            'product' => array(
                    "name" => $CI->lang->line("common_mod_product"),
                    "func" => array('add','edit','index','delete','cate_add','cate_edit','cate_index','cate_delete'),
                    "ignore" => array('upload_images' => array('add','edit'),'delete_images' => array('add','edit'))
                    ),
            'order' => array(
                    "name" => $CI->lang->line("common_mod_order"),
                    "func" => array('edit','index','delete')
                    ),
            'advertise' => array(
                    "name" => $CI->lang->line("common_mod_advertise"),
                    "func" => array('add','edit','index','delete','cate_add','cate_edit','cate_index','cate_delete')
                    ),
            'userreview' => array(
                    "name" => 'User đánh giá',
                    "func" => array('edit','index','delete')
                    ),
            'users' => array(
                    "name" => $CI->lang->line("common_mod_users"),
                    "func" => array('add','edit','index','delete','role_add','role_edit','role_index','role_delete')
                    ),
            'menu' => array(
                    "name" => $CI->lang->line("common_mod_menu"),
                    "func" => array('add','edit','index','delete',''),
                    "ignore" => array('autocomplete' => array('add','edit'))
                    ),
            'block' => array(
                    "name" => $CI->lang->line("common_mod_block"),
                    "func" => array('add','edit','index','delete'),
                    "ignore" => array('edit_form' => array('edit'),"delete_item" => array('delete'))
                    ),
            'support' => array(
                    "name" => $CI->lang->line("common_mod_support"),
                    "func" => array('add','edit','index','delete')
                    ),
            'contact' => array(
                    "name" => $CI->lang->line("common_mod_contact"),
                    "func" => array('edit','index','delete')
                    ),
            'setting' => array(
                    "name" => $CI->lang->line("common_mod_setting"),
                    "func" => array('index')
                    ),
            'cache' => array(
                    "name" => $CI->lang->line("common_mod_cache"),
                    "func" => array('index')
                    ),
            'filemanager' => array(
                    "name" => $CI->lang->line("common_mod_media"),
                    "func" => array('index','directory','files','create','delete','recursiveDelete','move','copy','recursiveCopy','folders','recursiveFolders','rename','upload')
                    ),
        );
        return $per;
	}
    function check_permission_test($id = 0){
        $CI = &get_instance();
        if (!$CI->session->userdata('userid')){
            redirect('users/login?refer='.current_url());
        }

        $session = (is_array($CI->session->userdata("test_password"))) ?  $CI->session->userdata("test_password") : array();

        if ($CI->input->post('test_password')){
            $password = get_password();
            if ($password[$id]['password'] == $CI->input->post("test_password")){

                if (!in_array($id,$session)){
                    array_push($session,$id);

                    $CI->session->set_userdata('test_password',$session);
                }
            }
        }
        if (!$session || !in_array($id,$session)){
	  	    $CI->load->helper('form');
    		$data['password'] = form_password('test_password',set_value('test_password'),'maxlength = "100" class="login_pass"');
    		$data['submit'] = form_submit('class_submit','Đăng nhập');
            return $CI->load->layout("common/test_login",$data);
        }
        return true;
    }
    function check_permission_class(){
        $CI = &get_instance();
        if (!$CI->session->userdata('userid')){
            redirect('users/login');
        }
        if (!$CI->session->userdata('class_id')){
            $CI->load->library('form_validation');
            if ($CI->input->post("class_submit")){
                $valid = array(
        			array(
                         'field'   => 'class_username',
                         'label'   => 'Tên đăng nhập',
                         'rules'   => 'required|min_length[3]|max_length[40]'
                      ),
                   array(
                         'field'   => 'class_password',
                         'label'   => 'Mật khẩu',
                         'rules'   => 'required|min_length[3]|max_length[40]'
                      ),
          		);
          		$CI->form_validation->set_rules($valid);
                if ($CI->form_validation->run() == true)
                {

                    $CI->load->model('users_model','user');
            		$row = $CI->user->check_class_login($CI->input->post('class_username'),$CI->input->post("class_password"));
            		if (!$row){
            			$data['errorcallback'] = 'Tên đăng nhập hoặc mật khẩu không đúng';
            		}
                    else{
                        $CI->session->set_userdata("class_name",$row['name']);
                        $CI->session->set_userdata("class_id",$row['id']);
                        redirect(current_url());
                    }

                }
            }
    		$CI->load->helper('form');
    		$data['username'] = form_input('class_username',set_value('class_username'),'maxlength = "100" class="login_user"');
    		$data['password'] = form_password('class_password',set_value('class_password'),'maxlength = "100" class="login_pass"');
    		$data['submit'] = form_submit('class_submit','Đăng nhập');
            return $CI->load->layout("common/class_login",$data);
        }
        return true;
    }
    /*
    public function _class_login_check(){
        $CI = &get_instance();

        $CI->load->model('users_model','user');
		$row = $CI->user->check_class_login($CI->input->post('class_username'),$CI->input->post("class_password"));
		if (empty($row)){
			$CI->form_validation->set_message('_class_login_check','Tên đăng nhập hoặc mật khẩu không đúng');
			return false;
		}
        else{
            $CI->class_data = $row;
            return true;
        }
    }*/
    function check_permission_backend($class = '',$method = '',$check_ignore = 1){
        $CI = &get_instance();
        $class = ($class == '') ? $CI->router->class : $class;
        $method = ($method == '') ? $CI->router->method : $method;
        //echo $class.'-'.$method; die;
        if ($class == 'users' AND $method == 'login'){
            if ($CI->session->userdata('userid') > 0) {
                redirect('users/profile');
            }
            else{
                return true;
            }
        }

        $permission = $this->permission;
        if (!$CI->session->userdata('userid') || empty($permission)){
            $CI->session->unset_userdata('userid');
            redirect('users/login');
        }
        else{
            // profile update or logout true
            if ($class == 'users' AND ($method == 'profile' OR $method == 'logout' OR $method == 'denied')){
                return true;
            }

            // administrator true
            if ($CI->session->userdata('roles') == 'administrator'){
                return true;
            }

            $fullper = $this->backend();
            // check ignore
            if ($check_ignore == 1){
                // co ignore
                if ($fullper[$class]['ignore'][$method]){
                    return $this->check_permission_backend($class,$fullper[$class]['ignore'][$method],0);
                }
            }

            if (!is_array($method)){
                $method = array($method);
            }
            foreach ($method as $m){
                if (array_key_exists($class,$permission) && in_array($m,$permission[$class]) && in_array($m,$permission[$class])){
                    return true;
                }
            }
            return false;
        }
    }
}
?>