<?php
$lang['common_home'] 		= "Trang chủ";
$lang['common_webview'] 	= "Xem website";
$lang['common_profile']		= "Thông tin";
$lang['common_logout']		= "Thoát";
$lang['common_save']		= "Lưu thông tin";
$lang['common_add']			= "Thêm";
$lang['common_edit']		= "Sửa";
$lang['common_index']		= "Danh sách";
$lang['common_delete']		= "Xoá";
$lang['common_close']		= "Đóng";
$lang['common_no']			= "Stt";
$lang['common_delete_button'] = "Xoá dữ liệu";
$lang['common_filter_submit'] = "Lọc dữ liệu";
$lang['common_add_button'] 	= "Thêm dữ liệu";
$lang['common_delete_confirm'] = "Bạn có chắc muốn xoá ?";
$lang['common_add_success'] = "Dữ liệu thêm thành công";

$lang['common_mod_contact']		= "Liên hệ";
$lang['common_mod_news']		= "Tin tức";
$lang['common_mod_news_cate']	= "Nhóm tin";
$lang['common_mod_comment']	    = "Bình luận";
$lang['common_mod_users']		= "Thành viên";
$lang['common_mod_users_role']	= "Quyền hạn";
$lang['common_mod_advertise']   = "Gallery";
$lang['common_mod_adv_cate']    = "Nhóm gallery";
$lang['common_mod_menu']		= "Menu";
$lang['common_mod_product']		= "Sản phẩm";
$lang['common_mod_media']		= "Media";
$lang['common_mod_product_cate']= "Nhóm sản phẩm";
$lang['common_mod_ads_cate']    = "Nhóm banner";
$lang['common_mod_block']       = "Block";
$lang['common_mod_system']		= "Hệ thống";
$lang['common_mod_setting']		= "Cấu hình web";
$lang['common_mod_cache']       = "Bộ nhớ đệm";
$lang['common_mod_order']       = "Đơn hàng";
$lang['common_mod_support']		= "Hỗ trợ trực tuyến";
$lang['common_mod_login']		= "Đăng nhập";
$lang['common_mod_static']		= "Tĩnh";
$lang['common_mod_link']		= "Link liên kết";


$lang['common_images']		    = "Ảnh";
$lang['common_files']		    = "File";
$lang['common_flash']		    = "Flash";

$lang['common_mod_select']		= "Chọn module";
$lang['common_mod_position']	= "Chọn vị trí";
