<?php
$lang['ques_cate_title']			= "Quản lý nhóm câu hỏi";
$lang['ques_cate_select_parent']	= "Nhóm chính";
$lang['ques_cate_name'] 			= "Tên nhóm";
$lang['ques_cate_ordering'] 		= "Thứ tự";
$lang['ques_id'] 	              	= "Mã số";
$lang['ques_cate_parent']	       	= "Cấp nhóm";
$lang['ques_cate_type']	        	= "Loại câu hỏi";
$lang['ques_cate_description']	    = "Mô tả";
$lang['ques_cate_delete_parent']	= "Phái xoá nhóm con trước";

$lang['ques_title']			= "Quản lý câu hỏi";
$lang['ques_name']			= "Câu hỏi";
$lang['ques_detail']    	= "Chi tiết";
$lang['ques_sound'] 		= "File âm thanh";
$lang['ques_images']		= "Ảnh mô tả";
$lang['ques_cate']			= "Nhóm tin";
$lang['ques_date_up']		= "Ngày đăng tin";
$lang['ques_read_more']		= "Xem tiếp";
$lang['ques_time']  		= "Thời gian";
$lang['ques_answer']  		= "Đáp án";
$lang['ques_publish']  		= "Xuất bản";

$lang['qtest_title']		= "Quản lý bài test";
$lang['qtest_name']			= "Tên bài test";
$lang['qtest_description']  = "Mô tả";
$lang['qtest_cate'] 		= "Nhóm";
$lang['qtest_publish'] 		= "Xuất bản";
$lang['qtest_id']           = "ID";
$lang['qtest_question'] 	= "Câu hỏi";