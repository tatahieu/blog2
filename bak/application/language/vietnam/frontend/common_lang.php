<?php
$lang['common_login']		         = "Đăng nhập";
$lang['common_register']		     = "Đăng ký";
$lang['common_help']		         = "Trợ giúp";
$lang['common_noresult']		     = "Không có dữ liệu";
$lang['common_back']		         = "Trở lại";
$lang['common_read_more']            = "Xem thêm";

$lang['common_paging_first_link']    = "Trang đầu";
$lang['common_paging_last_link']	 = "Trang cuối";
$lang['common_paging_next_link']	 = "tiếp";
$lang['common_paging_prev_link']	 = "trước";

$lang['common_page_pre']	 = "Trang trước";
$lang['common_page_next']	 = "Trang sau";
$lang['common_page_first']	 = "Trang đầu";
// COMMON MODULE //
$lang['common_support']	    	     = "Hỗ trợ trực tuyến";
$lang['common_hit_counter']	    	 = "Lượt người truy cập";
$lang['common_user_online']	    	 = "Đang online";
$lang['common_error_blank']	    	 = "Chưa nhập nội dung";

// COMMON MINI MODULE
$lang['common_back_to_top']	    	 = "Lên đầu trang";