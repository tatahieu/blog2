<?php
$lang['search_button']		         = "Tìm kiếm";
$lang['search_keyword']		         = "Từ khóa";
$lang['search_input_text']           = "Nhập từ khóa tìm kiếm";
$lang['search_keyword_too_short']    = "Từ tìm kiếm quá ngắn";
$lang['search_keyword_for']          = "Tìm kiếm từ ";
$lang['search_result']               = " kết quả";
