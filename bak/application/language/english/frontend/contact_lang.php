<?php
$lang['contact_title']		    = "Contact";
$lang['contact_fullname']		= "Yourname";
$lang['contact_email']      	= "Email";
$lang['contact_content']    	= "Content";
$lang['contact_phone']		    = "Mobile";
$lang['contact_send']		    = "Send contact";
$lang['contact_reset']		    = "Reset";
$lang['contact_captcha']		= "Security code";
$lang['contact_success_title']  = "Contact successfully";
$lang['contact_success']	    = "Thanks for your contact ! We will respond at the soonest time";
$lang['contact_email_subject']	= "Email liên hệ từ hệ thống";
$lang['contact_email_from']     = "Mail từ ";
$lang['contact_confirm']        = "Cảm ơn bạn đã gửi liên hệ! Chúng tôi sẽ trả lời bạn trong thời gian sớm nhất";