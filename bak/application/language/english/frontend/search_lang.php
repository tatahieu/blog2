<?php
$lang['search_button']		         = "Search";
$lang['search_keyword']		         = "Keyword";
$lang['search_input_text']           = "Nhập từ khóa tìm kiếm";
$lang['search_keyword_too_short']    = "Từ tìm kiếm quá ngắn";
$lang['search_keyword_for']          = "Tìm kiếm từ ";
$lang['search_result']               = " kết quả";
