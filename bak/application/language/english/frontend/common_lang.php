<?php
$lang['common_home']	    	     = "Homepage";
$lang['common_schedule']	    	 = "Schedule";

$lang['common_login']		         = "Login";
$lang['common_register']		     = "Register";
$lang['common_logout']		         = "Logout";

$lang['common_hello']		         = "Hello";

$lang['common_noresult']		     = "No data";
$lang['common_back']		         = "Back";
$lang['common_read_more']            = "Viewmore";

$lang['common_paging_first_link']    = "First";
$lang['common_paging_last_link']	 = "Last";
$lang['common_paging_next_link']	 = "Next";
$lang['common_paging_prev_link']	 = "Prev";
// COMMON MODULE //
$lang['common_support']	    	     = "Online support";
$lang['common_hit_counter']	    	 = "Hit counter";
$lang['common_user_online']	    	 = "Online";
$lang['common_error_blank']	    	 = "You must add content";

// COMMON MINI MODULE