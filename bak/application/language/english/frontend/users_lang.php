<?php
$lang['users_login_title']		     = "Login";
$lang['users_username']              = "Username";
$lang['users_password']		         = "Password";
$lang['users_captcha']               = "Security code";
$lang['users_login_submit']		     = "Login";
$lang['users_error_check']		     = "Wrong user or password";

$lang['users_profile']	 	         = "Change the profile";
$lang['users_register']	 	         = "Register";
$lang['users_fullname']           	 = "Fullname";
$lang['users_email']	 	         = "Email";
$lang['users_password_confirm']	 	 = "Confirm password";
$lang['users_sex']	 	           	 = "Gender";
$lang['users_male']	 	          	 = "Male";
$lang['users_female']	 	         = "Female";
$lang['users_yahoo'] 		         = "Yahoo";
$lang['users_skype'] 		         = "Skype";
$lang['users_register_submit']		 = "Register";
$lang['users_member_exist']		     = "Username or Email has been used";
$lang['users_register_subject']		 = "Email activates the user's account";

$lang['users_reg_success_title']	 = "Registered successfully";
$lang['users_reg_success_content']	 = "You have registered successfully. Please check your email to activate your account";

$lang['users_active_success']		 = "Activated %s account successfully !";
$lang['users_active_deleted']		 = "This link has expired!";
$lang['users_active_not_found']		 = "This link does not exist or has expired!";
$lang['users_active_title']		     = "Activate the user account!";
$lang['users_reactive_submit']		 = "Send the code again!";
$lang['users_reactive_email_check']	 = "This account has been activated or this email does not exist";
$lang['users_reactive_success_title']= "Send the activated code successfully";
$lang['users_reactive_success_content'] = "Please check your email to activate the account again";

$lang['users_profile']               = "User profile";
$lang['users_profile_submit']		 = "Change the profile";

$lang['users_forgot_email_exist']    = "This email does not exist";
$lang['users_forgot_success_content']= "We have sent you an email with a link to reset password";
$lang['users_forgot_success_title']  = "Reset password successfully";
$lang['users_forgot_email_subject']  = "Reset password";
$lang['users_new_pass_submit']       = "Set the password";
$lang['users_new_pass_success_title']= "Set the password successfully";
$lang['users_new_pass_success_content']= "Your new password has been updated by the system.";