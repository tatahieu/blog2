<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function temp_del_head()
{
    if (temp_check('delete') === false){
        return '';
    }
	return '<th width="30px"><input class="checkall" name="checkall" type="checkbox"> </th>';
}
function temp_del_check($id){
    if (temp_check('delete') === false){
        return '';
    }
	return '<td align="center"><input type="checkbox" title="delete"  name="cid[]" value="'.$id.'"/></td>';
}
function temp_edit_head(){
    if (temp_check('edit') === false){
        return '';
    }
	return '<th width="30px">Edit</th>';
}
function temp_edit_icon($url,$ext = '',$js = '',$refer = ''){
    if (temp_check('edit') === false){
        return '';
    }
    if ($js){
        return '<td align="center"><a '.$ext.' href="'.$url.'">'.img('edit.png').'</a></td>';
    }
    if ($refer){
        $refer = '?refer='.base64_encode(base_url().ltrim($_SERVER["REQUEST_URI"],'/'));
    }
	return '<td align="center"><a '.$ext.' href="'.site_url($url).$refer.'">'.img('edit.png').'</a></td>';
}
function temp_no(){
    $CI = &get_instance();
    return '<th width="30px">'.$CI->lang->line('common_no').'</th>';
}
function temp_delete_icon(){
	$CI = &get_instance();
    if (temp_check('delete') === true){
	   return '<div class="del_button"><input type="submit" name="delete" value="'.$CI->lang->line('common_delete_button').'" class="submit_button"/></div>';
    }
}
function temp_check($method){
    $CI = &get_instance();
    return $CI->permission->check_permission_backend('',$method);
}
function temp_quick_search(){
	return '<div id="quicksearch">'.img(array('src' => 'magnifier.png','vspace' => '5px','align' => 'left')).'  <input type="text" size="20"></div>';
}
/**
 * 1: Check 1 - 0
 * 2: LIST CHECK BLANK
 */
function temp_status($st,$ext = array(),$type=1){
    if ($type == 2){
        $st = ($st == '') ? 0 : 1;
    }
    $ext['rel'] = $st;
    if ($st < 1){
        $ext['src'] = 'no-tick.png';
	}
	else{
        $ext['src'] = 'tick.png';
	}
    return img($ext);
}
function to_excel($query, $filename = 'exceloutput')
{
    $headers = ''; // just creating the var for field headers to append to below
    $data = ''; // just creating the var for field data to append to below

    $obj = &get_instance();

    $fields = $query->field_data();
    if ($query->num_rows() == 0)
    {
        #echo '<p>The table appears to have no data.</p>';
        return 'empty';
    }
    else
    {
        # Headers array
        $headers_array = array();

        foreach ($fields as $field)
        {
            # Add to headers
            if (!in_array($field->name,$headers_array))
            {
                $headers .= $field->name . "\t";
                $headers_array[] = $field->name;
            }

        }

        foreach ($query->result() as $row)
        {
            $line = '';
            foreach ($row as $value)
            {
                if ((!isset($value)) or ($value == ""))
                {
                    $value = "\t";
                }
                else
                {
                    $value = str_replace('"', '""', $value);
                    //$value = utf8_encode($value);
                    $value = '"' . $value . '"' . "\t";
                }

                $line .= $value;

            }
            $data .= trim($line) . "\n";
        }

        $data = str_replace("\r", "", $data);
        //header('Content-Type:text/html; charset=UTF-8');
        //header("Content-type: application/x-msdownload");
        //header("Content-Disposition: attachment; filename=$filename.csv");
        echo "$headers\n$data";
    }
}
/**
 * Array to CSV
 *
 * download == "" -> return CSV string
 * download == "toto.csv" -> download file toto.csv
 */
if ( ! function_exists('array_to_csv'))
{
	function array_to_csv($array, $download = "")
	{
		if ($download != "")
		{
			header('Content-Type: application/csv');
			header('Content-Disposition: attachement; filename="' . $download . '"');
		}

		ob_start();
		$f = fopen('php://output', 'w') or show_error("Can't open php://output");
		$n = 0;
		foreach ($array as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();

		if ($download == "")
		{
			return $str;
		}
		else
		{
			echo $str;
		}
	}
}

// ------------------------------------------------------------------------

/**
 * Query to CSV
 *
 * download == "" -> return CSV string
 * download == "toto.csv" -> download file toto.csv
 */
if ( ! function_exists('query_to_csv'))
{
	function query_to_csv($query, $headers = TRUE, $download = "")
	{
		if ( ! is_object($query) OR ! method_exists($query, 'list_fields'))
		{
			show_error('invalid query');
		}

		$array = array();

		if ($headers)
		{
			$line = array();
			foreach ($query->list_fields() as $name)
			{
				$line[] = $name;
			}
			$array[] = $line;
		}

		foreach ($query->result_array() as $row)
		{
			$line = array();
			foreach ($row as $item)
			{
				$line[] = $item;
			}
			$array[] = $line;
		}

		echo array_to_csv($array, $download);
	}
}
/*
function delete_image($path){
    $CI = &get_instance();
    $arr = $CI->config->item('resize');
    $file = $CI->config->item('path_image').$path;
    @unlink($file);
    $filename = substr(strrchr($file, "/"), 1);

    foreach ($arr as $arr){
        $file_new_name = 'thumb_'.$arr[0].'x'.$arr[1].'_'.$filename;
        $file_new = substr($file,0,strrpos  ($file,'/') + 1).$file_new_name;
        @unlink($file_new);
    }
}
*/
?>