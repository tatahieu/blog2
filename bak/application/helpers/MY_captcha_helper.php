<?php
function get_captcha($name = 'captcha', $params = array()){
    $CI = &get_instance();
    $defaults = array('word' => substr(sha1(mt_rand()), 17, 4), 'img_path' => FCPATH.'captcha/', 'img_url' => base_url().'captcha/', 'img_width' => '90', 'img_height' => '30', 'font_path' => SYSDIR.'/fonts/tahoma.ttf', 'expiration' => 7200);

    $params = array_merge($defaults,$params);

    $CI->session->set_userdata($name,$params['word']);
    return create_captcha ($params);
}