<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function get_array($array)
{
	if (is_array($array)){
      foreach ($array as $a){
         get_array($a);
	  }
	}
    else{
        echo $array;
    }
}
/**
 * $param : {module,alias,id}
 */
function set_url($params){
    switch($params['module']){
        case 'news':
        $link = $params['alias'].'-nd'.$params['id'];
        break;
        case 'news_cate':
        $link = $params['alias'].'-nl'.$params['id'];
        break;
        case 'product_cate':
        $link = $params['alias'].'-pl'.$params['id'];
        break;
        case 'link':
        $link = $params['link'];
        break;
        default:
        $link = $params['contact'];
    }
    return $link;
}
/**
 * type=1: datetime to timestamp
        2: timestamp to datetime
 **/
function convert_datetime($str,$type = 1)
{
    switch ($type){
        case 1:
        list($date, $time) = explode(' ', $str);
        list($year, $month, $day) = explode('-', $date);
        list($hour, $minute, $second) = explode(':', $time);
        $timestamp = mktime($hour, $minute, $second, $month, $day, $year);
        break;
        case 2:
        $timestamp = date('Y-m-d H:i:s', $str);
        break;
        case 3:
            list($year, $month, $day) = explode('-', $str);
            $timestamp = mktime(0, 0, 0, $month, $day, $year);
        break;
        case 4:
            list($year, $month, $day) = explode('-', $str);
            $timestamp = mktime(23, 59, 59, $month, $day, $year);
        break;
        default:
        $timestamp = time();
    }
    return $timestamp;
}
function set_array()
{
	$arr = func_get_args();
	foreach ($arr as $arr){
		if (is_array($arr)){
			foreach ($arr as $key => $value){
				$row[$key] = $value;
			}
		}
		else{
			$row[$arr] = null;
		}
	}
	return $row;
}
function translate_answer($int){
    switch ($int){
        case 1:
        return 'A';
        break;
        case 2:
        return 'B';
        break;
        case 3:
        return 'C';
        break;
        case 4:
        return 'D';
        break;
    }
}
/**
 * 1: GET DATE
 * 2: GET DATE TIME
 */

function sql_to_date($date,$type = 1)
{
   if ($type == 1){
        $r = explode('-',$date);
        return $r[2].'/'.$r[1].'/'.$r[0];
   }
   if ($type == 2){
        $r = explode(' ',$date);
        $date = sql_to_date($r[0],1);
        $time = $r[1];
        return $time.' '.$date;
   }
}
function filter_vn($str)
{
    $marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề"
    ,"ế","ệ","ể","ễ",
    "ì","í","ị","ỉ","ĩ",
    "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
    ,"ờ","ớ","ợ","ở","ỡ",
    "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
    "ỳ","ý","ỵ","ỷ","ỹ",
    "đ",
    "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
    ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
    "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
    "Ì","Í","Ị","Ỉ","Ĩ",
    "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
    ,"Ờ","Ớ","Ợ","Ở","Ỡ",
    "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
    "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
    "Đ");

    $marKoDau=array("a","a","a","a","a","a","a","a","a","a","a"
    ,"a","a","a","a","a","a",
    "e","e","e","e","e","e","e","e","e","e","e",
    "i","i","i","i","i",
    "o","o","o","o","o","o","o","o","o","o","o","o"
    ,"o","o","o","o","o",
    "u","u","u","u","u","u","u","u","u","u","u",
    "y","y","y","y","y",
    "d",
    "A","A","A","A","A","A","A","A","A","A","A","A"
    ,"A","A","A","A","A",
    "E","E","E","E","E","E","E","E","E","E","E",
    "I","I","I","I","I",
    "O","O","O","O","O","O","O","O","O","O","O","O"
    ,"O","O","O","O","O",
    "U","U","U","U","U","U","U","U","U","U","U",
    "Y","Y","Y","Y","Y",
    "D");
    return str_replace($marTViet,$marKoDau,$str);
}
function set_alias_link($str){
	$string = filter_vn($str);
    $string = str_replace(array("\"",'”',"'","!","@","#","$","%","^","&","*","(",")"),"",$string);
	$string = preg_replace("`\[.*\]`U","",$string);
	$string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
	$string = htmlentities($string, ENT_COMPAT, 'utf-8');
	$string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
	$string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);

	return strtolower(trim($string, '-'));
}
function clear_cache($fd = array()){
    $CI = &get_instance();
    $CI->load->helper('file');
    $path =  APPPATH.'cache/';
    if (!empty($fd)){
        foreach ($fd as $fd){
            delete_files($path.$fd,TRUE,2);
        }
    }
    else{
        delete_files($path,TRUE,2);
    }
}
function cut_text($text,$chars = 100){
    if (strlen($text) < $chars){
        return $text;
    }
    $text = trim($text);
    $text = substr( $text, 0, $chars );
	$text = substr( $text, 0, strrpos( $text, ' ' ) );
    return $text.' ...';
}
function send_mail($to,$subject,$message){
    $CI = &get_instance();
    if (!defined('LOAD_MAIL_LIB')){
        $config = array(
            'smtp_host'	=> $CI->config->item('email_host'),
            'smtp_user' => $CI->config->item('email_username'),
            'smtp_pass' => $CI->config->item('email_password'),
            'smtp_port' => $CI->config->item('email_port'),
            'mailtype' => 'html',
            'protocol' => 'smtp',
            'newline'=>"\r\n"
        );
        $CI->load->library('email',$config);
        define("LOAD_MAIL_LIB",1);
    }
    else{
        $CI->email->clear();
    }
    // email gui ve cho admin
    $CI->email->from($config['smtp_user'], 'Ms Hoa TOEIC');
    $CI->email->to($to);
    $CI->email->subject($subject);
    $CI->email->message($message);
    $CI->email->send();
}
function access_module(){
    $arr = array(
        'Trang lẻ' => array('home' => '-- Trang chủ', 'contact' => "-- Liên hệ", "users" => "-- Thành viên"),
        'Tin tức' => array("news_lists" => "-- Danh sách","news_detail" => "-- Chi tiết"),
        'Bài test' => array("question_list" => "-- list câu hỏi"),
        'Nghe viết' => array("speaking_list" => '-- Trang nghe', "writing_list" => '-- Trang viếts')
    );
    return $arr;
}
function media_show($filename,$size = ''){
    $CI = &get_instance();
    $x = explode('.', $filename);
    $type =  end($x);
    $filename = ltrim($filename,'/');
    switch ($type){
        case 'swf':
            $out = '<embed ';
            if ($height > 0) {$out.= " height=\"$height\" ";}
            if ($width > 0) {$out.= ' width="'.$width.'" ';}
            $out.=  'type="application/x-shockwave-flash" src="'.base_url().'uploads/images/'.$filename.'" style="" quality="high" wmode="transparent" allowscriptaccess="always">';
        break;
        default:
        $out = '<img src="'. getimglink($filename,$size) . '">';
    }
    return $out;
}
function debug($data){
    $out = '<div class="debug" style="position: fixed; right: 0; bottom: 0; width: 500px; height: 200px; z-index: 99999; overflow: scroll; background: #000000; color: #FFF">';
    if (is_array($data)){
        $out .= '<pre>'. htmlspecialchars(print_r($data,true)). "</pre>\n";
    }
    else{
        $out .= var_dump($data);
    }
    $out .= '</div>';
    echo $out;
}
/**
 * @param: type: 1 (widthx0); 2 (0xheight)
 */
function getimglink($path,$size = '',$type = 0){
    $CI = &get_instance();
    $strsize = $CI->config->item("resize");
    $size = $strsize[$size];
    $rep = '';
    switch($type){
        case 1:
        $size[1] = 0;
        $rep = 'thumb_'.$size[0].'x0_';
        break;
        case 2:
        $rep = 'thumb_0x'.$size[1].'_';
        break;
        default:
            if ($size != ''){
                $rep = 'thumb_'.$size[0].'x'.$size[1].'_';
            }
    }
    $arrfile = explode('/',trim($path,'/'));
    $position = count($arrfile) - 1;
    $arrfile[$position] = $rep.$arrfile[$position];
    //$url_link = (is_numeric($arrfile[0]) && $arrfile[0] <= 2015 && $arrfile[1] <= 09) ? UPLOAD_CDN1 : UPLOAD_URL;
    $url_link = UPLOAD_URL;
    return $url_link.'images/'.implode('/',$arrfile);
}
function get_file_url($path){
    //$arrfile = explode('/',trim($path,'/'));
    //$url_link = (is_numeric($arrfile[0]) && $arrfile[0] <= 2015 && $arrfile[1] <= 09) ? UPLOAD_CDN1 : UPLOAD_URL;
    $url_link = UPLOAD_URL;
    return $url_link.ltrim($path,'/');
}
function get_breadcrumb ($arr = array()) {
    if (!$arr) {
        return '';
    }
    $html = '<ul class="breadcrumb">';
    $html .= '<li><a href="'.BASE_URL.'">Home</a></li>';
    $count = count($arr);
    $i = 1;
    foreach ($arr as $arr) {
        $html .= '<span class="divider">/</span>';
        if ($i == $count) {
            $html .= '<li class="active">'.$arr['name'].'</li>';
        }
        else {
            $html .= '<li><a href="'.$arr['link'].'">'.$arr['name'].'</a></li>';
        }
        $i ++;
    }
    $html = '</ul>';
    return $html;
}
function get_location(){
    $arr = array(
        1 => 'Hà Nội',
        2 => 'Hồ Chí Minh',
        3 => 'Đà Nẵng',
        0 => 'Khác'
    );
    return $arr;
}
function get_code_by_id($code){
    switch($code) {
        case 1:
        return 'level-0-100';
        break;
        case 2:
        return 'level-110-150';
        break;
        case 3:
        return 'level-160-200';
        break;
    }
}
function get_level_by_code($code){
    switch($code) {
        case 'level-0-100':
        $return = 1;
        break;
        case 'level-110-150':
        $return = 2;
        break;
        case 'level-160-200':
        $return = 3;
        break;
        default:
        $return = 0;
    }
    return $return;
}
function get_level($select = 0){
    $arr = array(
        1 => 'Level 0-100',
        2 => 'Level 110-150',
        3 => 'Level 160-200'
        );
    return ($select > 0) ? $arr[$select] : $arr;
}
function get_speaking_type($id = 0){
    $arr = array(
        1 => 'Đọc lại theo 1 đoạn văn', // 2 cau, 45s chuan bi + 45s doc
        2 => 'Đọc lại theo bức tranh', // 1 cau, 30s chuan bi + 45s mieu ta lai buc tranh
        3 => 'Trả lời câu hỏi nhanh', //3 cau, khong co time chuan bi + 15s 2 cau dau, 30s cau cuoi
        4 => 'Trả lời câu hỏi có thông tin', // 3 cau, 30s doc thong tin + 15s 2 cau dau, 30s cau cuoi
        5 => 'Nghe + trình bày vấn đề', //1 cau, 30s chuan bi , 60s noi
        6 => 'Nói theo chủ đề' //1 cau, 15s chuan bi, 60s noi
    );
    return ($id > 0) ? $arr[$id] : $arr;
}
function get_writing_type($id = 0){
    $arr = array(
        1 => 'Viết câu có từ cho trước', // 5 cau / 8 phut
        2 => 'Trả lời email', // 3 cau / 10 phut
        3 => 'Viết luận trả lời câu hỏi', // 3 cau / 30 phut
    );
    return ($id > 0) ? $arr[$id] : $arr;
}
// tao share_url
/**
 * @param: 1: speaking / 2: writing
 */
function get_code_by_type($type = 1){
    //////////////// speaking
    switch ($type) {
        case 2:
            $arr = array(
                0   => 'fulltest',
                1   => 'write-a-sentence',
                2   => 'respond-a-request',
                3   => 'write-an-opinion-essay',
            );
        break;
        default:
            $arr = array(
                0   => 'fulltest',
                1   => 'read-a-text-aloud',
                2   => 'describe-a-picture',
                3   => 'respond-questions-i',
                4   => 'respond-questions-ii',
                5   => 'propose-a-solution',
                6   => 'express-an-opinion'
            );
    }

    return  $arr;
}
function get_password($select = ''){
    $arr = array(
        1 => array('password' => 'mshoatoeic1', 'name' => 'Level 1 - Mid test'),
        2 => array('password' => 'mshoatoeic2', 'name' => 'Level 1 - Final test'),
        3 => array('password' => 'mshoatoeic3', 'name' => 'Level 2 - Mid test'),
        4 => array('password' => 'mshoatoeic4', 'name' => 'Level 2 - Final test'),
    );
    return ($select) ? $arr[$select] : $arr;
}
function check_class_speaking(){
    $CI = &get_instance();
    $classid = $CI->session->userdata("class_id");
    if (in_array($classid,array(1,2,5))){
        return false;
    }
    else {
        return true;
    }
}