<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['block']['news_cate'] = array(
    "name" => "Nhóm tin",
    "position" => array('right' => 'phải','left' => 'trái'),
    "params" => array("nums_item","id","title"),
    //"template" => array("left")
    );
$config['block']['static'] = array(
    "name" => "Tin tĩnh",
    "position" => array('left' => 'trái','right'=> 'Phải', 'footer' => 'Chân trang', 'static' => 'Popup'),
    "params" => array("content","title")
    );
$config['block']['menu'] = array(
    "name" => "Menu",
    "position" => array('top' => 'Trên','left' => 'Trái','footer' => 'Dưới'),
    "params" => array("title"),
    //"template" => array("left")
    );
$config['block']['support'] = array(
    "name" => "Hỗ trợ trực tuyến",
    "position" => array('left' => 'Trái'),
    "param" => array("nums_item"),
    );
$config['block']['advertise_cate'] = array(
    "name" => "Nhóm banner",
    "position" => array("content" => 'Slide trên','top' => "Top Banner",'static' => 'Slide 2 bên'),
    "params" => array("nums_item","id","thumb")
    );
$config['block']['breadcrumb'] = array(
    "name" => 'Breadcrumb',
    "position" => array("content" => "Trong nội dung")
);
$config['blockname'] = array(
    'nums_item' => 'Số tin',
    'thumb'     => 'Thumb',
    'title'     => 'Hiển thị tiêu đề'
);
