<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
/** ADMIN **/
$route['admin/(:any)'] = "backend/$1";
$route['admin'] = "backend/users/profile";

$route['tu-van.html'] = "frontend/news/support";
/** NEWS **/
$route['(:any)-nd(:num)'] = "frontend/news/detail/$2/$1";
$route['(:any)-nd(:num)-(:num)'] = "frontend/news/detail/$2/$1"; // redirect old link
$route['(:any)-nl(:num)'] = "frontend/news/lists/$2/$1";
$route['(:any)-tag(:num)'] = "frontend/news/tags/$2/$1";
/** SPEAKING **/
$route['speaking/(:any)/(:any)-(:num).html'] = 'frontend/speaking/detail/$3';
$route['speaking/(:any)/(:any).html'] = 'frontend/speaking/lists/$2/$1';
$route['speaking/(:any).html'] = 'frontend/speaking/lists/$1';
/** **** COURSE ********* **/
$route['course/luyen-nghe-tieng-anh/(:any)'] = 'frontend/course/complete/$1';
$route['course/tu-vung-theo-chu-de/(:any)'] = 'frontend/course/word/$1';
/** WRITING **/
$route['writing/(:any)/(:any)-(:num).html'] = 'frontend/writing/detail/$3';
$route['writing/(:any)/(:any).html'] = 'frontend/writing/lists/$2/$1';
$route['writing/(:any).html'] = 'frontend/writing/lists/$1';
/** VIDEO **/
$route['video/(:any)/(:any)-(:num).html'] = "frontend/news/video/$1/$3";
/** MODULE **/
$route['gallery'] = "frontend/ads/album";
$route['(:any)-alb(:num)'] = "frontend/ads/lists/$2/$1";
/** COMMON **/
$route['(:any)'] = "frontend/$1";
$route['default_controller'] = "frontend/news/index";

//$route['404_override'] = 'http://hanoicdc.com/';


/* End of file routes.php */
/* Location: ./application/config/routes.php */