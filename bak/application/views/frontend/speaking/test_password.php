<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="speaking_list">
    <div class="mod_title">
        <?php echo $password['name']; ?>
    </div>
    <div class="line"></div>
    <h2 class="list_title">Speaking</h2>
    <ul>
    <?php foreach ($speaking as $row) { ?>
        <li><a href="<?php echo $row['share_url']; ?>"><?php echo $row['name'] ?></a></li>
    <?php } ?>
    </ul>
    <h2 class="list_title">Writing</h2>
    <ul>
    <?php foreach ($writing as $row) { ?>
        <li><a href="<?php echo $row['share_url']; ?>"><?php echo $row['name'] ?></a></li>
    <?php } ?>
    </ul>
</div>