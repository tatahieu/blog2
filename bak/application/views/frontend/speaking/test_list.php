<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="speaking_list">
    <div class="mod_title">
        Luyện tập TOEIC Speaking <?php echo $seotitle ?>
    </div>
    <div class="line"></div>
    <ul id="cate_test">
    <?php 
    $i = 1;
    foreach ($rows as $row) { ?>
        <li class="testing_title cate_<?php echo ($i % 2 == 1) ? 'le' : 'chan'; ?>"><a href="<?php echo $row['share_url']; ?>"><?php echo $row['name'] ?></a></li>
    <?php $i++;} ?>
    </ul>
</div>