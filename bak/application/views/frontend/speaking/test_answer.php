<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="speaking_answer" class="speaking_answer">
    <div class="sp_content">
        <?php
        $i = 1; $total = 0;
        foreach ($rows as $row) {
        $total = $total + $useransweritem['point_teacher'];
        $question_number = $question[$row['speaking_id']];
        $useransweritem = $useranswer[$row['speaking_id']];
        ?>
        <div id="speaking_tab_<?php echo $i ?>" style="display: <?php echo ($i ==1) ? 'block' : 'none'; ?>" class="clearfix speaking_tab">
            <div class="question_content">
                <div class="sp_title">
                    <div class="fl"><?php echo $detail['name'] ?> - <?php echo $row['title'] ?></div>
                    <?php if ($row['sound']) { ?>
                    <div class="sound_question clear">
                        <?php if ($question_number <= 10 && $question_number >= 7) { ?>
                        <div onclick="show_tab_script(<?php echo $question_number ?>)" class="tab_script tab_script_<?php echo $question_number ?>"> </div>
                        <?php } ?>
                        <div onclick="speaking.preview_mp3('<?php echo get_file_url('files').'/'.$row['sound'] ?>')" class="repeat_sound" id="question"><img src="<?php echo $this->config->item("theme"); ?>images/button/speaking_loa.png" width="23px" height="23px" title="Nghe lại câu hỏi" alt="Nghe lại câu hỏi"/></div>
                        <div onclick="speaking.stop_preview()" class="repeat_sound"><img src="<?php echo $this->config->item("theme"); ?>images/button/speaking_pause.png" width="23px" height="23px" title="Dừng" alt="Dừng"/></div>
                    </div>
                    <?php } ?>
                </div>
                <div class="question">

                    <?php if ($row['images']) { ?>
                        <div class="qimages" style="text-align: center;">
                            <a href="<?php echo getimglink($row['images']); ?>" class="fancybox">
                                <img style="max-width: 300px;"  src="<?php echo getimglink($row['images'],"size4"); ?>" alt="<?php echo $row['title'] ?>"/>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="clear"></div>

                    <div class="answer_content answer_txt_<?php echo $question_number ?>" <?php if ($question_number <= 10 && $question_number >= 7) echo 'style="display: none" '?>><?php echo $row['content']; ?></div>
                </div>
            </div>
            <div class="question_answer">
                <div class="sp_title"><div class="fl">Model answer</div>
                    <div class="sound_answer clear">
                        <?php if ($useransweritem) { ?>
                        <div class="fl repeat_sound" onclick="speaking.preview_wav('<?php echo get_file_url('record').'/'.$useransweritem['sound'] ?>')">
                            <img src="<?php echo $this->config->item("theme"); ?>images/button/speaker.png" width="23px" height="23px" title="Nghe lại ghi âm" alt="Bạn đã trả lời"/>
                        </div>
                        <?php } ?>
                        <?php if ($row['answer_sound']) {?>
                        <div onclick="speaking.preview_mp3('<?php echo get_file_url('files').'/'.$row['answer_sound'] ?>')" class="repeat_sound" id="question"><img src="<?php echo $this->config->item("theme"); ?>images/button/speaking_loa.png" width="23px" height="23px" title="Nghe đáp án" alt="Nghe đáp án"/></div>
                         <?php } ?>
                         <div onclick="speaking.stop_preview()" class="repeat_sound" id="answer_pause"><img src="<?php echo $this->config->item("theme"); ?>images/button/speaking_pause.png" width="23px" height="23px" title="Dừng" alt="Dừng"/></div>
                    </div>
                </div>
                <div class="answer_content question">
                    <div style="padding-right: 5px;">
                    <?php echo $row['answer_text'] ?>
                    </div>
                </div>
            </div>
            <?php
            // phan danh cho giao vien
            if ($permission == 1 && $useransweritem['record_id'] > 0) {?>
            <div class="point_teacher" onclick="speaking.point_form(<?php echo $useransweritem['record_id'] ?>)">Chấm điểm</div>
            <?php } ?>
            <div class="point_area" id="point_view_<?php echo $useransweritem['record_id'] ?>"><?php echo $useransweritem['point_teacher']; ?></div>
            <div style="display: none;" class="point_area_comment" id="point_comment_<?php echo $useransweritem['record_id'] ?>"><?php echo nl2br($useransweritem['comment_teacher']); ?></div>
            <?php if ($useransweritem['comment_teacher']) {?>
            <div class="viewcomment" onclick="show_teacher_comment(<?php echo $useransweritem['record_id'] ?>)">Xem nhận xét</div>
            <?php } ?>
        </div>
        <div style="display: none;"id="preview_playing"><img src="<?php echo $this->config->item("theme"); ?>images/icons/playing.gif"/></div>
        <?php $i ++;} ?>
        <div class="question_number">
            <ul>
            <?php
            $i = 1;
            foreach ($question as $k => $q) { ?>
                <li onclick="change_answer(<?php echo $i ?>)" class="answer_number<?php if($i == 1) echo ' selected'; ?>" id="answer_number_<?php echo $i ?>"><?php echo $q ?></li>
            <?php
            $i ++;
            } ?>
            </ul>
        </div>
        <div class="button_exit"><a href="/users/speaking"><img width="75px" height="30px" alt="Exit" title="Exit" src="<?php echo $this->config->item("theme"); ?>images/button/speaking_exit.png"/></a></div>
        <div id="total_point" class="total_point">Tổng điểm: <span><?php echo $total ?></span></div>
    </div>
    <div class="opacity"></div>

</div>
<?php if ($permission == 1) { ?>

<div id="point_teacher_form" title="Chấm điểm"></div>
<div id="comment_teacher_view" title="Nhận xét của giáo viên"></div>
<?php } ?>
<!-- PLAYER -->
<div id="jplayer_mp3" class="player_preview"></div>
<div id="jplayer_wav" class="player_preview"></div>
<script type="text/javascript">
$(function() {
    $('.fancybox').fancybox();
});
</script>