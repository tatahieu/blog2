<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="speaking_answer" class="speaking_answer">
    <div class="content">
        <?php
        $i = 1;
        foreach ($rows as $row) {

        ?>
        <div id="speaking_tab_<?php echo $i ?>" style="display: <?php echo ($i ==1) ? 'block' : 'none'; ?>" class="clearfix speaking_tab">
            <div class="sp_title">
                <?php echo $detail['name'] ?> - <?php echo $row['title'] ?>
            </div>
            <div class="question_content">
                <div class="question">
                    <?php if ($row['images']) { ?>
                        <img src="<?php echo getimglink($row['images'],'size3'); ?>" alt="<?php echo $row['title']; ?>"/>
                    <?php } ?>
                    <div class="clear"></div>
                    <?php echo $row['content']; ?>
                </div>
            </div>
            <div class="sound_area">
                <?php if ($row['sound']) { ?>
                <div onclick="speaking.preview_mp3('<?php echo get_file_url('files').'/'.$row['sound'] ?>')" class="repeat_sound" id="question"><img src="<?php echo $this->config->item("theme"); ?>images/button/speaking_loa.png" width="26px" height="26px" title="Nghe lại câu hỏi" alt="Nghe lại câu hỏi"/></div>
               <?php } ?>
                <div onclick="speaking.stop_preview()" class="repeat_sound" id="answer"><img src="<?php echo $this->config->item("theme"); ?>images/button/speaking_pause.png" width="26px" height="26px" title="Dừng" alt="Dừng"/></div>
                <?php if ($row['user_sound']) {
                ?>

                <div onclick="speaking.preview_wav('<?php echo get_file_url('record').'/'.$row['user_sound'] ?>')" class="repeat_sound" id="answer"><img src="<?php echo $this->config->item("theme"); ?>images/button/speaking_play.png" width="26px" height="26px" title="Nghe lại ghi âm" alt="Nghe lại ghi âm"/></div>
                <?php } ?>
            </div>
            <?php
            // phan danh cho giao vien
            if ($permission == 1 && $row['record_id'] > 0) {?>
            <div class="point_teacher" onclick="speaking.point_form(<?php echo $row['record_id'] ?>,<?php echo (int)$row['point_teacher']; ?>)">Chấm điểm</div>
            <?php } ?>
            <div class="point_area" id="point_view_<?php echo $row['record_id'] ?>"><?php echo $row['point_teacher']; ?></div>

        </div>

        <div style="display: none;"id="preview_playing"><img src="<?php echo $this->config->item("theme"); ?>images/icons/playing.gif"/></div>
        <?php $i ++;} ?>
        <div id="change_question" max-page="<?php echo ($i-1) ?>" current-page="1">
            <div class="next_speaking" onclick="next_question()"></div>
            <div class="prev_speaking" onclick="prev_question()"></div>
        </div>
        <div class="button_exit"><a href="/users/speaking"><img width="75px" height="30px" alt="Exit" title="Exit" src="<?php echo $this->config->item("theme"); ?>images/button/speaking_exit.png"/></a></div>
    </div>
    <div class="opacity"></div>
</div>
<?php if ($permission == 1) { ?>
<div id="point_teacher_form"></div>
<?php } ?>
<div id="jplayer_mp3" class="player_preview"></div>
<div id="jplayer_wav" class="player_preview"></div>