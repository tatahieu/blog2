<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style>
.ui-progressbar {
    position: relative;
}
.progress-label {
    position: absolute;
    width: 100%;
    text-align: center;
    top: 4px;
    font-weight: bold;
    text-shadow: 1px 1px 0 #fff;
}
</style>

<div id="speaking" style="display: none;">
    <div class="sp_content">
        <?php if ($ques == 1 && $detail['detail']['type'] == 0) {?>
        <div class="first_question" id="first_question"><img data-sound="" src="<?php echo $this->config->item("theme")?>images/userguide/speaking_first.jpg" alt="Speaking test"/></div>
        <?php } ?>
        <div class="sp_title">
            <?php
            echo $detail['detail']['name']; ?> - Question <?php echo $ques; ?>
        </div>
        <div class="question_content">
            <div id="question_show" class="question" style="display: none;">
                <?php if ($rows['images']) { ?>
                <div class="image">
                <?php if ($ques == 7 || $ques == 8 || $ques == 9) { ?>
                    <img align="center" src="<?php echo getimglink($rows['images'],'size5') ?>" alt="<?php echo $rows['title'] ?>"/>
                <?php } else { ?>
                    <img align="center" src="<?php echo getimglink($rows['images'],'size4'); ?>" alt="<?php echo $rows['title'] ?>"/>
                <?php } ?>
                </div>
                <?php } ?>

                <?php
                if ($ques < 7 || $ques > 10){
                echo $rows['content'];
                }?>
            </div>
            <div id="speaking_userguide">
                <?php

                switch ($ques) {
                    case 1:
                        $img = '<div class="question">';
                        if ($detail['detail']['type'] == 0) {
                            $img.= '<img class="flash_support" data-sound="" alt="Speaking test" src="'.$this->config->item("img").'userguide/check_record.jpg">';
                            $img.= '<img class="flash_support" data-sound="" alt="Speaking test" src="'.$this->config->item("img").'userguide/direction.jpg">';
                        }
                        $img.= '<img class="flash_support" data-sound="'.$this->config->item("audio").'Part_1_1.mp3" alt="Speaking test" src="'.$this->config->item("theme").'images/userguide/sp_u'.$ques.'.jpg">';
                        $img.= '</div>';
                        echo $img;
                    break;
                    case 3:
                    case 4:
                    case 7:
                    case 10:
                    case 11:
                        echo '<img class="flash_support" data-sound="'.$this->config->item("audio").'Part_'.$ques.'_1.mp3" src="'.$this->config->item("theme").'images/userguide/sp_u'.$ques.'.jpg">';
                    break;
                    default:
                }?>
            </div>
        </div>
        <div class="top_button">
            <div style="display: none;" id="continue_button"><img alt="continue" title="Continue" src="<?php echo $this->config->item("theme"); ?>images/button/speaking_continue.gif"/></div>
        </div>
        <div class="button_exit" id="speaking_exit" data-rel="<?php echo $exitlink; ?>"><img width="75px" height="30px" alt="Exit" title="Exit" src="<?php echo $this->config->item("theme"); ?>images/button/speaking_exit.png"/></div>
        <div id="countdown_area">
            <div id="prepare_count_down"></div>
            <div id="response_count_down"></div>
            <div id="response_allow_down"></div>
        </div>
    </div>
    <div class="opacity"></div>
</div>
<div id="jplayer_speaking"></div>
<div id="jplayer_speaking_userguide"></div>
<div id="dialog-message" title="Thông báo"></div>
<script>
$(function(){
    if (typeof(speaking) != "undefined"){
        <?php if ($rows['sound']) { ?>
        speaking.question_audio = '<?php echo get_file_url('files').'/'.$rows['sound'] ?>';
        <?php } ?>
        speaking.param = {
            speaking_id : <?php echo $rows['speaking_id']; ?>,
            prepare_time:  <?php echo ($rows['prepare_time']) ? $rows['prepare_time'] : 0; ?>,
            response_time: <?php echo ($rows['response_time']) ? $rows['response_time'] : 0; ?>,
            type: <?php echo $detail['detail']['type']; ?>,
            test_id: <?php echo $detail['detail']['test_id'] ?>,
            question_number: <?php echo $ques ?>,
            link_exit: '<?php echo $exitlink; ?>'
        }
        speaking.speaking_run();
    }
});
</script>
