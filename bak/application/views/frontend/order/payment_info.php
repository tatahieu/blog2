<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<form action="" method="POST">
<div id="payment_info">
    <?php echo validation_errors(); ?>
    <table width="100%" cellpadding="5px">
        <tr>
            <td class="cl_left">
                <?php echo $this->lang->line('order_fullname'); ?>
            </td>
            <td class="cl_right">
                <?php echo $fullname?>
            </td>
        </tr>
        <tr>
            <td class="cl_left">
                <?php echo $this->lang->line('order_address'); ?>
            </td>
            <td class="cl_right">
                <?php echo $address?>
            </td>
        </tr>
        <tr>
            <td class="cl_left">
                <?php echo $this->lang->line('order_email'); ?>
            </td>
            <td class="cl_right">
                <?php echo $email?>
            </td>
        </tr>
        <tr>
            <td class="cl_left">
                <?php echo $this->lang->line('order_phone'); ?>
            </td>
            <td class="cl_right">
                <?php echo $phone?>
            </td>
        </tr>
        <tr>
            <td class="cl_left">
                <?php echo $this->lang->line('order_note'); ?>
            </td>
            <td class="cl_right">
                <?php echo $note?>
            </td>
        </tr>
        <tr>
            <td class="cl_left">
                <?php echo $this->lang->line('order_captcha'); ?>
            </td>
            <td class="cl_right">
                <?php echo $captcha?><?php echo $security_code?>
            </td>
        </tr>
        <tr>
            <td class="cl_left">

            </td>
            <td class="cl_right">
                <?php echo $submit?><?php echo $reset?>
            </td>
        </tr>
    </table>
</div>
</form>