<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="order_cart">
    <div class="mod_title">
        <div class="txt fl"><?php echo $this->lang->line("common_order"); ?></div>
        <div class="border"></div>
    </div>
    <div class="count_all_pro">
        <?php echo sprintf($this->lang->line("common_count_cart"),$this->cart->total_items()) ?>

    </div>
    <?php
    $i = 1;
    if (!empty($row)){?>
    <table cellpadding="6" cellspacing="0" style="width:100%" border="0" class="cart_list">
    <tr>
        <th><?php echo $this->lang->line('cart_product_name'); ?></th>
        <th><?php echo $this->lang->line('cart_quantity'); ?></th>
        <th style="text-align:right"><?php echo $this->lang->line('cart_item_price'); ?></th>
        <th style="text-align:right"><?php echo $this->lang->line('cart_sub_total'); ?></th>
        <th width="60px"></th>
    </tr>
    <?php foreach ($row as $key => $items){?>
	<tr class="lists_product cart_<?php echo $key ?>">
        <td valign="top" width="378px">
            <div class="img_desc">
                <img width="130px" src="<?php echo getimglink($items['image'],'size2') ?>" alt="<?php echo $items['name']; ?>"/>
            </div>
		    <?php echo $items['name']; ?>
        </td>


        <td valign="top"><?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '3')); ?></td>

        <td valign="top" style="text-align:right"><?php echo price_fomat($items['price']); ?></td>
        <td valign="top" style="text-align:right"><?php echo price_fomat($items['subtotal']); ?></td>
        <td valign="top" align="center"><a href="javascript:delete_cart_item('<?php echo ($key) ?>')"><img src="<?php echo $this->config->item("theme") ?>images/del_cart.gif" alt="Delete cart" title="<?php echo $this->lang->line("cart_delete") ?>"/></a></td>
    </tr>
    <?php $i++;
    }?>
    <tr class="price_total">
        <td></td>
        <td colspan="2"><span class="total_txt">Tổng tiền:</span></td>
        <td style="text-align:right"><span id="cart_total"><?php echo price_fomat($this->cart->total(),2); ?></span></td>
        <td></td>
    </tr>
</table>
<div class="notevat fl">
Lưu ý: Giá trên đã bao gồm thuế VAT
</div>
<div class="button_continue fr">
    <div class="continue"><?php echo $this->lang->line("cart_continue"); ?></div>
    <div class="checkout"><?php echo $this->lang->line("order_send"); ?></div>
</div>
<?php
    echo $payment_info;
}
else{?>
<div style="text-align: center;"><?php echo $this->lang->line('cart_empty'); ?></div>
<?}?>
</div>