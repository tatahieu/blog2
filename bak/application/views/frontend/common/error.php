<div id="common_noresult">
    <h2 class="title"> <?php echo $error['title']; ?></h2>
	<div class="line"></div>
    <div class="main_content">
    	<p>
        	<?php echo $error['message']; ?>
    	</p>
    	<p>
    		<a style="font-weight: bold; text-decoration: underline;" href="javascript:history.go(-1);">Quay lại</a>
    	</p>
    </div>
</div> 