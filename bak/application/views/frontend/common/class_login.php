<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="login">
    <form method="POST" action="">
        <?php if ($errorcallback) echo '<div class="error">'.$errorcallback.'</div>'; ?>
        <?=validation_errors()?>
            <div class="frmlogin">
                <div class="lgtitle"><b>Đăng nhập vào lớp học</b></div>
                <div class="boxlogin">
                    <div class="form_item"><span>Tên truy cập:</span><?=$username?></div>
                    <div class="form_item"><span>Mật khẩu:</span><?=$password?></div>
                    <p align="center"><?=$submit?></p>
                </div>
            </div>
    </form>
</div>