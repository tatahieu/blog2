<<<<<<< .mine
<footer id="footer" class="p-t-2 p-b-2">
    <div class="container">
        <div class="row">

            <div class="col-md-9 col-sm-12">
                <div class="company-name">
                    <h4>Ms HOA TOEIC - The Leading TOEIC Training Center in Vietnam</h4>
                    Website: <a href="http://www.mshoatoeic.com">www.mshoatoeic.com</a> - Email: <a href="mailto:<?php echo $this->config->item('email_support');?>"><?php echo $this->config->item('email_support');?></a>
                </div>
                
                <?php 
                    $hn = array
                    (
                        array("CS1", "89 Tô Vĩnh Diện, Thanh Xuân", "0466 811 242"),
                        array("CS2", "26/203 Hoàng Quốc Việt, Cầu Giấy", "0462 956 406"),
                        array("CS3", "141 Bạch Mai, Hai Bà Trưng", "0462 935 446"),
                        array("CS4", "20 Nguyễn Đổng Chi, Nam Từ Liêm", "0462 916 756"),
                        array("CS5", "LK13, KĐT Bắc Hà, Hà Đông", "0462 926 049"),
                        array("CS6", "29 Giải Phóng, Hai Bà Trưng", "0466 862 811")
                    );
                    
                    $hcm = array
                    (
                        array("CS1", "569 Sư Vạn Hạnh, P13, Q10", "0866 88 22 77"),
                        array("CS2", "49A Phan Đăng Lưu, P3, Q.Bình Thạnh", "0866 85 65 69"),
                        array("CS3", "82 Lê Văn Việt, Hiệp Phú, Q9", "0866 54 88 77"),
                        array("CS4", "427 Cộng Hòa, P15, Q Tân Bình", "0862 86 71 59"),
                        array("CS5", "224 Khánh Hội, P6, Q4", "0866 73 11 33")
                    );
                ?>
                
                <div class="row co-so clearfix">
                    <div class="col-sm-6">
                        <h5>Cơ sở Hà Nội</h5>
                        
                        <div class="row">
                            <ul>
                                <?php foreach ($hn as $key => $value) {?>
                                <li>
                                    <div class="col-sm-8"><span class="cs-name"><?php echo $value[0]; ?> -</span> <?php echo $value[1]; ?></div> 
                                    <div class="pull-right col-sm-4"><i class="fa fa-phone"></i> <?php echo $value[2]; ?></div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <h5>Cơ sở Hồ Chí Minh</h5>
                        
                        <div class="row">
                            <ul>
                                <?php foreach ($hcm as $key => $value) {?>
                                <li>
                                    <div class="col-sm-8"><span class="cs-name"><?php echo $value[0]; ?> -</span> <?php echo $value[1]; ?></div> 
                                    <div class="pull-right col-sm-4"><i class="fa fa-phone"></i> <?php echo $value[2]; ?></div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="foot-copyright clearfix">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="#" class="logo m-b-1">
                                <img class="img-responsive" src="<?php echo $this->config->item('img');?>/logo.png" alt="">
                            </a>    
                        </div>
                        <div class="col-sm-6">
                            <p class="copyright">
                                &copy; 2015 Ms.Hoa Toeic
                                <a class="text-gray-light" href="mailto:<?php echo $this->config->item('email_support');?>"><?php echo $this->config->item('email_support');?></a>
                            </p>    
                        </div>
                        <div class="col-sm-3">
                            <ul class="list-social-h list-inline list-unstyled">
                                <li>
                                    <a target="_blank" class="text-gray-light link-facebook" href="<?php echo $this->config->item('facebook');?>">
                                        <i class="fa fa-lg fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" class="text-gray-light link-twitter" href="<?php echo $this->config->item('twitter');?>">
                                        <i class="fa fa-lg fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" class="text-gray-light link-google" href="<?php echo $this->config->item('youtube');?>">
                                        <i class="fa fa-lg fa-youtube"></i>
                                    </a>
                                </li>
                            </ul>        
                        </div>
                    </div>
                    
                    

                    
                </div>
            </div>

            <div class="col-md-3 hidden-sm">
                <div class="fb-like-box text-right">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=<?php echo $this->config->item('facebook');?>&tabs&width=290&height=230&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="290" height="230" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>
            </div>

        </div>
    </div>

    <a href="javascript:;" class="btn-advisory" id="support_online">
        <img src="<?php echo $this->config->item('img');?>bg/support.png" alt="">
    </a>
    <div id="support_facebook" style="display: none; overflow-y: auto;">
        <div class="support_close">
            <img src="<?php echo $this->config->item('img');?>closebox.png">
        </div>

        <div class="fb-comments" data-height="auto" data-width="100%" data-href="<?php echo SITE_URL;?>"  data-num-posts="3"></div>
    </div>
</footer>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=391286151062361";
  fjs.parentNode.insertBefore(js, fjs);
||||||| .r4375
<footer id="footer" class="p-t-2 p-b-2">
    <div class="container">
        <div class="row">

            <div class="col-md-9 col-sm-12">
                <div class="company-name">
                    <h4>Ms HOA TOEIC - The Leading TOEIC Training Center in Vietnam</h4>
                    Website: <a href="http://www.mshoatoeic.com">www.mshoatoeic.com</a> - Email: <a href="mailto:<?php echo $this->config->item('email_support');?>"><?php echo $this->config->item('email_support');?></a>
                </div>
                
                <?php 
                    $hn = array
                    (
                        array("CS1", "89 Tô Vĩnh Diện, Thanh Xuân", "0466 811 242"),
                        array("CS2", "26/203 Hoàng Quốc Việt, Cầu Giấy", "0462 956 406"),
                        array("CS3", "141 Bạch Mai, Hai Bà Trưng", "0462 935 446"),
                        array("CS4", "20 Nguyễn Đổng Chi, Nam Từ Liêm", "0462 916 756"),
                        array("CS5", "LK13, KĐT Bắc Hà, Hà Đông", "0462 926 049"),
                        array("CS6", "29 Giải Phóng, Hai Bà Trưng", "0466 862 811")
                    );
                    
                    $hcm = array
                    (
                        array("CS1", "569 Sư Vạn Hạnh, P13, Q10", "0866 88 22 77"),
                        array("CS2", "49A Phan Đăng Lưu, P3, Q.Bình Thạnh", "0866 85 65 69"),
                        array("CS3", "82 Lê Văn Việt, Hiệp Phú, Q9", "0866 54 88 77"),
                        array("CS4", "427 Cộng Hòa, P15, Q Tân Bình", "0862 86 71 59"),
                        array("CS5", "224 Khánh Hội, P6, Q4", "0866 73 11 33")
                    );
                ?>
                
                <div class="row co-so clearfix">
                    <div class="col-sm-6">
                        <h5>Cơ sở Hà Nội</h5>
                        
                        <div class="row">
                            <ul>
                                <?php foreach ($hn as $key => $value) {?>
                                <li>
                                    <div class="col-sm-8"><span class="cs-name"><?php echo $value[0]; ?> -</span> <?php echo $value[1]; ?></div> 
                                    <div class="pull-right col-sm-4"><i class="fa fa-phone"></i> <?php echo $value[2]; ?></div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <h5>Cơ sở Hồ Chí Minh</h5>
                        
                        <div class="row">
                            <ul>
                                <?php foreach ($hcm as $key => $value) {?>
                                <li>
                                    <div class="col-sm-8"><span class="cs-name"><?php echo $value[0]; ?> -</span> <?php echo $value[1]; ?></div> 
                                    <div class="pull-right col-sm-4"><i class="fa fa-phone"></i> <?php echo $value[2]; ?></div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="foot-copyright clearfix">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="#" class="logo m-b-1">
                                <img class="img-responsive" src="<?php echo $this->config->item('img');?>/logo.png" alt="">
                            </a>    
                        </div>
                        <div class="col-sm-6">
                            <p class="copyright">
                                &copy; 2015 Ms.Hoa Toeic
                                <a class="text-gray-light" href="mailto:<?php echo $this->config->item('email_support');?>"><?php echo $this->config->item('email_support');?></a>
                            </p>    
                        </div>
                        <div class="col-sm-3">
                            <ul class="list-social-h list-inline list-unstyled">
                                <li>
                                    <a target="_blank" class="text-gray-light link-facebook" href="<?php echo $this->config->item('facebook');?>">
                                        <i class="fa fa-lg fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" class="text-gray-light link-twitter" href="<?php echo $this->config->item('twitter');?>">
                                        <i class="fa fa-lg fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" class="text-gray-light link-google" href="<?php echo $this->config->item('youtube');?>">
                                        <i class="fa fa-lg fa-youtube"></i>
                                    </a>
                                </li>
                            </ul>        
                        </div>
                    </div>
                    
                    

                    
                </div>
            </div>

            <div class="col-md-3 hidden-sm">
                <div class="fb-like-box text-right">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=<?php echo $this->config->item('facebook');?>&tabs&width=290&height=230&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="290" height="230" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>
            </div>

        </div>
    </div>

    <a href="javascript:;" class="btn-advisory" id="support_online">
        <img src="<?php echo $this->config->item('img');?>bg/support.png" alt="">
    </a>
    <div id="support_facebook" style="display: none;">
        <div class="support_close">
            <img src="<?php echo $this->config->item('img');?>closebox.png">
        </div>

        <div class="fb-comments" data-height="auto" data-width="100%" data-href="<?php echo SITE_URL;?>"  data-num-posts="3"></div>
    </div>
</footer>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=391286151062361";
  fjs.parentNode.insertBefore(js, fjs);
=======
<footer id="footer" class="p-t-2 p-b-2">
    <div class="container">
        <div class="row">

            <div class="col-md-9 col-sm-12">
                <div class="company-name">
                    <h4>Ms HOA TOEIC - The Leading TOEIC Training Center in Vietnam</h4>
                    Website: <a href="http://www.mshoatoeic.com">www.mshoatoeic.com</a> - Email: <a href="mailto:<?php echo $this->config->item('email_support');?>"><?php echo $this->config->item('email_support');?></a>
                </div>
                
                <?php 
                    $hn = array
                    (
                        array("CS1", "89 Tô Vĩnh Diện, Thanh Xuân", "02466 811 242"),
                        array("CS2", "26/203 Hoàng Quốc Việt, Cầu Giấy", "02462 956 406"),
                        array("CS3", "141 Bạch Mai, Hai Bà Trưng", "02462 935 446"),
                        array("CS4", "20 Nguyễn Đổng Chi, Nam Từ Liêm", "02462 916 756"),
                        array("CS5", "LK13, KĐT Bắc Hà, Hà Đông", "02462 926 049"),
                    );
                    
                    $hcm = array
                    (
                        array("CS1", "569 Sư Vạn Hạnh, P13, Q10", "02866 88 22 77"),
                        array("CS2", "49A Phan Đăng Lưu, P3, Q.Bình Thạnh", "02866 85 65 69"),
                        array("CS3", "82 Lê Văn Việt, Hiệp Phú, Q9", "02866 54 88 77"),
                        array("CS4", "427 Cộng Hòa, P15, Q Tân Bình", "02862 86 71 59"),
                        array("CS5", "224 Khánh Hội, P6, Q4", "02866 73 11 33")
                    );
                ?>
                
                <div class="row co-so clearfix">
                    <div class="col-sm-6">
                        <h5>Cơ sở Hà Nội</h5>
                        
                        <div class="row">
                            <ul>
                                <?php foreach ($hn as $key => $value) {?>
                                <li>
                                    <div class="col-sm-8"><span class="cs-name"><?php echo $value[0]; ?> -</span> <?php echo $value[1]; ?></div> 
                                    <div class="pull-right col-sm-4"><i class="fa fa-phone"></i> <?php echo $value[2]; ?></div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <h5>Cơ sở Hồ Chí Minh</h5>
                        
                        <div class="row">
                            <ul>
                                <?php foreach ($hcm as $key => $value) {?>
                                <li>
                                    <div class="col-sm-8"><span class="cs-name"><?php echo $value[0]; ?> -</span> <?php echo $value[1]; ?></div> 
                                    <div class="pull-right col-sm-4"><i class="fa fa-phone"></i> <?php echo $value[2]; ?></div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="foot-copyright clearfix">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="#" class="logo m-b-1">
                                <img class="img-responsive" src="<?php echo $this->config->item('img');?>/logo.png" alt="">
                            </a>    
                        </div>
                        <div class="col-sm-6">
                            <p class="copyright">
                                &copy; 2015 Ms.Hoa Toeic
                                <a class="text-gray-light" href="mailto:<?php echo $this->config->item('email_support');?>"><?php echo $this->config->item('email_support');?></a>
                            </p>    
                        </div>
                        <div class="col-sm-3">
                            <ul class="list-social-h list-inline list-unstyled">
                                <li>
                                    <a target="_blank" class="text-gray-light link-facebook" href="<?php echo $this->config->item('facebook');?>">
                                        <i class="fa fa-lg fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" class="text-gray-light link-twitter" href="<?php echo $this->config->item('twitter');?>">
                                        <i class="fa fa-lg fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" class="text-gray-light link-google" href="<?php echo $this->config->item('youtube');?>">
                                        <i class="fa fa-lg fa-youtube"></i>
                                    </a>
                                </li>
                            </ul>        
                        </div>
                    </div>
                    
                    

                    
                </div>
            </div>

            <div class="col-md-3 hidden-sm">
                <div class="fb-like-box text-right">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=<?php echo $this->config->item('facebook');?>&tabs&width=290&height=230&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="290" height="230" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>
            </div>

        </div>
    </div>

    <a href="javascript:;" class="btn-advisory" id="support_online">
        <img src="<?php echo $this->config->item('img');?>bg/support.png" alt="">
    </a>
    <div id="support_facebook" style="display: none;">
        <div class="support_close">
            <img src="<?php echo $this->config->item('img');?>closebox.png">
        </div>

        <div class="fb-comments" data-height="auto" data-width="100%" data-href="<?php echo SITE_URL;?>"  data-num-posts="3"></div>
    </div>
</footer>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=391286151062361";
  fjs.parentNode.insertBefore(js, fjs);
>>>>>>> .r4407
}(document, 'script', 'facebook-jssdk'));</script>