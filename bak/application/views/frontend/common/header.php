<header id="header" class="clearfix">

    <nav id="main-nav" class="main-nav navbar navbar-default m-b-0">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="http://mshoatoeic.com">
                            <i class="icon-home"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#gioithieu">Giới thiệu <span class="sr-only">(current)</span></a>
                    </li>
                    <li>
                        <a href="#khoahoc">Khoá học</a>
                    </li>
                    <li>
                        <a href="#khoahoc">Lịch khai giảng</a>
                    </li>
                    <li>
                        <a href="#camnhanhocvien">Cảm nhận học viên</a>
                    </li>
                    <li>
                        <a href="#vinhdanhhocvien">Vinh danh học viên</a>
                    </li>
                    <li>
                        <a href="#tailieu">Tài liệu</a>
                    </li>
                    <li>
                        <a href="#video">Video</a>
                    </li>
                    <li>
                        <a href="#reg2-form-control-6">Đăng kí</a>
                    </li>
                </ul>

                <ul class="social navbar-right list-unstyled list-inline">
                   <li>
                       <a class="link-facebook" href="<?php echo $this->config->item('facebook');?>">
                           <i class="fa fa-facebook"></i>
                       </a>
                   </li>
                    <li>
                        <a class="link-twitter" href="<?php echo $this->config->item('twitter');?>">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </nav> <!-- .navbar-collapse -->

        </div> <!-- .container -->
    </nav>
</header> <!--#page-header-->