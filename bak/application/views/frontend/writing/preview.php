<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="writing" style="display: none;">
    <div class="content">
        <?php
        $i = 1;
        foreach ($rows as $row) { ?>
        <div class="question_writing" id="question_<?php echo $i ?>" <?php if ($i != 1) echo 'style="display: none;"' ?>>
            <div class="sp_title">
            <?php
                echo $detail['name']; ?> - Question <?php echo $i; ?>
            </div>
            <div class="question_content">
                <div class="question">
                    <?php if ($row['images']) { ?>
                    <img src="<?php echo getimglink($row['images'],'size3'); ?>" alt="<?php echo $row['title']; ?>"/>
                    <?php } ?>
                    <div class="clear"></div>
                    <?php echo $row['question']; ?>
                </div>
                <div class="answer_form">
                    <textarea><?php echo $row['content'] ?></textarea>
                </div>
            </div>
            <?php
            // phan danh cho giao vien
            if ($permission == 1 && $row['answer_id'] > 0) {?>
            <div class="point_teacher" onclick="writing.point_form(<?php echo $row['answer_id'] ?>,<?php echo (int)$row['point_teacher']; ?>)">Chấm điểm</div>
            <?php } ?>
            <div class="point_area" id="point_view_<?php echo $row['answer_id'] ?>">
                <?php echo $row['point_teacher']; ?>
            </div>
        </div>
        <?php $i ++; } ?>
        <?php if ($i > 2) {?>
        <div class="top_button">
            <div id="change_question" max-page="<?php echo ($i-1) ?>" current-page="1">
                <div class="next_button" ></div>
                <div class="prev_button" ></div>
            </div>
        </div>
        <?php } ?>
        <div class="button_exit"><a href="/users/writing/<?php echo $detail['type']; ?>"><img width="60px" height="27px" alt="Exit" title="Exit" src="<?php echo $this->config->item("theme"); ?>images/button/speaking_exit.png"/></a></div>
    </div>
    <div class="opacity"></div>
    <div id="point_teacher_form"></div>
</div>
<script>
$(function(){
    if (typeof(writing_preview) != "undefined"){
        writing_preview.run();
    }
});
</script>