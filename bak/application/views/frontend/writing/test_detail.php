<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div id="writing" style="display: none;">
    <div class="sp_content">
        <?php if ($key == 'type_1' && $detail['detail']['type'] == 0) { ?>
        <div class="first_question" id="first_question">
            <img id="writing_direction" src="<?php echo $this->config->item("img").'userguide/writing_first.jpg'; ?>" width="789" height="581px" alt="writing first" />
        </div>
        <?php } ?>
        <form id="writing_answer_fr">
        <?php
        $i = 1;
        if ($key == 'type_2'){
            $pre = 5;
        }
        elseif ($key == 'type_3') {
            $pre = 7;
        }
        else {
            $pre = 0;
        }
        foreach ($rows as $row) { ?>
        <div class="question_writing" id="question_<?php echo $i ?>" style="display: none;">
            <div class="sp_title">
            <?php
                echo $detail['detail']['name']; ?> - Question <?php echo $pre + $i; ?>
            </div>
            <div class="question_content">
                <?php if ($key == 'type_2') { ?>
                <div>
                    <b>DIRECTION:</b> Read the Email Below.
                </div>
                <?php } elseif ($key =='type_3') { ?>
                <div>
                    <b>DIRECTION:</b> Read the question below. You have 30 minutes to plan, write and revise your essay. Typically and effective response will contain a minimum of 300 words.
                </div>
                <?php } else {?>
                <div>
                    <b>DIRECTION:</b> Write ONE sentence based on the picture using the TWO words or phrase under it. You may change the forms of the words and you may use them in any order
                </div>
                <?php } ?>
                <hr />
                <div class="question">
                    <?php if ($row['images']) { ?>
                    <div style="margin-bottom: 10px; text-align: center;">
                        <img style="max-height: 250px; border: 1px solid #EEEEEE; padding: 2px;" src="<?php echo getimglink($row['images'],'size4'); ?>" alt="<?php echo $row['title'] ?>"/>
                    </div>
                    <?php } ?>
                    <div class="clear"></div>
                    <?php echo $row['question']; ?>
                </div>
                <div class="line"></div>
                <div class="answer_form">
                    <?php echo form_textarea("answer[".$row['writing_id']."]","",'id="textarea_'.$i.'"'); ?>
                </div>
            </div>
        </div>
        <?php $i ++; } ?>
        <div id="writing_userguide">
            <?php if ($key == 'type_1' && $detail['detail']['type'] == 0) { ?>
            <img id="writing_direction" src="<?php echo $this->config->item("img").'userguide/writing_dir.jpg'; ?>" alt="writing direction" />
            <?php } ?>
            <img id="user_step" style="display: none;" src="<?php echo $this->config->item("img").'userguide/wt_'.$key.'.jpg'; ?>" alt="writing guide" />
        </div>
        </form>
        <div class="top_button">
            <div id="continue_button"><img alt="continue" title="Continue" src="<?php echo $this->config->item("img"); ?>button/speaking_continue.gif"/></div>
            <?php if ($i > 2) {?>
            <div style="display: none;" id="change_question" max-page="<?php echo ($i-1) ?>" current-page="1">
                <div class="next_button"></div>
                <div class="prev_button"></div>
            </div>
            <?php } ?>
        </div>
        <div id="show_count_word" style="display: none;">
            <div class="show_word">
                <div class="showtxt fl">Word count:<span class="txt fr">0</span></div>
                
            </div>
        </div>
        <div id="writing_countdown" style="display: none;">
            <div id="writing_count_down"></div>
            <!--<div id="hidetime" class="count_down_hide">Hide time</div>-->
        </div>
        <div class="button_exit" id="writing_exit"><img width="79px" height="36px" alt="Exit" title="Exit" src="<?php echo $this->config->item("theme"); ?>images/button/speaking_exit.png"/></div>
    </div>
    <div class="opacity"></div>
</div>
<div id="dialog-message" title="Thông báo"></div>
<script>
$(document).ready(function(){
    if (typeof(writing) != "undefined"){
        writing.param = {
            answer_time : <?php echo $time ?>,
            type        : <?php  echo $detail['detail']['type']; ?>,
            test_id     : <?php echo $detail['detail']['test_id'] ?>,
            link_exit   : '<?php echo $exitlink; ?>'
        }
        writing.run();
    }
});
</script>
