<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if ($testtype == 'part') { ?>
<ul id="class_test_tab" class="clear">
    <li><a href="/question/classtest">Read and List</a></li>
    <li style="border-left: 1px solid #FFF;"><a href="/speaking/byclass/fullpart">Speaking</a></li>
    <li class="active">Writing</li>
</ul>
<?php } elseif ($testtype == 'full') { ?>
<ul id="class_test_tab" class="clear">
    <li><a href="/question/classfull">Read and List</a></li>
    <li style="border-left: 1px solid #FFF;"><a href="/speaking/byclass/fulltest">Speaking</a></li>
    <li class="active">Writing</li>
</ul>
<?php }?>
<div class="title">
	Bài tập TOEIC Writing<?php //echo $this->session->userdata("class_name"); ?>
</div>
<div id="cate_test" class="main_content">
    <?php foreach ($rows as $row){?>
    <div class="testing_title">
        <a href="<?php echo $row['share_url']; ?>"><?php echo $row['name']; ?></a>
    </div>
    <?}?>
    <?php echo $paging; ?>
</div>