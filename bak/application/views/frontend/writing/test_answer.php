<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="speaking_answer" class="writing_answer">
    <div class="sp_content">
        <?php
        $i = 1;
        //var_dump($rows); die;
        foreach ($rows as $row) {
        $useransweritem = $useranswer[$row['writing_id']];
        ?>
        <div id="speaking_tab_<?php echo $i ?>" style="display: <?php echo ($i == 1) ? 'block' : 'none'; ?>" class="clearfix speaking_tab">
            <div class="question_content">
                <div class="sp_title">
                    <span style="float: left;"><?php echo $detail['name'] ?> - <?php echo $row['title'] ?></span>
                    <div class="tabscript" onclick="show_tab_script(<?php echo $question[$row['writing_id']] ?>)">
                        <img alt="Show answer" src="<?php echo $this->config->item("theme") ?>images/button/tab_script_speaking.gif" title="Hiện câu trả lời"/>
                    </div>
                      <div class="clear"></div>
                </div>
                <div class="question writing_question">
                    <?php if ($question[$row['writing_id']] <= 5) { ?>
                    <div>
                        <b>DIRECTION:</b> Write ONE sentence based on the picture using the TWO words or phrase under it. You may change the forms of the words and you may use them in any order
                    </div>
                    <?php } elseif ($question[$row['writing_id']] <=7) { ?>
                    <div>
                        <b>DIRECTION:</b> Read the Email Below.
                    </div>

                    <?php } else {?>
                    <div>
                        <b>DIRECTION:</b> Read the question below. You have 30 minutes to plan, write and revise your essay. Typically and effective response will contain a minimum of 300 words.
                    </div>

                    <?php } ?>

                    <div class="line"></div>

                    <?php if ($row['images']) { ?>
                        <img style="max-width: 300px;" src="<?php echo getimglink($row['images'],'size4',1); ?>" alt="<?php echo $row['title']; ?>"/>
                    <?php } ?>
                    <div class="clear"></div>
                    <?php echo $row['question']; ?>
                </div>
                <div style="display: none;" class="question writing_answer">
                    <?php echo $useranswer[$row['writing_id']]['content']; ?>
                </div>
            </div>
            <div class="question_answer">
                <div class="sp_title">
                    Model answer
                    <ul id="answer_count_<?php echo $i ?>">
                    </ul>
                </div>
                <div class="answer_content writing_content" id="wanswer_content_<?php echo $i ?>">
                    <div style="height: 350px; margin-right: 9px;">
                        <?php echo $row['answer'] ?>
                    </div>
                </div>
            </div>
            <?php
            // phan danh cho giao vien
            if ($permission == 1 && $useransweritem['answer_id'] > 0) {?>
            <div class="point_teacher" onclick="writing.point_form(<?php echo $useransweritem['answer_id'] ?>)">Chấm điểm</div>
            <?php } ?>
            <div class="point_area" id="point_view_<?php echo $useransweritem['answer_id'] ?>"><?php echo $useransweritem['point_teacher']; ?></div>
            <div style="display: none;" class="point_area_comment" id="point_comment_<?php echo $useransweritem['answer_id'] ?>"><?php echo nl2br($useransweritem['comment_teacher']); ?></div>
            <?php if ($useransweritem['comment_teacher']) {?>
                <div class="viewcomment" onclick="show_teacher_comment(<?php echo $useransweritem['answer_id'] ?>)">Xem nhận xét</div>
            <?php } ?>
        </div>
        <?php $i ++;} ?>
        <div class="question_number">
            <ul>
            <?php
            $i = 1;
            foreach ($question as $k => $q) { ?>
                <li onclick="change_answer(<?php echo $i ?>)" class="answer_number<?php if($i == 1) echo ' selected'; ?>" id="answer_number_<?php echo $i ?>"><?php echo $q ?></li>
            <?php
            $i ++;
            } ?>
            </ul>
        </div>
        <div class="button_exit"><a href="/users/writing"><img width="75px" height="30px" alt="Exit" title="Exit" src="<?php echo $this->config->item("theme"); ?>images/button/speaking_exit.png"/></a></div>
    </div>
    <div class="opacity"></div>
    <div id="point_teacher_form" title="Chấm điểm học viên"></div>
    <div id="comment_teacher_view" title="Nhận xét của giáo viên"></div>
</div>
<script>
$(document).ready(function() {
    var count = 1;
    <?php for ($j = 1; $j < $i ; $j ++ ) { ?>
    $("#wanswer_content_<?php echo $j ?>").find(".wanswer").each(function(index){

        $(this).addClass("answer_js_"+count);
        if (index > 0){
            $(this).hide();
            var $content = '<li class="answer_count answer_count_'+count+'" onclick="show_answer('+count+',<?php echo $j ?>)">'+(index + 1)+'</li>';
        }
        else{
           var $content = '<li class="answer_count answer_count_'+count+' selected" onclick="show_answer('+count+',<?php echo $j ?>)">'+(index + 1)+'</li>';
        }

        $("#answer_count_<?php echo $j ?>").append($content);
        ///////////////////////////
        count ++;
    });
    <?php } ?>
});
function show_answer(i,j){
    $("#answer_count_"+j).find(".answer_count").removeClass("selected");
    $("#answer_count_"+j).find(".answer_count_"+i).addClass("selected");

    $("#wanswer_content_"+j).find(".wanswer").hide();
    $("#wanswer_content_"+j).find(".answer_js_" + i).show();
}
function show_tab_script(id){
    var tabs = $("#speaking_tab_" + id).find(".writing_question");
    if (tabs.is(':hidden')){
        tabs.show();
        $("#speaking_tab_" + id).find(".writing_answer").hide();
    }
    else {
        tabs.hide();
        $("#speaking_tab_" + id).find(".writing_answer").show();
    }
}
</script>