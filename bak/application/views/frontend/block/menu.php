<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!-- MENU TOP -->
<div id="menu<?php echo $block['position']; ?>">
    <?php if (DEVICE_ENV == 1 && $block['position'] == 'left') {?>
    <div id="menu_drop_icon">
        <img src="<?php echo $this->config->item("img") ?>mobile/menu_drop_icon.png" alt="Menu drop">
    </div>
    <?php } ?>
    <?php if ($params['title']) { ?>
        <h3 class="title"><?php echo $block['name'] ?></h3>
    <?php } ?>
    <ul class="ulmenu1">
        <?php
        $i = 1;
        /* if ($block['position'] == 'top') {
            var_dump('<pre>',$rows); die;
        }*/
        foreach ($rows as $row) {
        ?>
        <li class="level1<?php echo ($row['selected']) ? ' selected' : ''; echo ($i == 1) ? ' first' : '' ?>">
            <div class="title"><a class="link1" target="<?php echo $row['target'] ?>" href="<?php echo ($row['link'] && $row['link'] != '#') ? $row['link'] : 'javascript:void(0);' ?>"><?php echo $row['name'] ?></a></div>
            <?php if ($row['child']){
                ?>
                <ul class="menuchild">
                    <?php foreach ($row['child'] as $child) {
                        
                        $islevel3 = (DEVICE_ENV != 1 && $child['child']) ? 1 : 0;
                    ?>
                    <li>
                    <span><a class="menu_level2" rel="<?php echo $child['menu_id']; ?>" target="<?php echo $child['target'] ?>" href="<?php echo (!$islevel3) ? $child['link'] : 'javascript:void()' ?>"><?php echo $child['name'] ?></a></span>
                    <?php if ($child['child']) {
                        $level3 = 1;
                        ?>
                        <ul id="level_3_<?php echo $child['menu_id'] ?>" class="level3" <?php if ($child['selected'] == 1) echo 'style="display: block;"' ?>>
                            <?php foreach ($child['child'] as $sub) {?>
                            <li><a target="<?php echo $sub['target'] ?>" class="link3" href="<?php echo $sub['link'] ?>"><?php echo $sub['name'] ?></a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    </li>
                    <?php } ?>

                </ul>
            <?php } ?>
        </li>
        <?php
        $i ++;
        } ?>
    </ul>
    <div class="clear"></div>
</div>
<?php if ($level3 == 1) { ?>
<script>
    $(".menu_level2").bind("click",function(){
        var rel = $(this).attr("rel");
        $("#level_3_"+rel).toggle();
    });
</script>
<?php } ?>