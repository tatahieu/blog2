<aside class="ranking m-b-2" id="vinhdanhhocvien">
    <h3 class="aside-head">
        Học viên <strong>điểm cao</strong>
    </h3>

    <div class="ranking-body">
        <ul class="list-unstyled m-b-0">
            <?php foreach ($rows as $key => $rows){?>
                <li>
                    <div class="media">
                        <div class="media-left p-r-1">
                            <a href="javascript:;">
                                <img class="media-object" src="<?php echo getimglink($rows['images'],'size2');?>" alt="<?php echo $rows['title'] ?>">
                            </a>
                        </div>

                        <div class="media-body media-middle">
                            <h4 class="media-heading">
                                <a href="<?php echo $rows['link'] ?>"><?php echo $rows['title'] ?></a>
                            </h4>
                            <p class="m-b-0">
                                Điểm thi đầu vào: <?php echo $rows['diemthi_dauvao'] ?>
                            </p>
                            <p class="m-b-0">
                                Điểm thi IIG: <?php echo $rows['diemthi_iig'] ?>
                            </p>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</aside>