<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ($block['position']== 'static') {
    if (!$this->session->userdata("popupstatic6")) {
    $this->session->set_userdata("popupstatic6",1);
    ?>
    <div id="static_<?php echo $block['position'] ?>" style="display: none;">
        <?php echo html_entity_decode($block['content']) ?>
    </div>
    <script>
    $(document).ready(function () {
        setTimeout(function() {
            $.fancybox({
                'href': '#static_<?php echo $block['position'] ?>',
                padding: 0,
                scrolling: 'no',
                beforeShow: function(){
                  $(".fancybox-skin").css("background","none");
                 }
            });
        }, 1000);
        
    });
    </script>
    <?php
    }
} else {
?>
<div id="static_<?php echo $block['position'] ?>">
    <?php if ($params['title']) { ?>
        <h3 class="title"><?php echo $block['name'] ?></h3>
    <?php } ?>
    <div class="content">
        <?php echo html_entity_decode($block['content']) ?>
    </div>
</div>
<?php } ?>