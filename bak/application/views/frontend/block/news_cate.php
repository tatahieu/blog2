<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="news_cate_<?php echo $block['position'] ?>">
    <?php if ($params['title']) { ?>
    <div class="title">
        <h3><?=$block['name']?></h3>
    </div>
    <?php } ?>
    <div class="list_news">
        <ul>
            <?if (!empty($rows)){
            $i = 1;
            foreach ($rows as $rows){?>
                <li class="clearfix">
                    <?php if ($rows['images'] != ''){ ?>
                        <a href="<?php echo $rows['share_url']; ?>">
                            <img src="<?php echo getimglink($rows['images'],"size2") ?>" alt="<?php echo $rows['title'] ?>"/>
                        </a>
                    <?php } ?>
                    <h2><a href="<?php echo $rows['share_url']; ?>">
                    <?php echo $rows['title'] ?>
                    </a></h2>
                </li>
            <?$i++;}}?>
        </ul>
    </div>
</div>