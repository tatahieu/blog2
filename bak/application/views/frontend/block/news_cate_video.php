<aside class="aside-video m-b-2" id="video">
    <h3 class="aside-head">
        Chuỗi Video bài giảng <strong>Ms Hoa</strong>
    </h3>

    <div class="aside-video-body">
        <ul class="list-unstyled m-b-0">
            <?php foreach ($rows as $key => $rows){?>
                <li>
                    <div class="media">
                        <div class="media-left p-r-1">
                            <a class="thumb popup-youtube" href="<?php echo $rows['description'] ?>">
                                <img class="media-object" src="<?php echo getimglink($rows['images'],'size3');?>" alt="<?php echo $rows['title'] ?>">
                            </a>
                        </div>

                        <div class="media-body media-middle">
                            <h4 class="media-heading m-b-0">
                                <a href="<?php echo $rows['description'] ?>" class="popup-youtube"><?php echo $rows['title'] ?></a>
                            </h4>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</aside>
<script type="text/javascript">
    $(document).ready(function() {
        $('.popup-youtube').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,

          fixedContentPos: false
        });
    });
</script>