<section id="camnhanhocvien" class="testimonial-2 p-y-3">
    <div class="container">
        <h2 class="heading heading-1 m-b-2 m-t-0">
            <?php echo $block['name'] ?>
        </h2>

        <div class="testimonial-body">
            <div class="row">
                <?php foreach ($rows as $key => $rows){?>
                    <div class="col-sm-6">
                        <div class="media">
                            <div class="media-left p-r-1">
                                <a href="<?php echo $rows['link'] ?>">
                                    <img class="media-object img-responsive" src="<?php echo getimglink($rows['images'],'size2',3);?>" alt="">
                                </a>
                            </div>

                            <div class="media-body media-middle">
                                <h4 class="media-heading">
                                    <a href="<?php echo $rows['link'] ?>"><?php echo $rows['title'] ?></a>
                                </h4>
                                <p class="m-b-0">
                                    <?php echo cut_text($rows['description'],350)?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section> <!--.human-resources-->