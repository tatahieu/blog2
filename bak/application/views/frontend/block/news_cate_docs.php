<aside class="aside-doc" id="tailieu">
    <h3 class="aside-head">
        Tài liệu TOEIC không nên bỏ qua
    </h3>

    <div class="aside-doc-body">
        <ul class="list-unstyled m-b-0">
            <?php foreach ($rows as $key => $rows){?>
                <li>
                    <div class="media">
                        <div class="media-left p-r-1">
                            <a class="thumb-video" target="_blank" href="<?php echo $rows['link'] ?>">
                                <img class="media-object" src="<?php echo getimglink($rows['images'],'size3');?>" alt="<?php echo $rows['title'] ?>">
                            </a>
                        </div>

                        <div class="media-body media-middle">
                            <h4 class="media-heading m-b-0">
                                <a target="_blank" href="<?php echo $rows['link'] ?>"><?php echo $rows['title'] ?></a>
                            </h4>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</aside>