<section class="human-resources p-y-3">
    <div class="container">
        <h2 class="heading heading-1 m-b-2 m-t-0">
            <?php echo $block['name'] ?>
        </h2>

        <div class="row hr-slick slick-arrow-chevron">

            <?php foreach ($rows as $key => $rows){?>

                <div class="item col-md-3 text-center">
                    <a class="item-thumbnail img-circle" href="javascript:;">
                        <img src="<?php echo getimglink($rows['images'],'size4');?>" alt="<?php echo $rows['title'] ?>">
                    </a>

                    <h3 class="item-heading"><?php echo $rows['title'] ?></h3>

                    <p class="item-excerpt m-b-1"><?php echo cut_text($rows['description'],350)?></p>
                </div>

            <?php } ?>
        </div>
    </div>
</section> <!--.human-resources-->