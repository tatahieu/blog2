<div class="breadcrumb" id="breadcrumb">
    <a href="<?php echo BASE_URL ?>">Home</a>
    <?php
    $count = count($rows);
    $i = 1;
    foreach ($rows as $row) { ?>
        <span class="divider">/</span>
        <?php
        if ($row['link']) { ?>
            <a href="<?php echo $row['link'] ?>"><?php echo  $row['name'] ?></a>
        <?php }
        else { ?>
            <?php echo $row['name'] ?>
        <?php }
        $i ++;
    }?>
</div>