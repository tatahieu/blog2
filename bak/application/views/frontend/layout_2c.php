<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
<meta property="fb:app_id" content="451381954966271"/>
<meta property="fb:admins" content="1109703477"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<a href="https://plus.google.com/106437603903530473547" rel="publisher" />
<title><?=$title?></title>
<?
$metakey = (isset($metakey) || $metakey == "") ? $metakey : $this->config->item("metakey");
$metades = (isset($metades) || $metakey == "") ? $metades : $this->config->item("metadesc");
?>
<?php if ($ogimage) {?>
<meta property="og:image" itemprop="thumbnailUrl" content="<?php echo $ogimage; ?>" />
<?php } ?>
<?php if ($ogtitle) {?>
<meta property="og:title" content="<?php echo $ogtitle; ?>" />
<?php } ?>
<?php if ($ogurl) {?>
<meta property="og:url" content="<?php echo $ogurl; ?>" />
<?php } ?>
<?php if ($ogdescription) {?>
<meta property="og:description" content="<?php echo $ogdescription; ?>" />
<?php } ?>
<link href="<?php echo $this->config->item("img")?>favicon.ico" rel="shortcut icon" type="image/ico" />
<meta name="keywords" content="<?=$metakey?>" />
<meta name="description" content="<?php echo htmlspecialchars($metades)?>"/>
<? get_array($common); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29346056-1', 'mshoatoeic.com');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
    if (FlashDetect.installed == true) {
      var FLASH_DETECT = FlashDetect.installed;
    }
</script>
<!-- Google Tag Manager -->
<!--<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PGNBG2K');</script>-->
<!-- End Google Tag Manager -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M4NP4ND');</script>
<!-- End Google Tag Manager -->
<script>ga('send', 'event', {
 eventCategory: 'ggform',
 eventAction: 'click',
 eventLabel: 'gglead',
 eventValue: 10});</script>
</head>
<body>
 <div class="wrapper">
    <div id="header" class="clear">
    	<div class="logo fl">
    		<a href="<?php echo BASE_URL ?>"><img src="<?php echo $this->config->item("img") ?>logo.jpg" alt="Luyen thi toeic, Luyện thi toeic 4 kỹ năng, Ms Hoa" title="Luyen thi toeic, Luyện thi toeic 4 kỹ năng, Ms Hoa" /></a></div>
    	<div class="slogan fl">
            <img src="<?php echo $this->config->item("img") ?>slogan.gif" alt="Slogan"/>
    	</div>
        <?php echo $this->load->view("users/mini_login"); ?>
    </div>
    <div class="topposition">
        <?php get_array($top); ?>
    </div>
   <div id="main_content" class="clearfix" >
   <div id="cl_left">
		 <div id="cl_left1" class="fl" >
            <?php get_array($left); ?>
		</div>
		<div id="cl_left2" class="fr twocolum" style="width: 757px; padding-top: 10px;">
            <?=$content_for_layout?>
		</div>
        <div class="clearfix"></div>
        <div class="endleft"></div>
	</div>
	<div class="clearfix"></div>
	<?php get_array($footer); ?>
</div>
<div id="fb-root"></div>
<?php if (!$offSupport) {?>
<div id="support_online"></div>
<div id="support_facebook" style="overflow-y: auto;">
    <div class="support_close"><img src="<?php echo $this->config->item("img"); ?>close-lb.gif"></div>
    <div class="fb-comments" data-height="auto" data-width="510px" data-href="<?php echo BASE_URL; ?>tu-van.html"  data-num-posts="3"></div>
</div>
<?php } ?>
<?php get_array($static); ?>
<div id="message_alert" title="Thông báo"></div>
    <?php
    get_array($script)?>

<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '1567859360112952']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '660105501004281'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=660105501004281&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1567859360112952&amp;ev=PixelInitialized" /></noscript>
<!-- Google Tag Manager (noscript) -->
<!--<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PGNBG2K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>-->
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M4NP4ND"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>
<?php
if (ENVIRONMENT == 'development'){
$sections = array(
    'config'  => false,
    'http_headers' => false
    );

$this->output->set_profiler_sections($sections);
$this->output->enable_profiler(TRUE);
}
?>