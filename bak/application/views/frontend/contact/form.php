<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<form action="" method="POST">
<div id="register">
    <div class="mod_title"><?=$this->lang->line('contact_title'); ?></div>
    <div class="line"></div>
    <div class="main_content box_register">
        <?php echo validation_errors(); ?>
        <div class="form_item"><span><?php echo $this->lang->line('contact_fullname'); ?></span><?php echo $fullname?></div>
        <div class="form_item"><span><?php echo $this->lang->line('contact_email'); ?></span><?php echo $email?></div>

        <div class="form_item"><span><?php echo $this->lang->line('contact_phone'); ?></span><?php echo $phone?></div>
        <div class="form_item"><span><?php echo $this->lang->line('contact_content'); ?></span><?php echo $content?></div>
        <div class="form_item captcha"><span><?php echo $this->lang->line('contact_captcha'); ?></span><?php echo $captcha?><?php echo $security_code?></div>
        <div align="center"><?php echo $submit?></div>
    </div>
</div>
</form>