<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<div id="contact_result">
    <div class="mod_title"><?=$title ?></div>
    <div class="line"></div>
    <div class="main_content">
        <?=$result?>
    </div>
</div>