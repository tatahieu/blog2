<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //debug($rows); ?>
<div id="course_complete">
    <div class="title">
        Listen and Fill in the Blanks. Topic <?php echo $topicdetail['name']; ?>
    </div>
    <div class="pagingcomplete" title="Bài tập tiếp">
        <?php echo $paging; ?>
    </div>
    <div class="clear"></div>
    <div class="line clear" style="margin-top: 10px;"></div>
    <div class="cp_border_top"></div>
    <div class="cp_content">
        <div class="cp_padding">

            <div class="wccontent">
            <?php
            if ($rows) {
            $i = 1;
            foreach ($rows as $row) { ?>
            <div style="display: none;" id="course_com_<?php echo $row['com_id'] ?>" class="item ques_<?php echo $i ?>">
                <div class="question clearfix">
                    <?php echo $i ?>. <?php echo str_replace('[.]','<input type="text" autocomplete="off">',$row['question'])  ?>
                </div>
                <div class="audio" style="display: none;"><?php echo $row['audio'] ?></div>
                <div class="images"><img src="<?php echo getimglink($row['images'],'size3')?>"/></div>

            </div>
            <?php $i++;} ?>
            <div id="mediaplayer" class="player"></div>
            <?php if ($i > 2) { ?>
            <div id="pagingjs" class="global-tab-container clearfix">
                <div style="float: right;">
                <?php for ($j = 1; $j < $i; $j ++) { ?>
                    <div class="inactive-button page_<?php echo $j ?>"><?php echo $j ?></div>
                <?php } ?>
                </div>
            </div>
            <?php } ?>
            <div class="clear">
                <div class="submit row">
                    <div id="complete_result" class="col-xs-6 col-sm-3 global-button-green-1"><span>Đáp án</span></div>
                    <div id="complete_check" class="col-xs-6 col-sm-3 global-button-green-1"><span>Chấm điểm</span></div>
                    <div id="complete_next" class="col-xs-6 col-sm-3 global-button-green-1"><span>Sau</span></div>
                    <div id="complete_prev" class="col-xs-6 col-sm-3 global-button-green-1"><span>Trước</span></div>
                </div>
            </div>
            <?php } else { ?>
            Bài tập đang được cập nhật, vui lòng quay lại sau
            </div>

            <?php } ?>
            <div id="wordtopic">
                <div class="topictitle mod_title">Chủ đề</div>
                <div class="line"></div>
                <ul>
                <?php foreach ($topicall as $row) {?>
                    <li <?php if ($row['topic_id'] == $topicdetail['topic_id']) echo 'class="active"'; ?>><a href="/course/luyen-nghe-tieng-anh/<?php echo $level; ?>/<?php echo $row['alias']; ?>"><?php echo $row['name']; ?></a></li>
                <?php } ?>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="cp_border_bottom"></div>
</div>
<script>
    $(function(){
        if (typeof(course_complete) != "undefined"){
            course_complete.total = <?php echo $i - 1 ?>;
            course_complete.answer = {
            <?php
            $i = 1;
            foreach ($rows as $row) { ?>
            <?php
            if ($i != 1) echo ',';
            echo $row['com_id'] ?> : <?php echo $row['answer']; ?>
            <?php $i ++; } ?>
            }
            course_complete.run();
        };
    });
</script>