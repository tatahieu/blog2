<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="word_title" class="clearfix">
    <div class="title">Học từ mới thông qua chủ đề: <?php echo $topicdetail['name']; ?></div>
    <div class="pagingcomplete fr" title="Bài tập tiếp">
        <?php echo $paging; ?>
    </div>
    <div class="clear"></div>
    <div class="line clear"></div>
    <ul>
        <li rel="word_list" class="lam_quen">Làm quen</li>
        <li rel="word_drop" class="ghi_nho">Ghi nhớ</li>
        <li rel="retype" class="end thuoc_long">Học thuộc lòng</li>
    </ul>

</div>
<div class="cp_border_top"></div>
<div class="cp_content">
    <div class="cp_padding">
        <div id="word_list" class="course_word_item">
            <div class="global-activity-guide">
                <div class="text">Bấm chọn hình thu nhỏ để xem cỡ chuẩn. Bấm vào ảnh lớn để lật thẻ xem chi tiết của từ.</div>
            </div>
            <div class="left">
                <ul class="clear">
                <?php
                $i = 1;
                foreach ($rows as $row) { ?>
                <li>
                    <img class="thumb<?php echo $i ?><?php if ($i == 1) echo ' active' ?>" rel="<?php echo $i ?>" alt="<?php echo $row['word'] ?>" src="<?php echo getimglink($row['images'],'size2') ?>"/>
                </li>
                <?php $i ++;} ?>
                </ul>
            </div>
            <div class="right">
                <?php
                $i = 1;
                foreach ($rows as $row) {?>
                <div <?php echo ($i > 1) ? 'style="display: none"' : '' ?> class="item data<?php echo $i ?>">
                    <div class="image">
                        <img alt="<?php echo $row['word'] ?>" src="<?php echo getimglink($row['images'],'size4') ?>"/>
                        <div class="img-caption"><?php echo $row['word'] ?></div>
                    </div>
                    <div class="translate" style="display: none;">
                        <div class="title-flip-outer"><?php echo $row['word'] ?></div>
                        <div class="word-phonetic"><?php echo $row['phienam'] ?></div>
                        <div class="vietnamese-meaning"><?php echo $row['vietnamese'] ?></div>
                        <div class="subject">Nghĩa Tiếng Anh</div>
                        <div class="english-content"><?php echo $row['english'] ?></div>
                        <div class="example">
                            <span class="subject">Ví dụ</span>:
                            <span class="example-content"><?php echo $row['exam'] ?></span>
                        </div>
                    </div>
                </div>
                <?php $i++;} ?>
                <div class="tool clearfix">
                    <div class="record" title="Ghi âm"></div>
                    <div class="play" title="Nghe lại thu âm"></div>
                    <div class="stop" title="Dừng thu âm"></div>
                    <div class="split"></div>
                    <div class="speaker" title="Nghe từ"></div>
                    <div class="flip-card" title="Lật thẻ để xem thêm thông tin"></div>
                    <div class="split"></div>
                    <div class="btn-next" title="Tranh sau" style="opacity: 1; cursor: pointer;"></div>
                    <div class="btn-prev" title="Tranh trước" style="cursor: pointer; opacity: 1;"></div>
                    <div class="global-clear-both"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="word_drop" class="course_word_item" style="display: none;">
            <div class="global-activity-guide">
                <div class="text">Kéo thả từ vào tranh tương ứng.</div>
            </div>
            <div class="left">
                <?php $i = 1;
                shuffle ($rows);
                foreach ($rows as $row) {?>
                <div class="word_item">
                    <?php echo $row['word'] ?>
                </div>
                <?php $i ++; } ?>
            </div>
            <div class="right">
                <?php $i = 1;
                shuffle ($rows);
                foreach ($rows as $row) {?>
                <div id="pic_<?php echo $row['word_id'] ?>" class="word_drop droppable">
                    <img alt="<?php echo $row['english'] ?>" src="<?php echo getimglink($row['images'],'size4') ?>"/>
                </div>
                <?php $i ++; } ?>
            </div>
            <div class="clearfix">
                <div class="submit">
                    <div id="droprefresh" class="global-button-green-1">Làm lại</div>
                    <div id="loadResultDrop" class="global-button-green-1">Hoàn thành</div>
                    <div id="loadCheckDrop" class="global-button-green-1">Đáp án</div>
                </div>
            </div>
        </div>
        <div id="retype" class="course_word_item" style="display: none;">
            <div class="global-activity-guide">
                <div class="text">Nhập nghĩa tiếng Anh và nhấn phím Enter để kiểm tra đáp án.</div>
            </div>
            <div class="left">
                <div class="result-title-bar">Kết quả</div>
                <div class="result-content-outer">
                    <div class="remain">
                        <div class="text">Còn lại</div>
                        <div class="number global-float-right"><?php echo $i - 1 ?></div>
                    </div>
                    <div class="correct">
                        <div class="text">Chính xác</div>
                        <div class="number global-float-right">0</div>
                    </div>
                    <div class="incorrect">
                        <div class="text">Không chính xác</div>
                        <div class="number global-float-right">0</div>
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="img-wrapper">
                <?php $i = 1;
                shuffle ($rows);
                foreach ($rows as $row) {?>
                    <img id="type_<?php echo $row['word_id'] ?>" class="img-main item<?php echo $i ?>" style="display: <?php echo ($i == 1) ? 'block' : 'none' ?>;" alt="<?php echo $row['english'] ?>" src="<?php echo getimglink($row['images'],'size4') ?>"/>
                <?php $i ++; } ?>
                    <div class="answer_help"></div>
                    <div class="answer_result"></div>
                </div>
                <div class="user-input">
                    <input title="Ấn enter để kiểm tra đáp án" class="textbox global-float-left"/>
                    <div title="Nghe lại" class="listen-again global-float-left"></div>
                    <a title="Xem đáp án" onclick="void(0)" class="show-answer"></a>
                </div>
            </div>
            <div class="clearfix">
                <div class="submit">
                    <div id="retype_next" class="global-button-green-1 col-xs-6 col-sm-6"><span>Sau</span></div>
                    <div id="retype_prev" class="global-button-green-1 col-xs-6 col-sm-6"><span>Trước</span></div>
                </div>
            </div>
        </div>
        <div id="wordtopic">
            <div class="topictitle mod_title">Chủ đề</div>
            <div class="line"></div>
            <ul>
            <?php foreach ($topicall as $row) {?>
                <li <?php if ($row['topic_id'] == $topicdetail['topic_id']) echo 'class="active"'; ?>><a href="/course/tu-vung-theo-chu-de/<?php echo $level; ?>/<?php echo $row['alias']; ?>"><?php echo $row['name']; ?></a></li>
            <?php } ?>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="cp_border_bottom"></div>
<div id="jplayer_word"></div>
<script>
    $(function(){

        //console.log(course.isFunction("word"));
        if (typeof(course) != "undefined"){
            course.result = {
                <?php
                $i = 1;
                foreach ($rows as $row) {
                if ($i != 1) echo ',';
                ?>
                'pic_<?php echo $row['word_id'] ?>' : '<?php echo $row['word'] ?>'
                <?php $i ++;} ?>
            };
            course.total = <?php echo $i - 1 ?>;
            course.word();
        }
    });
</script>
