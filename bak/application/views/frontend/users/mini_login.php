<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<div class="logo fl">

    <a href="<?php echo BASE_URL ?>"><img src="<?php echo $this->config->item("img") ?>logo.jpg" alt="<?php echo $this->config->item("site_title"); ?>" title="<?php echo $this->config->item("site_title"); ?>"/></a>

</div>

<div class="slogan fl">

    <img src="<?php echo $this->config->item("img") ?>slogan.gif" alt="Slogan"/>

</div>

<div class="frmsearch fl">

    <?php if($this->session->userdata('username') && $this->session->userdata('userid') ){?>

        <div id="logins">

            <div class="login"><b>Xin chào : 

                <a href="/users/profile">

                <font color="red"><?echo $this->session->userdata('username');?></font> </b><?echo anchor('users/logout','Logout')?>

                </a>

            </div>

        </div>

    <?}else{?>

        <div id="logins">

            <div class="login"><a href="/users/login">Đăng nhập</a> / <a href="/users/register">Đăng ký</a></div>

        </div>

        <!-- DANG NHAP NHANH -->

        <div id="mini_login" title="Đăng nhập" style="display: none;">

        	<form id="quick_login" method="POST" action="">

                <div id="quick_login_error"></div>

                <table width="100%">

                    <tr>

                        <td class="label">Tên đăng nhập:</td>

                        <td><input id="log_username" type="text" name="log_username" value=""/></td>

                    </tr>

                    <tr>

                        <td class="label">Mật khẩu:</td>

                        <td><input type="password" name="log_password" value=""/></td>

                    </tr>

                    <tr>

                        <td class="label">Nhớ mật khẩu:</td>

                        <td><input id="log_remember" type="checkbox" name="log_remember" value="1"/></td>

                    </tr>

                </table>

        	</form>

        </div>

        <!-- QUEN MAT KHAU -->

        <div id="user_forgot" title="Quên mật khẩu" style="display: none;">

                <div id="user_forgot_error"></div>

                <table width="100%">

                    <tr>

                        <td class="label">Email đăng ký:</td>

                        <td><input id="users_forgot_email" type="text" value=""/></td>

                    </tr>

                </table>

        </div>

    <?}?>

    <?php if ($this->session->userdata("class_name")){?>



        <div style="font-weight: bold; float: right;">Lớp học: <span style="color: red;"> <?php echo $this->session->userdata("class_name"); ?></span></div>

    <?php }?>

    <div class="formseach">

    	<form onsubmit="return check_search_submit()" action="/news/search" method="GET">

            <input type="text" name="keyword" id="quick_search_input" class="input_search" value="<?=$this->input->get('keyword')?>"/>

        	<button type="submit" style="height: 22px; border: 0pt none;" class="button_search"/></button>

        </form>

    </div>

</div>