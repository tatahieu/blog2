<div class="menu">
    <div class="icon"></div>
    Menu
</div>
<div class="slogan">
  <a href="<?php echo BASE_URL; ?>">
      <img src="<?php echo $this->config->item("img"); ?>mobile/slogan.png" alt="<?php echo $this->config->item("site_title"); ?>" />
  </a>
</div>
<div class="member">
    <?php if ($this->session->userdata("userid")) { ?>
    <a href="javascript:void(0)">
        <div class="icon icon_logined"></div>
        <?php echo $this->session->userdata("fullname"); ?>
    </a>
    <div class="member_extend">
        <a href="/users/profile">Thay đổi thông tin</a>
        <a href="/users/logout">Thoát</a>
    </div>
    <?php } else {?>
    <a href="javascript:void(0)">
        <div class="icon"></div>
        Login
    </a>
    <div class="member_extend">
        <a href="/users/login">Đăng nhập</a>
        <a href="/users/register">Đăng ký</a>
    </div>
    <?php } ?>
</div>
<div class="clear"></div>
<div class="search_box">
    <form method="GET" action="/news/search">
        <input type="text" name="keyword" value="" placeholder="Tìm kiếm ...">
    </form>
</div>