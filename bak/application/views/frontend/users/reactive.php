<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<form action="" method="POST">
    <?=validation_errors()?>
    <?=$this->lang->line('users_email'); ?>
    <?=$email?>
    <?=$this->lang->line('users_captcha')?><?=$input_captcha?><?=$captcha?>
    <?=$submit?>
</form>