<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="menuleft">
    <div class="title"><b>Khu vực thành viên</b></div>
    <ul class="menuchild">
        <li class="level2"><span>
            <a class="link1" href="/users/speaking">Những bài nói đã làm</a></span>
        </li>
        <li class="level2"><span>
            <a class="link1" href="/users/writing">Những bài viết đã làm</a></span>
        </li>
        <li class="level2"><span>
            <a class="link1" href="/users/profile">Thay đổi thông tin</a></span>
        </li>
    </ul>
</div>