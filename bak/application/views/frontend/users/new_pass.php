<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="register">
    <form method="POST" action="">
        <div class="mod_title">
            Đặt lại mật khẩu
        </div>
        <div class="line"></div>
        <div class="box_register" id="users_new_pass">
            <?=validation_errors()?>
            <div class="form_item"><span>Password: </span><?=$password?></div>
            <div class="form_item"><span>Password confirm: </span><?=$password_confirm?></div>
            <div class="form_item captcha"><span><?=$this->lang->line('users_captcha')?>: </span><?=$input_captcha?><?=$captcha?></div>
            <div class="form_item submit"><span><?=$submit?></span></div>

        </div>
    </form>
</div>