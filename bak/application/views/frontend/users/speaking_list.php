<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<div id="user_speaking">
    <table class="user_lists">
    	<tr>
    		<th>STT</th>
    		<th>Câu hỏi</th>
            <th>Thời gian</th>
            <th>Xem lại</th>
    	</tr>
    	<?php
    	if (!empty($rows)){
            $i = 0; $tmp = 0; $flag = 0;
        	foreach ($rows as $row){
                if ($row['time'] == $tmp){ // neu trung thoi gian
                    $flag = 1;
                }
                else{
                    $i ++; $flag = 0; $tmp = $row['time'];
                }
            ?>
        	<tr>
        		<td width="30px" align="center"><b><?php echo ($flag == 0) ? $i : '' ?></b></td>
                <td><?php echo $row['test_name'] ?></td>
                <td width="130px"><?php echo date('H:i:s | d/m/Y',$row['time'])  ?></td>
                <td width="60px" align="center"><a href="/speaking/answer/<?php echo $row['test_id'] ?>/<?php echo $row['time'] ?>"><img src="<?php echo $this->config->item("theme").'images/icons/view-icon.png' ?>"/></a></td>
        	</tr>
        	<?
        	}
    	}
        else{?>
        <tr>
            <td colspan="4" align="center">
                <p><b>Bạn chưa làm bài speaking nào</b></p>
            </td>
        </tr>
        <?php }?>
    </table>
    <?php echo $paging?>
</div>
