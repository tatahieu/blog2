<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="register">
    <form method="POST" action="">
        <div class="mod_title">
            <?=$this->lang->line('users_forgot_title')?>
        </div>
<form method="POST" action="">
    <div id="users_forgot_pass">
        <?=validation_errors()?>
        <div class="form_item"><span><?=$this->lang->line('users_forgot_email')?>: </span><?=$email?></div>
        <div align="center"><?=$submit?></div>
    </div>
</form>