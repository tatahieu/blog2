<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="register">
<form method="POST" action="">
    <div class="mod_title">
        <?php if ($this->session->userdata("userid")) { ?>
            <?php echo $this->lang->line("users_profile"); ?>
        <?php } else { ?>
            <?php echo $this->lang->line("users_register"); ?>
        <?php } ?>
    </div>
    <div class="line"></div>
    <?php if ($message) {?>
    <div class="error"><?php echo $message ?></div>
    <?php } ?>
    <?=validation_errors(); ?>
    <div class="box_register">
        <div class="form_item"><span>Full name</span><?=$fullname ?></div>
        <div class="form_item"><span>Username</span><?=$username ?></div>
        <div class="form_item"><span>Password</span><?=$password ?></div>
        <div class="form_item"><span>Confirm password</span><?=$password_confirm ?></div>
        <div class="form_item"><span><?=$this->lang->line('users_email'); ?></span><?=$email ?></div>
        <div class="form_item"><span>Mobile</span><?=$mobile ?></div>

        <div class="form_item"><span>Where do you live ?</span><?=$where_live ?></div>
        <div class="form_item"><span>Your birth day</span>Ngày: <?=$birthday_date ?> Tháng: <?=$birthday_month ?> Năm: <?php echo $birthday_year ?> </div>

        <div class="form_item"><span>Receive new article</span><?=$send_mail ?></div>
        <div class="form_item captcha"><span>Security code</span><?=$input_captcha ?>&nbsp;<?php echo $captcha['image']?></div>
        <div align="center"><?=$submit?></div>
    </div>
</form>
</div>