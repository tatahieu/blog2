<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
$state = ($this->input->get("refer")) ? '&state='.$this->input->get('refer') : '';
?>
<div id="register">
    <form method="POST" action="">
        <div class="mod_title">
            <?=$this->lang->line('users_login_title')?>
        </div>
        <div class="line"></div>
        <div class="box_register login">
            <?=validation_errors()?>
            <div class="form_item"><span><?=$this->lang->line('users_username')?>: </span><?=$username?></div>
            <div class="form_item"><span><?=$this->lang->line('users_password')?>: </span><?=$password?></div>
            <div class="form_item ext_login">
                <a href="/users/register">Đăng ký</a> | <a href="/users/forgot_pass">Quên mật khẩu</a>
            </div>
            <div class="submit" align="center"><?=$submit?></div>
            <div class="form_item social">
                <a class="facebook_login" href="https://www.facebook.com/dialog/oauth?client_id=<?php echo $this->config->item("facebook_app_id"); ?>&scope=email,public_profile&redirect_uri=<?php echo urlencode(BASE_URL.'users/loginsocial/facebook') ; ?><?php echo  $state; ?>">Facebook</a>
                <a class="google_login" href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo $this->config->item("google_app_id"); ?>&response_type=code&scope=email&redirect_uri=<?php echo urlencode(BASE_URL.'users/loginsocial/google') ?><?php echo  $state; ?>">Google</a>
            </div>
        </div>
    </form>
</div>