<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<div id="user_speaking">
    <table class="user_lists">
    	<tr>
    		<th>STT</th>
    		<th>Câu hỏi</th>
            <th>Thời gian</th>
            <th>Xem lại</th>
    	</tr>
    	<?php
    	if (!empty($rows)){
            $i = 1;
        	foreach ($rows as $row){
            ?>
        	<tr>
        		<td width="30px" align="center"><b><?php echo ($flag == 0) ? $i : '' ?></b></td>
                <td><?php echo $row['test_name'] ?></td>
                <td width="130px"><?php echo date('H:i:s | d/m/Y',$row['time'])  ?></td>
                <td width="60px" align="center"><a href="/writing/answer/<?php echo $row['test_id'] ?>/<?php echo $row['time'] ?>"><img src="<?php echo $this->config->item("theme").'images/icons/view-icon.png' ?>"</a></td>
        	</tr>
        	<?
            $i ++;
        	}
    	}
        else{?>
        <tr>
            <td colspan="4" align="center">
                <p><b>Bạn chưa làm bài writing nào</b></p>
            </td>
        </tr>
        <?php }?>
    </table>
    <?php echo $paging?>
</div>
