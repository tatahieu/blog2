<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="title">
	<?=$name?>
</div>
<div id="popup"></div>
<div id="table_test">
    <div class="close_fulltest" data-exit="<?php echo $exitlink;?>">
        <?=img("fulltest/close.jpg"); ?>
    </div>
    <form id="fulltest_form" action="/question/answer" method="POST">
    <div class="display_time_out clearfix">
       <div id="show_count_down">
           Total time: <span time="<?=$time?>" class="kkcount-down"></span>
       </div>
       <?php if ($type != 3){?>
       <div id="show_part" style="display: none;" class="fl">
            <a href="javascript:show_part(1)">Part 1</a> |
            <a href="javascript:show_part(2)">Part 2</a> |
            <a href="javascript:show_part(3)">Part 3</a> |
            <a href="javascript:show_part(4)">Part 4</a> |
            <a href="javascript:show_part(5)">Part 5</a> |
            <a href="javascript:show_part(6)">Part 6</a> |
            <a href="javascript:show_part(7)">Part 7</a>
       </div>
       <?}?>
    </div>
    <div class="table_bg">
        <div class="table-left">
                <div class="media">
                    <?php get_array($media)?>
                </div>
        </div>
        <div class="table-right">
            <div class="result">
                <table width="100%">
                <?php
                $j = 1; $c = 1;
                foreach ($answer as $a1){
                    foreach ($a1 as $a2){?>
                    <tr id="answer_<?=$j?>" class="full_answer">
                        <td align="right" valign="top"><b><?=$j?>.</b></td>
                        <?php
                        $k = 1;
                        foreach ($a2 as $a){?>
                        <td width="50px" class="testing_answer_<?=$a['question_id']?>">
                            <div class="qanswer_<?=$a['question_id']?>_<?=$k?>">
                            <input class="qid_answer_<?=$a['question_id']?>" type="radio"  name="answer_<?=$a['question_id']?>" value="<?=$k?>"/> <?=translate_answer($a['result']); ?>
                            <div class="answer_status fr" style="width: 12px;"></div>
                            </div>
                        </td>
                        <?
                        $k ++;
                        }
                        if ($k == 4) echo '<td width="45px"></td>';
                        ?>
                            <input type="hidden" name="question_id[]" value="<?=$a['question_id']?>"/>

                    </tr>
                    <?
                    $j ++;
                    }
                    $c ++;
                }?>
                    <tr>
                        <td colspan="5" height="40px" valign="bottom">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="button">
        <div class="button-left">
            <div class="current_view" style="display: none;">0</div>
            <button id="button_back_full" style="display: none;" class="bt_pr testing_next" onclick="fulltest_back()" type="button">BACK</button>
            <button id="button_next_full" class="bt_pr testing_next" onclick="fulltest_next()" type="button">NEXT</button>
        </div>
        <div class="button-right">
            <div class="score">Result: <span>0</span> / <?=$j-1?></div>
            <button id="submit_fulltest" class="bt_pr testing_submit" onclick="return send_answer_fulltest()" type="button">SCORE</button>
            <button class="bt_pr testing_submit" onclick="return location.reload();" type="button">AGAIN</button>
            <input type="hidden" name="fulltest_id" value="<?=$fullid?>"/>
            <input type="hidden" name="score_id" value="<?php echo $score_id; ?>">
            <input type="hidden" name="score_token" value="<?php echo $this->security->generate_token_post($score_id) ; ?>">
        </div>
    </div>
    <div id="monitor"></div>
</form>
</div>
<div id="dialog-message"></div>
<script>
    $(document).ready(function() {
        /** POPUP**/
        var height = $(window).height();
        var width = $(window).width();
        $("#table_test").css({left: ((width - 852) / 2)});
        $(".close_fulltest").bind("click",function(){
            var answer = confirm("Bạn chắc là muốn đóng cửa sổ này ?")
            if (answer){
                /**** CLOSE POPUP
                $("#popup").remove();
                $(".popup_image").remove();
                $(this).remove();
                ****/
                var url = $(this).attr("data-exit");
                if (url != ''){
                    redirect(url);
                } else {
                    history.go(-1);
                }

            }

        });
        /** ENDPOPUP **/
        fulltest_next();
        // count downtime
        function liftOff() {
            alert("Thời gian làm bài test đã hết");
            send_answer_fulltest();
        }
        shortly = new Date();
            shortly.setSeconds(shortly.getSeconds() + <?php echo ($total_time) ? $total_time : 0 ?>);
        $('#show_count_down').countdown('option', {until: shortly});
        $('#show_count_down').countdown({until: shortly,
        onExpiry: liftOff});

    });
    $(document).ready(function(){
        keep_alive_session();
    });
</script>