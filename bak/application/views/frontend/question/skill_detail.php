<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

?>

<div id="part_<?=$part?>" class="full" style="display: none;">

    <div class="quest_content">

    	<?=$test_info['description']?>

    </div>

    <?

    foreach ($question as $question){

        ?>

        <div id="question_<?=$start?>" class="waiting alias_answer" rel="<?=$start?>" style="display: none;">

            <div class="div_display clearfix">

        		<div class="stt"><b><?=$start?>.  </b></div>

                <?php

                if ($question['images'] != ''){?>

                <div class="div_img">

                    <img src="<?php echo getimglink($question['images'],'size4'); ?>" alt="<?php echo $question['title']; ?>"/>

                </div>

                <?}?>

                <?php if ($question['sound']){?>

        		<div class="media_bt">

                    <script type="text/javascript">

                        parser_audio('<?php echo get_file_url("audio/".$question['sound'])?>');
                        console.log('<?php echo get_file_url("audio/".$question['sound'])?>');
                    </script>

                </div>

                <?}?>

            </div>

            <?php if (!empty($answer[$question['question_id']]) && $show_answer == 1){?>

            <div class="result_hide result_choose">

        		<b>Question <?=$start?>: <?php if ($part == 2) echo $question['title'] ?></b>

                <?php

                $j = 1;

                foreach ($answer[$question['question_id']] as $a){?>

                <div class="anwser_hide_<?php echo $question['question_id']; ?>_<?php echo $j; ?>">

                    <div class="answer_status"></div>

                    <?=translate_answer($a['result']); ?>. <?php echo $a['content']?>

                </div>

                 <?

                 $j ++;

                 }?>

        	</div>

            <?}?>

        </div>

    <?

    $start ++;

    }?>

</div>