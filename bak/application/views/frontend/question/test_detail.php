<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="title">
	<?=$test_info['name']?> / <?=$test_info['title']?>
</div>
<div id="test_random" class="main_content list_and_read">
    <div class="quest_content">
    	<?=$test_info['description']?>
    </div>
    <form id="testing_form" action="/question/answer" method="POST">
    <?php
    $i = 1;
    foreach ($question as $question){?>
    <div class="item clearfix">

    	<div class="div_display">
    		<?php if ($question['images'] != ''){?>
            <div class="div_img">
                <img src="<?php echo getimglink($question['images'],'size4');?>" alt="<?php echo $question['title']; ?>"/>

            </div>
            <?}?>
            <?php if ($question['sound']){?>
    		<div class="media_bt">
                <script type="text/javascript">
                    parser_audio('<?php echo  get_file_url("audio/".$question['sound'])?>');
                </script>
            </div>
            <?}?>
    	</div>
    </div>
    <div id="testing_answer_<?=$question['question_id']?>" class="div_text result_choose">
		<b>Question <?=$i?>:</b>
        <?php
        $j = 1;
        foreach ($answer[$question['question_id']] as $a){?>
        <div class="item">
            <div class="answer_status"></div>
            <input type="radio" name="answer_<?=$question['question_id']?>" value="<?=$j?>"/> <?=translate_answer($a['result']); ?>. <span class="answer_content"><?=$a['content']?></span>
         </div>
         <?
         $j ++;
         }?>
	</div>
    <input type="hidden" name="question_id[]" value="<?=$question['question_id']?>"/>
    <?
    $i ++;
    }?>
    <div class="practice_bt">
        <div class="score">Score: <span>0</span>/<?=$i-1?></div>
		<button type="button" onclick="return send_answer_training(<?=$i-1?>)" class="bt_pr testing_submit">TAPESCRIPT</button>
        <button type="button" onclick="return tabscript(<?=$i-1?>)" class="bt_pr testing_submit">SCORE</button>
		<button onclick="location.reload();" type="button" class="bt_pr">AGAIN</button>
		<?php if (isset($next_button)){?>
        <a href="<?=$next_button?>"><button type="button" class="bt_pr">NEXT</button></a>
        <?}?>
    </div>
    </form>
    <div id="share">
        <ul class="share_social">
             <li>
                <div class="fb-like" data-href="<?php echo site_url($row['share_url']); ?>" data-width="100" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
             </li>
             <li>
                <a href="<?php echo current_url(); ?>" class="twitter-share-button" data-lang="en">Tweet</a>
             </li>
             <li class="google" style="60px">
                <div class="g-plusone" data-size="medium" data-href="<?php echo current_url(); ?>"></div>

             </li>
        </ul>
    </div>
    <div id="more_pactice">
    	<div class="title">Luyện tập thêm</div>
    	<?php if (!empty($relate)) {?>
        <ul>
            <?php 
            $i = 1;
            foreach ($relate as $relate){?>
    		<li class="<?php echo ($i % 2 == 1) ? 'cate_le' : 'cate_chan'; ?>">
                <a href="<?=site_url('question/test/'.$relate['qtest_id'])?>"><?=$relate['title']?></a>
            </li>
    		<?php $i ++; }?>

    	</ul>
        <?}?>
    </div>

    <div class="clearfix"></div>
    <?php if (isset($news)){?>
    <div id="news_other">
    	<div class="title">Bài viết liên quan:</div>
    	<ul>
            <?php 
            $i = 1;
            foreach ($news as $news){?>
    		<li class="<?php echo ($i % 2 == 1) ? 'cate_le' : 'cate_chan'; ?>">
            <a href="<?php echo $news['share_url'] ?>"><?php echo $news['title']; ?></a>
            </li>
            <?php $i++; }?>
    	</ul>
    </div>
    <?}?>
</div>