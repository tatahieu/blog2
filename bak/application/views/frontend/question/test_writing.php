<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="title">
	<?=$test_info['name']?> / <?=$test_info['title']?>
</div>
<div id="test_list" class="main_content">
<div class="quest_content">
	<?=$test_info['title']?>
</div>
<div class="quest_description" style="margin-bottom: 10px;">
    <?=$test_info['description']?>
</div>
   <?php
    $i = 1;
    foreach ($question as $question){?>
    <div id="testing_answer_<?=$question['question_id']?>" class="div_text">
        <div class="question_detail"><?=nl2br($question['title']); ?></div>
        <div class="left">

        </div>
        <div class="media" id="test_writing">
            <div class="answerwriting<?php if ($question['images'] != '') echo ' left fl' ?>">
                <textarea></textarea>
                <div class="show_answer" style="display: none;">
                    <?=$question['detail']?>
                </div>
            </div>
            <?php if ($question['images'] != ''){?>
    		<div class="answer fr">
                <img src="<?php echo getimglink($question['images'],'size2'); ?>" alt="<?php echo $question['title']; ?>"/>
            </div>
            <?}
            ?>
        </div>
	</div>
    <?
    $i ++;
    }?>
    <div class="practice_bt">
		<button type="button" onclick="send_answer_writing()" class="bt_pr testing_submit">ANSWER</button>
		<button onclick="location.reload();" type="button" class="bt_pr">AGAIN</button>
		<?php if (isset($next_button)){?>
        <a href="<?=$next_button?>"><button type="button" class="bt_pr">NEXT</button></a>
        <?}?>
  </div>
<div id="share" class="clear">
    <ul class="share_social">
         <li>
            <div class="fb-like" data-href="<?php echo current_url(); ?>" data-width="100" data-layout="button_count" data-show-faces="false" data-send="false"></div>
         </li>
         <li style="width: 90px;">
            <fb:share-button type="button_count" class="url" href="<?=current_url()?>"></fb:share-button>
         </li>
         <li>
            <a href="<?php echo current_url(); ?>" class="twitter-share-button" data-lang="en">Tweet</a>
         </li>
         <li class="google" style="60px">
            <div class="g-plusone" data-size="medium" data-href="<?php echo current_url(); ?>"></div>

         </li>

    </ul>
</div>
<div id="more_pactice">
	<h2>Luyện tập thêm</h2>
	<?php if (!empty($relate)) {?>
    <ul>
        <?php foreach ($relate as $relate){?>
		<li><a href="<?=site_url('question/test/'.$relate['qtest_id'])?>"><?=$relate['title']?></a></li>
		<?}?>

	</ul>
    <?}?>
</div>
<div class="clearfix"></div>
<!--
 <div id="news_other">
	<h2>Bài viết liên quan:</h2>
	<ul>
		<li><a href="#">Tôi đi Phượt</a></li>
		<li><a href="#">Hạnh phúc</a></li>
		<li><a href="#">Một sáng sớm với suy nghĩ mới</a></li>
	</ul>
</div>-->
</div>
<script>
$("#test_list .div_text").each(function(index, value){
    if (index % 2 == 0){
        $(this).addClass("chan");
    }
})
</script>