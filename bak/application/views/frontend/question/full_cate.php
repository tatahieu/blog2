<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if ($func == 'classfull' && check_class_speaking() === TRUE) { ?>
<ul id="class_test_tab" class="clear">
    <li class="active">Read and List</li>
    <li style="border-right: 1px solid #FFF;"><a href="/speaking/byclass/fulltest">Speaking</a></li>
    <li><a href="/writing/byclass/fulltest">Writing</a></li>
</ul>
<?php } ?>
<div class="title">
	<?=$name?>
</div>
<div id="cate_test" class="main_content">
    <?php 
    $i =1;
    foreach ($part as $part){?>
    <div class="testing_title cate_<?php echo ($i % 2 == 1) ? 'le' : 'chan'; ?>">
        <?php
            echo anchor("question/".$func."/".$part['full_id'],$part['name']);
        ?>
    </div>
    <?php $i++; }?>
</div>