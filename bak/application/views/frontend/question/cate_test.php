<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="title">
	<?php echo $cate_info['name']?>
</div>
<div id="cate_test" class="main_content">
    <?php 
    $i = 1;
    foreach ($test as $test){?>
    <div class="testing_title cate_<?php echo ($i % 2 == 1) ? 'le' : 'chan'; ?>">
        <?php
            if ($type == 1){
                echo anchor("question/test/".$test['qtest_id'],$test['title']);
            }
            elseif ($type == 2){
                echo anchor("question/skilltest/".$test['qtest_id'],$test['title']);
            }
        ?>
    </div>
    <?php $i++; }?>
</div>