<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if (check_class_speaking() === TRUE) { ?>
<ul id="class_test_tab" class="clear">
    <li class="active">Read and List</li>
    <li style="border-right: 1px solid #FFF;"><a href="/speaking/byclass/fullpart">Speaking</a></li>
    <li><a href="/writing/byclass/fullpart">Writing</a></li>
</ul>
<?php } ?>
<div class="title">
	Bài tập TOEIC Listening và TOEIC Reading<?php //echo $this->session->userdata("class_name"); ?>
</div>
<?php if ($row) {?>
<div id="cate_test" class="main_content">
    <?php 
    $i = 1;
    foreach ($row as $row){?>
    <div class="testing_title cate_<?php echo ($i % 2 == 1) ? 'le' : 'chan'; ?>">
        <a href="<?php echo "/question/test/".$row['qtest_id']; ?>"><?php echo $row['title']; ?></a>
    </div>
    <?php $i ++ ;}?>
    <?php echo $paging; ?>
</div>
<?php } ?>