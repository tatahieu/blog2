<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="title">
	<?=$name?>
</div>
<div class="popup_image">
    <form id="fulltest_form" action="/question/answer" method="POST">
    <div class="display_time_out clearfix">
       <div id="show_count_down" class="fr">
           Total time: <span time="<?=$time?>" class="kkcount-down"></span>
       </div>
       <?php if ($type != 3){?>
       <div id="show_part" style="display: none;" class="fl">
            <a href="javascript:show_part(1)">Part 1</a> |
            <a href="javascript:show_part(2)">Part 2</a> |
            <a href="javascript:show_part(3)">Part 3</a> |
            <a href="javascript:show_part(4)">Part 4</a> |
            <a href="javascript:show_part(5)">Part 5</a> |
            <a href="javascript:show_part(6)">Part 6</a> |
            <a href="javascript:show_part(7)">Part 7</a>
       </div>
       <?}?>
    </div>
        <div class="media list_and_read">
            <?=get_array($media)?>
        </div>
        <div class="result result_choose">
            <?php
            $j = 1; $c = 1;
            foreach ($answer as $a1){

                foreach ($a1 as $a2){?>
                <div id="answer_<?=$j?>" class="full_answer">
                    <b><?=$j?>.</b>
                    <?php
                    $k = 1;
                    foreach ($a2 as $a){?>
                   
                        <div class="qanswer_<?=$a['question_id']?>_<?=$k?>">
                            <div class="answer_status"></div>
                            <input class="qid_answer_<?=$a['question_id']?>" type="radio"  name="answer_<?=$a['question_id']?>" value="<?=$k?>"/> <?=translate_answer($a['result']); ?>
                            
                        </div>
                    
                    <?
                    $k ++;
                    }
                    if ($k == 4) echo '<td width="45px"></td>';
                    ?>
                    <input type="hidden" name="question_id[]" value="<?=$a['question_id']?>"/>

                </div>
                <?
                $j ++;
                }
                $c ++;
            }?>
        </div>
          
        <div class="full_nav">
            <div class="current_view" style="display: none;">0</div>
            <button id="button_back_full" style="display: none;" class="bt_pr testing_next" onclick="fulltest_back()" type="button">BACK</button>
            <button id="button_next_full" class="bt_pr testing_next" onclick="fulltest_next()" type="button">NEXT</button>
        </div>

        <div class="score">Result: <span>0</span> / <?=$j-1?></div>
        <div class="result_button">
            <button id="submit_fulltest" class="bt_pr testing_submit" onclick="return send_answer_fulltest()" type="button">SCORE</button>
            <button class="bt_pr testing_submit" onclick="return location.reload();" type="button">AGAIN</button>
            <input type="hidden" name="fulltest_id" value="<?=$fullid?>"/>
            <input type="hidden" name="score_id" value="<?php echo $score_id; ?>">
            <input type="hidden" name="score_token" value="<?php echo $this->security->generate_token_post($score_id) ; ?>">
            <div class="clear"></div>
        </div>
    <div id="monitor"></div>
    </form>
</div>
<div id="dialog-message"></div>
<script>
    $(document).ready(function() {
        /** ENDPOPUP **/
        fulltest_next();
        // count downtime
        function liftOff() {
            alert("Thời gian làm bài test đã hết");
            send_answer_fulltest();
        }
        shortly = new Date();
            shortly.setSeconds(shortly.getSeconds() + <?php echo $total_time ?>);
        $('#show_count_down').countdown('option', {until: shortly});
        $('#show_count_down').countdown({until: shortly,
        onExpiry: liftOff});
       
    });

    $(document).ready(function(){
        keep_alive_session();
    });
</script>