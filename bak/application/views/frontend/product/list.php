<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="product_list">
    <ul>
    <?php
        $i = 1;
        foreach ($arrProduct as $item){?>
        <li class="item<?php if ($i % 3 == 1) echo ' first' ?>">
            <?php
            if ($item['images'] != ''){ ?>
            <div class="pro_img">
                <a href="<?php echo SITE_URL.$item['share_url'] ?>">
                    <img src="<?php echo getimglink($item['images'],'size3') ?>" alt="<?php echo $item['title'] ?>"/>
                </a>
            </div>
            <?php }
            ?>
            <h2>
                <a href="<?php echo SITE_URL.$item['share_url'] ?>">
                    <?php echo $item['title'] ?>
                </a>
            </h2>
            <div class="description"><?php echo cut_text($item['description'],150)?></div>
            <div class="quick_order" onclick="add_to_cart(<?php echo $item['pro_id']?>)">
                <span><?php echo $this->lang->line('common_add_cart')?></span>
            </div>
            <div class="price_list">
                <?php echo price_format($item['price']); ?>
            </div>
        </li>
    <?php $i++; }
    ?>
    </ul>
</div>
<?php
echo $paging;
?>