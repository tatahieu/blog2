<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div id="product_detail">
    <h3><?php echo $productDetail['title']?></h3>
    <img src="<?php echo getimglink($productDetail['images'],'size3') ?>" alt="<?php echo $productDetail['title'] ?>"/>
    <div class="description">
        <?php echo $productDetail['description']?>
    </div>
    <div class="detail">
        <?php echo $productDetail['detail'] ?>
    </div>
    <!-- RELATE -->
    <?php if ($relate) { ?>
    <ul>
    <?php foreach ($relate as $key => $item) {?>
        <?php
        if ($item['images'] != ''){ ?>
        <div class="pro_img">
            <a href="<?php echo SITE_URL.$item['share_url'] ?>">
                <img src="<?php echo getimglink($item['images'],'size3') ?>" alt="<?php echo $item['title'] ?>"/>
            </a>
        </div>
        <?php } ?>
        <h2>
            <a href="<?php echo SITE_URL.$item['share_url'] ?>">
                <?php echo $item['title'] ?>
            </a>
        </h2>
    <?php } ?>
    </ul>
    <?php } ?>
</div>