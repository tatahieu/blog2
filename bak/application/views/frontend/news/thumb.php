<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if ($page != 'tags') { ?>
<h2 class="title">
    <?php echo $cate['name']?>
</h2>
<div class="line"></div>
<?php } ?>
<div id="videodetail">
    <h1><?php echo $detail['title']; ?></h1>
    <div class="player"><?php echo html_entity_decode($detail['detail']); ?></div>
    <div class="desc"><?php echo $detail['description']; ?></div>
    <div id="share">
        <ul class="share_social">
             <li style="width: 115px;">

                <div class="fb-like" data-href="<?php echo site_url($detail['share_url']); ?>" data-width="100" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>

             </li>
             <li>
                <a href="<?php echo site_url($detail['share_url']); ?>" class="twitter-share-button" data-lang="en">Tweet</a>
             </li>
             <li class="google" style="60px">
                <div class="g-plusone" data-size="medium" data-href="<?php echo site_url($detail['share_url']); ?>"></div>

             </li>
        </ul>
    </div>
</div>
<div id="comment" style="margin-left: 0; margin-bottom: 15px;">
    <div class="title_cm "><b>Post a Comment</b><div class="counts"><fb:comments-count href=<?=current_url()?>></fb:comments-count> Comment</div></div>
    <div class="comment_box">
        <div class="view_comment clear"></div>
        <div class="fb_comment" style="display: block;">
            <div class="fb-comments" data-height="auto" data-width="510px" data-href="<?=current_url()?>"  data-num-posts="3"></div>
        </div>
    </div>
</div>
<div class="container">
    <div id="news_thumb" class="row">
    <?php
        $i = 1;
        foreach ($rows as $rows){?>
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 item">
            <div class="thumb_news">
                <div class="video_img">
                <?php
                if ($rows['images'] != ''){?>
                    <a href="<?php echo $rows['share_url']; ?>">
                        <img src="<?php echo getimglink($rows['images'],'size3') ?>" alt="<?php echo $rows['title'] ?>"/>
                        <div class="videoicon"></div>
                    </a>
                <?php } ?>
                </div>
                <div class="video_title">
                    <a href="<?php echo $rows['share_url']; ?>"><?php echo $rows['title'] ?></a>
                </div>
            </div>
        </div>
        <?php 
        if ($i % 2 == 0) echo '<div class="clearfix visible-xs-block"></div>';
        if ($i % 3 == 0) echo '<div class="clearfix visible-sm-block visible-sm-block visible-md-block visible-lg-block"></div>';
        $i ++; }
    echo $paging;
    ?>
    </div>
</div>