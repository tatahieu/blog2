<xml version="1.0" encoding="utf-8"
<rss version="2.0"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:admin="http://webns.net/mvcb/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:content="http://purl.org/rss/1.0/modules/content/">

    <channel>

    <title><?php echo $feed_name; ?></title>

    <link><?php echo $feed_url; ?></link>
    <description><?php echo $page_description; ?></description>
    <dc:language><?php echo $page_language; ?></dc:language>
    <dc:creator><?php echo $creator_email; ?></dc:creator>

    <dc:rights>Copyright <?php echo gmdate("Y", time()); ?></dc:rights>
    <admin:generatorAgent rdf:resource="http://www.hanoicdc.com/" />

    <?php foreach($rows as $rows){ ?>

        <item>

          <title><?php echo xml_convert($rows['title']); ?></title>
          <link><?php echo site_url('news/detail/0/' . $rows['news_id'].'/'.set_alias_link($rows['title'])) ?></link>
          <guid><?php echo site_url('news/detail/0/' . $rows['news_id'].'/'.set_alias_link($rows['title'])) ?></guid>

          <description><![CDATA[
      <?=$rows['description']?>
      ]]></description>
      <pubDate><?php echo date ('r', strtotime($rows['date_up']));?></pubDate>
        </item>


    <?php } ?>

    </channel></rss>