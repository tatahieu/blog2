<?php
    $this->load->config('data');
    $local = $this->config->item('local');
?>
<?php foreach($arrData as $local_id => $cate) { ?>
    <div class="course-item m-b-2">
        <h2 class="course-head text-center">
            <?php echo $local[$local_id];?>
        </h2>

        <div class="table-responsive">
            <table class="course-table table table-bordered text-middle m-b-0">
                <thead>
                <tr>
                    <th>Khóa học</th>
                    <th>Lớp</th>
                    <th>Thời gian học</th>
                    <th>Lịch Khai giảng</th>
                    <th>Đăng ký</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach($cate as $cate_id => $article) { 
                    if(!$article){
                        break;
                    }
                    $count = count($article); 
                    $first_class = array_shift($article); ?>
                    <tr>
                        <th class="fit" rowspan="<?php echo $count;?>">
                            <a href="<?php echo site_url($arrCate[$cate_id]['link']);?>" class="table-title"><?php echo $arrCate[$cate_id]['name'];?></a> <br>
                            <small>(<?php echo $arrCate[$cate_id]['description'];?>)</small>
                        </th>
                        <?php if($first_class){?>
                        <td><span class="font14"><?php echo $first_class['title'];?></span></td>
                        <td>
                            <span class="font14"><?php echo nl2br($first_class['description']);?></span>
                        </td>
                        <td><span class="font14"><?php echo date('d/m/Y',$first_class['publish_time']);?></span></td>
                        <td>
                            <a class="btn btn-sm btn-primary btn-reg link-reg" data-id="<?php echo $first_class['news_id'];?>" data-local="<?php echo $local_id;?>" href="#form_register">Đăng ký</a>
                        </td>
                        <?php }?>
                    </tr>
                    <?php foreach($article as $article){?>
                    <tr>
                        <td><span class="font14"><?php echo $article['title'];?></span></td>
                        <td>
                            <span class="font14"><?php echo nl2br($article['description']);?></span>
                        </td>
                        <td><span class="font14"><?php echo date('d/m/Y',$article['publish_time']);?></span></td>
                        <td>
                            <a class="btn btn-sm btn-primary btn-reg link-reg" data-id="<?php echo $article['news_id'];?>" data-local="<?php echo $local_id;?>" href="#form_register">Đăng ký</i></a>
                        </td>
                    </tr>
                    <?php }?>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#crouse_list').on('click','.link-reg', function () {
            var local_id = $(this).attr('data-local');
            var news_id = $(this).attr('data-id');
            $('#reg2-form-control-6').val(local_id);
            $('#reg_news').val(news_id);
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 400);

            return false;
        });
    });
</script>