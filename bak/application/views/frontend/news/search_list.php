<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<h2 class="title">
    <?=$this->lang->line('search_keyword_for').'"'.$keyword.'"'?> <span><?=$total?><?=$this->lang->line('search_result')?></span>
</h2>
<div class="line"></div>
<div id="news_lists" class="main_content clearfix">
<?php
    $i = 1;
    foreach ($rows as $rows){?>
    <div class="item clearfix">
            <div class="img_news">
                <?php
                if ($rows['images'] != ''){?>
                    <a href="<?php echo $rows['share_url'] ?>">
                        <img src="<?php echo getimglink($rows['images'],'size2') ?>" alt="<?php echo $rows['title'] ?>"/>
                    </a>
                <?php } ?>
            </div>
            <div class="description">
                <a href="<?php echo $rows['share_url'] ?>"><?php echo $rows['title'] ?></a>
                <br />
                <?=cut_text($rows['description'],200)?>
            </div>
    </div>
    <? $i ++;
    }
echo $paging;
?>
</div>