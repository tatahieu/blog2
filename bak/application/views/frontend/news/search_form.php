<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="quicksearch">
	<form onsubmit="return check_search_submit()" action="<?=site_url('news/search')?>/" method="GET">
    <input type="text" name="keyword" id="quick_search_input" class="input_search" style="border: 0; padding-left: 3px; height: 22px;" value="<?=$this->input->get('keyword')?>"/>
	<button type="submit" style="height: 22px; border: 0pt none;" class="button_search"/></button>
    </form>
</div>
<script>
    function check_search_submit(){
        if ($("#quick_search_input").val().length < 3){
            alert("<?=$this->lang->line('common_error_blank')?>");
            $("#quick_search_input").focus();
            return false;
        }; 
    }
</script>