<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php if ($page != 'tags') { ?>
<h2 class="title">
    <?php echo $cate['name']?>
</h2>
<div class="line"></div>
<?php } ?>
<div id="news_lists" class="main_content clearfix">
<?php
    $i = 1;
    foreach ($rows as $rows){?>
    <div class="item clearfix">
            <div class="img_news">
                <?php
                if ($rows['images'] != ''){?>
                    <a href="<?php echo $rows['share_url'] ?>">
                        <img src="<?php echo getimglink($rows['images'],'size2') ?>" alt="<?php echo $rows['title'] ?>"/>
                    </a>
                <?php } ?>
            </div>
            <div class="description">
                <a href="<?php echo $rows['share_url'] ?>"><?php echo $rows['title'] ?></a>
                <br />
                <?php echo cut_text($rows['description'],200)?>
            </div>
    </div>
    <? $i ++;
    }
echo $paging;
?>
</div>
<?php if (DEVICE_ENV == 1) {?>
<div id="bottom_support">
    <a class="r_phone r_blue" href="tel:0969264966"><i class="vcon-phone"></i> 0969264966</a>
    <a class="r_sms r_blue" href="sms:0969264966"><i class="vcon-sms"></i> SMS</a>
    <a target="_blank" class="r_form r_blue" href="https://docs.google.com/a/mshoatoeic.com/forms/d/1ybOX1zI79Q2S4ood9k4awujjRrwt8PGTYcPK6_l79bE/viewform">Form đăng ký</a>
    <a class="r_mail r_blue" href="mailto:hoa.nguyen@mshoatoeic.com">
        <i class="vcon-mail"></i>
        hoa.nguyen@mshoatoeic.com              
    </a>
</div
<?php }?>