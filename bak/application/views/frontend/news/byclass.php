<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="title">
    Bài tập <?php echo $this->session->userdata("class_name"); ?>
</div>
<div id="news_lists" class="main_content content_detail clearfix">
<? if (!empty($rows)){
    $i = 1;
    foreach ($rows as $rows){?>
    <div class="item clearfix">
            <div class="img_news">
                <?
                if ($rows['images'] != ''){ ?>
                <a href="<?php echo $rows['share_url']; ?>">
                    <img src="<?php echo getimglink($rows['images'],'size2') ?>" alt="<?php echo $rows['title'] ?>"/>
                </a>
                <?php }
                ?>
            </div>
            <div class="description">
                <a href="<?php echo $rows['share_url']; ?>">
                    <?php echo $rows['title']; ?>
                </a>
                <br />
                <?=cut_text($rows['description'],200)?>
            </div>
    </div>
    <? $i ++;
    }
}
echo $paging;
?>
</div>