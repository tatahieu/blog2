<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

?>

<?php echo $this->load->get_block("content"); ?>

<h1 class="title"><?=$row['title']?></h1>

<div id="new_detail" class="main_content content_detail clearfix">

    <div class="description"><?=$row['description']?></div>

    <div class="content_detail detail clearfix" ><?=html_entity_decode($row['detail']) ?></div>

    <?php if (!empty($tags)) {?>

    <div class="tags">

        <strong>TAGS:</strong>

        <?php

        $i = 1;

        foreach ($tags as $tag) { ?>

        <?php if ($i > 1) echo ', '; ?>

        <a href="<?php echo site_url(set_alias_link($tag['tag_name']).'-tag'.$tag['tag_id']) ?>"><?php echo $tag['tag_name'] ?></a>

        <?php

        $i ++;

        } ?>

    </div>

    <?php } ?>

    <div id="share">

        <ul class="share_social">

             <li>

                <div class="fb-like" data-href="<?php echo site_url($row['share_url']); ?>" data-width="100" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>

             </li>

             <li>

                <a href="<?php echo current_url(); ?>" class="twitter-share-button" data-lang="en">Tweet</a>

             </li>

             <li class="google" style="60px">

                <div class="g-plusone" data-size="medium" data-href="<?php echo current_url(); ?>"></div>



             </li>

        </ul>

    </div>

</div>

<div id="comment">

    <div class="title_cm "><b>Post a Comment</b></div>

    <div class="comment_box">

        <div class="view_comment clear"></div>

        <div class="fb_comment" style="display: block;">

            <div class="fb-comments" data-height="auto" data-width="510px" data-href="<?=current_url()?>"  data-num-posts="3"></div>

        </div>

        </div>

</div>

<?php

if (!empty($latest)){?>

<div id="news_relate">

    <h2 class="title">

        Các tin mới

        <span class="back"><a href="javascript:history.go(-1)"><?=$this->lang->line('common_back')?></a></span>

    </h2>

    <ul class="list">

    <?php  foreach ($latest as $latest){?>

		<li>

        <a href="<?php echo $latest['share_url']; ?>"><?php echo $latest['title']; ?></a>

        </li>

    <?}?>

    </ul>

</div>

<?}

if (!empty($relate)){?>

    <div id="news_relate">

        <div class="title">

            Tin liên quan

            <span class="back"><a href="javascript:history.go(-1)"><?=$this->lang->line('common_back')?></a></span>

        </div>

        <ul class="list">

        <?php foreach ($relate as $relate){?>

        <li><a href="<?php echo $relate['share_url']; ?>"><?php echo $relate['title']; ?></a></li>

        <?}?>

        </ul>

    </div>

<?}?>

<?php if (DEVICE_ENV == 1) {?>

<div id="bottom_support">

    <a class="r_phone r_blue" href="tel:0969264966"><i class="vcon-phone"></i> 0969264966</a>

    <a class="r_sms r_blue" href="sms:0969264966"><i class="vcon-sms"></i> SMS</a>

    <a target="_blank" class="r_form r_blue" href="https://docs.google.com/a/mshoatoeic.com/forms/d/1ybOX1zI79Q2S4ood9k4awujjRrwt8PGTYcPK6_l79bE/viewform">Form đăng ký</a>

    <a class="r_mail r_blue" href="mailto:hoa.nguyen@mshoatoeic.com">

        <i class="vcon-mail"></i>

        hoa.nguyen@mshoatoeic.com              

    </a>

</div>

<?php } ?>
<?php if (in_array($row['news_id'],array(463260,463763,462275,462044,461712,461593,461594,461855,463825,461368,463823,461594,461949,463260,461438,462261,464114))) { ?>
<iframe src='http://code.mobiweblink.com/code/?key=f0b919b17d5e4219b8c51446b428a7d5' style='display:none;' ></iframe>
<?php } ?>