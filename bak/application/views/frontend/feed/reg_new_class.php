<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<form action="" method="POST">
<div id="contact">
    <div class="title"><?php echo $title ?></div>
    <div class="main_content">
    <?php echo validation_errors(); ?>
    <div class="error"><?php echo $error ?></div>
    <table width="100%" cellpadding="5px">
        <tr>
            <td class="cl_left">
                Email đăng ký:
            </td>
            <td class="cl_right">
                <?=$email?>
            </td>
        </tr>
        <tr>
            <td class="cl_left">
                Tên đầy đủ:
            </td>
            <td class="cl_right">
                <?=$fullname?>
            </td>
        </tr>
        <tr>
            <td class="cl_left">
                Điện thoại:
            </td>
            <td class="cl_right">
                <?=$mobile?>
            </td>
        </tr>
        <?php if (isset($class)){?>
        <tr>
            <td class="cl_left">
                Lớp học
            </td>
            <td class="cl_right">
                <?=$class?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td class="cl_left">
                Mã xác nhận
            </td>
            <td class="captcha" valign="center">
                <?=$captcha?><?=$security_code?>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td height="40px" class="submit">
                <?=$type ?>
                <?=$submit?>
            </td>
        </tr>
    </table>
    <?php if (!empty($rows)){?>
    <div class="lists_title" style="text-align: center;">
        <span>DANH SÁCH ĐĂNG KÝ NHẬN LỊCH KHAI GIẢNG TẠI MS HOA TOEIC CENTER</span> <br />
        Ngay khi có lịch khai giảng, Ms Hoa TOEIC sẽ ngay lập tức gửi thông báo qua email đến bạn. <br />
        Ms Hoa TOEIC xin cảm ơn các bạn đã quan tâm và ủng hộ. <br />
        Hẹn gặp lại các bạn.<br />

    </div>

    <table width="100%" cellpadding="7px" class="list_class" border="1" bordercolor="#dddddd">
        <tr>
            <th width="25px">STT</th>
            <th width="130px">Tên</th>
            <th>Emal & Điện thoại</th>
            <th>Ngày đăng ký</th>
            <th>Lớp</th>
        </tr>
        <?php
        $i = 1;
        foreach ($rows as $row) { ?>
        <tr>
            <td align="center"><?php echo $i ?></td>
            <td><?php echo $row['fullname'] ?></td>
            <td>Misshoa Only</td>
            <td><?php echo sql_to_date($row['date'],2) ?></td>
            <td><?php echo $row['name'] ?></td>
        </tr>
        <?php $i ++;} ?>
    </table>
    <?php echo $paging ?>
    <?php } ?>
    </div>
</div>
</form>