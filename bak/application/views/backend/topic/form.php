<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left">Tên topic</td>
			<td class="right"><?php echo $name?></td>
		</tr>
        <tr>
			<td class="left">Level</td>
			<td class="right"><?php echo $level?></td>
		</tr>
        <tr>
			<td class="left">Loại</td>
			<td class="right"><?php echo $type?></td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?php echo $submit ?>
			</td>
		</tr>
	</table>
</form>