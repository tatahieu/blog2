<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>

<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<?php
if (isset($error))
echo '<div class="error">'.$error.'</div>';
?>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th>Tên</th>
        <th>Level</th>
        <th>Loại</th>
		<?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach ($row as $row){ ?>
	<tr>
		<td align="center"><?php echo $i?></td>
		<?=temp_del_check($row['topic_id']) ?>
		<td><?php echo $row['name']?></td>
        <td width="70px"><?php echo (!$row['level']) ? 'All' : get_level($row['level'])?></td>
        <td width="70px"><?php echo ($row['type'] == 1) ? 'Word' : 'Complete'; ?></td>
		<?=temp_edit_icon('topic/edit/'.$row['topic_id']) ?>
	</tr>
	<?
	$i ++; }?>
	</tbody>
</table>
</form>
<?php echo $filter; ?>