<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?php echo temp_delete_icon()?>
	<?php echo temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="30px">
			<?php echo $this->lang->line('common_no'); ?>
		</th>
		<?php echo temp_del_head(); ?>
		<th><?php echo $this->lang->line('news_name'); ?></th>
        <th><?php echo $this->lang->line('news_hit'); ?></th>
        <th><?php echo $this->lang->line('news_date_up'); ?></th>
		<th><?php echo $this->lang->line('news_special'); ?></th>
		<th><?php echo $this->lang->line('news_publish'); ?></th>
		<?php echo temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){?>
	<tr>
		<td align="center"><?php echo $i?></td>
		<?php echo temp_del_check($row['news_id']) ?>
		<td><?php echo $row['title']?></td>
		<td align="center" width="60px"><?php echo $row['hit']?></td>
        <td align="center" width="70px"><?php echo date('d/m/Y H:i:s',$row['date_up'])?></td>
		<td align="center" width="60px"><?php echo temp_status($row['special'])?></td>
		<td align="center" width="60px"><?php echo temp_status($row['publish'])?></td>
		<?php echo temp_edit_icon('news/edit/'.$row['news_id'],'','',1) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?php echo $paging?>
<?php echo $filter; ?> 