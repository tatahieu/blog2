<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>

<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<?php
if (isset($error))
echo '<div class="error">'.$error.'</div>';
?>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('news_cate_name'); ?></th>
		<th><?=$this->lang->line('news_cate_ordering'); ?></th>
		<?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?
	show_cate($row);
	function show_cate($row,$parent = 0,$level = '',$i = 1){ 
		if (!empty($row[$parent])){
			foreach ($row[$parent] as $r){
			$name = ($level == '') ? '<span class="data_level1">'.$r['name'].'</span>' : $r['name'];
			?>
			<tr>
				<td align="center"><? if ($parent === 0) echo $i?></td>
				<?=temp_del_check($r['cate_id']) ?>
				<td><?=$level.$name?></td>
				<td><?=$r['ordering']?></td>
				<?=temp_edit_icon('news/cate_edit/'.$r['cate_id']) ?>
			</tr>
			<?
			$i ++;
			show_cate($row,$r['cate_id'],$level.' -- ');
			}
		}
	}?>
	</tbody>
</table>
</form>