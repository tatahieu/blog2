<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
echo validation_errors();
get_array($error_upload);
?>

<style>
.select_cate{
	width: 200px;
}
</style>
<form action="" method="POST"  enctype="multipart/form-data">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('news_name') ?></td>
			<td class="right"><?=$title ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('news_publish') ?></td>
			<td class="right"><?=$publish ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('news_special') ?></td>
			<td class="right"><?=$special ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('news_cate') ?></td>
			<td class="right">
				<?=$category ?>
                Nhóm sản phẩm chính: <br />
                <?php echo $original_value; ?>
                <?php echo $original_cate; ?>
                <p><i>(*)Click đúp vào nhóm sản phẩm để chọn 1 nhóm chính</i></p>
			</td>
		</tr>

		<tr>
			<td class="left"><?=$this->lang->line('news_images') ?></td>
			<td class="right"><?=$image ?>
            <?php if ($row['images']){?>
            <p style="margin: 5px 0;"><img src="<?php echo getimglink($row['images'],'size2') ?>"/></p>
            <?php } ?>
            </td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('news_description') ?></td>
			<td class="right">
				<?=$description ?>
			</td>
		</tr>
        <tr>
            <td class="left"><?=$this->lang->line('news_detail'); ?></td>
            <td class="right">
                <?=$detail?>
            </td>
        </tr>
        <tr>
			<td class="left">Phân quyền cho lớp</td>
			<td class="right">
				<?=$role ?>
			</td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('news_tags') ?></td>
			<td id="taginput" class="right">
				<?php foreach ($tags as $tag) echo $tag; ?>
			</td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('news_date_up') ?></td>
			<td class="right">
				<?=$date_up ?>
			</td>
		</tr>
        <tr>
			<td class="left">SEO title</td>
			<td class="right">
				<?=$seo_title ?>
			</td>
		</tr>
        <tr>
			<td class="left">SEO description</td>
			<td class="right">
				<?=$seo_desc ?>
			</td>
		</tr>
        <tr>
			<td></td>
			<td class="right">
				<?=$submit ?> 
				<?php if ($row['news_id']) {?>
					<?php if ($row['publish'] == 1) {?>
					<a target="_blank" href="<?php echo $row['share_url']; ?>">Xem ngoài site</a>
					<?php } else { ?>
					<a target="_blank" href="/news/preview/<?php echo $row['news_id']; ?>">Xem trước</a>
					<?php } ?>
				<?php } ?>
			</td>
		</tr>
	</table>
</form>
<script>
	$(function() {
		// suggest for tag
		$( "#taginput" ).find('input.tag').tagedit({
			autocompleteURL: $site_url + 'news/suggest_tag'
		});
        // datetime picker
        $('input[name=date_up]').datetimepicker({
            dateFormat: "yy-mm-dd",
        });
	});
</script>
<script>
    $(".select_cate").bind("dblclick",function(){
        var value = this.value;
        if (value == 0){
            text = ''
        }
        else{
            var text = $(this).find("option[value="+value+"]").text();
        }
        $("input[name=original_value]").val(text);
        $("input[name=original_cate]").val(value);
    });
</script>