<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST"  enctype="multipart/form-data">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('setting_site_title') ?></td>
			<td class="right"><?=$site_title ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('setting_metakey') ?></td>
			<td class="right"><?=$metakey ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('setting_metadesc') ?></td>
			<td class="right"><?=$metadesc ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('setting_email_admin') ?></td>
			<td class="right"><?=$email_admin ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('setting_email_host') ?></td>
			<td class="right"><?=$email_host ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('setting_email_username') ?></td>
			<td class="right"><?=$email_username ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('setting_email_password') ?></td>
			<td class="right"><?=$email_password?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('setting_email_port') ?></td>
			<td class="right"><?=$email_port?></td>
		</tr>
		<tr>
			<td class="left">&#7842;nh og:image</td>
			<td class="right"><?=$images ?>
            <?php if ($row['images']){?>
            	<p style="margin: 5px 0;"><img src="<?php echo getimglink($row['images'],'size2') ?>"/></p>
            <?php } ?>
            </td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$submit ?><?=$sid?>
			</td>
		</tr>
	</table>
</form>