<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
echo validation_errors();
get_array($error_upload);
?>
<div id="product_detail">
<form action="" method="POST"  enctype="multipart/form-data">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?php echo $this->lang->line('product_name') ?></td>
			<td class="right"><?php echo $title ?></td>
		</tr>
		<tr>
			<td class="left"><?php echo $this->lang->line('product_price') ?></td>
			<td class="right"><?php echo $price ?></td>
		</tr>
        <tr>
			<td class="left"><?php echo $this->lang->line('product_discount') ?></td>
			<td class="right"><?php echo $discount ?></td>
		</tr>
        <tr>
			<td class="left"><?php echo $this->lang->line('product_publish') ?></td>
			<td class="right"><?php echo $publish ?></td>
		</tr>
		<tr>
			<td class="left"><?php echo $this->lang->line('product_special') ?></td>
			<td class="right"><?php echo $special ?></td>
		</tr>
		<tr>
			<td class="left"><?php echo $this->lang->line('product_cate') ?></td>
			<td class="right">
				<?php echo $category ?>
                Nhóm sản phẩm chính: <br />
                <?php echo $original_value; ?>
                <?php echo $original_cate; ?>
                <p><i>(*)Click đúp vào nhóm sản phẩm để chọn 1 nhóm chính</i></p>
			</td>
		</tr>
		<tr>
			<td class="left"><?php echo $this->lang->line('product_images') ?></td>
			<td class="right"><?php echo $image ?></td>
		</tr>
		<tr>
			<td class="left"><?php echo $this->lang->line('product_description') ?></td>
			<td class="right">
				<?php echo $description ?>
			</td>
		</tr>
        <tr>
            <td class="left"><?php echo $this->lang->line('product_detail'); ?></td>
            <td class="right">
                <?php echo $detail?>
            </td>
        </tr>
		<tr>
			<td class="left"><?php echo $this->lang->line('product_tags') ?></td>
			<td id="taginput" class="right">
				<?php foreach ($tags as $tag) echo $tag; ?>
			</td>
		</tr>
        <tr>
			<td class="left"><?php echo $this->lang->line('product_date_up') ?></td>
			<td class="right">
				<?php echo $date_up ?>
			</td>
		</tr>
        <?php echo $product_images?>
		<tr>
			<td></td>
			<td class="right">
				<?php echo $submit ?>
			</td>
		</tr>
	</table>
</form>
</div>
<script>
	$(function() {
		// Fullexample
		$( "#taginput" ).find('input.tag').tagedit({
			autocompleteURL: $site_url + 'product/suggest_tag'
		});
        // datetime picker
        $('input[name=date_up]').datetimepicker({
            dateFormat: "yy-mm-dd",
        });
	});
</script>
<script>
    $(".select_cate").bind("dblclick",function(){
        var value = this.value;
        var text = $(this).find("option[value="+value+"]").text();
        $("input[name=original_value]").val(text);
        $("input[name=original_cate]").val(value);
    });
</script>