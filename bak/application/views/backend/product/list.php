<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="30px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('product_images'); ?></th>
		<th><?=$this->lang->line('product_name'); ?></th>
        <th><?=$this->lang->line('product_price'); ?></th>
        <th><?=$this->lang->line('product_discount'); ?></th>
        <th><?=$this->lang->line('product_hit'); ?></th>
		<th><?=$this->lang->line('product_special'); ?></th>
		<th><?=$this->lang->line('product_publish'); ?></th>
		<?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){?>
	<tr>
		<td align="center"><?=$i?></td>
		<?=temp_del_check($row['pro_id']) ?>
		<td width="70px" align="center"><?php if ($row['images']) echo img(array('src' => getimglink($row['images'],'size2'),'width' => 80)); ?></td>
		<td><?=$row['title']?></td>
        <td width="80"><?=price_fomat($row['price'])?></td>
        <td width="80"><?=price_fomat($row['price_discount'])?></td>
		<td align="center" width="50px"><?=$row['hit']?></td>
		<td align="center" width="50px"><?=temp_status($row['special'])?></td>
		<td align="center" width="50px"><?=temp_status($row['publish'])?></td>
		<?=temp_edit_icon('product/edit/'.$row['pro_id'],'','',1) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?=$paging?>
<?=$filter; ?>