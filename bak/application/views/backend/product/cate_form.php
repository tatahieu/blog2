<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
get_array($error_upload);
?>
<form action="" method="POST" enctype="multipart/form-data">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('product_cate_name')?></td>
			<td class="right"><?=$cate_name?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('product_cate_ordering')?></td>
			<td class="right"><?=$cate_ordering ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('product_cate_parent')?></td>
			<td class="right"><?=$cate_parent ?></td>
		</tr>
		<tr>
			<td class="left">Loại</td>
			<td class="right"><?=$cate_type ?></td>
		</tr>
        <tr>
			<td class="left"><?php echo $this->lang->line('product_cate_images') ?></td>
			<td class="right"><?php echo $cate_image ?>
            <?php if ($row['images']){?>
            <p style="margin: 5px 0;"><img src="<?php echo getimglink($row['images'],'size2') ?>"/></p>
            <?php } ?>
            </td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('product_cate_metakey')?></td>
			<td class="right"><?=$cate_metakey ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('product_cate_metadesc')?></td>
			<td class="right"><?=$cate_desc ?></td>
		</tr>
        <tr>
			<td></td>
			<td class="right">
				<?=$cate_submit?>
			</td>
		</tr>
	</table>
</form>