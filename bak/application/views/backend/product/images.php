<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?=js('ajaxupload.js')?>
<tr>
	<td class="left"><?=$this->lang->line('product_images_sub') ?></td>
	<td class="right" id="pro_images">
        <div id="upload" class="button"></div>
        <div id="other_images">
        <?php
        if (!empty($rows)){
        $i = 1;
        foreach ($rows as $rows) {?>
            <div class="images_item">
                <?=img(array("src" =>getimglink($rows['images'],'size2')))?>
                <?php if ($delete_images == 1) { ?>
                <br />
                <?=img(array("src"=>"delete.gif","onclick" => "delete_product_images(".$pro_id.",'".$rows['images']."')","style" => "cursor: pointer;","alt" => $rows['images'])); ?>
                <?php } ?>
            </div>
        <?
        $i ++;
        }
        }?>
        </div>
    </td>
</tr>
<script>
function delete_product_images (pid,images){
    $.post($site_url+'product/delete_images',{ pid:pid,images : images }, function(data) {
        $("#other_images img[alt|=\""+images+"\"]").parent().remove();
    });
}
$(document).ready(function () {
    new AjaxUpload('#upload', {
		action: '<?=site_url('product/upload_images')?>/<?=$pro_id?>',
		name: 'image_pro',
		autoSubmit: false,
		responseType: 'json',
		onChange: function(file, extension) {
			this.submit();
		},
		onSubmit: function(file, extension) {
			$('#upload').append('<?=img(array("src" => "filemanager/loading.gif", "class" => "loading", "style" => "padding-left: 10px;"))?>');
		},
		onComplete: function(file, json) {
			if (json.success) {
				alert("Success");
                var filename = json.success
                $("#other_images").prepend('<div class="images_item"><img src="'+json.success+'" /><?php if ($delete_images == 1) { ?><br /><img src="<?=$this->config->item("img")?>delete.gif" style = "cursor: pointer;" alt ="'+json.success+'" onclick = "delete_product_images(<?=$pro_id?>,\''+json.success+'\')"><?php } ?></div>');
			}
			if (json.error) {
				alert(json.error);
			}

			$('.loading').remove();
		}
	});
});
</script>