<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<form action="" method="POST">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('cache_folder'); ?></th>
		<th><?=$this->lang->line('cache_view')?></th>
	</tr>
	</thead>
    <?php if (!empty($dir)){
    $i = 1;
    ?>
	<tbody>
        <?php foreach ($dir as $dir => $val){?>
    	<tr>
    		<td align="center"><?=$i?></td>	
            <?=temp_del_check($dir) ?>
    		<td><strong><?=$dir?></strong></td>
            <td width="100px" align="center"><?=img('images/folder.jpg')?></td>
    	</tr>
        <?php if (!empty($val)){
        foreach ($val as $val){?>
        <tr>
    		<td align="center">-</td>	
            <?=temp_del_check($dir.'/'.$val) ?>
    		<td>--- <?=$val?></td>
            <td width="100px" align="center"><?=img('images/folder.jpg')?></td>
    	</tr>    
        <?}
        }?>
        <? $i ++;
        }
     ?>
	</tbody>
    <?}?>
</table>
</form>