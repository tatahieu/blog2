<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<style>
	td.right_order{
		border-bottom: 1px dotted;
	}
	.left_order{
		width: 20%; height: 30px;
	}
	.title_report{
		text-align: center; font-weight: bold; font-size: 18px; margin-top: 15px;
	}
	tr.title th{
	   border-bottom: 1px dotted; padding-bottom: 5px;
	}
    tr.order_total td{
       border-top: 1px dotted; padding-top: 5px;
    }
	.order_detail {
		margin: 15px 0; font-weight: bold; color: red; font-size: 13px;
	}
	tr.order_product td{
		
	}
</style>
<table id="content" width="100%">
	<tr>

		<td>
			<?=$this->lang->line("order_code");?>: <?=$row['order_id']?>		
		</td>
		<td align="right">
		</td>
	</tr>
</table>
<h2 class="title_report"><?=$this->lang->line("order_info")?></h2>
<table width="100%">
	<tr>
		<td class="left_order">
			<?=$this->lang->line("order_fullname")?>:
		</td>
		<td class="right_order">
            <?=$row['fullname']?>
        </td>
	</tr>
	<tr>
		<td class="left_order">
            <?=$this->lang->line("order_address")?>
		</td>
		<td class="right_order"><?=$row['address']?></td>
	</tr>
	<tr>
		<td class="left_order">
			<?=$this->lang->line("order_phone")?>
		</td>
		<td class="right_order"><?=$row['phone']?></td>
	</tr>
	<tr>
		<td class="left_order">
            <?=$this->lang->line('order_payment'); ?>
		</td>
		<td class="right_order"><?=$row['payment']?></td>
	</tr>
	<tr>
		<td class="left_order">
			<?=$this->lang->line("order_date")?>
		</td>
		<td class="right_order"><?=sql_to_date($row['date'],2)?></td>
	</tr>
</table>
<?php if (!empty($product)){?>
<div class="order_detail"><?=$this->lang->line("order_detail")?></div>
<table width="100%">
	<tr class="title">
		<th width="2%" align="center" class="atitle">#</td>
		<th><?=$this->lang->line("product_name"); ?></th>
        <th width="10%" align="right"><?=$this->lang->line("product_price"); ?></th>
		<th width="10%" ><?=$this->lang->line("product_quantity"); ?></th>
		<th width="10%" align="right"><?=$this->lang->line("order_subtotal")?></th>
	</tr>
	<tr>
		<td colspan="11"><div class="dot"></div></td>
	</tr>
	<? 
    $i = 1; $total = 0;
    foreach ($product as $product){
    $sub_price = $product['quantity']*$product['price_item'];
    $total = $total + $sub_price
    ?>
	<tr>
		<td align="center" class="atitle"><?=$i?></td>
		<td width="10%">
            <strong><?=$product['pro_name']?></strong>
            <?php if ($product['option'] != ''){
                $option = json_decode($product['option']);
                foreach ($option as $k => $v){?>
                <p><?=$k?>: <?=$v?></p>    
                <?} 
            }?>
        </td>
        <td width="10%" align="right"><?=price_fomat($product['price_item']) ?></td>
		<td width="10%" align="center"><?=$product['quantity'] ?></td>
		<td width="10%" align="right"><?=price_fomat($sub_price)?></td>
	</tr>
    <?
    $i ++;
    }?>
    <tr class="order_total">
		<td colspan="11" align="right">
			<b><?=$this->lang->line("order_total"); ?>: <?=price_fomat($total)?></b>
		</td>
	</tr>
	<tr>
		<td colspan="11" align="left">
			<a href="<?=$print_button?>">
            <button type="button"><?=$this->lang->line("order_print");?></button>
            </a>
		</td>

	</tr>
</table>
<table width="100%">
	<tr>
		<td width="33%" height="50px" align="center"><?=$this->lang->line("order_manager"); ?></td>
		<td width="33%" align="center"><?=$this->lang->line("order_transformer"); ?></td>
		<td width="33%" align="center"><?=$this->lang->line("order_customer"); ?></td>
</table>
<?}?>