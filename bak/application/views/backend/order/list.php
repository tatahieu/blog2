<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('order_fullname'); ?></th>
		<th><?=$this->lang->line('order_address'); ?></th>
        <th><?=$this->lang->line('order_email'); ?></th>
		<th><?=$this->lang->line('order_phone'); ?></th>
		<th><?=$this->lang->line('order_date'); ?></th>
        <th><?=$this->lang->line('order_status'); ?></th>
        <th><?=$this->lang->line('order_view'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?
	if (!empty($row)){
		foreach ($row as $row){?>
		<tr>
			<td align="center"><?=$i?></td>
			<?=temp_del_check($row['order_id']) ?>
			<td><?=$row['fullname']?></td>
			<td><?=$row['address']?></td>
			<td><?=$row['email']?></td>
			<td><?=$row['phone']?></td>
            <td><?=$row['date']?></td>
			<td width="60px" align="center"><?=temp_status($row['status'])?></td>
			<?=temp_edit_icon('order/edit/'.$row['order_id']) ?>
		</tr>
		<?
		$i ++;
		}
	}
	?>
	</tbody>
</table>
</form>
<?php echo $paging?>