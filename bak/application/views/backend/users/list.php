<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?php echo temp_delete_icon()?>
	<?php echo temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?php echo $this->lang->line('common_no'); ?>
		</th>
		<?php echo temp_del_head(); ?>
		<th><?php echo $this->lang->line('users_username'); ?></th>
		<th><?php echo $this->lang->line('users_email'); ?></th>
		<th><?php echo $this->lang->line('users_fullname'); ?></th>
		<th><?php echo $this->lang->line('users_roles_permission'); ?></th>
		<th><?php echo $this->lang->line('users_active'); ?></th>
		<?php echo temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?
	if (!empty($row)){
	foreach ($row as $row){?>

	<tr>
		<td align="center"><?php echo $i?></td>
		<?php echo temp_del_check($row['user_id']) ?>
		<td><?php echo $row['username']?></td>
		<td><?php echo $row['email']?></td>
		<td><?php echo $row['fullname']?></td>
		<td><?php echo $row['name']?></td>
		<td align="center"><?php echo temp_status($row['active'])?></td>
		<?php echo temp_edit_icon('users/edit/'.$row['user_id']) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?php echo $paging?>
<div class="clear"></div>
<form action="<?php echo site_url('users/index?tmp=xls'); ?>" method="GET">
	<div class="del_button">
		<select name="type">
			<option value="register">Danh sách đăng ký</option>
			<option value="test">Danh sách làm test</option>
		</select>
		<input type="hidden" name="tmp" value="xls"/>
		From date: <input id="fromdate" type="text" value="<?php echo date('Y-m-d H:i:s',time() - 30*24*3600); ?>" name="fromdate"> To date: <input id="todate" type="text" name="todate" value="<?php echo date('Y-m-d H:i:s',time()); ?>">
		<button class="submit_button">Export</button>
	</div>
</form>
<?php echo $filter; ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#fromdate').datetimepicker({
	        dateFormat: "yy-mm-dd",
	    });
	    $('#todate').datetimepicker({
	        dateFormat: "yy-mm-dd",
	    });
	})
</script>