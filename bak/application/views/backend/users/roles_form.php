<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('users_roles_name')?></td>
			<td class="right"><?=$roles_name?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_roles_permission')?></td>
			<td class="right">
				<table width="100%" cellpadding="4px" class="table_permission">
					<tr>
						<th align="left" width="130px">Module</th>
						<th>Function</th>
						<th>All</th>
					</tr>
				    <?php 
				    foreach ($roles_module as $key => $roles){?>
					<tr>
						<td><?php echo $roles['name'] ?></td>
						<td class="permission_func">
                            <ul id="role_<?php echo $key ?>">
                            <?php foreach ($roles['func'] as $fc){
                               $check = ($roles_permission[$key] && in_array($fc,$roles_permission[$key])) ? true : false; 
                               ?>
                               <li><span><?php echo $fc ?></span><?php echo form_checkbox('permission['.$key.'][]',$fc,$check) ?></li>
                            <?php } ?>
                            </ul>
                        </td>
                        <td width="60px" align="center"><?php echo form_checkbox('',$fc,FALSE,'rel="'.$key.'" class="permission_check"') ?></td>
					</tr>
				    <?}?>
				</table>
			</td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$roles_submit?>
			</td>
		</tr>
	</table>
</form>
<script>
	$(function () {
        $(".permission_check").bind("click",function(){
            var name = $(this).attr("rel");
            
            if($(this).attr("checked")){ 
                $('#role_'+name).find("input").attr('checked','checked');
            }
            else{
                $('#role_'+name).find("input").removeAttr('checked');
            }
        });
    });
</script>