<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>Hanoi CDC Online Solution., JSC</title>
	<meta name="robots" content="no-cache" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<?php echo link_tag('tqn_login.css'); ?>
</head>
<body>
<form action="" method="POST" >
	<div>
		<table cellpadding="0" cellspacing="0" align="center" class="login_main">
			<tr>
				<td width="160px" valign="top">
					<?=img('login/login_logo.jpg'); ?>
				</td>
				<td>

					<table cellpadding="4" cellspacing="0" width="100%">
					<tr>
						<td height="40px" colspan="2">
							<div class="login_title">
							<?=$this->lang->line('users_login_title') ?>
							</div>
						</td>
					</tr>
					<tr>
						<td width="96px" align="right">
							<label class="login_label"><?=$this->lang->line('users_username') ?></label>
						</td>
						<td>
							<?=$username?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="login_label"><?=$this->lang->line('users_password') ?></label>
						</td>
						<td>
							<?=$password?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="login_label"><?=$this->lang->line('users_captcha') ?></label>
						</td>
						<td>
							<?=$input_captcha?><span style="margin-left: 2px; margin-right: 2px;float: right;"><?=$captcha?></span>
						</td>
					</tr>
					<tr>
						<td>

						</td>
						<td>
							<?=$submit?>
						</td>
					</tr>
					<tr>
						<td colspan="2">
						<?php echo validation_errors(); ?>
						</td>
					</tr>
					</table>
				</td>
			</tr>
		</table>
		<div class="login_bottom"></div>
	</div>
</form>
</body>
</html>