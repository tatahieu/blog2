<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('users_username')?></td>
			<td class="right"><?=$username?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_password')?></td>
			<td class="right"><?=$password?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_roles_permission')?></td>
			<td class="right"><?=$roles?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_active')?></td>
			<td class="right"><?=$active?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_email')?></td>
			<td class="right"><?=$email?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_fullname')?></td>
			<td class="right"><?=$fullname?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_sex')?></td>
			<td class="right"><?=$sex?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_yahoo')?></td>
			<td class="right"><?=$yahoo?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_skype')?></td>
			<td class="right"><?=$skype?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('users_date_reg')?></td>
			<td class="right"><?=$date_reg?></td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$submit?>
				<? get_array($hidden); ?>
			</td>
		</tr>
	</table>
</form>