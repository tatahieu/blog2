<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?php echo temp_delete_icon()?>
	<?php echo temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?php echo $this->lang->line('common_no'); ?>
		</th>
		<?php echo temp_del_head(); ?>
		<th><?php echo $this->lang->line('users_roles_name'); ?></th>
		<?php echo temp_edit_head(); ?>
	</tr>
	</thead>
	<? 
	if (!empty($row)){
	foreach ($row as $row){?>
	<tbody>
	<tr>
		<td align="center"><?php echo $i?></td>
		<?php echo temp_del_check($row['roles_id']) ?>
		<td><?php echo $row['name']; ?></td>
		<?php echo temp_edit_icon('users/role_edit/'.$row['roles_id']) ?>
	</tr>
	</tbody>
	<?
	$i ++;
	}
	}?>
</table>
</form>