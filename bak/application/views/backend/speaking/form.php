<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
echo validation_errors();
get_array($error_upload);
?>
<form action="" method="POST"  enctype="multipart/form-data">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left">Tiêu đề</td>
			<td class="right"><?=$title ?></td>
		</tr>
		<tr>
			<td class="left">Ảnh</td>
			<td class="right"><?=$images ?>
            <?php if ($row['images']){?>
            <p style="margin: 5px 0;" id="media_images"><img src="<?php echo getimglink($row['images'],'size2'); ?>"/> - <a href="javascript:delete_media('images')">Delete</a></p>
            <?php } ?>
            </td>
		</tr>
		<tr>
			<td class="left">Sound</td>
			<td class="right">
                <?=$sound ?>
                <?php if ($row['sound']){?>
                <p style="margin: 5px 0;" id="media_sound"><?php echo $row['sound']; ?> - <a href="javascript:delete_media('sound')">Delete</a></p>
                <?php } ?>
            </td>
		</tr>
        <tr>
			<td class="left">Chủ đề</td>
			<td class="right"><?=$topic ?></td>
		</tr>
        <tr>
			<td class="left">Level</td>
			<td class="right"><?=$level ?></td>
		</tr>
        <tr>
			<td class="left">Thời gian chuẩn bị</td>
			<td class="right"><?=$prepare_time ?></td>
		</tr>
        <tr>
			<td class="left">Thời gian nói</td>
			<td class="right"><?=$response_time ?></td>
		</tr>
        <tr>
            <td class="left">Nội dung</td>
            <td class="right">
                <?=$content?>
            </td>
        </tr>
        <tr>
            <td class="left">Đáp án text</td>
            <td class="right">
                <?=$answer_text?>
            </td>
        </tr>
        <tr>
            <td class="left">Đáp án sound</td>
            <td class="right">
                <?=$answer_sound?>
                <?php if ($row['answer_sound']){?>
                <p style="margin: 5px 0;" id="media_answer_sound"><?php echo $row['answer_sound']; ?> - <a href="javascript:delete_media('answer_sound')">Delete</a></p>
                <?php } ?>
            </td>
        </tr>
        <tr>
			<td></td>
			<td class="right">
				<?=$submit ?>
			</td>
		</tr>
	</table>
</form>
<script>
    function delete_media(type){
        $.post("",{delete_media: type}, function(data) {
            if (data.status == 'success'){
                $("#media_"+type).remove();
            }
        },'JSON');
    }
</script>