<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $this->config->item("img")?>favicon.ico" rel="shortcut icon" type="image/ico" />
		<title><?php echo $title?></title>
		<?php echo get_array($header); ?>
        <?
        if (isset($script)){
             get_array($script);
        }?>
	</head>
	<body>
		<div id="site">
        <div class="banner">
		  <?php echo anchor('http://www.hanoicdc.com',img(array('src' => 'banner.gif'))); ?>
		</div>
        <div class="menu">
			<div class="home_icon">
				<?php echo anchor(site_url(),$this->lang->line('common_home')); ?>
			</div>
			<div class="web_icon">
				<?php echo anchor(base_url(),$this->lang->line('common_webview')); ?>
			</div>
			<div class="profile_icon">
				<?php echo anchor('users/profile',$this->lang->line('common_profile').''); ?>
			</div>
			<div class="logout_icon">
				<?php echo anchor('users/logout',$this->lang->line('common_logout')); ?>
			</div>
		</div>
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="left_colum" valign="top">
					<div class="title_panel" style="margin-bottom: 5px">Modules</div>
                    <?php echo $left_menu?>
				</td>
				<td width="7px">
					<?php echo img(array('src' => 'icon/hide_icon.gif', 'class' => 'hidetd')); ?>
				</td>
				<td class="right_colum" valign="top">
					<div class="title_panel">
						<?php
						if (isset($title))
						echo $title
						?>
					</div>
					<div class="content">
						<?php echo $content_for_layout?>
					</div>
				</td>
			</tr>
		</table>
		<div class="footer">
			<b>CDC Online Technology., JSC</b><br />
			Office: No.5 alley 106/1 Chua Lang St, Dong Da Dist. Hanoi<br />
			Online Support: 0975.8888.57 / Email: info@hanoicdc.com / Website:<a href="http://www.hanoicdc.com">http://www.hanoicdc.com</a>

		</div>
		</div>
        <?php get_array($footer); ?>

		<script>
		/** QUICK SEARCH **/
		$('#quicksearch input').click(function(){
			$('#quicksearch input').quicksearch('table.table_list   tbody tr');
		});
		$('table.table_list tbody tr').bind('mouseover',function(){
			$(this).addClass("tr_hover");
		});
		$('table.table_list tbody tr').bind('mouseout',function(){
			$(this).removeClass("tr_hover");
		});
		/** CHECK ALL TO DELETE **/
		$('.checkall').click(function () {
  				$("input[name=checkall]").each(function () {
                if (this.checked == true) {
                    $("input[title=delete]").attr('checked', 'checked');
                }
                else {
                    $("input[title=delete]").attr('checked', false);
                }

            });
  		});
  		$('.delete_form').submit(function(){
			 var answer = confirm('<?php echo $this->lang->line('common_delete_confirm')?>');
 			 return answer;
  		});
  		/** ADD CLASS FOR INPUT **/
		/* $(document).ready(function(){
		  	$(".right_colum td input[type='text']").addClass('text');
			$(".right_colum input[type='password']").addClass('text');
		}); */
		</script>
		<!-- SHOW OR HIDE LEFT COLLUM -->
		<script language="javascript">
        if (readCookie('left_colum_status') == 'hide'){
            $(".left_colum").hide();
        }
		$(".hidetd").bind('click',function(){
			if ($(".left_colum").is(':hidden')){
			     $(".left_colum").show();
                 delCookie('left_colum_status');
            }
            else{
                $(".left_colum").hide();
                createCookie('left_colum_status','hide',3600);
            }
		});

		</script>
	</body>
</html>