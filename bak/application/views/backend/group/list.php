<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th>Tên lớp</th>
		<th>Tên truy cập</th>
		<th>Mật khẩu</th>
		<?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?
	if (!empty($row)){
		foreach ($row as $row){?>
		<tr>
			<td align="center"><?=$i?></td>
			<?=temp_del_check($row['id']) ?>
			<td><?=$row['name']?></td>
			<td><?=$row['username']?></td>
			<td><?=base64_decode($row['password'])?></td>
			<?=temp_edit_icon('group/edit/'.$row['id']) ?>
		</tr>
		<?
		$i ++;
		}
	}
	?>
	</tbody>
</table>
</form>