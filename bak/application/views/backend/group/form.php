<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left">Tên lớp</td>
			<td class="right"><?=$name?></td>
		</tr>
		<tr>
			<td class="left">Tên đăng nhập</td>
			<td class="right"><?=$username ?></td>
		</tr>
		<tr>
			<td class="left">Mật khẩu</td>
			<td class="right"><?=$password ?></td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$submit?>
			</td>
		</tr>
	</table>
</form>