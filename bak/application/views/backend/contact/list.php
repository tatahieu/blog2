<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<?=temp_no()?>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('contact_username'); ?></th>
		<th><?=$this->lang->line('contact_phone'); ?></th>
        <th><?=$this->lang->line('contact_email'); ?></th>
		<th><?=$this->lang->line('contact_date'); ?></th>
		<th><?=$this->lang->line('contact_status'); ?></th>
		<th><?=$this->lang->line('contact_view'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){
	$status = ($row['status'] == 1) ? 1 : 0;   
    ?>
	<tr>
		<td align="center"><?=$i?></td>
		<?=temp_del_check($row['contact_id']) ?>
		<td><?=$row['username']?></td>
		<td width="100px"><?=$row['phone']?></td>
		<td width="160px"><?=$row['email']?></td>
		<td width="120px"><?=sql_to_date($row['date'],2)?></td>
		<td align="center" width="60px"><?=temp_status($status,array('id'=> 'status_'.$row['contact_id'],'onclick' => 'change_status('.$row['contact_id'].','.$status.')'))?></td>
		<?=temp_edit_icon('#','rel="contact_'.$row['contact_id'].'" class="show_content"',1) ?>
	</tr>
	<div style="display: none;" id="contact_<?=$row['contact_id']?>"><?=$row['content']?></div>
    <?
	$i ++;
	
    }
	}?>
	</tbody>
</table>
</form>
<?=$paging?>
<?=$filter; ?>
<script language="javascript">
$(document).ready(function () {
    $('.show_content').simpleDialog({
        closeLabel: '<?=$this->lang->line("common_close"); ?>',
        width: '500px',
        height: '400px',
        open: function (event) {
            var data = event.currentTarget.rel;
            data = data.replace('contact_','');
            change_status(data,0);
        }, 
    });
});
function change_status(id,st){
    $("#status_"+id).parent().load('<?=site_url('contact/edit')?>/'+id+'/'+st);
}
</script>