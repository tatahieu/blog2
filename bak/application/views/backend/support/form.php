<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('sup_job')?></td>
			<td class="right"><?=$job?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('sup_yahoo')?></td>
			<td class="right"><?=$yahoo ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('sup_skype')?></td>
			<td class="right"><?=$skype ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('sup_phone')?></td>
			<td class="right"><?=$phone ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('sup_ordering')?></td>
			<td class="right"><?=$ordering ?></td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$submit?>
			</td>
		</tr>
	</table>
</form>