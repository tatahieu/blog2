<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('sup_job'); ?></th>
		<th><?=$this->lang->line('sup_yahoo'); ?></th>
		<th><?=$this->lang->line('sup_skype'); ?></th>
		<th><?=$this->lang->line('sup_phone'); ?></th>
		<th><?=$this->lang->line('sup_ordering'); ?></th>
		<?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?
	if (!empty($row)){
		foreach ($row as $row){?>
		<tr>
			<td align="center"><?=$i?></td>
			<?=temp_del_check($row['sup_id']) ?>
			<td><?=$row['job']?></td>
			<td><?=$row['yahoo']?></td>
			<td><?=$row['skype']?></td>
			<td><?=$row['phone']?></td>
			<td width="50px" align="center"><?=$row['ordering']?></td>
			<?=temp_edit_icon('support/edit/'.$row['sup_id']) ?>
		</tr>
		<?
		$i ++;
		}
	}
	?>
	</tbody>
</table>
</form>