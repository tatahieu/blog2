<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xml:lang="en">
<head>
<title><?php echo $this->lang->line('fmag_heading_title'); ?></title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<?=js('jquery-1.8.3.min.js')?>
<?=link_tag('jquery-ui.css');?>
<?=js('jquery-ui-1.10.0.custom.min.js'); ?>
<?=js('jquery.bgiframe-2.1.2.js')?>
<?=js('jtree/jquery.tree.min.js')?>
<?=js('ajaxupload.js')?>
<?=js('libraries.js')?>
<style type="text/css">
body {
	padding: 0;
	margin: 0;
	background: #F7F7F7;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
img {
	border: 0;
}
#container {
	padding: 0px 10px 7px 10px;
	height: 340px;
}
#menu {
	clear: both;
	height: 29px;
	margin-bottom: 3px;
}
#column-left {
	background: #FFF;
	border: 1px solid #CCC;
	float: left;
	width: 20%;
	height: 320px;
	overflow: auto;
}
#column-right {
	background: #FFF;
	border: 1px solid #CCC;
	float: right;
	width: 78%;
	height: 320px;
	overflow: auto;
	text-align: center;
}
#column-right div {
	text-align: left;
	padding: 5px;
}
#column-right a {
	display: inline-block;
	text-align: center;
	border: 1px solid #EEEEEE;
	cursor: pointer; overflow: hidden;
	margin: 5px; width: 60px;
	padding: 5px;
}
#column-right a.selected {
	border: 1px solid #7DA2CE;
	background: #EBF4FD;
}
#column-right input {
	display: none;
}
#column-right img{
    max-width: 40px;
    max-width: 40px;
}
#dialog {
	display: none;
}
.button {
	display: block;
	float: left;
	padding: 8px 5px 8px 25px;
	margin-right: 5px;
	background-position: 5px 6px;
	background-repeat: no-repeat;
	cursor: pointer;
}
.button:hover {
	background-color: #EEEEEE;
}
.thumb {
	padding: 5px;
	width: 105px;
	height: 105px;
	background: #F7F7F7;
	border: 1px solid #CCCCCC;
	cursor: pointer;
	cursor: move;
	position: relative;
}
.img_name{width: 80px; overflow: hidden;}
</style>
</head>
<body>
<div id="container">
  <div id="menu">
        <a id="create" class="button" style="background-image: url('<?=$theme?>folder.png');"><?php echo $this->lang->line('fmag_button_folder'); ?></a>
        <a id="delete" class="button" style="background-image: url('<?=$theme?>edit-delete.png');"><?php echo $this->lang->line('fmag_button_delete'); ?></a>
        <a id="move" class="button" style="background-image: url('<?=$theme?>edit-cut.png');"><?php echo $this->lang->line('fmag_button_move'); ?></a>
        <a id="copy" class="button" style="background-image: url('<?=$theme?>edit-copy.png');"><?php echo $this->lang->line('fmag_button_copy'); ?></a>
        <a id="rename" class="button" style="background-image: url('<?=$theme?>edit-rename.png');"><?php echo $this->lang->line('fmag_button_rename'); ?></a>
        <a id="upload" class="button" style="background-image: url('<?=$theme?>upload.png');">
        <?php echo $this->lang->line('fmag_button_upload'); ?></a>
        <a id="refresh" class="button" style="background-image: url('<?=$theme?>refresh.png');"><?php echo $this->lang->line('fmag_button_refresh'); ?></a>
        <?php if ($formname) { ?>
        <a id="selectimg" class="button" style="background-image: url('<?=$theme?>select.png');"><?php echo $this->lang->line('fmag_button_select'); ?></a>
        <?php } ?>

  </div>
  <div id="column-left"></div>
  <div id="column-right"></div>
</div>
<script type="text/javascript"><!--
$(document).ready(function () {


	$('#column-left').tree({
		data: {
			type: 'json',
			async: true,
			opts: {
				method: 'POST',
				url: '<?=site_url('filemanager/directory')?>/?type=<?=$type?>'
			}
		},
		selected: 'top',
		ui: {
			theme_name: 'classic',
			animation: 700
		},
		types: {
			'default': {
				clickable: true,
				creatable: false,
				renameable: false,
				deletable: false,
				draggable: false,
				max_children: -1,
				max_depth: -1,
				valid_children: 'all'
			}
		},
		callback: {
			beforedata: function(NODE, TREE_OBJ) {
			    var $dir = readCookie('dirupload');

				if (NODE == false) {
                    TREE_OBJ.settings.data.opts.static = [
						{
							data: '<?=$type?>',
							attributes: {
								'id': 'top',
								'directory': ''
							},
							state: 'closed'
						}
					];
					return { 'directory': '' }
				}else {
					TREE_OBJ.settings.data.opts.static = false;

					return { 'directory': $(NODE).attr('directory') }
				}
			},
			onselect: function (NODE, TREE_OBJ) {
                //console.log(NODE);
                //console.log(readCookie('dirupload'));
				$.ajax({
					url: '<?=site_url('filemanager/files')?>/?type=<?=$type?>',
					type: 'POST',
					data: 'directory=' + encodeURIComponent($(NODE).attr('directory')),
					dataType: 'json',
					success: function(json) {
						html = '<div>';

						if (json) {
							for (i = 0; i < json.length; i++) {

								name = '';
								name += '<span class="img_name">' + json[i]['filename'] + '</span><br />';
								name += json[i]['size'];

								html += '<a file="' + json[i]['file'] + '"><img src="' + json[i]['thumb'] + '" title="' + json[i]['filename'] + '" /><br />' + name + '</a>';
							}
						}

						html += '</div>';

						$('#column-right').html(html);
					}
				});
			}
		}
	});

	$('#column-right a').live('click', function (e) {
		if ($(this).attr('class') == 'selected') {

            if (e.ctrlKey)
            {
                $(this).removeAttr('class');
            }
            else{
                $('#column-right a').removeAttr('class');
                $(this).attr('class', 'selected');
            }
		} else {
            if (!e.ctrlKey)
            {
                $('#column-right a').removeAttr('class');
            }
			$(this).attr('class', 'selected');
		}
	});
    $('#selectimg').bind("click",function(){
        var html = '';
        $("#column-right").find(".selected").each(function(){
            html += '<p><img alt="" src="<?php echo $directory; ?>'+$(this).attr('file')+'"></p>';
        });
        if (html == ''){
            alert("<?php echo $this->lang->line("fmag_error_file"); ?>");
            return false;
        }
        window.opener.CKEDITOR.instances.<?php echo $formname ?>.insertHtml( html );
        self.close();
        window.opener.CKEDITOR.dialog.getCurrent().click('cancel');;
    })
	$('#column-right a').live('dblclick', function () {
		<?php
        if ($fckeditor OR $fckeditor === '0') { ?>

		window.opener.CKEDITOR.tools.callFunction(<?php echo $fckeditor; ?>, '<?php echo $directory; ?>' + $(this).attr('file'));
        self.close();


		<?php } else { ?>
		parent.$('#<?php echo $field; ?>').attr('value', $(this).attr('file'));
		parent.$('#dialog').dialog('close');
		parent.$('#dialog').remove();
		<?php } ?>
        var tree = $.tree.focused();
        var selected = encodeURIComponent($(tree.selected).attr('directory'))
        createCookie("dirupload",selected,100);
	});

	$('#create').bind('click', function () {
		var tree = $.tree.focused();

		if (tree.selected) {
			$('#dialog').remove();

			html  = '<div id="dialog">';
			html += '<?php echo $this->lang->line('fmag_entry_folder'); ?> <input type="text" name="name" value="" /> <input type="button" value="Submit" />';
			html += '</div>';

			$('#column-right').prepend(html);

			$('#dialog').dialog({
				title: '<?php echo $this->lang->line('fmag_button_folder'); ?>',
				resizable: false
			});

			$('#dialog input[type=\'button\']').bind('click', function () {
				$.ajax({
					url: '<?=site_url('filemanager/create')?>/?type=<?=$type?>',
					type: 'POST',
					data: 'directory=' + encodeURIComponent($(tree.selected).attr('directory')) + '&name=' + encodeURIComponent($('#dialog input[name=\'name\']').val()),
					dataType: 'json',
					success: function(json) {
						if (json.success) {
							$('#dialog').remove();

							tree.refresh(tree.selected);

							alert(json.success);
						} else {
							alert(json.error);
						}
					}
				});
			});
		} else {
			alert('<?php echo $this->lang->line('fmag_error_directory'); ?>');
		}
	});

	$('#delete').bind('click', function () {
		path = $('#column-right a.selected').attr('file');

		if (path) {


            if (!confirm("<?php echo $this->lang->line('common_delete_confirm') ?> File: "+path)) {
                return false;
            }
            var arrpath = new Array();
            $('#column-right a.selected').each(function(index) {
                arrpath[index] = $(this).attr("file");
            });
			$.ajax({
				url: '<?=site_url('filemanager/delete')?>/?type=<?=$type?>',
				type: 'POST',
				data: 'path=' + encodeURIComponent(arrpath),
				dataType: 'json',
				success: function(json) {
					if (json.success) {
						var tree = $.tree.focused();

						tree.select_branch(tree.selected);

						alert(json.success);
					}

					if (json.error) {
						alert(json.error);
					}
				}
			});
		} else {
			var tree = $.tree.focused();

			if (tree.selected) {
                if (!confirm("<?php echo $this->lang->line('common_delete_confirm') ?> Folder: "+$(tree.selected).attr('directory'))) {
                    return false;
                }
				$.ajax({
					url: '<?=site_url('filemanager/delete')?>/?type=<?=$type?>',
					type: 'POST',
					data: 'path=' + encodeURIComponent($(tree.selected).attr('directory')),
					dataType: 'json',
					success: function(json) {
						if (json.success) {
							tree.select_branch(tree.parent(tree.selected));

							tree.refresh(tree.selected);

							alert(json.success);
						}

						if (json.error) {
							alert(json.error);
						}
					}
				});
			} else {
				alert('<?php echo $this->lang->line('fmag_error_select'); ?>');
			}
		}
	});

	$('#move').bind('click', function () {
		$('#dialog').remove();

		html  = '<div id="dialog">';
		html += '<?php echo $this->lang->line('fmag_entry_move'); ?> <select name="to"></select> <input type="button" value="Submit" />';
		html += '</div>';

		$('#column-right').prepend(html);

		$('#dialog').dialog({
			title: '<?php echo $this->lang->line('fmag_button_move'); ?>',
			resizable: false
		});

		$('#dialog select[name=\'to\']').load('<?=site_url('filemanager/folders')?>/?type=<?=$type?>');

		$('#dialog input[type=\'button\']').bind('click', function () {
			path = $('#column-right a.selected').attr('file');

			if (path) {
                var arrpath = new Array();
			    $('#column-right a.selected').each(function(index) {
                    arrpath[index] = $(this).attr("file");
                });
				$.ajax({
					url: '<?=site_url('filemanager/move')?>/?type=<?=$type?>',
					type: 'POST',
					data: 'from=' + encodeURIComponent(arrpath) + '&to=' + encodeURIComponent($('#dialog select[name=\'to\']').val()),
					dataType: 'json',
					success: function(json) {
						if (json.success) {
							$('#dialog').remove();

							var tree = $.tree.focused();

							tree.select_branch(tree.selected);

							alert(json.success);
						}

						if (json.error) {
							alert(json.error);
						}
					}
				});
			} else {
				var tree = $.tree.focused();
				$.ajax({
					url: '<?=site_url('filemanager/move')?>/?type=<?=$type?>',
					type: 'POST',
					data: 'from=' + encodeURIComponent($(tree.selected).attr('directory')) + '&to=' + encodeURIComponent($('#dialog select[name=\'to\']').val()),
					dataType: 'json',
					success: function(json) {
						if (json.success) {
							$('#dialog').remove();

							tree.select_branch('#top');

							tree.refresh(tree.selected);

							alert(json.success);
						}

						if (json.error) {
							alert(json.error);
						}
					}
				});
			}
		});
	});

	$('#copy').bind('click', function () {
		$('#dialog').remove();

		html  = '<div id="dialog">';
		html += '<?php echo $this->lang->line('fmag_entry_copy'); ?> <input type="text" name="name" value="" /> <input type="button" value="Submit" />';
		html += '</div>';

		$('#column-right').prepend(html);

		$('#dialog').dialog({
			title: '<?php echo $this->lang->line('fmag_button_copy'); ?>',
			resizable: false
		});

		$('#dialog select[name=\'to\']').load('<?=site_url('filemanager/folder')?>/?type=<?=$type?>');

		$('#dialog input[type=\'button\']').bind('click', function () {
			path = $('#column-right a.selected').attr('file');

			if (path) {
				$.ajax({
					url: '<?=site_url('filemanager/copy')?>/?type=<?=$type?>',
					type: 'POST',
					data: 'path=' + encodeURIComponent(path) + '&name=' + encodeURIComponent($('#dialog input[name=\'name\']').val()),
					dataType: 'json',
					success: function(json) {
						if (json.success) {
							$('#dialog').remove();

							var tree = $.tree.focused();

							tree.select_branch(tree.selected);

							alert(json.success);
						}

						if (json.error) {
							alert(json.error);
						}
					}
				});
			} else {
				var tree = $.tree.focused();

				$.ajax({
					url: '<?=site_url('filemanager/copy')?>/?type=<?=$type?>',
					type: 'POST',
					data: 'path=' + encodeURIComponent($(tree.selected).attr('directory')) + '&name=' + encodeURIComponent($('#dialog input[name=\'name\']').val()),
					dataType: 'json',
					success: function(json) {
						if (json.success) {
							$('#dialog').remove();

							tree.select_branch(tree.parent(tree.selected));

							tree.refresh(tree.selected);

							alert(json.success);
						}

						if (json.error) {
							alert(json.error);
						}
					}
				});
			}
		});
	});

	$('#rename').bind('click', function () {
		$('#dialog').remove();

		html  = '<div id="dialog">';
		html += '<?php echo $this->lang->line('fmag_entry_rename'); ?> <input type="text" name="name" value="" /> <input type="button" value="Submit" />';
		html += '</div>';

		$('#column-right').prepend(html);

		$('#dialog').dialog({
			title: '<?php echo $this->lang->line('fmag_entry_rename'); ?>',
			resizable: false
		});

		$('#dialog input[type=\'button\']').bind('click', function () {
			path = $('#column-right a.selected').attr('file');

			if (path) {
				$.ajax({
					url: '<?=site_url('filemanager/rename')?>/?type=<?=$type?>',
					type: 'POST',
					data: 'path=' + encodeURIComponent(path) + '&name=' + encodeURIComponent($('#dialog input[name=\'name\']').val()),
					dataType: 'json',
					success: function(json) {
						if (json.success) {
							$('#dialog').remove();

							var tree = $.tree.focused();

							tree.select_branch(tree.selected);

							alert(json.success);
						}

						if (json.error) {
							alert(json.error);
						}
					}
				});
			} else {
				var tree = $.tree.focused();

				$.ajax({
					url: '<?=site_url('filemanager/rename')?>/?type=<?=$type?>',
					type: 'POST',
					data: 'path=' + encodeURIComponent($(tree.selected).attr('directory')) + '&name=' + encodeURIComponent($('#dialog input[name=\'name\']').val()),
					dataType: 'json',
					success: function(json) {
						if (json.success) {
							$('#dialog').remove();

							tree.select_branch(tree.parent(tree.selected));

							tree.refresh(tree.selected);

							alert(json.success);
						}

						if (json.error) {
							alert(json.error);
						}
					}
				});
			}
		});
	});

	new AjaxUpload('#upload', {
		action: '<?=site_url('filemanager/upload')?>/?type=<?=$type?>',
		name: 'image',
        multi: true,
		autoSubmit: false,
		responseType: 'json',
		onChange: function(file, extension) {
			var tree = $.tree.focused();

			if (tree.selected) {
				this.setData({'directory': $(tree.selected).attr('directory')});
			} else {
				this.setData({'directory': ''});
			}

			this.submit();
		},
		onSubmit: function(file, extension) {
			$('#upload').append('<img src="<?=$theme?>loading.gif" class="loading" style="padding-left: 5px;" />');
		},
		onComplete: function(file, json) {
			if (json.success) {
				var tree = $.tree.focused();

				tree.select_branch(tree.selected);

				alert(json.success);
			}

			if (json.error) {
				alert(json.error);
			}

			$('.loading').remove();
		}
	});

	$('#refresh').bind('click', function () {
		var tree = $.tree.focused();

		tree.refresh(tree.selected);
	});
});


//--></script>
</body>
</html>