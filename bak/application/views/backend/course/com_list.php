<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?php echo temp_delete_icon()?>
	<?php echo temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="30px">
			<?php echo $this->lang->line('common_no'); ?>
		</th>
		<?php echo temp_del_head(); ?>
		<th>Câu hỏi</th>
        <th>Ảnh</th>
        <th>Audio</th>
        <th>Topic</th>
        <th>Level</th>
		<?php echo temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){?>
	<tr>
		<td align="center"><?php echo $i?></td>
		<?php echo temp_del_check($row['com_id']) ?>
		<td><?php echo $row['question']?></td>
		<td align="center" width="70px"><?php echo ($row['images'] != '') ? temp_status(1) : temp_status(0) ?></td>
        <td align="center" width="70px"><?php echo ($row['audio'] != '') ? temp_status(1) : temp_status(0) ?></td>
		<td align="center" width="90px"><?php echo $row['topic_name']?></td>
        <td align="center" width="90px"><?php if ($row['level'] > 0) echo get_level($row['level']) ?></td>
		<?php echo temp_edit_icon('course/com_edit/'.$row['com_id'],'','',1) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?php echo $paging?>
<?php echo $filter ?>