<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
echo validation_errors();
get_array($error_upload);
?>
<style>
.select_cate{
	width: 200px;
}
</style>
<form action="" method="POST"  enctype="multipart/form-data">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left">Câu hỏi</td>
			<td class="right"><?php echo $question ?> * Dùng [.] thể hiện từ cần điền</td>
		</tr>
		<tr>
			<td class="left">Trả lời</td>
			<td class="right"><?php echo $answer ?> * Cách nhau dấu ,</td>
		</tr>
        <tr>
			<td class="left">Ảnh</td>
			<td class="right"><?=$images ?>
                <?php if ($row['images'] != '') {?>
                <br/>
                <img src="<?php echo getimglink($row['images'],"size2") ?>" />
                <?php } ?>
            </td>
		</tr>
        <tr>
			<td class="left">Audio</td>
			<td class="right"><?=$audio ?></td>
		</tr>
        <tr>
			<td class="left">Topic</td>
			<td class="right"><?=$topic ?></td>
		</tr>
        <tr>
			<td class="left">Level</td>
			<td class="right"><?=$level ?></td>
		</tr>
        <tr>
			<td></td>
			<td class="right">
				<?=$submit ?>
			</td>
		</tr>
	</table>
</form>