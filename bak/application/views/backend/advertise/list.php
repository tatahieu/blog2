<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('adv_image'); ?></th>
		<th><?=$this->lang->line('adv_name'); ?></th>
        <th><?=$this->lang->line('adv_link'); ?></th>
        <th><?=$this->lang->line('adv_ordering'); ?></th>
        <th><?=$this->lang->line('adv_cate'); ?></th>
        <th><?=$this->lang->line('adv_publish'); ?></th>
		<?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){?>
	<tr>
		<td align="center"><?=$i?></td>
		<?=temp_del_check($row['adv_id']) ?>
		<td width="50px" align="center"><?=img(array('src' => getimglink($row['images'],'size2'),'width'=>'50')); ?></td>
        <td><?=$row['name']?></td>
        <td><?=$row['link']?></td>
        <td align="center" width="50px"><?=$row['ordering']?></td>
        <td><?=$row['cate_name']?></td>
		<td align="center" width="50px"><?=temp_status($row['publish'])?></td>
		<?=temp_edit_icon('advertise/edit/'.$row['adv_id'],'','',1) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?=$paging?>
<?=$filter; ?>