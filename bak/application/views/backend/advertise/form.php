<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
echo validation_errors();
get_array($error_upload);
?>
<form action="" method="POST"  enctype="multipart/form-data">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('adv_name') ?></td>
			<td class="right"><?=$name ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('adv_link') ?></td>
			<td class="right"><?=$link ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('adv_publish') ?></td>
			<td class="right"><?=$publish ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('adv_image') ?></td>
			<td class="right"><?=$image ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('adv_ordering') ?></td>
			<td class="right"><?=$ordering ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('adv_cate') ?></td>
			<td class="right"><?=$cate ?></td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$submit ?>
			</td>
		</tr>
	</table>
</form>