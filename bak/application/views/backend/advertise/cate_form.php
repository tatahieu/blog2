<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('adv_cate_name')?></td>
			<td class="right"><?=$cate_name?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('adv_cate_parent')?></td>
			<td class="right"><?=$cate_parent ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('adv_cate_ordering')?></td>
			<td class="right"><?=$cate_ordering ?></td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$cate_submit?>
			</td>
		</tr>
	</table>
</form>