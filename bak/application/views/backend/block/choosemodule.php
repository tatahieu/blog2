<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<table cellpadding="4px" width="100%">
    <tr>
		<td class="left">Chọn module</td>
		<td class="right">
            <select onchange="change_block_module(this.value)">
                <option  value="" selected="selected">Chọn module</option>
                <?php foreach ($option as $key => $val) {?>
                <option  value="<?php echo $key ?>"><?php echo $val ?></option>
                <?php } ?>
            </select>
        </td>
	</tr>
</table> 
<script>
    function change_block_module(val){
        if (val){
            var url = $site_url + '/block/add/' + val;
            redirect(url);
        }
    }
</script>       