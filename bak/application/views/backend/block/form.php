<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST" id="block_form">
	<table cellpadding="4px" width="100%">
        <tr>
			<td class="left"><?php echo $this->lang->line('block_module')?></td>
			<td class="right"><?php echo $module ?></td>
		</tr>
        <tr>
			<td class="left"><?php echo $this->lang->line('block_name')?></td>
			<td class="right"><?php echo $name ?></td>
		</tr>
        <tr>
			<td class="left"><?php echo $this->lang->line('block_publish')?></td>
			<td class="right"><?php echo $publish ?></td>
		</tr>
        <tr>
			<td class="left"><?php echo $this->lang->line('block_position')?></td>
			<td class="right"><?php echo $position ?></td>
		</tr>
        <?php
        if ($params) {
        foreach ($params as $label => $value) { ?>
        <tr>
			<td class="left"><?php echo $value['name'] ?></td>
			<td class="right"><?php echo $value['form'] ?></td>
		</tr>
        <?php }
        }?>
        <tr>
			<td class="left"><?php echo $this->lang->line('block_ordering')?></td>
			<td class="right"><?php echo $ordering ?></td>
		</tr>
        <?php if ($content) {?>
        <tr id="static_content">
			<td class="left">HTML</td>
			<td class="right">
                <?php echo $content ?>
            </td>
		</tr>
        <?php } ?>
        <tr>
            <td class="left">Thiết bị</td>
            <td class="right">
                <?php echo $device ?>
            </td>
            
        </tr>
        <tr>
            <td class="left">Truy cập</td>
			<td class="right">
                <br />
                <?php echo $access ?>
                <div style="margin-top: 7px;" id="module_access" >
                <?php
                echo $moduleaccess;
                ?>
                </div>
            </td>
        </tr>
    </table>
    <table id="" cellpadding="4px" width="100%">
		<tr>
			<td class="left"></td>
			<td class="right">
				<?php echo $submit?>
			</td>
		</tr>
	</table>
</form>
<script>
$(document).ready(function() {
    var cache = {};
	$( "#autoc" ).autocomplete({
		minLength: 2,
        focus: function( event, ui ) {
			$(this).val( ui.item.value );
			return false;
		},
        select: function( event, ui ) {
			$(this).val( ui.item.value);
            // set for param
            $("#proid").val(ui.item.id);
			return false;
		},
        change: function (event, ui) {
            if (!ui.item) {
                 $(this).val('');
            }
        },
		source: function( request, response ) {
		    request.module = '<?php echo $module_key ?>';
			var term = request.term;
            var key = term;
			if ( key in cache ) {
				response( cache[ key ] );
				return;
			}
			$.getJSON( $site_url + "menu/autocomplete", request, function( data, status, xhr ) {
				cache[key] = data;
				response( data );
			});
		}
	});
    $("#block_access").bind("change",function(){
        if ($(this).val() == 'all'){
            $("#module_access").hide();
        }
        else{
            $("#module_access").show();
        }
    });
    if ($("#block_access").val() == 'all'){
        $("#module_access").hide();
    }
    else{
        $("#module_access").show();
    }
})
</script>