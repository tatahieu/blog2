<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<?php
if (isset($error))
echo '<div class="error">'.$error.'</div>';
?>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('block_name'); ?></th>
        <th><?=$this->lang->line('block_params'); ?></th>
        <th><?=$this->lang->line('block_module'); ?></th>
        <th><?=$this->lang->line('block_position'); ?></th>
		<th><?=$this->lang->line('block_ordering'); ?></th>
        <th><?=$this->lang->line('block_publish'); ?></th>
		<?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?
	if (!empty($rows)){
        $i = 1;
        foreach ($rows as $row){?>
		<tr>
			<td align="center"><?=$i?></td>
			<?=temp_del_check($row['block_id']) ?>
			<td width="180px"><?=$row['name']?></td>
            <td><?=$row['params']?></td>
			<td width="90px"><?=$row['module']?></td>
            <td align="center" width="60px"><?=$row['position']?></td>

            <td align="center" width="60px"><?=$row['ordering']?></td>
            <td align="center" width="60px"><?=temp_status($row['publish'])?></td>

            <?=temp_edit_icon('block/edit/'.$row['block_id'],'','',1) ?>
		</tr>
		<?
		$i ++;
		}
	}
	?>
	</tbody>
</table>
</form>
<?=$filter?>