<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<?php
if (isset($error))
echo '<div class="error">'.$error.'</div>';
?>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="30px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('qtest_name'); ?></th>
        <?php if ($parent == 0){?>
        <th><?=$this->lang->line('qtest_description'); ?></th>
        <th>Part</th>
        <th>Add</th>
        <th>View</th>
        <th><?=$this->lang->line('qtest_cate'); ?></th>
        <?php }else{?>
        <th>Audio</th>
        <?}?>
        <th><?=$this->lang->line('qtest_id'); ?></th>
        <th><?=$this->lang->line('qtest_publish'); ?></th>
        <?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){?>
	<tr>
		<td align="center"><?=$i?></td>
		<?=temp_del_check($row['qtest_id']) ?>
		<td><?=$row['title']?></td>
		<?php if ($parent == 0){?>
        <td width="180px"><?=$row['description']?></td>
        <td align="center" width="40px"><?=$row['part']?></td>
        <td align="center" width="40px"><a href="<?=site_url('question_test/add')?>/?parent=<?=$row['qtest_id']?>">Thêm</a></td>
        <td align="center" width="40px"><a href="<?=site_url('question_test')?>/?parent=<?=$row['qtest_id']?>">Xem</a></td>
        <td align="center" width="70px"><?=$row['name']?></td>
        <?php }else{?>
        <td align="center" width="70px"><?=temp_status($row['sound'],array(),2)?></td>
        <?}?>
        <td align="center" width="40px"><?=$row['qtest_id']?></td>
        <td align="center" width="60px"><?=temp_status($row['publish'])?></td>
		<?=temp_edit_icon('question_test/edit/'.$row['qtest_id']) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?=$paging?>
<?=$filter; ?>