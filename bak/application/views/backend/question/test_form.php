<?php if (!defined('BASEPATH'))
exit('No direct script access allowed');
echo validation_errors();

?>
<style>
.select_cate{
width: 200px;
}
</style>
<form action="" method="POST" onsubmit="return check_submit()"  enctype="multipart/form-data">
<table cellpadding="4px" width="100%">
    <tr>
        <td class="left"><?=$this->lang->line('qtest_name') ?></td>
        <td class="right"><?=$title ?></td>
    </tr>
    <?php if ($parent == 0){?>
    <tr>
        <td class="left"><?=$this->lang->line('qtest_cate') ?></td>
        <td class="right"><?=$category ?></td>
    </tr>
    <tr>
        <td class="left">Part</td>
        <td class="right"><?=$part ?></td>
    </tr>
    <?}else{?>
        <tr>
    		<td class="left"><?=$this->lang->line('ques_sound') ?></td>
    		<td class="right"><?=$sound ?></td>
	   </tr>
    <?}?>

    <tr>
        <td class="left"><?=$this->lang->line('qtest_publish') ?></td>
        <td class="right">
        <?=$publish ?>
        </td>
    </tr>
    <tr>
        <td class="left"><?=$this->lang->line('qtest_description'); ?></td>
        <td class="right">
        <?=$description?>
        </td>
    </tr>
    <tr>
        <td class="left"><?=$this->lang->line('qtest_question'); ?></td>
        <td class="right">
        <div class="qtest_question clr">
        <div id="add_question" style="float: left; display: none;">
            <div style="margin-top: 5px;">
                <?=$question?>
            </div>
        </div>
        <?=img(array('src'=>'add_icon.jpg','style' => 'margin: 5px;')); ?>
        <span id="count_question" style="float: left; margin: 0 10px; line-height: 30px;">0</span>
        </div>
        <div class="add_question_content">
            <?php if (!empty($qselected)){
                foreach ($qselected as $qselected){?>
                    <div style="margin-top: 5px;"><input class="autoc" type="text" name="question[]" value="<?=$qselected['question_id']?>"/><span style="margin-left: 10px; width: 300px; overflow: hidden"><?=cut_text($qselected['title'],40)?></span></div>

                <?}
            }?>
        </div>
        </td>
    </tr>
    <tr>
        <td class="left">
            Quyền lớp học
        </td>
        <td class="right">
            <?php echo $role ?>
        </td>
    </tr>
    <tr>
        <td></td>
        <td class="right">
            <?=$submit ?>
        </td>
    </tr>
</table>
</form>

<script language="javascript">
$("#count_question").text($(".add_question_content div").size());
$(".qtest_question img").live("click",function(){
    var count = parseInt($("#count_question").text());
    count = count + 1;
    $("#count_question").text(count);

    var html = $("#add_question").html();
    $(".add_question_content").append(html);
});
function check_submit(){
    if ($("input[name=title]").val() == ""){
    alert("Chưa nhập tiêu đề");
    $("input[name=title]").focus();
    return false;
    }
}
$(".autoc").live("focus", function (event) {
    $(this).autocomplete("<?=site_url('question_test/autocomplete')?>", {
        //width: 260,
        minChars: 2,
    	selectFirst: true,
        multiple: false,
        dataType: "json",
        delay: 100,
        mustMatch: false,
        matchContains: false,
    	// autoFill: true,
    	parse: function(data) {
        	return $.map(data, function(row) {
        		return {
        			data: row,
        			value: row.title, // hien thi luc tim kiem
        			result: row.question_id // luc chon se
        		}
        	});
        },
        formatItem: function(item) {
    		return item.title + " ("+item.question_id+")";
    	}
    }).result(function(e, item) {
        $(this).parent().find("span").remove();
        $(this).parent().append('<span style="margin-left: 10px; width: 300px; overflow: hidden">'+item.title+'</span>');
    });
});
</script>