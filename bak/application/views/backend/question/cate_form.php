<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('ques_cate_name')?></td>
			<td class="right"><?=$cate_name?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('ques_cate_ordering')?></td>
			<td class="right"><?=$cate_ordering ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('ques_cate_parent')?></td>
			<td class="right"><?=$cate_parent ?></td>
		</tr>
        <tr>
			<td class="left">Tin tức liên quan</td>
			<td class="right"><?=$relate_news ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('ques_cate_description')?></td>
			<td class="right"><?=$cate_description ?></td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$cate_submit?>
			</td>
		</tr>
	</table>
</form>