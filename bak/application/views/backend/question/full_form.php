<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left">Tên</td>
			<td class="right"><?=$name?></td>
		</tr>
        <tr>
			<td class="left">Loại</td>
			<td class="right"><?=$type?></td>
		</tr>
        <tr>
			<td class="left">Part 1</td>
			<td class="right"><?=$part1?></td>
		</tr>
        <tr>
			<td class="left">Part 2</td>
			<td class="right"><?=$part2?></td>
		</tr>
        <tr>
			<td class="left">Part 3</td>
			<td class="right"><?=$part3?></td>
		</tr>
        <tr>
			<td class="left">Part 4</td>
			<td class="right"><?=$part4?></td>
		</tr>
        <tr>
			<td class="left">Part 5</td>
			<td class="right"><?=$part5?></td>
		</tr>
        <tr>
			<td class="left">Part 6</td>
			<td class="right"><?=$part6?></td>
		</tr>
        <tr>
			<td class="left">Part 7</td>
			<td class="right"><?=$part7?></td>
		</tr>
        <tr>
			<td class="left">Thời gian (phút): </td>
			<td class="right"><?=$time?></td>
		</tr>
		<tr>
			<td class="left">Hiện đáp án </td>
			<td class="right"><?=$show_answer ?></td>
		</tr>
        <tr>
			<td class="left">Phân cho lớp: </td>
			<td class="right"><?=$role?></td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$cate_submit?>
			</td>
		</tr>
	</table>
</form>