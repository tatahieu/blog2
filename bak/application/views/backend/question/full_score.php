<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="export_form">
<div class="search">
	<button class="submit_button" id="export_data" type="submit" value="1" name="export">Export Data</button>
	<?=temp_quick_search() ?>
</div>
<?php
if (isset($error))
echo '<div class="error">'.$error.'</div>';
?>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="50px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<th>Người làm bài</th>
        <th>Điểm list</th>
        <th>Điểm read</th>
        <th>Tổng điểm</th>
        <th>Thời gian</th>
        <th>Kết thúc</th>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){?>
	<tr>
		<td align="center"><?=$i?></td>
        <td width="40px" align="center"><?=$row['fullname']?> - <?=$row['email']?> - <?php echo $row['phone'] ?> - <?php echo $row['birthday'] ?></td>
        <td width="60px" align="center"><?=$row['list_score']?>/<?php echo $row['list_correct']; ?></td>
        <td width="60px" align="center"><?=$row['read_score']?>/<?php echo $row['read_correct']; ?></td>
        <td width="60px" align="center"><?=$row['total_score']?></td>
        <td width="80px" align="center"><?=date('d/m/Y H:i:s',$row['create_time'])?></td>
        <td width="80px" align="center"><?php echo $row['update_time'] ? date('d/m/Y H:i:s',$row['update_time']) : 'Chưa hoàn thành'?></td>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?=$paging?>
<?=$filter; ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#filter_from_date, #filter_to_date').datepicker({
        	dateFormat: "yy-mm-dd",
        	changeMonth: true,
      		changeYear: true
    	});
    	$("#export_data").bind("click",function(){

    	})
	});	
</script>