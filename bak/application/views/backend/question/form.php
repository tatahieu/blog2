<?php if (!defined('BASEPATH'))
exit('No direct script access allowed');
echo validation_errors();
get_array($error_upload);
?>

<style>
.select_cate{
	width: 200px;
}
</style>
<form action="" method="POST"  enctype="multipart/form-data">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('ques_name') ?></td>
			<td class="right"><?=$title ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('ques_cate') ?></td>
			<td class="right">
				<?=$category ?>
			</td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('ques_publish') ?></td>
			<td class="right">
				<?=$publish ?>
			</td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('ques_images') ?></td>
			<td class="right"><?=$image ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('ques_sound') ?></td>
			<td class="right"><?=$sound ?></td>
		</tr>
        <tr>
            <td class="left"><?=$this->lang->line('ques_detail'); ?></td>
            <td class="right">
                <?=$detail?>
            </td>
        </tr>
		<tr>
			<td class="left"><?=$this->lang->line('ques_date_up') ?></td>
			<td class="right">
				<?=$date_up ?>
			</td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('ques_answer') ?></td>
			<td class="right">
				<table width="100%" class="question_answer">
                <?php
                for ($i = 1; $i < 5; $i ++){?>
                    <tr>
                        <td width="60px"><?=$i?>. </td>
                        <td><?=$answer[$i]?> <?=$correct[$i]?></td>
                    </tr>
                <?}?>



                </table>
			</td>
		</tr>
        <tr>
			<td></td>
			<td class="right">
				<?=$submit ?>
			</td>
		</tr>
	</table>
</form>
<script language="javascript">
    $(".question_answer input[type=checkbox]").bind("click",function(){
        $(".question_answer input").attr("checked",false);
        $(this).attr("checked",true);
    })
</script>