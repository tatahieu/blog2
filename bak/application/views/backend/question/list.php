<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="30px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('ques_name'); ?></th>
        <th><?=$this->lang->line('ques_images'); ?></th>
        <th><?=$this->lang->line('ques_sound'); ?></th>
        <th>Test id</th>
        <th><?=$this->lang->line('ques_publish'); ?></th>
        <th><?=$this->lang->line('ques_id'); ?></th>
        <?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){?>
	<tr>
		<td align="center"><?=$i?></td>
		<?=temp_del_check($row['question_id']) ?>
		<td><?=$row['title']?></td>
		<td align="center" width="70px"><?=temp_status($row['images'],array(),2)?></td>
		<td align="center" width="90px"><?=temp_status($row['sound'],array(),2)?></td>
        <td align="center" width="50px"><a href="<?=site_url("question_test/edit/".$row['test_id'])?>" target="_blank"><?=$row['test_id']?></a></td>
        <td align="center" width="60px"><?=temp_status($row['publish'])?></td>
        <td align="center" width="70px"><?=$row['question_id']?></td>
		<?=temp_edit_icon('question/edit/'.$row['question_id']) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?=$paging?>
<?=$filter; ?>