<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<?php
if (isset($error))
echo '<div class="error">'.$error.'</div>';
?>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="50px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th>Tên</th>
        <th>Loại</th>
        <th>Part 1</th>
        <th>Part 2</th>
        <th>Part 3</th>
        <th>Part 4</th>
        <th>Part 5</th>
        <th>Part 6</th>
        <th>Part 7</th>
        <th>Time</th>
		<?=temp_edit_head(); ?>
		<th>Score</th>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){
	foreach ($row as $row){?>
	<tr>
		<td align="center"><?=$i?></td>
		<?=temp_del_check($row['full_id']) ?>
		<td><?=$row['name']?></td>
        <td width="40px" align="center"><?=$show_type[$row['type']]?></td>
        <td width="40px" align="center"><?=$row['part1']?></td>
        <td width="40px" align="center"><?=$row['part2']?></td>
        <td width="40px" align="center"><?=$row['part3']?></td>
        <td width="40px" align="center"><?=$row['part4']?></td>
        <td width="40px" align="center"><?=$row['part5']?></td>
        <td width="40px" align="center"><?=$row['part6']?></td>
        <td width="40px" align="center"><?=$row['part7']?></td>
        <td width="40px" align="center"><?=$row['time']?></td>
        <?=temp_edit_icon('question/full_edit/'.$row['full_id']) ?>
        <td width="60px"><a href="<?php echo site_url('/question/full_score/'.$row['full_id']); ?>">Xem điểm</a></td>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?=$paging?>