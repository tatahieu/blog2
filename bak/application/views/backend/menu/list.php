<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>

<form action="" method="POST" class="delete_form">
<div class="search">
	<?=temp_delete_icon()?>
	<?=temp_quick_search() ?>
</div>
<?php
if (isset($error))
echo '<div class="error">'.$error.'</div>';
?>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="40px">
			<?=$this->lang->line('common_no'); ?>
		</th>
		<?=temp_del_head(); ?>
		<th><?=$this->lang->line('menu_name'); ?></th>
        <th><?=$this->lang->line('menu_link'); ?></th>
        <th><?=$this->lang->line('menu_target'); ?></th>
        <th><?=$this->lang->line('menu_position'); ?></th>
		<th><?=$this->lang->line('menu_ordering'); ?></th>
		<?=temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?
	show_menu($row);
	function show_menu($row,$parent = 0,$level = '',$i = 1){
		if (!empty($row[$parent])){

            $position = $row[0][$i-1]['position'];
            foreach ($row[$parent] as $r){
			$name = ($level == '') ? '<span class="data_level1">'.$r['name'].'</span>' : $r['name'];
			?>
			<tr>
				<td align="center"><? if ($parent == 0) echo $i?></td>
				<?=temp_del_check($r['menu_id']) ?>
				<td><?=$level.$name?></td>
				<td><?=$r['link']?></td>
                <td><?=$r['target']?></td>
                <td><? if($parent!= 0 AND $position != $r['position']) echo '<div class="warming">'.$r['position'].'</div>'; else echo $r['position']; ?></td>
                <td><?=$r['ordering']?></td>
				<?=temp_edit_icon('menu/edit/'.$r['menu_id'],'','',1) ?>
			</tr>
			<?
			$i ++;
			show_menu($row,$r['menu_id'],$level.' -- ');
			}
		}
	}?>
	</tbody>
</table>
</form>
<?=$filter?>