<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left"><?=$this->lang->line('menu_name')?></td>
			<td class="right"><?=$name?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('menu_link')?></td>
			<td class="right">
                <p class="menu_module"><?=$module?><?=$getname?></p>
                <?php echo $item_id ?>
                <?=$link?>
            </td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('menu_ordering')?></td>
			<td class="right"><?=$ordering ?></td>
		</tr>
		<tr>
			<td class="left"><?=$this->lang->line('menu_position')?></td>
			<td class="right"><?=$position ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('menu_parent')?></td>
			<td class="right"><?=$parent ?></td>
		</tr>
        <tr>
			<td class="left"><?=$this->lang->line('menu_target');?></td>
			<td class="right"><?=$target?></td>
		</tr>
		<tr>
			<td></td>
			<td class="right">
				<?=$submit?>
			</td>
		</tr>
	</table>
</form>
<script>

$(document).ready(function(){
    // block run
    if (typeof(menu) != 'undefined'){
        menu.defaultparam = jQuery.parseJSON('<?php echo $paramdefault ?>');
        menu.run();
    }
    ///////////////////// complete /////////////////////////////////////
})
</script>