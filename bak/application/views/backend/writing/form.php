<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
echo validation_errors();
get_array($error_upload);
?>
<form action="" method="POST"  enctype="multipart/form-data">
	<table cellpadding="4px" width="100%">
		<tr>
			<td class="left">Tiêu đề</td>
			<td class="right"><?=$title ?></td>
		</tr>
		<tr>
			<td class="left">Ảnh</td>
			<td class="right"><?=$images ?>
            <?php if ($row['images']){?>
            <p style="margin: 5px 0;" id="media_images"><img src="<?php echo getimglink($row['images'],'size2'); ?>"/> - <a href="javascript:delete_media('images')">Delete</a></p>
            <?php } ?>
            </td>
		</tr>
        <tr>
			<td class="left">Chủ đề</td>
			<td class="right"><?=$topic ?></td>
		</tr>
        <tr>
			<td class="left">Level</td>
			<td class="right"><?=$level ?></td>
		</tr>
        <tr>
            <td class="left">Câu hỏi</td>
            <td class="right">
                <?=$question?>
            </td>
        </tr>
                <tr>
            <td class="left">Trả lời</td>
            <td class="right">
                <?=$answer?>
            </td>
        </tr>
        <tr>
			<td></td>
			<td class="right">
				<?=$submit ?>
			</td>
		</tr>
	</table>
</form>
<script>
    function delete_media(type){
        $.post("",{delete_media: type}, function(data) {
            if (data.status == 'success'){
                $("#media_"+type).remove();
            }
        },'JSON');
    }
</script>