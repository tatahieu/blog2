<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?php echo temp_delete_icon()?>
	<?php echo temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="30px">
			<?php echo $this->lang->line('common_no'); ?>
		</th>
		<th>Tên</th>
        <th>Thời gian</th>
        <th>Thành viên</th>
        <th>Nhận xét</th>
		<?php echo temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){

	foreach ($row as $row){?>
	<tr>
		<td align="center"><?php echo $i?></td>

		<td><?php echo $row['test_name']?> - <?php echo $row['test_id']; ?></td>
        <td width="160px"><?php echo date('H:i:s | d/m/Y',$row['time'])  ?></td>
        <td width="150px"><?php echo $row['username']  ?></td>
        <td width="70px" align="center"><?php echo temp_status($row['comment_teacher']) ?></td>

		<?php echo temp_edit_icon(BASE_URL.'writing/preview/'.$row['test_id'].'/'.$row['time'].'/'.$row['user_id']) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?php echo $paging?>
<?php echo $filter; ?>