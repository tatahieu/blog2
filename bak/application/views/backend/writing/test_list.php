<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?php echo temp_delete_icon()?>
	<?php echo temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="30px">
			<?php echo $this->lang->line('common_no'); ?>
		</th>
		<?php echo temp_del_head(); ?>
		<th>Tên</th>
        <?php for ($i = 1; $i <= 8; $i ++) {?>
        <th>Câu <?php echo $i ?></th>
        <?php } ?>
        <th>Part</th>
        <th>Level</th>
		<?php echo temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){

	foreach ($row as $row){?>
	<tr>
		<td align="center"><?php echo $i?></td>
		<?php echo temp_del_check($row['test_id']) ?>
		<td><?php echo ($row['password']) ? '<b>'.$row['name'].'</b>' : $row['name']; ?></td>
        <?php for ($i = 1; $i <= 8; $i ++) {?>
		<td width="50px" align="center"><?php echo $row['question_'.$i] ?></td>
        <?php } ?>
        <th align="center" width="40px"><?php echo $row['type']; ?></th>
        <th><?php if ($row['level']) echo get_level($row['level']);?></th>
		<?php echo temp_edit_icon('writing/test_edit/'.$row['test_id'],'','',1) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?php echo $paging?>
<?php echo $filter; ?>