<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
echo validation_errors();
?>
<form action="" method="POST">
	<table style="float: left; width: 50%;" cellpadding="4px" >
		<tr>
			<td class="left">Tiêu đề</td>
			<td class="right"><?=$name ?></td>
		</tr>
        <tr>
            <td class="left">Part</td>
			<td class="right"><?=$type ?></td>
        </tr>
        <tr>
			<td class="left">Level</td>
			<td class="right"><?=$level ?></td>
		</tr>
		<tr class="writing_question" id="question_1">
			<td class="left">Câu 1</td>
			<td class="right"><?=$question_1 ?></td>
		</tr>
        <tr class="writing_question" id="question_2">
			<td class="left">Câu 2</td>
			<td class="right"><?=$question_2 ?></td>
		</tr>
        <tr class="writing_question" id="question_3">
			<td class="left">Câu 3</td>
			<td class="right"><?=$question_3 ?></td>
		</tr>
        <tr class="writing_question" id="question_4">
			<td class="left">Câu 4</td>
			<td class="right"><?=$question_4 ?></td>
		</tr>
        <tr class="writing_question" id="question_5">
			<td class="left">Câu 5</td>
			<td class="right"><?=$question_5 ?></td>
		</tr>
        <tr class="writing_question" id="question_6">
			<td class="left">Câu 6</td>
			<td class="right"><?=$question_6 ?></td>
		</tr>
        <tr class="writing_question" id="question_7">
			<td class="left">Câu 7</td>
			<td class="right"><?=$question_7 ?></td>
		</tr>
        <tr class="writing_question" id="question_8">
			<td class="left">Câu 8</td>
			<td class="right"><?=$question_8 ?></td>
		</tr>
        <tr>
			<td class="left">Quyền trong lớp</td>
			<td class="right"><?=$role ?></td>
		</tr>
        <tr>
			<td class="left">Mật khẩu</td>
			<td class="right"><?=$password ?></td>
		</tr>
        <tr>
			<td></td>
			<td class="right">
				<?=$submit ?>
			</td>
		</tr>
	</table>
 </form>
    <table class="table_list" style="float: left; width: 50%;" cellpadding="4px">
		<tr>
			<th>#</th>
			<th>Câu hỏi</th>
            <th>Level</th>
            <th>ID</th>
		</tr>
        <?php
        $i = 1;
        foreach ($suggest as $sug) { ?>
        <tr>
			<td width="20px" align="center"><?php echo $i ?></td>
			<td><?php echo $sug['title'] ?></td>
            <td width="30px" align="center"><?php echo $fulllevel[$sug['level']] ?></td>
            <td width="30px" align="center"><?php echo $sug['writing_id'] ?></td>
		</tr>
        <?php $i++;} ?>
	</table>
 <div class="clear"></div>
<script>
    function change_type_test(val){
        var type_to_question = $.parseJSON('<?php echo $jstype ?>');
        if (val == 0){
            $(".writing_question").show();
        }
        else{
            $(".writing_question").hide();
            var arr = type_to_question[val];
            $.each(arr , function(index, value) {
                $("#question_" + value).show();
            });
        }
    }
    change_type_test($("#writing_type").val());
</script>