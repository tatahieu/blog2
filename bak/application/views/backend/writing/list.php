<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$i = 1;
?>
<form action="" method="POST" class="delete_form">
<div class="search">
	<?php echo temp_delete_icon()?>
	<?php echo temp_quick_search() ?>
</div>
<table class="table_list">
	<thead>
	<tr class="title">
		<th width="30px">
			<?php echo $this->lang->line('common_no'); ?>
		</th>
		<?php echo temp_del_head(); ?>
		<th>Tiêu đề</th>
        <th>Level</th>
        <th>Chủ đề</th>
		<th>Ảnh</th>
        <th>ID</th>
		<?php echo temp_edit_head(); ?>
	</tr>
	</thead>
	<tbody>
	<?php
	if (!empty($row)){

	foreach ($row as $row){?>
	<tr>
		<td align="center"><?php echo $i?></td>
		<?php echo temp_del_check($row['writing_id']) ?>
		<td><?php echo $row['title']?></td>
        <td width="120px"><?php echo $level[$row['level']]?></td>
        <td width="120px"><?php echo $topic[$row['topic']]?></td>
		<td align="center" width="60px"><?php echo temp_status($row['images'])?></td>
        <td align="center" width="50px"><?php echo $row['writing_id']?></td>
		<?php echo temp_edit_icon('writing/edit/'.$row['writing_id'],'','',1) ?>
	</tr>
	<?
	$i ++;
	}
	}?>
	</tbody>
</table>
</form>
<?php echo $paging?>
<?php echo $filter; ?>