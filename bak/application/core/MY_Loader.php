<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class MY_Loader extends CI_Loader{
	var $_queue = array();
    var $_registry = array();
	var $data = array();
    var $position = array();
    protected $_block;
	function __construct(){
		parent::__construct();
		$ci                      = get_instance();
        $class                   = strtolower(get_class($ci));
        $this->_registry[$class] = &$ci;
        $this->_queue[]          = &$this->_registry[$class];
 	}
	function model($model, $name = '', $db_conn = FALSE)
	{
		if (is_array($model))
		{
			foreach ($model as $babe)
			{
				$this->model($babe);
			}
			return;
		}
		if ($model == '')
		{
			return;
		}
		$CI =& get_instance();
		$model = $CI->config->item('panel').'/'.$model;
		$path = '';

		// Is the model in a sub-folder? If so, parse out the filename and path.
		if (($last_slash = strrpos($model, '/')) !== FALSE)
		{
			// The path is in front of the last slash
			$path = substr($model, 0, $last_slash + 1);

			// And the model name behind it
			$model = substr($model, $last_slash + 1);
		}

		if ($name == '')
		{
			$name = $model;
		}

		if (in_array($name, $this->_ci_models, TRUE))
		{
			return;
		}
		if (isset($CI->$name))
		{
			show_error('The model name you are loading is the name of a resource that is already being used: '.$name);
		}

		$model = strtolower($model);

		foreach ($this->_ci_model_paths as $mod_path)
		{
			if ( ! file_exists($mod_path.'models/'.$path.$model.EXT))
			{
				continue;
			}

			if ($db_conn !== FALSE AND ! class_exists('CI_DB'))
			{
				if ($db_conn === TRUE)
				{
					$db_conn = '';
				}

				$CI->load->database($db_conn, FALSE, TRUE);
			}

			if ( ! class_exists('CI_Model'))
			{
				load_class('Model', 'core');
			}

			require_once($mod_path.'models/'.$path.$model.EXT);

			$model = ucfirst($model);

			$CI->$name = new $model();

			$this->_ci_models[] = $name;
			return;
		}

		// couldn't find the model
		show_error('Unable to locate the model you have specified: '.$model);
	}
	/**
	 * $type = 0: load controller from root
	 * $type = 1: load controller from module
	 */
	function controller($segments = array(),$type = 'module'){

		$ci = get_instance();
		if (!is_array($segments)){
        	$segments = explode('/',$segments);
        }
        $count = count($segments);
		/** NEW SET **/

		$class = $segments[0];
        $method = (array_key_exists(1,$segments)) ? $segments[1] : 'index';
		$directory = $ci->router->directory;
        $directory = $directory.'/'.$type.'/';
        $directory = str_replace('//','/',$directory);

		/**
		 * echo $directory;
		 * 		echo $class;
		 * 		echo $method;
		 */
		$path      = APPPATH . 'controllers/' . $directory . $class . EXT;

        if (!class_exists($class)) {
            include_once($path);
        }

        // I disapprove!
        if (!class_exists($class) || $method == 'controller' || strncmp($method, '_', 1) == 0) {
            show_error('Wick could not light ' . $class . '/' . $method . ', either the class does not exist or the method is protected.');
        }

        // Fetches controller from registry or creates new one
        if (isset($this->_registry[$class])) {
            $controller = &$this->_registry[$class];
        } else {
            $controller              = new $class();
            $this->_registry[$class] = &$controller;
        }
        $this->_queue[] = &$controller;


        // Throws error if method doesn't exist
        if (!in_array(strtolower($method), array_map('strtolower', get_class_methods($controller)))) {
            show_error('Wick could not light "' . $class . '/' . $method . '". The method doesn\'t exist.');
        }

        // Tranfers data into the controller, calls it
        // and transfers data back into the previous one
        $trans     = &$this->_transfer($controller);
        $return = call_user_func_array(array(&$trans, $method), array_slice($segments, 2));
        $this->_transfer($this->_queue[count($this->_queue)-1]);

        // Pops the controller off the queue
        array_pop($this->_queue);

        // Returns whatever the mighty controller fancies
        return $return;
	}
	function &_transfer(&$to)
    {
        $ci = &get_instance();
        foreach (array_keys(get_object_vars($ci)) as $variable) {
            $to->$variable = &$ci->$variable;
        }

        $ci = $to;

        return $ci;
    }
    function layout($view, $data=null, $return = FALSE, $layout = '')
    {
        if (is_array($layout)) {
            if (DEVICE_ENV == 1) {
                $layout = $layout['mobile'];
            }
        }
        if (is_array($view)) {
            if (DEVICE_ENV == 1) {
                $view = $view['mobile'];
            }
            else {
                $view = $view['pc'];
            }
        }
    	$CI = &get_instance();
        //// load block ////
        $this->model('block_model','block');
        $CI->block->get_all_block();
        ///////////////////
        if ($view != ''){
			$this->data['content_for_layout'] = $this->view($view,$data,true);
		}
		else{
			$this->data['content_for_layout'] = $data;
		}
        //$this->controller('common/index');
		if ($layout == ''){
            $layout = $CI->config->item('layout');
		}
        $this->view($layout , $this->data, false);
        if (ENVIRONMENT == 'development'){
            $sections = array(
                'config'  => false,
                'http_headers' => false
                );
            
            $CI->output->set_profiler_sections($sections);
            $CI->output->enable_profiler(TRUE);
        }
    }
    function view($view, $vars = array(), $return = TRUE)
	{
		$CI = &get_instance();
		$view = $CI->config->item('panel').'/'.$view;
		return $this->_ci_load(array('_ci_view' => $view, '_ci_vars' => $this->_ci_object_to_array($vars), '_ci_return' => $return));
	}
    /**
     * $potion = array(
	 * 			'left' => 'data_left'),
	 * 			'right => 'data_right'
	 * 			);
     */
    function setData($position,$data = ''){
    	if (is_array($position)){
    		foreach ($position as $key => $value){
                $this->data[$key] = (array_key_exists($key,$this->position)) ? $this->position[$key] : $value;
    		}
    	}
    	else{
    	   $this->data[$position] = (array_key_exists($position,$this->position)) ? $this->position[$position] :  $data;
   	    }
    }
    /** 
     * $pre: TRUE, FALSE - Position push data on array() - default end
     */
    function setArray($position,$data = array(),$pre = FALSE){
        
        if (is_array($position)){
            foreach ($position as $key => $value){
                $this->setArray($key,$value);
            }
            return true;
        }
        
        if (array_key_exists($position,$this->data)){
            //echo 456;
            $d = $this->data[$position];
            if (!is_array($d)){
                $d = array($d);
            }
            if ($pre == TRUE){
                array_unshift($d,$data);
            }
            else{
                array_push($d,$data);
            }
            $this->data[$position] = $d;
        }
        else{
            $this->data[$position] = array($data);
        }
    }
    public function get_block($position){
        return get_array($this->data[$position]);
    }
}