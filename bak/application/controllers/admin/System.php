<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class System extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->lang->load('backend/system');
		$this->load->setData('title',$this->lang->line('sys_title'));
	}
    function backup(){
        // ini_set("memory_limit","256M");
        //$this->db->save_queries = false;  
        // Load the DB utility class
        $this->load->dbutil();
        
        // Backup your entire database and assign it to a variable
        $backup = $this->dbutil->backup();
        // Load the file helper and write the file to your server
        $this->load->helper('file');
        write_file('mybackup.gz', $backup);
        
        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        force_download('mybackup.gz', $backup); 
    }
} ?>