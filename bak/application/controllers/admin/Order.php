<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->lang->load('backend/order');
		$this->load->setData('title',$this->lang->line('order_title'));
	}
	public function index(){
		if ($this->input->post('delete'))
		{
			return $this->_action('delete');
		}
		$this->load->setArray(array("isLists" => 1));
		$data = $this->_index();
		// render view
		$this->load->layout('order/list',$data);
	}
	public function edit($id) {
        $id = (int) $id;
        if ($this->input->post('submit')) {
            return $this->_action('edit',array('id' => $id));
        }
        $this->load->setArray(array("isForm" => 1));
        $data = $this->_edit($id);
        $this->load->layout('order/form',$data);
    }
	private function _index(){
		$limit = $this->config->item("limit_item");
		$this->load->model('admin/order_model','order');
		// get level of user 
		$page = (int) $this->input->get('page');
		$offset = ($page > 1) ? ($page - 1) * $limit : 0;
		$params = array('limit' => $limit + 1,'offset' => $offset);
		////////////////// FITER /////////
		$params_filter = array_filter(array(
			'order_id' => $this->input->get('order_id'),
			'status' => $this->input->get('status'),
			'fullname' => $this->input->get('fullname'),
			'payment_status' => $this->input->get('payment_status')
		),'filter_item');
		$params = array_merge($params,$params_filter);
		// get data
		$rows = $this->order->lists($params);
		/** PAGING **/
		$config['total_rows'] = count($rows);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$paging = $this->paging->create_links();
		unset($rows[$limit]);
		// load config
		$this->load->config('data');
		// set limit
		$this->load->setArray(array("isLists" => 1));
		// set data
		return array('rows' => $rows, 'paging' => $paging,'filter' => $params_filter);
	}
	public function _edit($id){
		$id = (int) $id;
        $this->load->model('admin/order_model','order');
        // get order
        $row = $this->order->detail($id);
        if (!$row) {
            show_404();
        }
        // get detail order
        $detail = $this->order->order_detail($id);
        // get config 
        $this->load->config('data');
        // set data to view
        return  array(
            'row' => $row,
            'detail' => $detail
        );
    }
	public function _action($action, $params = array()) {
		$this->load->model('admin/order_model','order');
		switch ($action) {
			case 'edit':
				
					$input = array(
						'status' => (int) $this->input->post('status'),
						'payment_status' => (int) $this->input->post('payment_status'),
					);
					
					if ($this->security->verify_token_post($params['id'],$this->input->post('token'))) {
						$result = $this->order->update($params['id'],$input);	
					}
					if ($result) {
						$html =$this->load->view('order/form',$this->_edit($params['id'])); 
						return $this->output->set_output(json_encode(array('status' => 'success', 'html' => $html, 'result' => $result, 'message' => $this->lang->line("common_update_success"))));
					}
				
				break;
			case 'delete':
				$arrId = $this->input->post('cid');
				$arrId = (is_array($arrId)) ? array_map('intval', $arrId) : (int) $arrId;
				if (!$arrId) {
					return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_delete_min_select"))));
				}
				if (!$this->permission->check_permission_backend('delete')){
					return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_access_denied"))));
				}
				$result = $this->order->delete($arrId);
				
				if ($result) {
					$html = $this->load->view('order/list',$this->_index()); 
					return $this->output->set_output(json_encode(array('status' => 'success','html' => $html, 'result' => $result, 'message' => $this->lang->line("common_delete_success"))));
				}
			break;
			default:
				# code...
				break;
		}	
		return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_no_row_update"))));
	}
}