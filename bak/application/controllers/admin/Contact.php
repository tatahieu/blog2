<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact extends CI_Controller{
    function __construct(){
		parent::__construct();
		$this->lang->load('backend/contact');
		$this->load->setData('title',$this->lang->line('contact_title'));
	}
    public function index(){
        if ($this->input->post('delete'))
        {
            return $this->_action('delete');
        }
        $this->load->setArray(array("isLists" => 1));
        $data = $this->_index();
        // render view
        $this->load->layout('contact/list',$data);
    }
    public function edit() {
        if ($this->input->post('submit')){
            return $this->_action('edit');
        }
        return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_update_validator_error"))));
    }
    public function export(){
        $this->load->config('data');
        $local = $this->config->item('local');
        $params = array_filter(array(
            'local' => (int)$this->input->get('local'),
            'start_time' => ($this->input->get('start_time')) ? convert_datetime($this->input->get('start_time'),3) : 0,
            'end_time' => ($this->input->get('end_time')) ? convert_datetime($this->input->get('end_time'),3) : 0,
        ),'filter_item');
        $this->load->model('admin/contact_model','contact');   
        $rows = $this->contact->export($params);
        $filename = 'Export-'.date('d-m-Y').'.xlsx';
        $this->load->library('PHPExcel');
        $objPHPExcel = new PHPExcel();
        $i = 1; 
        $baseRow = 2;
        foreach($rows as $key => $row){   
            $count = $baseRow + $key;
            if($i == 1){
                $objPHPExcel->getActiveSheet(0)
                ->setCellValue('A'.$i, "Thời gian đăng ký")
                ->setCellValue('B'.$i, "Cơ sở")
                ->setCellValue('C'.$i, "Họ và tên đệm")
                ->setCellValue('D'.$i, "Tên")
                ->setCellValue('E'.$i, "Email")
                ->setCellValue('F'.$i, "Số điện thoại")
                ->setCellValue('G'.$i, "Ngày sinh")
                ->setCellValue('H'.$i, "Lớp học")
                ->setCellValue('I'.$i, "Nội dung");
            }
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($count,1);
            $objPHPExcel->getActiveSheet(0)
                ->setCellValue('A'.$count, date('d/m/Y H:i',$row['create_time']))
                ->setCellValue('B'.$count, $local[$row['local']])
                ->setCellValue('C'.$count, $row['firstname'])
                ->setCellValue('D'.$count, $row['lastname'])
                ->setCellValue('E'.$count, $row['email'])
                ->setCellValue('F'.$count, $row['phone'])
                ->setCellValue('G'.$count, $row['birthday'])
                ->setCellValue('H'.$count, $row['title'])
                ->setCellValue('I'.$count, $row['content']);
            $i++;
        }
        $objPHPExcel->getActiveSheet()->setTitle('Export-'.date('d-m-Y'));
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachement; filename="' . $filename . '"');
        ob_end_clean();
        return $objWriter->save('php://output');exit();
    }
    public function _index(){
        $limit = $this->config->item("limit_item");
        $this->load->model('admin/contact_model','contact');        
        // get paging
        $page = (int) $this->input->get('page');
        $offset = ($page > 1) ? ($page - 1) * $limit : 0;
        $params = array('limit' => $limit + 1,'offset' => $offset);
        ////////////////// FITER /////////
        $params_filter = array_filter(array(
            'local' => (int)$this->input->get('local'),
            'start_time' => ($this->input->get('start_time')) ? convert_datetime($this->input->get('start_time'),3) : 0,
            'end_time' => ($this->input->get('end_time')) ? convert_datetime($this->input->get('end_time'),3) : 0,
        ),'filter_item');
        $params = array_merge($params,$params_filter);
        // get array contact
        $rows = $this->contact->lists($params);
        /** PAGING **/
        $config['total_rows'] = count($rows);
        $config['per_page'] = $limit;
        $this->load->library('paging',$config);
        $paging = $this->paging->create_links();
        unset($rows[$limit]);
        // set data
        return array('rows' => $rows, 'paging' => $paging, 'filter' => $params_filter);
    }
    public function _action($action, $params = array()) {
        $this->load->model('admin/contact_model','contact');
        
        switch ($action) {
            case 'edit':
            case 'delete':

                // get cid
                $arrId = $this->input->post('cid');
                $arrId = (is_array($arrId)) ? array_map('intval', $arrId) : (int) $arrId;
                if (!$arrId) {
                    return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_delete_min_select"))));
                }
                if ($action == 'edit') {
                    if (!$this->permission->check_permission_backend('edit')){
                        return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_access_denied"))));
                    }
                    $result = $this->contact->update_status($arrId);   
                }
                else {
                    if (!$this->permission->check_permission_backend('delete')){
                        return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_access_denied"))));
                    }
                    $result = $this->contact->delete($arrId);
                }  
                if ($result) {
                    $html = $this->load->view('contact/list',$this->_index()); 
                    return $this->output->set_output(json_encode(array('status' => 'success','html' => $html, 'result' => $result, 'message' => $this->lang->line("common_delete_success"))));
                }
            break;
            default:
                # code...
                break;
        }   
        return $this->output->set_output(json_encode(array('status' => 'error', 'message' => $this->lang->line("common_no_row_update"))));
    }
}