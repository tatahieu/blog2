<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ads extends CI_Controller{
    function __construct(){
		parent::__construct();
	}
    function album(){
        $aid = 16; $limit = 1;
        $this->load->model('advertise_model','adv');
        $page = (int)$this->input->get('page');
        $page = ($page > 1) ? $page : 1;

        $config['total_rows'] = $this->adv->count_cate_child($aid);
        $config['per_page'] = $limit;
        $this->load->library('pagination',$config);
        $data['paging'] =  $this->pagination->create_links();

        $data['rows'] = $this->adv->get_cate_child($aid,$limit,$page);
        $this->load->layout("advertise/album",$data);
        // list all album
    }
	public function lists($cate_id){
        $limit = 16; $cate_id = (int)$cate_id;
        if ($cate_id <= 0){
            show_404();
        }
        $page = (int)$this->input->get('page');
        $page = ($page > 1) ? $page : 1;

        $this->load->model('advertise_model','adv');
        $config['total_rows'] = $this->adv->lists_count($cate_id);
        $config['per_page'] = $limit;
        $this->load->library('pagination',$config);
        $data['paging'] =  $this->pagination->create_links();
        $data['rows'] = $this->adv->lists($cate_id,$limit,$page);
        $cate = $this->adv->get_cate_name($cate_id);
        $data['cate'] = $cate;

        $this->load->setData('title',$cate['name']);
        $this->load->layout('advertise/lists',$data);
	}
}
