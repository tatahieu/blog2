<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question extends CI_Controller{
    function __construct(){
		parent::__construct();
        $list_score = 15;
        $read_score = 10;
        $score[0] = array(0,0);
        for ($i=1; $i <= 100; $i++) {
            $score[$i] = array($list_score,$read_score);
            if ($i < 3) {
                $list_score += 5;
                $read_score += 0;
            }
            elseif ($i > 96) {
                $list_score += 0;
                $read_score += 5;
            }
            else {
                $list_score += 5;
                $read_score += 5;  
            }
            
        }
        $this->score = $score;
	}
    var $data = array();
    var $count = 1;
    var $class_data = array();
    var $score = array();
    /**
     * type = 1: test binh thuong
     * type = 2: tinh thoi gian
     **/
    public function catetest($cate_id = 0,$type = 1){
        $this->config->set_item("menu_select",array('item_mod' => 'question_cate', 'item_id' => $cate_id));
        $this->config->set_item("mod_access",array("name" => "question_list"));
        $this->load->model("question_model","ques");
        $data['cate_info'] = $this->ques->get_cate_info((int)$cate_id);
        $data['test'] = $this->ques->get_cate_test((int)$cate_id);
        if (empty($data['cate_info']) OR empty($data['test'])){
            show_404();
        }
        $data['type'] = $type;
        $this->load->layout('question/cate_test',$data);
    }
    public function test($test_id){

        $test_id = (int)$test_id;
        $this->load->model("question_model","ques");
        $test_info = $this->ques->get_test_info($test_id);

        if (empty($test_info)){
            return $this->load->layout("common/noresult");
        }
        // check permission
        if ($test_info['isclass'] == 1){
            $return = $this->ques->check_question_by_role($test_id,$this->session->userdata('class_id'));
            if ($return <= 0){
                return $this->load->layout("question/permission_denied");
            }
        }

        switch ($test_info['part']){
            case 1:
            $data = $this->_testlist($test_id,1);
            $layout = 'test_detail';
            break;
            case 2:
            $data = $this->_testlist($test_id,2);
            $layout = 'test_response';
            break;
            case 5:
            $data = $this->_testlist($test_id,4);
            $layout = 'test_list';
            break;
            case 6:
            $data = $this->_testparent($test_id,2,1);
            $layout = 'test_parent';
            break;
            case 7:
            case 3:
            case 4:

            $data = $this->_testparent($test_id,1,1);
            $data['part'] = $test_info['part'];
            $layout = 'test_parent';
            break;
            case 8:
            $data = $this->_test_writing($test_id);
            $layout  = 'test_writing';
            break;
            default:
            show_404();

        }
        //$data['api_sound'] = get_file_url('audio/');; 

        $data['audioflash'] = $this->config->item('theme').'images/player.swf';
        $data['test_info'] = $test_info;
        $data['relate'] = $this->ques->get_relate_test($test_info['cate_id'],$test_id,$test_info['isclass']);
        if ($test_info['relate_news'] > 0){
            $data['news'] = $this->ques->get_relate_news($test_info['relate_news']);
        }
        $js = array(
            link_tag('jplayer/skin/blue.monday/jplayer.blue.monday.css'),
            js('jplayer/jquery.jplayer.min.js'),
            js('practice.js')
        );
        $seotitle = ($test_info['title']) ? $test_info['title'] : $this->config->item('metakey');
        $seldesc = ($test_info['description'] != '') ? htmlentities(strip_tags($test_info['description'])) : $this->config->item('metadesc');

        $this->load->setData('title',$seotitle);
        $this->load->setData('metakey',$seotitle);
        $this->load->setData('metades',$seldesc);

        $this->load->setArray("script",$js);
        $this->config->set_item("mod_access",array("name" => "question_list"));
        $this->load->layout('question/'.$layout,$data);
    }
    public function _test_writing($test_id){
        $limit = 1;
        $data['question'] = $this->ques->random_by_test($test_id,$limit);
        if (empty($data['question'])){
            show_404();
        }
        if ($limit != 0){
            if (count($data['question']) == $limit + 1){
                $q = ($this->input->get("q")) ? (int)$this->input->get("q") : 1;
                $data['next_button'] = current_url().'/?q='.($q + 1);
                unset($data['question'][$limit]);
            }
        }
        return $data;
    }
    public function _testlist($test_id = 0,$limit = 0){
        $data['question'] = $this->ques->random_by_test($test_id,$limit);

        if (empty($data['question'])){
            show_404();
        }
        if ($limit != 0){
            if (count($data['question']) == $limit + 1){
                $q = ($this->input->get("q")) ? (int)$this->input->get("q") : 1;
                $data['next_button'] = current_url().'/?q='.($q + 1);
                unset($data['question'][$limit]);
            }
        }
        $j = 0;
        foreach ($data['question'] as $q){
            $qarr[] = $q['question_id'];
            $j ++;
        }
        $this->count = $this->count + $j;
        $answer = $this->ques->get_answer($qarr);

        foreach ($answer as $a){
            $data['answer'][$a['question_id']][] = $a;

        }

        return $data;
    }
    /**
     * type = 1: reading
     * type = 2: text-complete
     ***/
    public function _testparent($test_id = 0,$type = 1,$limit = 0){
        $child = $this->ques->get_test_child($test_id,$limit);
        if (empty($child)){

           show_404();
        }
        if ($limit != 0){
            if (count($child) == 2){
                $q = ($this->input->get("q")) ? (int)$this->input->get("q") : 1;
                $data['next_button'] = current_url().'/?q='.($q + 1);
                unset($child[1]);
            }
        }

        $i = 1;
        if ($limit == 1){

            foreach ($child as $child){
                $data['test_child'] = $child;
                $qtest = $this->ques->random_by_test($child['qtest_id']);
                $data['question'] = $qtest;
                foreach ($qtest as $q){
                    $qarr[] = $q['question_id'];
                }
            }
        }
        else{
            $data['test_child'] = $child;
            //var_dump($child); die;
            foreach ($child as $child){ 
                $arrChildTest[] =  $child['qtest_id'];
            }
            $qtest = $this->ques->random_by_test($arrChildTest);
            $data['question'] = $qtest;

            $j = 0;
            foreach ($qtest as $q2){
                foreach ($q2 as $q){
                    $qarr[] = $q['question_id'];
                    $j ++;
                }
            }
            $this->count = $this->count + $j;
        }
        if (empty($qarr)){
            show_404();
        }

        $answer = $this->ques->get_answer($qarr);
        foreach ($answer as $a){
            $data['answer'][$a['question_id']][] = $a;
        }
        $data['type'] = $type;
        return $data;
    }
    // ajax check result
    public function answer(){
        $this->load->model('question_model','ques');
        $qid = $this->input->post("question_id");
        $fullId = $this->input->post("fulltest_id");
        $data['result'] = 0; 
        // check score security
        $score_id = (int) $this->input->post('score_id');
        if ($fullId && !$this->security->verify_token_post($score_id,$this->input->post('score_token'))) {
            return $this->output->set_output(json_encode(array('result' => 'error', 'message' => 'Mã bảo mật không chính xác, kết quả bài test không được ghi nhận')));
        }
        //// testing ///
        $result = $this->ques->get_testing_result($qid);
        foreach ($result as $result){
            $ObjectResult[$result['question_id']] = $result['result'];
        }
        $list = 0; $read = 0;
        $total = (int)(count($qid) / 2);
        $fullId = $this->input->post("fulltest_id");
        $fullDetail = $this->ques->get_all_detail($fullId);
        $i = 1;
        foreach ($qid as $qid){
            if (!$ObjectResult[$qid]) {
                $data = array('result' => 'error', 'message' => 'Câu hỏi ' + $qid + 'chưa có đáp án đúng.');
                return $this->output->set_output(json_encode($data));
            }
            if ($this->input->post('answer_'.$qid) == $ObjectResult[$qid]){
                if ($i <= $total){
                    $list = $list + 1;
                }
                else{
                    $read = $read + 1;
                }
                $data['result'] = $data['result'] + 1;
            }
            $data['answer'][$qid] = $ObjectResult[$qid];
            $i ++;
        }
        // check 
        if ($fullDetail){
            
            $data['total'] = $i - 1;
            if ($fullDetail['type'] == 2){
                $list = $list * 2; $read = $read * 2;
            }
            //$list = ($list == 0 OR $list >= 16) ? $list : 16;
            //$read = ($read == 0 OR $read >= 16) ? $read : 16;
            $data['score_list'] = $this->score[$list][0];
            $data['score_read'] = $this->score[$read][1];
            $data['type'] = $fullDetail['type'];
            $data['save_score'] = 'Kết quản của bạn đã được lưu vào hệ thống';
            $input = array(
                //"user_id" => $this->session->userdata("userid"),
                //"test_id" => (int)$this->input->post("fulltest_id"),
                "list_score" => $data['score_list'],
                "read_score" => $data['score_read'],
                "total_score" => $data['score_list'] + $data['score_read'],
                "list_correct" => $list,
                "read_correct" => $read
                //"type" => $fullDetail['type']
            );
            $updateResult = $this->ques->save_question_score($score_id, $input);
            if ( !$updateResult ) {
                $data = array('result' => 'error', 'message' => 'Không lưu được thông tin bài làm');
            }
            if ($fullDetail['show_answer'] == 0) {
                unset($data['answer']);
            }
        }
        $this->output->set_output(json_encode($data));
    }
    public function _time($params = array()){
        $test_id = (int)$params['test_id'];
        $start = ($params['count']) ? (int) $params['count'] : 1;
        $test_info = $this->ques->get_test_info($test_id);
        if (empty($test_info)){
            return $this->load->layout("common/noresult");
        }
        $part = $test_info['part'];
        $this->data['api_sound'] = get_file_url('audio/');
        $this->data['audioflash'] = $this->config->item('theme').'images/player.swf';

        switch ($part){
            case 1:
            case 2:
            $part1 = $this->_testlist($test_id);
            $part1['show_answer'] = $params['show_answer'];
            $part1['start'] = $start;
            $part1['test_info'] = $test_info;
            //$part1['api_resize'] = $this->data['api_resize'];
            $part1['api_sound'] = $this->data['api_sound'];
            $part1['audioflash'] = $this->data['audioflash'];
            krsort($part1['answer']);
            $part1['part'] = $part;
            $this->data['answer'][$part] = $part1['answer'];
            $this->data['media'][] = $this->load->view("question/skill_detail",$part1);
            break;
            case 5:
            $part5 = $this->_testlist($test_id,0);
            $part5['start'] = $start;
            $part5['test_info'] = $test_info;
            $part5['show_answer'] = $params['show_answer'];
            krsort($part5['answer']);
            $part5['part'] = $part;
            $this->data['answer'][$part] = $part5['answer'];
            $this->data['media'][] = $this->load->view("question/skill_incomplete",$part5);
            break;
            case 7:
            case 3:
            case 4:
            case 6:
            $part7 = $this->_testparent($test_id);
            $part7['start'] = $start;
            $part7['test_info'] = $test_info;
            $part7['show_answer'] = $params['show_answer'];
            //$part7['api_resize'] = $this->data['api_resize'];
            $part7['api_sound'] = $this->data['api_sound'];
            $part7['audioflash'] = $this->data['audioflash'];
            krsort($part7['answer']);
            $part7['part'] = $part;
            $this->data['answer'][$part] = $part7['answer'];
            if ($part == 6) $part7['type'] = 2;
            $this->data['media'][] = $this->load->view("question/skill_parent",$part7);
            break;
        }
    }

    public function skilltest($type = "part",$id = 0){

        $this->load->model("question_model",'ques');
        $this->config->set_item("mod_access",array("name" => "question_list"));
        if ($type == "part"){
            $data['part'] = $this->ques->get_all_full(3,$id);
            $data['name'] = 'Skill Test';
            $data['func'] = 'skilltest';
            return $this->load->layout('question/full_cate',$data);
        }
        
        
        if (is_numeric($type) AND $type > 0){
            $data = $this->_get_fulltest($type,3);
            if ($data['error']) {
                return $this->load->layout('common/error',$data);
            }
            // static
            $js = array(
                js('jquery.plugin.min.js'),
                js('jquery.countdown.js'),
                js('jplayer/jquery.jplayer.min.js'),
                js('practice.js'),
                link_tag('jquery.countdown.css'),
                link_tag('jplayer/skin/blue.monday/jplayer.blue.monday.css')

            );
            $this->load->setArray("script",$js);
            $data['type'] = 3;
            $data['name'] = 'Skill Test';
            $data['func'] = 'skilltest';
            $data['fullid'] = $type;
            return $this->load->layout(array("pc" => "question/fulltest", "mobile"=>"question/m_fulltest"),$data);
        }
    }
    public function minitest($id = 0){
        if (!$this->session->userdata('userid')){
            redirect('users/login?refer='. base64_encode(current_url()));
        }
        $this->load->setArray("common",$css);
        $this->load->model("question_model",'ques');
        $this->config->set_item("mod_access",array("name" => "question_list"));
        if ((int)$id <= 0){
            $data['part'] = $this->ques->get_all_full(2);
            $data['name'] = 'Mini Test';
            $data['func'] = 'minitest';

            $this->load->layout('question/full_cate',$data);
        }
        else{
            
            $data["fullid"] = $id;
            $data = $this->_get_fulltest($id,2);
            if ($data['error']) {
                return $this->load->layout('common/error',$data);
            }
            // static
            $js = array(
                js('jquery.plugin.min.js'),
                js('jquery.countdown.js'),
                js('jplayer/jquery.jplayer.min.js'),
                js('practice.js'),
                link_tag('jquery.countdown.css'),
                link_tag('jplayer/skin/blue.monday/jplayer.blue.monday.css')
            );
            $this->load->setArray("script",$js);
            $data['exitlink'] = '/question/minitest';
            $data['type'] = 2;
            $data['name'] = 'Mini Test';
            $data['func'] = 'minitest';
            $data['fullid'] = $id;
            $this->load->layout(array("pc" => "question/fulltest", "mobile"=>"question/m_fulltest"),$data);
        }

    }
    public function fulltest($id = 0){
        if (!$this->session->userdata('userid')){
            redirect('users/login?refer='. base64_encode(current_url()));
        }
        $this->load->model("question_model",'ques');
        $this->config->set_item("mod_access",array("name" => "question_list"));
        if ((int)$id <= 0){
            $data["fullid"] = $id;
            $data['part'] = $this->ques->get_all_full(1);
            $data['name'] = 'Full Test';
            $data['type'] = 1;
            $data['func'] = 'fulltest';


            $this->load->layout('question/full_cate',$data);
        }
        else{
            
            $data = $this->_get_fulltest($id);
            if ($data['error']) {
                return $this->load->layout('common/error',$data);
            }
            // static
            $js = array(
            js('jquery.plugin.min.js'),
            js('jquery.countdown.js'),
            js('jplayer/jquery.jplayer.min.js'),
            js('practice.js'),
            link_tag('jquery.countdown.css'),
            link_tag('jplayer/skin/blue.monday/jplayer.blue.monday.css')
            );
            $this->load->setArray("script",$js);

            $data['exitlink'] = '/question/fulltest';
            $data['name'] = 'Full Test';
            $data['func'] = 'fulltest';
            $data['fullid'] = $id;
            $this->load->layout(array("pc" => "question/fulltest", "mobile"=>"question/m_fulltest"),$data);
        }
    }
    public function classtest($id = 0){
        $limit = 20;
        $checkpermission = $this->permission->check_permission_class();
        $page = (int)$this->input->get('page');
        $page = ($page > 1) ? $page : 1;
        if ($checkpermission != true){
            return $checkpermission;
        }
        $this->load->model("question_model",'ques');
        if ((int)$id <= 0){
            $param = array('role_id' => $this->session->userdata("class_id"),'limit' => $limit, 'page' => $page);
            $config['total_rows'] = $this->ques->total_full_question_by_class($param);

            if ($config['total_rows'] > 0) {
                $config['per_page'] = $limit;
                $this->load->library('pagination',$config);
                $data['paging'] =  $this->pagination->create_links();
                $data['row'] = $this->ques->get_full_question_by_class($param);
            }
            $this->config->set_item("mod_access",array("name" => "question_list"));

            /// meta ///
            $seotitle = 'Bài tập TOEIC Listening và TOEIC Reading - Ms Hoa TOEIC';
            $seldesc = 'Bài tập TOEIC Listening và TOEIC Reading'. ' - '. $this->config->item('metadesc');
            $this->load->setData('title',$seotitle);
            $this->load->setData('metakey',$seotitle);
            $this->load->setData('metades',$seldesc);
            /// layout ///
            $this->load->layout('question/class_test',$data);
        }
        else{
           $data['exitlink'] = '/question/classtest';
           $this->test($id,$this->session->userdata("class_id"));
        }

    }
    public function classfull($id = 0){
        $checkpermission = $this->permission->check_permission_class();
        if ($checkpermission != true){
            return $checkpermission;
        }
        $this->load->model("question_model",'ques');
        if ((int)$id <= 0){
            $data["fullid"] = $id;
            $data['part'] = $this->ques->get_all_by_class($this->session->userdata("class_id"));
            $data['name'] = 'Bài tập: '.$this->session->userdata("class_name");
            $data['type'] = 0;
            $data['func'] = 'classfull';
            $this->config->set_item("mod_access",array("name" => "question_list"));
            /// meta ///
            $seotitle = 'Bài tập TOEIC Listening và TOEIC Reading, Fulltest - Ms Hoa TOEIC';
            $seldesc = 'Bài tập TOEIC Listening và TOEIC Reading, Fulltest'. ' - '. $this->config->item('metadesc');
            $this->load->setData('title',$seotitle);
            $this->load->setData('metakey',$seotitle);
            $this->load->setData('metades',$seldesc);
            //// layout ////
            $this->load->layout('question/full_cate',$data);
        }
        else{
            $data = $this->_get_fulltest($id,4);
            if ($data['error']) {
                return $this->load->layout('common/error',$data);
            }
            // static
            $js = array(
                js('jquery.plugin.min.js'),
                js('jquery.countdown.js'),
                js('jplayer/jquery.jplayer.min.js'),
                js('practice.js'),
                link_tag('jquery.countdown.css'),
                link_tag('jplayer/skin/blue.monday/jplayer.blue.monday.css')
            );
            $this->load->setArray("script",$js);
            // return data
            $data['exitlink'] = '/question/classfull';
            $data['name'] = 'Bài tập lớp: '.$this->session->userdata("class_name");
            $data['func'] = 'classfull';
            $data['fullid'] = $id;
            $this->load->layout(array("pc" => "question/fulltest", "mobile"=>"question/m_fulltest"),$data);
        }
    }

    private function _get_fulltest($id,$type = 1){
        $role = $this->ques->get_role_question_full($id);
        $role_id = $this->session->userdata("role_id");
        $detail = $this->ques->get_all_detail((int)$id);
        if (!$detail) {
            show_404();
        }
        $j = 1;
        for ($i = 1; $i <= 7; $i ++){
            $tid = $detail['part'.$i];
            if ($tid > 0){
                $this->_time(array('test_id' => $tid,'count' => $this->count,'show_answer' => $detail['show_answer']));
                $part = $i;
                $j ++;
            }

        }
        if ($j != 2 OR $part < 0){
            $part = 0;
        }
        $data = $this->data;
        if ($type == 4){
            //$data['part'] = $this->ques->get_all_by_class($this->session->userdata("class_id"));
            $data['part'] = array();
        }
        else{
            $data['part'] = $this->ques->get_all_full($type,$part);
        }
        $data['time'] = time() + ($detail['time']*60);
        $data['total_time'] = $detail['time']*60;
        // check score of user  & insert score
        $score_id = $this->ques->insert_user_score(array('user_id' => $this->session->userdata('userid'),'test_id' => $id,'type' => $type));
        if (!$score_id) {
            return array('error' => array('title' => 'Lỗi: Thời gian làm bài quá gần', 'message' => 'Bạn vui lòng chờ 10 phút sau để vào làm lại bài test này nhé'));
        }
        $data['score_id'] = $score_id;
        // 
        $seotitle = ($detail['name']) ? $detail['name'] : $this->config->item('metakey');
        $seldesc = $seotitle . ' - '. $this->config->item('metadesc');

        $this->load->setData('title',$seotitle);
        $this->load->setData('metakey',$seotitle);
        $this->load->setData('metades',$seldesc);

        return $data;
    }
}