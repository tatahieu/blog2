<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact extends CI_Controller{
    function __construct(){
		parent::__construct();
        $this->lang->load('frontend/contact');

        $this->config->set_item("breadcrumb",array(array("name" => $this->lang->line("contact_title"))));
        $this->config->set_item("menu_select",array('item_mod' => 'contact', 'item_id' => 0));
        $this->config->set_item("mod_access",array("name" => "contact"));
	}
    public function index(){
        if ($this->input->post('contact_send')){
            $this->_validation();
            if ($this->form_validation->run()){
                $input = array(
                        'username' => strip_tags($this->input->post('fullname')),
                        'email' => strip_tags($this->input->post('email')),
                        'phone' => strip_tags($this->input->post('phone')),
                        //'user_id' => (int) $this->config->item("user_id"),
                        'content' => strip_tags($this->input->post('content')),
                        'date' => date('Y-m-d H:i:s')
                );
                $this->load->model('contact_model','contact');
                $this->contact->insert($input);
                $content = $this->load->view('contact/email',$input);
                send_mail($this->config->item('email_admin'),$this->lang->line('contact_email_from').$this->config->item("site_name"),$content);
                send_mail($input['email'],$this->lang->line('contact_email_from').$this->config->item("site_name"),$this->lang->line('contact_confirm'));
                redirect('contact/success');
            }
        }
        $this->load->helper('form');
        $this->load->helper('captcha');
        // SET CAPTCHA //
        $capcha = get_captcha("contact_captcha");
        $data['security_code'] = $capcha['image'];


        $data['fullname'] = form_input('fullname',set_value('fullname'));
        $data['email'] = form_input('email',set_value('email'));
        $data['phone'] = form_input('phone',set_value('phone'));
        $data['content'] = form_textarea('content',set_value('content'));
        $data['captcha'] = form_input('captcha',set_value('captcha'));
        $data['reset'] = form_reset('contact_reset',$this->lang->line('contact_reset'));
        $data['submit'] = form_submit('contact_send',$this->lang->line('contact_send'));

        $this->load->setData('title',$this->lang->line('contact_title'));
        $this->load->layout('contact/form',$data);
    }
    public function success(){
        $data['title'] = $this->lang->line('contact_success_title');
        $data['result'] = $this->lang->line('contact_success');
        $this->load->layout('contact/result',$data);
    }
    private function _validation(){
  		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'fullname',
                 'label'   => $this->lang->line('contact_fullname'),
                 'rules'   => 'required'
              ),
           	array(
                 'field'   => 'email',
                 'label'   => $this->lang->line('contact_email'),
                 'rules'   => 'required|valid_email'
              ),
			array(
                 'field'   => 'phone',
                 'label'   => $this->lang->line('contact_phone'),
                 'rules'   => 'required|numeric'
              ),
            array(
                 'field'   => 'content',
                 'label'   => $this->lang->line('contact_content'),
                 'rules'   => 'required|min_length[10]'
              ),
            array(
                 'field'   => 'captcha',
                 'label'   => $this->lang->line('contact_captcha'),
                 'rules'   => 'required|matches_str['.$this->session->userdata('contact_captcha').']'
              ),

		);
  		$this->form_validation->set_rules($valid);
    }
}
