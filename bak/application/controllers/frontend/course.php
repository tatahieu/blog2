<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Course extends CI_Controller{
    function __construct(){
		parent::__construct();
	}
    public function word($leveltxt = '',$topic = ''){
        $limit = 10;
        $leveltxt = ($leveltxt) ? $this->security->xss_clean($leveltxt) : show_404();
        $page = (int) ($this->input->get("page") > 1) ? $this->input->get("page") : 1;
        $level = (int) get_level_by_code($leveltxt);
        if ($level == 0) redirect(BASE_URL);
        $this->load->model("course_model","course");
        $param = array("limit" => $limit + 1, 'level' => $level, 'offset' => ($page - 1) * $limit);
        $topicall = $this->course->get_all_topic(array('type' => 1, 'level' => $level));
        $url = $this->uri->uri_string();
        if (strpos($url,'course/word') !== FALSE) {
            $url = str_replace('course/word','course/tu-vung-theo-chu-de',$url);
            redirect($url,'location',301);
        }
        if ($topic) {
            $topicdetail = $this->course->get_topic_by_alias($topic);
            if ($topicdetail['type'] != 1 || ($topicdetail['level'] != $level && $topicdetail['level'] != 0)) {
                show_404();
            }
            if ((int)$topicdetail['topic_id'] <= 0){
                redirect('/course/tu-vung-theo-chu-de/'.$leveltxt);
            }
        }
        else {
            $topicrand = rand(0,count($topicall) - 1);
            $topicdetail = $topicall[$topicrand];
        }
        $param['topic_id'] = (int)$topicdetail['topic_id'];
        $rows = $this->course->get_word_list($param);
        $config['total_rows'] =  count($rows);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($rows[$limit]);
        $data['rows'] = $rows;

        $data['topicall'] = $topicall;
        $data['topicdetail'] = $topicdetail;
        $data['level'] = $leveltxt;
        ////////////// static ////////
        $this->config->set_item("menu_select",array('item_mod' => 'course_word', 'item_id' => $level));

        $this->load->setArray("script",js('course.js'));
        $this->load->setArray("script",js('jplayer/jquery.jplayer.min.js'));
        $this->load->setArray("script",js('record/recorder.js'));

        if ($topic) {
            $this->load->setData('title',"Học từ mới theo chủ đề " . $topicdetail['name'] . ' - Mshoa Toeic');
        }
        else {
            $this->load->setData('title',"Học từ mới - Mshoa Toeic");
        }
        $this->load->setData('metakey',"Học từ mới, hoc tu moi, từ vựng");
        $this->load->setData('metades',$this->config->item("metadesc"));
        $this->load->layout("course/word_list",$data);
    }
    public function complete($leveltxt = '',$topic=''){
        $leveltxt = ($leveltxt) ? $this->security->xss_clean($leveltxt) : show_404();
        $limit = 10;
        $level = (int) get_level_by_code($leveltxt);
        $page = (int) ($this->input->get("page") > 1) ? $this->input->get("page") : 1;
        if ($level == 0) redirect(BASE_URL);
        $url = $this->uri->uri_string();
        if (strpos($url,'course/complete') !== FALSE) {
            $url = str_replace('course/complete','course/luyen-nghe-tieng-anh',$url);
            redirect($url,'location',301);
        }

        $this->load->model("course_model","course");
        $param = array("limit" => $limit + 1, 'level' => $level, 'offset' => ($page - 1) * $limit);
        $topicall = $this->course->get_all_topic(array('type' => 2, 'level' => $level));
        if ($topic) {
            $topicdetail = $this->course->get_topic_by_alias($topic);
            if ($topicdetail['type'] != 2 || ($topicdetail['level'] != $level && $topicdetail['level'] != 0)) {
                show_404();
            }
            if ((int)$topicdetail['topic_id'] <= 0){
                redirect('/course/luyen-nghe-tieng-anh/'.$leveltxt);
            }
        }
        else {
            $topicrand = rand(0,count($topicall) - 1);
            $topicdetail = $topicall[$topicrand];
        }
        $param['topic_id'] = (int)$topicdetail['topic_id'];
        $rows = $this->course->get_word_complete($param);

        $config['total_rows'] =  count($rows);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($rows[$limit]);

        $data['rows'] = $rows;
        $data['topicall'] = $topicall;
        $data['topicdetail'] = $topicdetail;
        $data['level'] = $leveltxt;
        /////////// static ////
        if ($topic) {
            $this->load->setData('title',"Điền từ vào chỗ trống: chủ đề " . $topicdetail['name'] . ' - Mshoa Toeic');
        }
        else {
            $this->load->setData('title',"Điền từ vào chỗ trống - Mshoa Toeic");
        }
        $this->config->set_item("menu_select",array('item_mod' => 'course_complete', 'item_id' => $level));
        $this->load->setArray("script",js('course.js'));
        $this->load->setArray("common",link_tag('jplayer/skin/mac/style.css'));
        $this->load->setArray("script",js('jplayer/jquery.jplayer.min.js'));
        $this->load->setArray("script",js('mediaplayer.js'));
        $this->load->layout("course/word_complete",$data);
    }
    public function senddatarecord(){
        $this->load->helper("images_helper");
        $result = images_upload('recordfile',array('file_name' => $this->session->userdata("userid").time().rand(1111,9999)),'record');
        $this->output->set_output(json_encode($result));
    }
    public function getgoogle($txt){
        $expires = 60*60*24*14;
        //header("Pragma: public");
        header("Cache-Control: maxage=".$expires);
        header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');

        header("Content-type: audio/mpeg");  
        header("Content-Transfer-Encoding: binary");  
        //header('Pragma: no-cache');  
        header('Expires: 0');  

        $url = 'http://translate.google.com/translate_tts?ie=UTF-8&q='.urlencode($txt).'&tl=en';
        $ctx        = stream_context_create(array("http"=>array("method"=>"GET","header"=>"Referer: \r\n")));
        $soundfile = file_get_contents($url,false,$ctx);
        echo $soundfile;
    }
}