<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Controller{
    protected $user_data;
    protected $username = '';
    public function __construct(){
        parent::__construct();
        $this->lang->load('frontend/users');
        $this->config->set_item("mod_access",array("name" => "user"));
    }
    public function loginsocial($type){
        $this->load->library("Curl","curl");
        $this->load->model("users_model","users");

        $code = $this->input->get("code");
        $refer = ($this->input->get('state')) ? base64_decode($this->input->get('state')) : BASE_URL;
        if (!$code) {
            die("Lỗi kết nối");
        }
        if ($type == 'facebook') {
            // Get access token info
            $facebook_url = 'https://graph.facebook.com/oauth/access_token';
            $params       = array(
                'client_id' => $this->config->item("facebook_app_id"),
                //'type'          => 'client_cred',
                'client_secret' => $this->config->item("facebook_app_secret"),
                'redirect_uri' => BASE_URL . 'users/loginsocial/facebook',
                'code' => $code
            );
            $facebook_url = $facebook_url . '?' . http_build_query($params);
            $token_key    = file_get_contents($facebook_url);
            //var_dump($facebook_url,$token_key);  die; 
            if (strpos($token_key, 'access_token') === false) {
                die("Lỗi không tạo được token key");
            }
            $access_token = @json_decode($token_key,TRUE);
            // Get user's infomation
            $urlInfo      = 'https://graph.facebook.com/me?access_token=' . $access_token['access_token'].'&fields=name,email,gender';
            $response     = file_get_contents($urlInfo);

            // decode json user
            $facebookUser = json_decode($response, TRUE);

            if (!empty($facebookUser['error']) || empty($facebookUser['id'])) {
                die("Không tồn tại user này");
            }
            if (empty($facebookUser['email'])) {
                die("Bạn phải cho phép chúng tôi truy cập vào tài khoản email để tạo tài khoản");
            } 
            $input = array(
                'username' => 'facebook_'.$facebookUser['id'].'_'.time(),
                'fullname' => $facebookUser['name'],
                'email' => $facebookUser['email'],
                //'birthday' => $facebookUser['birthday'],
                'active' => 1,
                'social_id' => $facebookUser['id'],
                'social_type' => 1,
                'roles_id' => 0,
                'sex' => ($facebookUser['gender'] == 'male') ? 1 : 0,
                'date_reg' => date('Y-m-d'),
                'send_mail' => 1
            );
            $userProfile = $this->users->insert_from_social($input);
            /// set session //
            $this->permission->set_user_session($userProfile);
            redirect($refer);
        }
        elseif ($type == 'google') {
            $google_access_token_uri = 'https://accounts.google.com/o/oauth2/token';
            $postData = array(
                'client_id' => $this->config->item("google_app_id"),
                'client_secret' => $this->config->item("google_app_secret"),
                'redirect_uri' => BASE_URL.'users/loginsocial/google',
                'grant_type' => 'authorization_code',
                'code' => $code
            );
            $this->load->library('Curl','curl');
            $response = $this->curl->simple_post($google_access_token_uri, $postData);
            //$this->curl->debug();

            if (!$response ) {
                die("Không tạo được token key");
            }

            $response = json_decode($response,TRUE);
            $access_token = $response['access_token'];
            if (!$access_token){
                die("token key lỗi");
            }
            // Get user's infomation
            $urlInfo = 'https://www.googleapis.com/plus/v1/people/me?access_token='.$access_token;
            $googleUser = $this->curl->simple_get($urlInfo);
            if (!$googleUser) {
                die("không tôn tại user này");
            }
            $googleUser = json_decode($googleUser,TRUE);
            if (!$googleUser['id']) {
                die("Fomat user không chính xác");
            }
            $input = array(
                'username' => 'google_'.$googleUser['id'].'_'.time(),
                'fullname' => $googleUser['displayName'],
                'email' => $googleUser['emails'][0]['value'],
                'active' => 1,
                'social_id' => $googleUser['id'],
                'social_type' => 2,
                'roles_id' => 0,
                'sex' => ($googleUser['gender'] == 'male') ? 1 : 0,
                'date_reg' => date('Y-m-d'),
                'send_mail' => 1
            );
            $userProfile = $this->users->insert_from_social($input);
            /// set session //
            $this->permission->set_user_session($userProfile);

            redirect($refer);
        }
    }
    public function keeplife(){
        $this->session->set_userdata('keeplife',1);
        return true;
    }
    public function success($type = 1){
        // 1: register success
        // 2: forgot success
        // 3: set_new_pass success
        // 4: reactive success
        switch ($type){
            case 2:
            $data['title'] = $this->lang->line("users_forgot_success_title");
            $data['msg'] = $this->lang->line("users_forgot_success_content");
            break;
            case 3:
            $data['title'] = $this->lang->line("users_new_pass_success_title");
            $data['msg'] = $this->lang->line("users_new_pass_success_content");
            break;
            case 4:
            $data['title'] = $this->lang->line("users_reactive_success_title");
            $data['msg'] = $this->lang->line("users_reactive_success_content");
            break;
            case 5:
            $data['title'] = $this->lang->line("users_reg_success_title");
            $data['msg'] = 'Bạn đã đăng ký thành công tài khoản tại Mshoatoeic.com. <p>Mời bạn đăng nhập tài khoản vừa đăng ký để trải nghiệm hàng nghìn bài tập TOEIC 4 kỹ năng đầu tiên tại Việt Nam. Trân Trọng</p>';
            break;
            default:
            $data['title'] = $this->lang->line("users_reg_success_title");
            $data['msg'] = 'Bạn đã đăng ký thành công tài khoản tại Mshoatoeic.com. <p>Mời bạn đăng nhập tài khoản vừa đăng ký để trải nghiệm hàng nghìn bài tập TOEIC 4 kỹ năng đầu tiên tại Việt Nam. Trân Trọng</p>';
        }

        return $this->load->layout("users/result",$data);
    }
/** =================================== LOGIN ========================================= **/
    public function login(){
        if ($this->session->userdata('userid')){
    			redirect("users/profile");
    		}
    		if ($this->input->post('log_submit')){
    			$this->_login_validation();
    			if ($this->form_validation->run() == true)
    			{

    			    $row = $this->user_data;
                    // set permission
                    $this->permission->set_user_session($row);
                    $refer = ($this->input->get("refer")) ? base64_decode($this->input->get("refer")) : '/';
    				redirect($refer);
    			}
    		}
    		$this->load->helper('form');
    		$data['username'] = form_input('log_username',set_value('log_username'),'maxlength = "100" class="login_user"');
    		$data['password'] = form_password('log_password',set_value('log_password'),'maxlength = "100" class="login_pass"');
    		$data['submit'] = form_submit('log_submit',$this->lang->line('users_login_submit'));
        $this->load->layout('users/login_form',$data);
    }
    public function login_ajax(){
        if ($this->session->userdata('userid')){
			return $this->output->set_output(json_encode(array('error' => 'Đã đăng nhập rồi')));
		}
		$this->_login_validation();
		if ($this->form_validation->run() == true)
		{
		    $row = $this->user_data;
		    $this->permission->set_user_session($row);
			return $this->output->set_output(json_encode(array('error' => '','success' => 'Đăng nhập thành công')));
		}
		else{
            return $this->output->set_output(json_encode(array('error' => validation_errors())));
		}
    }
   	private function _login_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'log_username',
                 'label'   => $this->lang->line('users_username'),
                 'rules'   => 'required|min_length[3]|max_length[40]'
              ),
           array(
                 'field'   => 'log_password',
                 'label'   => $this->lang->line('users_password'),
                 'rules'   => 'required|min_length[3]|max_length[40]|callback__user_check'
              )
  		);
  		$this->form_validation->set_rules($valid);
	}
    /**
     * @desc: Kiem tra user dang nhap
     * @param: post action
     */
    public function _user_check(){
		$this->load->model('users_model');
		$row = $this->users_model->check_login();
		if (empty($row)){
			$this->form_validation->set_message('_user_check',$this->lang->line('users_error_check'));
			return false;
		}
        else{
            $this->user_data = $row;
            ////////////// remember password ////////////
            if ($this->input->post("log_remember") == 1){
                $cookie = array(
                    'name'   => USER_REMEMBER_PASSWORD,
                    'value'  =>  base64_encode('username='.$row['username'].'&password='.$row['password']),
                    'expire' => 2592000, // 30 ngay
                );
                set_cookie($cookie);
            }
            /////// return //////////
            return true;
        }
	}
/** ======================================= REGISTER ===================================== **/
    public function register(){
        if ($this->session->userdata('username') && $this->session->userdata('userid')){
			redirect('users/profile');
		}
  		if ($this->input->post('reg_submit')){
			$this->load->model('users_model','users');
            $this->_reg_validation();
			if ($this->form_validation->run() == true)
			{
			    $code = $this->users->register();

                $content = 'Chào bạn.
                <br>
Để hoàn thành việc đăng ký thành viên tại Ms Hoa TOEIC, bạn phải xác nhận tài khoản bằng liên kết dưới đây. <br>
<a href="'.BASE_URL.'users/active/'.$this->input->post('username').'/'.$code.'">'.BASE_URL.'users/active/'.$this->input->post('username').'/'.$code.'</a><br>
<br>Cảm ơn bạn đã đăng ký.
<br>Ms Hoa TOEC';
                send_mail($this->input->post('email'),$this->lang->line('users_register_subject'),$content);
                redirect("users/success/1");
			}
		}

        $row = set_array('fullname','sex','yahoo','skype');
		$data = $this->_users_form($row);
		$data['title'] = $this->lang->line('users_register');
        $data['username'] = form_input('username',set_value('username'));
		$data['email'] = form_input('email',set_value('email'));
		$data['submit'] = form_submit('reg_submit',$this->lang->line('users_register_submit'));
        $this->load->layout('users/register',$data);
    }
    public function _reg_validation(){
        $this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'username',
                 'label'   => $this->lang->line('users_username'),
                 'rules'   => 'required|alpha_dash|min_length[3]|max_length[40]'
              ),
           array(
                 'field'   => 'password',
                 'label'   => $this->lang->line('users_password'),
                 'rules'   => 'required|min_length[3]|max_length[40]'
              ),
           array(
                 'field'   => 'password_confirm',
                 'label'   => $this->lang->line('users_password_confirm'),
                 'rules'   => 'required|matches[password]'
              ),
           array(
                 'field'   => 'fullname',
                 'label'   => $this->lang->line('users_fullname'),
                 'rules'   => 'required|min_length[3]|max_length[50]'
              ),
           array(
                 'field'   => 'email',
                 'label'   => $this->lang->line('users_email'),
                 'rules'   => 'required|valid_email'
              ),
           array(
                 'field'   => 'reg_captcha',
                 'label'   => $this->lang->line('users_captcha'),
                 'rules'   => 'required|matches_str['.$this->session->userdata('reg_captcha').']|callback__check_exist'
              )
  		);
  		$this->form_validation->set_rules($valid);
    }
    public function _check_exist(){
        $row = $this->users->check_exist($this->input->post('username'),$this->input->post('email'));
        if (!empty($row)){
            $this->form_validation->set_message('_check_exist',$this->lang->line('users_member_exist'));
            return false;
        }
        return true;
    }
    public function ajax_check_username(){
        $username = $this->input->post("username");
        $this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_dash|min_length[3]|max_length[40]');
        if ($this->form_validation->run() == true)
		{
            $this->load->model('users_model','users');
            if ($this->users->check_username_exist($username) > 0){
                $this->output->set_output(0);
            }
            else{
                $this->output->set_output(1);
            }
        }
        else{
            $this->output->set_output(0);
        }
    }
    public function ajax_check_email(){
        $email = $this->input->post("email");
        $this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if ($this->form_validation->run() == true)
		{
            $this->load->model('users_model','users');
            if ($this->users->check_email_exist($email) > 0){
                $this->output->set_output(0);
            }
            else{
                $this->output->set_output(1);
            }
        }
        else{
            $this->output->set_output(0);
        }
    }
/** ===================================== PROFILE ==================================== **/
    public function update_miss() {
        if (!$this->session->userdata('userid')){
            return $this->output->set_output(json_encode(array('status' => 'error', 'message' => 'Chưa đăng nhập')));
        }
        if ($this->input->post('fullname')) {
            $arrData['fullname'] = trim(strip_tags($this->input->post('fullname')));
        }
        if ($this->input->post('birth_year')) {
            $arrData['birthday'] = (int) $this->input->post("birth_year").'-'.(int) $this->input->post("birth_month").'-'.(int) $this->input->post("birth_date");
        }
        if ($this->input->post('where_live')) {
            $arrData['where_live'] = trim(strip_tags($this->input->post('where_live')));
        }
        if ($this->input->post('phone')) {
            $arrData['phone'] = trim(strip_tags($this->input->post('phone')));
        }
        if (!$arrData) {
            return $this->output->set_output(json_encode(array('status' => 'error', 'message' => 'Chưa nhập thông tin')));
        }
        $this->load->model("users_model",'users');
        $this->users->update_profile($arrData);
        // get user
        $row = $this->users->detail($this->session->userdata('userid'));
        $this->permission->set_user_session($row);

        return $this->output->set_output(json_encode(array('status' => 'success', 'message' => 'Đã cập nhật')));
    }
    public function profile(){
        $message = '';
        	if (!$this->session->userdata('userid')){
    			redirect('users/register');
    		}
            $this->load->model('users_model','users');
            if ($this->input->post('profile_submit')){
    			$this->_profile_validation();
    			if ($this->form_validation->run() == true)
    			{
    			    $this->users->update_profile();
                    $message = 'Thông tin đã được cập nhật';
    			}
    		}
    		$row = $this->users->detail($this->session->userdata('userid'));
            if (empty($row)){
                show_404();
        }
        $data = $this->_users_form($row);
        $data['title'] = $this->lang->line('users_profile');
        $data['message'] = $message;
    		$data['username'] = $row['username'];
    		$data['email'] = $row['email'];
    		$data['submit'] = form_submit('profile_submit',$this->lang->line('users_profile_submit'));
        $this->load->layout('users/register',$data);
    }
    public function _users_form($row){
		$this->load->helper('captcha');
		$this->load->helper('form');
		$data['input_captcha'] = form_input('reg_captcha');
		$data['captcha'] = get_captcha('reg_captcha');
        $data['where_live'] = form_dropdown("where_live",get_location(),$row['where_live']);

        $arrday = array();
        if ($row['birthday']){
            $arrday = explode('-',$row['birthday']);
        }

        for ($i = 1; $i <= 31; $i++){
            $date[$i] = $i;
        }
        for ($i = 1; $i <= 12; $i++){
            $month[$i] = $i;
        }
        for ($i = 1930; $i < 2007; $i ++){
            $year[$i] = $i;
        }
        $data['birthday_date'] = form_dropdown("birth_date",$date,$arrday[2]);
        $data['birthday_month'] = form_dropdown("birth_month",$month,$arrday[1]);
        $data['birthday_year'] = form_dropdown("birth_year",$year,$arrday[0]);
        $data['fullname'] = form_input('fullname',set_value('fullname',$row['fullname']));
        $data['mobile'] = form_input('phone',set_value('phone',$row['phone']));
		$data['password'] = form_password('password',set_value('password'));
        $data['password_confirm'] = form_password('password_confirm',set_value('password_confirm'));
        $data['send_mail'] = form_checkbox('send_mail',1,true);
        return $data;
    }
    public function _profile_validation(){
        $this->load->library('form_validation');
		$valid = array(
           array(
                 'field'   => 'password',
                 'label'   => $this->lang->line('users_password'),
                 'rules'   => 'min_length[3]|max_length[40]'
              ),
           array(
                 'field'   => 'fullname',
                 'label'   => $this->lang->line('users_fullname'),
                 'rules'   => 'required|min_length[3]|max_length[50]'
              ),
           array(
                 'field'   => 'reg_captcha',
                 'label'   => $this->lang->line('users_captcha'),
                 'rules'   => 'required|matches_str['.$this->session->userdata('reg_captcha').']'
              )
  		);
        if ($this->input->post('password')){
            $valid[] = array(
                 'field'   => 'password_confirm',
                 'label'   => $this->lang->line('users_password_confirm'),
                 'rules'   => 'required|matches[password]'
              );
        }
  		$this->form_validation->set_rules($valid);
    }
/** ======================================= COMMON =============================== **/
    public function logout(){
        $this->session->sess_destroy();
        delete_cookie(USER_REMEMBER_PASSWORD);
        redirect();
    }
/** ================================== FORGOT PASS ================================ **/
    public function forgotajax(){
        if ($this->session->userdata('userid')){
			return $this->output->set_output(json_encode(array('error' => 'Đã đăng nhập rồi')));
		}
		$this->load->model('users_model','users');
        $this->_forgot_pass_validation();
		if ($this->form_validation->run() == true)
		{
		    $email = $this->input->post("email");
		    $code = $this->users->forgot_create_code($this->username);
            $link = site_url('users/new_pass/'.$this->username.'/'.$code);
            $link = 'Chào bạn : '.$this->username.'<br>
            Bạn vừa yêu cầu lấy lại mật khẩu của tài khoản '.$this->username.' trên Website Mshoatoeic.com.<br>
            Chú ý: Bạn có thể bỏ qua email này nếu người yêu cầu lấy lại mật khẩu không phải là bạn<br><br>
            Hãy click vào dòng dưới đây để lấy lại mật khẩu của mình <br>
            '.$link.'<br>

            Mọi thắc mắc vui lòng liên hệ:<br>
            Ms Hoa TOEIC Center<br>
            Cơ sở 1: 89 Tô Vĩnh Diện, Thanh Xuân, Hà Nội. SĐT: 0466 811 242 <br>
            Cơ sở 2: 26/203 Hoàng Quốc Việt, Cầu Giấy, Hà Nội. SĐT: 0462 956 406<br>
            Email: hoa.nguyen@mshoatoeic.com<br>
            Website: http:///www.mshoatoeic.com<br>
            ';
            send_mail($email,'Lay lai mat khau tren Mshoatoeic.com',$link);
            return $this->output->set_output(json_encode(array('error' => '','success' => 'Bạn vui lòng kiểm tra Email, làm theo hướng dẫn để lấy lại mật khẩu.')));
		}
        else{
            return $this->output->set_output(json_encode(array('error' => validation_errors())));
        }
    }
    public function forgot_pass(){
        if ($this->session->userdata('userid')){
			redirect();
		}
        if ($this->input->post('users_forgot_submit')){
			$this->load->model('users_model','users');
            $this->_forgot_pass_validation();
			if ($this->form_validation->run() == true)
			{
			    $email = $this->input->post("email");
			    $code = $this->users->forgot_create_code($this->username);
                $link = BASE_URL.'/users/new_pass/'.$this->username.'/'.$code;
                $message = '
                Chào bạn : '.$this->username.'<br>

                Bạn vừa yêu cầu lấy lại mật khẩu của tài khoản giangnguyen2831987 trên Website Mshoatoeic.com.<br>
                Chú ý: Bạn có thể bỏ qua email này nếu người yêu cầu lấy lại mật khẩu không phải là bạn<br>
                <br>
                Hãy click vào dòng dưới đây để lấy lại mật khẩu của mình <br>
                <a href="'.$link.'">'.$link.'</a><br>
                <br>
                Mọi thắc mắc vui lòng liên hệ:<br>
                <b>Trung tâm Ms Hoa TOEIC</b> <br>
                Tầng 6, 82 Vương Thừa Vũ, Thanh Xuân, HN<br>
                Email: hoa.nguyen@mshoatoeic.com<br>
                Website: http:///www.mshoatoeic.com';

                send_mail($email,$this->lang->line("users_forgot_email_subject"),$link);
                redirect("users/success/2");
			}
		}
        $this->load->helper('form');
        $data['email'] = form_input("email",set_value("email"));
        $data['submit'] = form_submit("users_forgot_submit",$this->lang->line("users_forgot_submit"));
        $this->load->layout("users/forgot_pass",$data);
    }
    public function _forgot_pass_validation(){
        $this->load->library('form_validation');
		$valid = array(
           array(
                 'field'   => 'email',
                 'label'   => $this->lang->line('users_email'),
                 'rules'   => 'required|valid_email|callback__forgot_check'
              )
  		);
  		$this->form_validation->set_rules($valid);
    }
    public function _forgot_check(){
        $row = $this->users->check_email_forgot($this->input->post('email'));
        if (empty($row)){
            $this->form_validation->set_message('_forgot_check',$this->lang->line('users_forgot_email_exist'));
            return false;
        }
        else{
            $this->username = $row['username'];
            return true;
        }
    }
    public function new_pass($username = '',$code = ''){
        if ($this->session->userdata('userid')){
    			redirect();
    		}
        if ($username == '' OR $code == ''){
            show_404();
        }
        $this->load->model('users_model','users');
        if ($this->users->check_forgot_code($username,$code) === TRUE){
            if ($this->input->post('new_pass_submit')){
              $this->_new_pass_validation();
        			if ($this->form_validation->run() == true)
        			{
        			    $this->users->set_new_password($username,$this->input->post("password"));
                        redirect("users/success/3");
        			}
    		    }
            $this->load->helper('captcha');
    		$this->load->helper('form');
    		$data['input_captcha'] = form_input('captcha');
    		$captcha = get_captcha('captcha');
            $data['captcha'] = $captcha['image'];
    		$data['password'] = form_password('password',set_value('password'));
            $data['password_confirm'] = form_password('password_confirm',set_value('password_confirm'));
            $data['submit'] = form_submit('new_pass_submit',$this->lang->line("users_new_pass_submit"));
            $this->load->layout("users/new_pass",$data);
        }
        else{
            show_404();
        }
    }
    public function _new_pass_validation(){
        $this->load->library('form_validation');
		$valid = array(
           array(
                 'field'   => 'password',
                 'label'   => $this->lang->line('users_password'),
                 'rules'   => 'required|min_length[3]|max_length[40]'
              ),
           array(
                 'field'   => 'password_confirm',
                 'label'   => $this->lang->line('users_password_confirm'),
                 'rules'   => 'required|matches[password]'
              ),
           array(
                 'field'   => 'captcha',
                 'label'   => $this->lang->line('users_captcha'),
                 'rules'   => 'required|matches_str['.$this->session->userdata('captcha').']'
              )
  		);
  		$this->form_validation->set_rules($valid);
    }
/** ============================================= ACTIVE ================================ **/
    public function active($user = '',$code = ''){
        if ($user == '' OR $code == ''){
            show_404();
        }
        $this->load->model('users_model','users');
        $status = $this->users->active($user,$code);
        $data['title'] = $this->lang->line('users_active_title');
        if ($status === true){
            $data['msg'] = sprintf($this->lang->line('users_active_success'),$user);
            // tao tai khoan bank
        }
        elseif($status == false){
            $data['msg'] = $this->lang->line('users_active_not_found');
        }
        else{
            $data['msg'] = $this->lang->line('users_active_deleted');
        }
        $this->load->layout('users/result',$data);
    }
    public function reactive(){
        if ($this->session->userdata('userid')){
            show_404();
        }
        if ($this->input->post('reactive_submit')){
			$this->load->model('users_model','users');
            $this->_reactive_validation();
			if ($this->form_validation->run() == true)
			{
			    $email = $this->input->post("email");
			    $code = $this->users->reactive_code($this->username);
                $link = site_url('users/active/'.$this->username.'/'.$code);
                send_mail($email,$this->lang->line("users_active_title"),$link);
                redirect("users/success/4");
			}
		}
        $this->load->helper('form');
        $this->load->helper('captcha');
        $data['email'] = form_input('email',set_value('email'));
        $data['input_captcha'] = form_input('reactive_captcha');
		$data['captcha'] = captcha_code('captcha');
        $data['submit'] = form_submit('reactive_submit',$this->lang->line('users_reactive_submit'));
        $this->load->layout('users/reactive',$data);
    }
	private function _reactive_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'email',
                 'label'   => $this->lang->line('users_email'),
                 'rules'   => 'required|valid_email'
              ),
           array(
                 'field'   => 'reactive_captcha',
                 'label'   => $this->lang->line('users_captcha'),
                 'rules'   => 'required|matches_str['.$this->session->userdata('captcha').']|callback__check_reactive_mail'
              )
  		);
  		$this->form_validation->set_rules($valid);
	}
    public function _check_reactive_mail(){
        $row = $this->users->check_email_active($this->input->post('email'));
        if (empty($row)){
            $this->form_validation->set_message('_check_reactive_mail',$this->lang->line('users_reactive_email_check'));
            return false;
        }
        else{
            $this->username = $row['username'];
            return true;
        }
    }
    /** =========================== USER AREA ================================ **/
    public function speaking(){
        if (!$this->session->userdata('userid')){
			redirect("/");
		}
        $limit = 20;
        $page = (int)$this->input->get('page');
        $page = ($page > 1) ? $page : 1;
        $this->load->model('users_model','users');
        $param = array('user_id' => $this->session->userdata("userid"),"limit" => $limit, "page" => $page);
        // GET PAGINATION
        $data['rows'] = $this->users->get_user_speaking($param);

        $this->config->set_item("menu_select",array('item_mod' => 'speaking', 'item_id' => 1));
        $this->config->set_item("mod_access",array("name" => "speaking"));

        $config['total_rows'] = count($data['rows']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['rows'][$limit]);
        $this->load->layout('users/speaking_list',$data);
    }
    public function writing(){
        if (!$this->session->userdata('userid')){
			redirect("/");
		}
        $limit = 20;
        $page = (int)$this->input->get('page');
        $page = ($page > 1) ? $page : 1;
        $this->load->model('users_model','users');
        $param = array('user_id' => $this->session->userdata("userid"),"limit" => $limit, "page" => $page);
        // GET PAGINATION
        $data['rows'] = $this->users->get_user_writing($param);
        $config['total_rows'] = count($data['rows']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['rows'][$limit]);
        $this->config->set_item("menu_select",array('item_mod' => 'writing', 'item_id' => 1));
        $this->config->set_item("mod_access",array("name" => "writing"));
        $this->load->layout('users/writing_list',$data);
    }
}