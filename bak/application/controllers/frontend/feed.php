<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Feed extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('news_model', 'news', TRUE);
        $this->load->helper('xml');
    }

    function index()
    {
        $data['encoding'] = 'utf-8';
        $data['feed_name'] = 'Ms Hoa Toeic';
        $data['feed_url'] = 'http://www.mshoatoeic.com';
        $data['page_description'] = 'Website of Ms Hoa';
        $data['page_language'] = 'en-ca';
        $data['creator_email'] = 'info@mshoatoeic.com';
        $data['rows'] = $this->news->feed();
        //header("Content-Type: application/rss+xml");
        $this->load->view('news/feed', $data,FALSE);
    }
    public function success(){
        $data['title'] = "Đăng ký thành công";
        $data['result'] = "Chúng tôi đã nhận được thông tin của bạn.";
        $this->load->setData('title',"Đăng ký thành công");
        $this->load->layout('contact/result',$data);
    }
    public function form($type = 1){
        $this->load->model("feed_model","feed");
        if ($this->input->post('reg_new_class_send')){
            $this->_reg_validation();
            if ($this->form_validation->run()){
                // kiem tra email ton tai hay chua

                //$count = $this->feed->check_email_exist($this->input->post("email"),$type);
                // if ($count <= 0){
                    if (!$this->session->userdata("reg_".$type)){
                        $this->feed->reg_by_class();
                        $this->session->set_userdata("reg_".$type,1);
                        redirect(current_url());
                    }
                    else{
                        $data['error'] = 'Bạn đã đăng ký rồi';
                    }
                /* }
                else{
                    $data['error'] = 'Email của bạn đã có trong danh sách';
                } */
            }
        }
        $this->load->helper('form');
        $this->load->helper('captcha');
        $email = ($this->session->userdata("user_email")) ? $this->session->userdata("user_email") : "";
        $fullname = ($this->session->userdata("fullname")) ? $this->session->userdata("fullname") : "";
        $data['email'] = form_input('email',set_value('email',$email));
        $data['fullname'] = form_input('fullname',set_value('fullname',$fullname));
        $data['mobile'] = form_input('mobile',set_value('mobile'));
        if ($type == 1){
            $class = $this->feed->class_list();
            $option[0] = 'Chọn lớp đăng ký nhận Email thông báo khai giảng';
            foreach ($class as $class){
                $option[$class['id']] = $class['name'];
            }
            $data['class'] = form_dropdown("class",$option);
            $data['title'] = 'ĐĂNG KÝ NHẬN THÔNG BÁO KHAI GIẢNG QUA EMAIL';
        }
        if ($type == 2){
            $class = $this->feed->class_list();
            $option[0] = 'Chọn lớp học mong muốn';
            foreach ($class as $class){
                $option[$class['id']] = $class['name'];
            }
            $data['class'] = form_dropdown("class",$option);
            $data['title'] = 'ĐĂNG KÝ THI THỬ & TƯ VẤN LỘ TRÌNH HỌC';
        }
        if ($type == 3){
            $class = $this->feed->class_list();
            $option[0] = 'Chọn lớp học mong muốn';
            foreach ($class as $class){
                $option[$class['id']] = $class['name'];
            }
            $data['class'] = form_dropdown("class",$option);
            $data['title'] = 'ĐĂNG KÝ KIỂM TRA ĐỂ THAM GIA LỚP HỌC TẠI MS HOA TOEIC';
        }
        $data['inttype'] = $type;
        $data['type'] = form_hidden("type",$type);
        $data['security_code'] = captcha_code('captcha_feed_new_class');
        $data['captcha'] = form_input('captcha',set_value('captcha'));
        $data['submit'] = form_submit('reg_new_class_send',"Gửi đăng ký");
        ////////////////////////////////////////////////////////

        $page = (int)$this->input->get('page');
        $limit = 15;
        $page = ($page > 1) ? $page : 1;
        $config['total_rows'] = $this->feed->list_member_total($type);
        $config['per_page'] = $limit;
        $this->load->library('pagination',$config);
        $data['paging'] =  $this->pagination->create_links();

        if ($config['total_rows'] > 0){
            $data['rows'] = $this->feed->list_member_reged($type,$limit,$page);
        }
        /////////////////////////////
        $this->load->setData('title',$data['title']);
        $this->load->layout('feed/reg_new_class',$data);
    }
    private function _reg_validation(){
  		$this->load->library('form_validation');
		$valid = array(
           	array(
                 'field'   => 'email',
                 'label'   => 'email',
                 'rules'   => 'required|valid_email'
              ),
            array(
                 'field'   => 'fullname',
                 'label'   => 'Tên',
                 'rules'   => 'required'
              ),
            array(
                 'field'   => 'mobile',
                 'label'   => 'Điện thoại',
                 'rules'   => 'required'
              ),
            array(
                 'field'   => 'class',
                 'label'   => 'Lớp học',
                 'rules'   => 'required|is_natural_no_zero'
              ),
            array(
                 'field'   => 'captcha',
                 'label'   => 'Mã xác nhận',
                 'rules'   => 'required|matches_str['.$this->session->userdata('captcha_feed_new_class').']'
              ),

		);
  		$this->form_validation->set_rules($valid);
    }
}
?>