<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Writing extends CI_Controller{
    private $type_to_question = array(
                            1 => array(1,2,3,4,5),
                            2 => array(6,7,8),
                            3 => array(9,10,11)
                        );
    public function __construct(){
		parent::__construct();
        $this->config->set_item("mod_access",array("name" => "writing"));
	}
    public function answer($id,$time = 0,$user_id = 0){
        $id = (int) $id;
        if (!$this->session->userdata("userid")){
            redirect("/users/login");
        }
        $permission = $this->session->userdata("permission");
        $data['permission'] = 0;
        if ($permission){
            $data['permission'] = (array_key_exists('writing',$permission) && in_array('point_detail',$permission['writing'])) ? 1 : 0;
        }

        $this->load->model("writing_model","writing");
        // view
        $detail  = $this->writing->get_test_detail($id);
        $count = 0;
        for ($i = 1; $i <=8 ; $i++){
            if ($detail['question_'.$i] > 0){
                $question_id[] = $detail['question_'.$i];
                $data['question'][$detail['question_'.$i]] = $i;
                $count ++;
            }
        }
        $data['detail'] = $detail;
        $data['rows'] = $this->writing->get_answer($question_id);
        // get latest user answer

        $userid = ($data['permission'] == 1 && $user_id) ? $user_id : $this->session->userdata("userid");

        $param = array("user_id" => $userid,"limit" => $count,"test_id" => $id,"time" => $time);
        $user_answer = $this->writing->get_answer_by_test($param);
        foreach ($user_answer as $uwer){
            if (!$time){
                $time = $uwer['time'];
            }
            if ($time != $uwer['time']){
                break;
            }
            $useranswer[$uwer['writing_id']] = $uwer;
        }
        $data['useranswer'] = $useranswer;
        //debug($useranswer);
        // add static

        $js = array(
            js('writing.js'),
            //link_tag('speaking.css'),
        );
        $this->load->setArray("script",$js);
        $this->load->layout("writing/test_answer",$data);
    }
    /*
    public function preview($id,$time,$userid = 0){
        $id = (int) $id;
        if (!$this->session->userdata("userid")){
            redirect("/users/login");
        }
        $permission = $this->session->userdata("permission");
        $data['permission'] = (array_key_exists('writing',$permission) && in_array('point_detail',$permission['writing'])) ? 1 : 0;
        // check thanh vien da lam bai do chua
        $this->load->model("writing_model","writing");
        $js = array(
            js('writing.js'),
            link_tag('speaking.css'),
        );
        $this->load->setArray("script",$js);

        // view
        $rows  = $this->writing->get_test_detail($id);
        for ($i = 1; $i <=11 ; $i ++){
            if ($rows['question_'.$i] > 0){
                $arr[] = $rows['question_'.$i];
            }
        }
        $param = array("test_id" => $id, "time"=>$time, "writing_id" => $arr);
        $param['user_id'] = ($data['permission'] == 1 && $userid > 0) ? $userid : $this->session->userdata("userid");
        $data['rows'] = $this->writing->get_question_by_arr($param);
        $data['detail'] = $rows;
        $this->load->layout("writing/preview",$data);
    }*/
    public function lists ($testname = '',$level = ''){
        // get data
        $code = get_code_by_type(2);
        $part = (int)array_search(strtolower($testname),$code);
        $seotitle = str_replace('-',' ',$testname).' - '.str_replace('-',' ',$level);
        if ($part == 0 && ($testname != 'fulltest' || $level != '')){
            redirect('/writing/fulltest.html','location','301');
        }
        // xoa session dang lam bai
        $level = (int) get_level_by_code($level);
        if ($part > 0 && $level <= 0){
            redirect('/writing/fulltest.html','location','301');
        }
        $this->session->unset_userdata("writing_test");
        // get data
        $limit = 20;
        $this->load->model("writing_model","writing");
        $page = (int)($this->input->get("page")) ? $this->input->get("page") : 1;
        $param = array( "type" => $part , "limit" => $limit, "page" => $page, "level" => $level);

        $data['rows'] = $this->writing->test_lists($param);
        /// meta ///
        $seotitle = ($testname == 'fulltest' || $level == '') ? 'Toeic Writing, toeic Writing online free, Ms Hoa TOEIC' : 'Luyen tap TOEIC Writing - '. $seotitle;
        $seldesc = 'TOEIC writing, TOEIC writing online free'. ' - '. $this->config->item('metadesc');
        $this->load->setData('title',$seotitle);
        $this->load->setData('metakey',$seotitle);
        $this->load->setData('metades',$seldesc);
        /// css ////
        //$static = link_tag('speaking.css');
        $this->load->setArray("common",$static);
        $this->config->set_item("mod_access",array("name" => "writing_list"));
        $this->config->set_item("menu_select",array('item_mod' => 'writing_cate_'.$part, 'item_id' => $level));
        $this->load->layout("writing/test_list",$data);
    }
    /**
     * @desc: Lay bai tap theo lop
     * @param: $part = fulltest / fullpart / 0,1,2,3,4
     */
    public function byclass($part = null){
        $part = $this->security->xss_clean($part);
        $limit = 100;
        $checkpermission = $this->permission->check_permission_class();
        if ($checkpermission != true){
            return $checkpermission;
        }
        $classid = $this->session->userdata("class_id");
        $page = (int)($this->input->get("page")) ? $this->input->get("page") : 1;
        /////////get data ///////
        $this->load->model("writing_model","writing");
        switch ($part) {
            case 'fulltest':
                $part = 0;
                $data['testtype'] = 'full';
            break;
            case 'fullpart':
            case is_numeric($part):
                $part = $part;
                $data['testtype'] = 'part';
            break;
            default:
                $part = null;
        }
        /// unset list //
        $this->session->unset_userdata("writing_test");
        /// meta ///
        $seotitle = 'Bài tập TOEIC Writing - Ms Hoa TOEIC';
        $seldesc = 'Bài tập TOEIC Writing'. ' - '. $this->config->item('metadesc');
        $this->load->setData('title',$seotitle);
        $this->load->setData('metakey',$seotitle);
        $this->load->setData('metades',$seldesc);
        /// role ///
        $this->config->set_item("mod_access",array("name" => "writing_list"));
        $this->config->set_item("menu_select",array('item_mod' => 'writing_class'.$part, 'item_id' => $part));
        ///////////
        $param = array( "type" => $part , "limit" => $limit, "page" => $page, "role" => $classid);
        $data['rows'] = $this->writing->test_lists($param);
        $this->load->layout("writing/byclass",$data);
    }
    public function detail ($id){
        if (!$this->session->userdata("userid")){
            $refer = base64_encode(current_url());
            redirect("users/login?refer=".$refer);
        }
        $id = (int) $id;
        $this->load->model("writing_model","writing");
        // lay cau hoi ra
        // get cau hoi
        $param = $this->session->userdata("writing_test");
        //$param = '';
        if (!$param || $param['detail']['test_id'] != $id || count($param['question']) < 1){ // neu co session va session do dung voi bai test hien tai
            $test = $this->writing->get_test_detail($id);
            if (!$test) {
                show_404();
            }
            for ($i = 1; $i <=8 ; $i++){
                if ($test['question_'.$i] > 0){
                    if ($i <= 5){
                        $arr['type_1'][] = $test['question_'.$i];
                    }
                    elseif ($i < 8) {
                        $arr['type_2'][] = $test['question_'.$i];
                    }
                    else{
                        $arr['type_3'][] = $test['question_'.$i];
                    }

                }
            }
            $param = array(
                'time'              => time(),
                'detail'            => $test,
                'question'          => $arr,
            );
            $this->session->set_userdata("writing_test",$param);

        }
        if ($param['detail']['isclass'] == 1){
            $checkpermission = $this->permission->check_permission_class();
            if ($checkpermission != true){
                return $checkpermission;
            }
        }
        if ($param['detail']['password'] > 0){
            $checkpermission = $this->permission->check_permission_test($param['detail']['password']);
            if ($checkpermission != true){
                return $checkpermission;
            }
        }
        $url = $this->uri->uri_string();
        if ($url != trim($param['detail']['share_url'],'/')){
            redirect($param['detail']['share_url'],'location','301');
        }
        $level = get_code_by_id($param['detail']['level']);
        $type = get_code_by_type (2);
        $type = $type[$param['detail']['type']];
        if ($param['detail']['isclass'] == 1){
            $data['exitlink'] =  ($level && $type != 'fulltest') ? '/writing/byclass/fullpart' : '/speaking/writing/fulltest';
        }
        elseif ($param['detail']['password'] > 0) {
            $data['exitlink'] =  '/speaking/finaltest/'.$param['detail']['password'];
        }
        else{
            $data['exitlink'] =  ($level && $type != 'fulltest') ? '/writing/'.$level.'/'.$type.'.html' : '/writing/fulltest.html';
        }


        $question_id = array_slice($param['question'],0,1);
        $key = key($question_id);
        $listid = array_shift($param['question']);
        // xoa phan tu khoi mang
        switch ($key){
            case 'type_1':
            $data['time'] = 8*60;
            break;
            case 'type_2':
            $data['time'] = 10*60;
            break;
            case 'type_3':
            $data['time'] = 30*60;
            break;
            default:
            return $this->output->set_output('Câu hỏi bị lỗi') ;
        }

        // luu session moi
        $this->session->set_userdata("writing_test",$param);

        $this->load->helper("form");

        // lay thong tin chi tiet cau hoi
        $data['key'] = $key;
        $data['rows'] = $this->writing->detail_question($listid);
        $data['detail'] = $param;
        $js = array(

            js('writing.js'),
            js('jquery.plugin.min.js'),
            js('jquery.countdown.js'),
            js('jplayer/jquery.jplayer.min.js'),
            link_tag('jquery.countdown.css'),
        );
        $this->load->setArray("script",$js);
        $seotitle = ($param['detail']['name']) ? $param['detail']['name'] : $this->config->item('metakey');
        $seldesc = $seotitle. ' - '. $this->config->item('metadesc');
        $this->load->setData('title',$seotitle);
        $this->load->setData('metakey',$seotitle);
        $this->load->setData('metades',$seldesc);

        $this->load->layout("writing/test_detail",$data);
    }
    public function sendanswer(){
        $session = $this->session->userdata("writing_test");
        if (!$this->session->userdata('userid')){
            $result['error'] = 'Chưa đăng nhập';
            return $this->output->set_output(json_encode($result));

        }
        $answer = $this->input->post("answer");
        if (empty($session['detail']) || empty($answer)){
            $result['error'] = 'Không tìm thấy thông tin bài test';
            return $this->output->set_output(json_encode($result));
        }
        foreach ($answer as $key => $value){
            $input[] = array(
                'content'       => $value,
                'writing_id'    => $key,
                'user_id'       => $this->session->userdata("userid"),
                'time'          => $session['time'],
                'test_id'       => (int)$session['detail']['test_id'],
            );
        }
        $this->load->model("writing_model","writing");
        $this->writing->insert_user_answer($input);
        // luu vao database
        $result['success'] = 1;
        // neu het cau hoi se load bai test khac
        if (count($session['question']) <= 0){

            $param = array('test_id' => $session['detail']['test_id'], "type" => $session['detail']['type']);
            $row = $this->writing->get_next_test($param);
            if ($row['test_id'] > 0){
                $result['next_test'] = (int)$row['test_id'];
            }
            else{
                $result['next_test'] = -1;
            }
        }
        else{
            $result['next_test'] = 0;
        }
        $this->output->set_output(json_encode($result));
    }
}