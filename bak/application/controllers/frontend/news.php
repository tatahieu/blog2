<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller{

    function __construct(){

		parent::__construct();

	}

    public function index(){

        $keycache = 'news/detail/461371';

        if ( ! $detail = $this->cache->get($keycache))

        {

             $this->load->model('news_model','news');

             $detail = $this->news->detail(461371);

             $this->cache->save($keycache, $detail, 3600);

        }

        $data['row'] = $detail;

        // set homepage menu

        $this->config->set_item("menu_select",array('item_mod' => 'home', 'item_id' => 0));

        $this->config->set_item("mod_access",array("name" => "home"));

        $this->load->setData('title',$this->config->item("site_title"). ' - '.$this->config->item("metakey"));

        $this->load->setData('metakey',$this->config->item("metakey"));

        $this->load->setData('metades',$this->config->item("metadesc"));



        $this->load->layout('news/index',$data);

    }

    public function support() {

        $this->load->layout('news/support',array('offSupport' => 1),FALSE,'layout_2c');

    }

    public function video($type = 'cate', $id = 0){

        if (!in_array($type,array('detail','cate'))) {

            show_404();

        }

        $limit = 12; $id = (int)$id; $cate_id = (int)$cate_id;

        //var_dump($cate_id); die;

        // instance

        $page = (int)$this->input->get('page');

        $page = ($page > 1) ? $page : 1;

        $this->load->model('news_model','news');

        // GET CATE DETAIL

        $listcate['rows'] = $this->news->get_list_cate(array("style" => 2,"parent" => 1));

        $menu = $this->load->view("block/menu_right",$listcate);

        $this->load->setArray("right",$menu);

        ///// get all news

        $url = $this->uri->uri_string();

        if ($type == 'detail'){

            $detail = $this->news->detail($id);

            // check valid url

            if ($url != trim($detail['share_url'],'/')){

                redirect($detail['share_url'],'location','301');

            }

            $cateid = $detail['original_cate'];

            $param['exclude'] = $detail['news_id'];

        }

        else {

            $cateid = $id;

        }

        $cate = $this->news->get_cate_detail($cateid);

        if ($type == 'cate') {

            if ($url != trim($cate['share_url'],'/')){

                redirect($cate['share_url'],'location','301');

            }

        }

        if (!$cate){

            show_404();

        }

        if ($cate['style'] != 2){

            redirect($cate['share_url']);

        }

        // GET PAGINATION

        $param['category_id'] =  $cateid;

        $config['total_rows'] = $this->news->list_total_rule1($param);

        $param['limit'] = $limit;

        $param['page'] = $page;

        if ($config['total_rows'] < 1){

            return $this->load->layout('common/noresult');

        }

        $config['per_page'] = $limit;

        $this->load->library('pagination',$config);

        $data['paging'] =  $this->pagination->create_links();

        // GET LIST NEWS

        if ($type == 'detail'){

            $data['detail'] = $detail;

            $data['rows'] = $this->news->lists_by_cate_rule1($param);

            $seotitle = $detail['title'];

            $seldesc = $detail['description'];

            //// count hit ///////

            $this->news->count_hit($id);



        }

        elseif ($type == 'cate') {

            $data['rows'] = $this->news->lists_by_cate_rule1(array("category_id" => $cateid,"limit" => $limit + 1,"page"=> $page));

            $first = array_shift($data['rows']);

            $detail = $first;

            $data['detail'] = $this->news->detail($first['news_id']);

            $seotitle = ($cate['metakey'] != '') ? $cate['metakey'] : $data['cate_name'];

            $seldesc = ($cate['metadesc'] != '') ? $cate['metadesc'] : $data['cate_name'].' - '. $this->config->item('metadesc');

            //// count hit ///////

            $this->news->count_hit($first['news_id']);

        }



        // GET DETAIL CATE

        $data['cate'] = $cate;

        // SET META DATA && SET CONFIG

        $this->load->setData('ogimage',getimglink($detail['images']));

        $this->load->setData('ogtitle',$detail['title']);

        $this->load->setData('ogurl',current_url());

        $this->load->setData('ogdescription',$detail['description']);



        $this->load->setData('title',$seotitle);

        $this->load->setData('metakey',$seotitle);

        $this->load->setData('metades',$seldesc);

        // set data common

        $this->config->set_item("breadcrumb",array(array("name" => $cate['name'])));

        $this->config->set_item("mod_access",array("name" => "news_video"));

        $this->config->set_item("menu_select",array('item_mod' => 'news_video', 'item_id' => $cate_id));

        $this->load->layout('news/thumb',$data);

    }

	public function lists($cate_id = 0){

        $limit = 12; $cate_id = intval($cate_id);

        //var_dump($cate_id); die;

        if ($cate_id <= 0){

            show_404();

        }

        // instance

        $page = (int)$this->input->get('page');

        $page = ($page > 1) ? $page : 1;

        $this->load->model('news_model','news');

        // GET CATE DETAIL

        $cate = $this->news->get_cate_detail($cate_id);

        $url = $this->uri->uri_string();

        if ($url != trim($cate['share_url'],'/')){

            redirect($cate['share_url'],'location','301');

        }

        // GET PAGINATION

        $param = array('category_id' => $cate_id,"limit" => $limit, "page" => $page);

        $config['total_rows'] = $this->news->list_total_rule1($param);

        if ($config['total_rows'] < 1){

            return $this->load->layout('common/noresult');

        }

        $config['per_page'] = $limit;

        $this->load->library('pagination',$config);

        $data['paging'] =  $this->pagination->create_links();

        // GET LIST NEWS

        $data['rows'] = $this->news->lists_by_cate_rule1($param);

        // GET DETAIL CATE



        $data['cate'] = $cate;

        if ($cate['parent'] > 0) {

            $parent = $this->news->get_cate_detail($cate['parent']);

        }

        // SET META DATA && SET CONFIG

        $seotitle = ($cate['metakey']) ? $cate['metakey'] : $data['name'];

        $seldesc = ($cate['metadesc']) ? $cate['metadesc'] : $data['name'].' - '. $this->config->item('metadesc');

        $this->load->setData('title',$seotitle);

        $this->load->setData('metakey',$seotitle);

        $this->load->setData('metades',$seldesc);

        // set data common

        if ($parent) {

            $this->config->set_item("breadcrumb",array(array("name" => $parent['name'], "link" => $parent['link'])));

        }

        $this->config->set_item("mod_access",array("name" => "news_lists"));

        $this->config->set_item("menu_select",array('item_mod' => 'news_cate', 'item_id' => $cate_id));

        $this->load->layout('news/list',$data);

	}

    public function tags($tagid){

        //print_r($this->session->userdata);

        $limit = 6; $tagid = intval($tagid);

        if ($tagid <= 0){

            show_404();

        }

        $page = (int)$this->input->get('page');

        $page = ($page > 1) ? $page : 1;



        $this->load->model('news_model','news');

        $config['total_rows'] = $this->news->tag_total($tagid);

        if ($config['total_rows'] < 1){

            return $this->load->layout('common/noresult');

        }

        $config['per_page'] = $limit;

        $this->load->library('pagination',$config);

        $data['paging'] =  $this->pagination->create_links();

        $data['rows'] = $this->news->tags_list($tagid,$limit,$page);

        $data['cate'] = $tagid;

        $data['cate_name'] = $data['rows']['tag_name'];

        $data['page'] = 'tags';



        $this->load->setData('title',$data['rows']['tag_name']);

        $this->load->setData('metakey',$data['rows']['tag_name']);

        $this->load->setData('metades',$data['rows']['tag_name']);

        $this->load->layout('news/list',$data,false,'layout_3c');

    }

    public function detail($id = ''){

        $this->load->model('news_model','news');

        $detail = $this->news->detail($id);

        if (empty($detail)){

            show_404();

        }

        // check valid url

        $url = $this->uri->uri_string();

        if ($url != trim($detail['share_url'],'/')){

            redirect($detail['share_url'],'location','301');

        }

        ///

        $data['row'] = $detail;

        if ($detail['original_cate'] > 0){

            $data['relate'] = $this->news->relate($id,$detail['original_cate']);

            $data['latest'] = $this->news->latest($id,$detail['original_cate']);

            // neu can lay category info

            $cate = $this->news->get_cate_detail($detail['original_cate']);

            $this->config->set_item("menu_select",array('item_mod' => 'news_cate', 'item_id' => $detail['original_cate']));

        }

        else{

            $cate = array("name" => "About us");

            $this->config->set_item("menu_select",array('item_mod' => 'news', 'item_id' => $id));

        }

        $data['cate'] = $cate;

        $data['tags'] = $this->news->get_tags($id);

        //$data['comment_form'] = $this->load->view('news/comment_form');

        ///////////////////////////// SET DATA ///////////////////////

        $this->config->set_item("breadcrumb",array(array("name" => $cate['name'],"link" => $cate['share_url'])));

        $this->config->set_item("mod_access",array("name" => "news_detail"));



        ($detail['seo_title']) ? $this->load->setData('title',$detail['seo_title']) : $this->load->setData('title',$detail['title']);

        $this->load->setData('metakey',$detail['title'].' - '.$this->config->item('metakey'));

        ($detail['seo_desc']) ? $this->load->setData('metades',$detail['seo_desc']) :  $this->load->setData('metades',$detail['description']);

        $this->load->setData('ogimage',getimglink($detail['images'],'size5'));

        $this->load->setData('ogtitle',$detail['title']);

        $this->load->setData('ogurl',current_url());

        $this->load->setData('ogdescription',$detail['description']);

        //var_dump($detail['images']); die;

        $this->news->count_hit($id);

        $this->load->layout('news/detail',$data);

    }

    public function preview($id){

        $this->load->model('news_model','news');

        $detail = $this->news->detail($id,array('preview' => 1));

        if (empty($detail) || !$this->session->userdata('userid')){

            show_404();

        }

        ///

        $data['row'] = $detail;

        if ($detail['original_cate'] > 0){

            $data['relate'] = $this->news->relate($id,$detail['original_cate']);

            $data['latest'] = $this->news->latest($id,$detail['original_cate']);

            // neu can lay category info

            $cate = $this->news->get_cate_detail($detail['original_cate']);

            $this->config->set_item("menu_select",array('item_mod' => 'news_cate', 'item_id' => $detail['original_cate']));

        }

        else{

            $cate = array("name" => "About us");

            $this->config->set_item("menu_select",array('item_mod' => 'news', 'item_id' => $id));

        }

        $data['cate'] = $cate;

        $data['tags'] = $this->news->get_tags($id);

        //$data['comment_form'] = $this->load->view('news/comment_form');

        ///////////////////////////// SET DATA ///////////////////////

        $this->config->set_item("breadcrumb",array(array("name" => $cate['name'],"link" => $cate['share_url'])));

        $this->config->set_item("mod_access",array("name" => "news_detail"));



        ($detail['seo_title']) ? $this->load->setData('title',$detail['seo_title']) : $this->load->setData('title',$detail['title']);

        $this->load->setData('metakey',$detail['title'].' - '.$this->config->item('metakey'));

        ($detail['seo_desc']) ? $this->load->setData('metades',$detail['seo_desc']) :  $this->load->setData('metades',$detail['description']);

        $this->load->setData('ogimage',getimglink($detail['images']));

        $this->load->setData('ogtitle',$detail['title']);

        $this->load->setData('ogurl',current_url());

        $this->load->setData('ogdescription',$detail['description']);

        //var_dump($detail['images']); die;

        $this->news->count_hit($id);

        $this->load->layout('news/detail',$data);

    }

    public function byclass($cate_id = 0){

        $checkpermission = $this->permission->check_permission_class();

        if ($checkpermission != true){

            return $checkpermission;

        }

        //print_r($this->session->userdata);

        $limit = 10; $cate_id = intval($cate_id);

        $this->load->model('news_model','news');

        $page = (int)$this->input->get('page');

        $page = ($page > 1) ? $page : 1;

        $config['total_rows'] = $this->news->count_list_for_roles($this->session->userdata("class_id"),$cate_id);

        if ($config['total_rows'] < 1){

            return $this->load->layout('common/noresult');

        }

        $config['per_page'] = $limit;

        $this->load->library('pagination',$config);

        $data['paging'] =  $this->pagination->create_links();

        $data['rows'] = $this->news->list_for_roles($this->session->userdata("class_id"),$cate_id,$limit,$page);

        $data['cate'] = $cate_id;



        $this->config->set_item("mod_access",array("name" => "news_lists"));

        $this->config->set_item("menu_select",array('item_mod' => 'news_cate', 'item_id' => $cate_id));



        $this->load->layout('news/byclass',$data);



    }

    public function search(){

        $limit = 10;

        $this->load->model('news_model','news');

        $this->lang->load('frontend/search');

        $keyword = strip_tags($this->input->get('keyword',true));

        $total = $this->news->search_count($keyword);

        if ($total <= 0){

            $this->load->layout('common/noresult');

            return;

        }

        $config['total_rows'] = $total;

        $config['per_page'] = $limit;

        $this->load->library('pagination',$config);

        $data['keyword'] = $keyword;

        $data['total'] = $total;

        $data['paging'] =  $this->pagination->create_links();

        $data['rows'] = $this->news->search($keyword,$this->pagination->cur_page);

        $this->load->layout('news/search_list',$data);

    }

}