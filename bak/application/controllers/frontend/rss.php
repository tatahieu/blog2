<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rss extends CI_Controller{
    public function __construct(){
		parent::__construct();
	}
    public function index()  
    {  
        $this->load->model("rss_model","rss"); 
        $this->load->helper('xml');  
        $this->load->helper('text');  
        
        $limit = 10;
        $data['encoding']           = 'utf-8'; // the encoding
        $data['feed_name']          = base_url(); // your website  
        $data['feed_url']           = site_url("rss"); // the url to your feed  
        $data['page_description']   = $this->config->item("site_title"); // some description  
        $data['page_language']      = 'en-en'; // the language  
        $data['creator_email']      = 'quocnam@hanoicdc.com'; // your email  
        $data['rows']              = $this->rss->recent_post($limit);  
        //header("Content-Type: application/rss+xml"); // important!
        @header('Content-Type:text/xml');
        $this->load->view('rss/news', $data, FALSE);
    }
}