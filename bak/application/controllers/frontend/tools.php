<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tools extends CI_Controller{
    public function __construct(){
        parent::__construct();
    }
    public function index() {
        $this->load->view("tools/test",array(),FALSE);
    }
    public function test(){
        echo base64_decode('NTAwdG9laWM=');
    }
    /*
    // update share_url speaking detail
    public function shareurlspeaking(){
        $query = $this->db->get('speaking_test');
        $rows = $query->result_array();
        foreach ($rows as $row) {
            $level = get_code_by_id($row['level']);
            $level = ($row['type'] > 0) ? $level : 'fulltest';
            $shareurl = '/speaking/'.$level.'/'.set_alias_link($row['name']).'-'.$row['test_id'].'.html';
            /// update
            $this->db->set("share_url",$shareurl);
            $this->db->where("test_id",$row['test_id']);
            $this->db->update('speaking_test');
        }
    }
    // update share_url writing detail
    public function shareurlwriting(){
        $query = $this->db->get('writing_test');
        $rows = $query->result_array();
        foreach ($rows as $row) {
            $level = get_code_by_id($row['level']);
            $level = ($row['type'] > 0) ? $level : 'fulltest';
            $shareurl = '/writing/'.$level.'/'.set_alias_link($row['name']).'-'.$row['test_id'].'.html';
            /// update
            $this->db->set("share_url",$shareurl);
            $this->db->where("test_id",$row['test_id']);
            $this->db->update('writing_test');
        }
    }
    // update news
    public function convertnews(){
        $query = $this->db->get('news');
        $rows = $query->result_array();
        foreach ($rows as $row) {
            $shareurl = '/'.set_alias_link($row['title'].'-nd'.$row['news_id']);
            $datacreate = convert_datetime($row['date_up']);
            // get original cate
            $this->db->select('t.cate_id,c.level');
            $this->db->where("news_id",$row['news_id']);
            $this->db->join("news_cate as c","c.cate_id = t.cate_id");
            $query = $this->db->get("news_to_cate as t");
            $r = $query->result_array();
            $luutam = -1;
            //var_dump('<pre>',$r);
            foreach ($r as $r) {
                if ($r['level'] > $luutam){
                    $luutam = $r['level'];
                    $original_cate = $r['cate_id'];
                }

            }
            /// update
            $this->db->set("share_url",$shareurl);
            $this->db->set("date_create",$datacreate);
            $this->db->set("original_cate",$original_cate);
            if ($row['images']){
                //$this->db->set("images", '/news_images/'.$row['images']);
            }
            $this->db->where("news_id",$row['news_id']);
            $this->db->update('news');
        }
    }
    // update cac anh trong bai viet
    public function updatedetailimage(){
        $query = $this->db->get("news");
        $rows = $query->result_array();

        foreach ($rows as $row){
            $detail = preg_replace('/src\s*=\s*"([^http].*)"/i','src="http://www.mshoatoeic.com/uploads/images/userfiles/$1"',$row['detail']);
            $this->db->set('detail',$detail);
            $this->db->where("news_id",$row['news_id']);
            $this->db->update('news');
        }
    } */
    public function repaircache($id){
        $id = (int) $id;
        if ($id <= 0) {
            return false;
        }
        $this->db->where("news_id",$id);
        $query = $this->db->get("news_bak");
        $row = $query->row_array();
        if ($row) {
            $this->db->set('detail',$row['detail']);
            $this->db->where("news_id",$row['news_id']);
            $this->db->update('news');
        }
        echo 'done '.$id;
    }
}