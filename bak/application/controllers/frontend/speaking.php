<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Speaking extends CI_Controller{
    public function __construct(){
		parent::__construct();
        $this->config->set_item("mod_access",array("name" => "speaking"));
	}

    public function finaltest($password = 0){
        $password = (int) $password;
        if ($password == 0) {
            $data['listclass'] = get_password();
            return $this->load->layout('speaking/finaltest0',$data,false,'layout_3c');
        }
        $checkpermission = $this->permission->check_permission_test($password);
        if ($checkpermission != true){
            return $checkpermission;
        }
        $limit = 10;
        $this->load->model("speaking_model","speaking");
        $this->load->model("writing_model","writing");
        $param = array("limit" => $limit, "page" => 1, 'password' => $password);
        $data['speaking'] = $this->speaking->test_lists($param);
        $data['writing'] = $this->writing->test_lists($param);
        $data['password'] = get_password($password);

        /// unset list //
        $this->session->unset_userdata("writing_test");
        $this->session->unset_userdata("speaking_test");
        $this->load->layout("speaking/test_password",$data,false,'layout_3c');

        //var_dump($this->session->userdata("test_password"));
    }
    public function lists ($testname = '',$level = ''){

        // get data
        $code = get_code_by_type();
        $seotitle = str_replace('-',' ',$testname).' - '.str_replace('-',' ',$level);
        $part = (int)array_search(strtolower($testname),$code);
        if ($part == 0 && ($testname != 'fulltest' || $level != '')){
            redirect('/speaking/fulltest.html','location','301');
        }
        $level = (int) get_level_by_code($level);
        if ($part > 0 && $level <= 0){
            redirect('/speaking/fulltest.html','location','301');
        }
        // xoa session dang lam bai
        $this->session->unset_userdata("speaking_test");
        $this->load->model("speaking_model","speaking");
        $page = (int)($this->input->get("page")) ? $this->input->get("page") : 1;
        $param = array( "type" => $part , "limit" => $limit, "page" => $page, "level" => $level);

        $data['rows'] = $this->speaking->test_lists($param);
        $data['seotitle'] = ($testname == 'fulltest' || $level == '') ? '' : $seotitle;
        /// meta ///
        $seotitle = ($testname == 'fulltest' || $level == '') ? 'Toeic Speaking, toeic Speaking online free, Ms Hoa TOEIC' : 'Luyen tap TOEIC Speaking - '. $seotitle;
        $seldesc = 'Toeic Speaking, toeic Speaking online'. ' - '. $this->config->item('metadesc');
        $this->load->setData('title',$seotitle);
        $this->load->setData('metakey',$seotitle);
        $this->load->setData('metades',$seldesc);

        $this->config->set_item("mod_access",array("name" => "speaking_list"));
        $this->config->set_item("menu_select",array('item_mod' => 'speaking_cate_'.$part, 'item_id' => $level));
        $this->load->layout("speaking/test_list",$data);
    }
    /**
     * @desc: Lay bai tap theo lop
     * @param: $part = fulltest / fullpart / 0,1,2,3,4
     */
    public function byclass($part = null){
        $part = $this->security->xss_clean($part);
        $limit = 100;
        $checkpermission = $this->permission->check_permission_class();
        if ($checkpermission != true){
            return $checkpermission;
        }
        $classid = $this->session->userdata("class_id");
        $page = (int)($this->input->get("page")) ? $this->input->get("page") : 1;
        /////////get data ///////
        $this->load->model("speaking_model","speaking");
        switch ($part) {
            case 'fulltest':
                $part = 0;
                $data['testtype'] = 'full';
            break;
            case 'fullpart':
            case is_numeric($part):
                $part = $part;
                $data['testtype'] = 'part';
            break;
            default:
                $part = null;
                return show_404();
        }
        $param = array( "type" => $part , "limit" => $limit, "page" => $page, "role" => $classid);
        /// unset list //
        $this->session->unset_userdata("speaking_test");
        /// meta ///
        $seotitle = 'Bài tập TOEIC Speaking - Ms Hoa TOEIC';
        $seldesc = 'Bài tập TOEIC Speaking'. ' - '. $this->config->item('metadesc');
        $this->load->setData('title',$seotitle);
        $this->load->setData('metakey',$seotitle);
        $this->load->setData('metades',$seldesc);
        /// role ///
        $this->config->set_item("mod_access",array("name" => "speaking_list"));
        $this->config->set_item("menu_select",array('item_mod' => 'speaking_class'.$part, 'item_id' => $part));

        $data['rows'] = $this->speaking->test_lists($param);
        $this->load->layout("speaking/byclass",$data);
    }
    public function answer($id,$timeanswer = 0,$user_id = 0){
        $id = (int) $id;
        if (!$this->session->userdata("userid")){
            $refer = base64_encode(current_url());
            redirect("/users/login?refer=".$refer);
        }
        $this->load->model("speaking_model","speaking");
        // kiem tra permission
        $permission = $this->session->userdata("permission");
        $data['permission'] = 0;
        if ($permission){
            $data['permission'] = ($this->permission->check_permission_backend('speaking',array('point','point_detail'))) ? 1 : 0;
        }

        // lay cau hoi ra
        // get cau hoi + cau tra loi
        $detail = $this->speaking->get_test_detail($id);
        $count = 0;
        for ($i = 1; $i <=11 ; $i++){
            if ($detail['question_'.$i] > 0){
                $question_id[] = $detail['question_'.$i];
                $data['question'][$detail['question_'.$i]] = $i;
                $count ++;
            }
        }
        // lay thong tin chi tiet cau hoi
        $data['detail'] = $detail;
        $data['rows'] = $this->speaking->get_answer($question_id);
        // lay bai tra loi gan nhat cua hoc vien
        $userid = ($data['permission'] == 1 && $user_id) ? $user_id : $this->session->userdata("userid");
        $param = array("user_id" => $userid,"limit" => $count,"test_id" => $id,"time" => $timeanswer);

        $user_answer = $this->speaking->get_answer_by_testid($param);
        foreach ($user_answer as $uwer){
            if (!$time){
                $time = $uwer['time'];
            }
            if ($time != $uwer['time']){
                break;
            }
            $useranswer[$uwer['speaking_id']] = $uwer;
        }
        $data['useranswer'] = $useranswer;
        // nap static
        $js = array(
            js('speaking.js'),
            js('jplayer/jquery.jplayer.min.js'),
            js('jquery.fancybox.pack.js'),
            link_tag('jquery.fancybox.css')
        );
        $this->load->setArray("script",$js);
        $this->load->layout("speaking/test_answer",$data);
    }
    public function detail ($id){
        if (!$this->session->userdata("userid")){
            $refer = base64_encode(current_url());
            redirect("users/login?refer=".$refer);
        }
        $id = (int) $id;
        $this->load->model("speaking_model","speaking");
        // lay cau hoi ra
        // get cau hoi''
        $param = $this->session->userdata("speaking_test");

        if (!$param || $param['detail']['test_id'] != $id || count($param['question']) < 1){ // neu co session va session do dung voi bai test hien tai
            $test = $this->speaking->get_test_detail($id);
            if (!$test){
                show_404();
            }
            for ($i = 1; $i <=11 ; $i++){
                if ($test['question_'.$i] > 0){
                    $arr['question_'.$i] = $test['question_'.$i];
                }
            }
            $param = array(
                'time'              => time(),
                'detail'            => $test,
                'question'          => $arr
            );
        }
        if ($param['detail']['isclass'] == 1){
            $checkpermission = $this->permission->check_permission_class();
            if ($checkpermission != true){
                return $checkpermission;
            }
        }
        if ($param['detail']['password'] > 0){
            $checkpermission = $this->permission->check_permission_test($param['detail']['password']);
            if ($checkpermission != true){
                return $checkpermission;
            }
        }
        $url = $this->uri->uri_string();
        if ($url != trim($param['detail']['share_url'],'/')){
            redirect($param['detail']['share_url'],'location','301');
        }

        $level = get_code_by_id($param['detail']['level']);
        $type = get_code_by_type ();
        $type = $type[$param['detail']['type']];
        /// truong hop bai tap nay thuoc lop
        if ($param['detail']['isclass'] == 1){
            $data['exitlink'] =  ($level && $type != 'fulltest') ? '/speaking/byclass/fullpart' : '/speaking/byclass/fulltest';
        }
        elseif ($param['detail']['password'] > 0) {
            $data['exitlink'] =  '/speaking/finaltest/'.$param['detail']['password'];
        }
        else{
            $data['exitlink'] =  ($level && $type != 'fulltest') ? '/speaking/'.$level.'/'.$type.'.html' : '/speaking/fulltest.html';
        }

        $data['ques'] = str_replace('question_','',array_shift(array_keys($param['question']))) ;
        $question_id = array_shift($param['question']);
        // neu con question trong session
        $this->session->set_userdata("speaking_test",$param);
        // lay thong tin chi tiet cau hoi
        $data['rows'] = $this->speaking->detail_question($question_id);
        $data['detail'] = $param;

        $seotitle = ($param['detail']['name']) ? $param['detail']['name'].' - Mshoa Toeic' : $this->config->item('metakey');
        $seldesc = $seotitle. ' - '. $this->config->item('metadesc');
        $this->load->setData('title',$seotitle);
        $this->load->setData('metakey',$seotitle);
        $this->load->setData('metades',$seldesc);

        $js = array(
            js('speaking.js'),
            js('jplayer/jquery.jplayer.min.js'),
            js('record/recorder.js'),
            js('jquery.plugin.min.js'),
            js('jquery.countdown.js'),
            link_tag('jquery.countdown.css'),
        );
        $this->load->setArray("script",$js);
        $this->load->layout("speaking/test_detail",$data);
    }
    public function sendrecord(){
        $session = $this->session->userdata("speaking_test");

        if (!$this->session->userdata('userid')){
            $result['error'] = 'Chưa đăng nhập';
            return $this->output->set_output(json_encode($result));

        }
        $speaking_id = (int)$this->input->post("speaking_id");
        if (empty($session['detail']) || $speaking_id <= 0){
            $result['error'] = 'Không tìm thấy thông tin bài test';
            return $this->output->set_output(json_encode($result));
        }
        $this->load->helper("images_helper");
        //upload bai ghi am
        $result = images_upload('recordfile',array('file_name' => $this->session->userdata("userid").'-'.time().rand(1111,9999)),'record');

        // luu vao database
        $this->load->model("speaking_model","speaking");

        $input = array(
            'speaking_id'   => $speaking_id,
            'user_id'       => (int)$this->session->userdata("userid"),
            'time'          => $session['time'],
            'test_id'       => (int)$session['detail']['test_id'],
            'sound'         => $result['success']
        );

        $this->speaking->insert_user_answer($input);
        // neu het cau hoi se load bai test khac
        if (count($session['question']) <= 0){
            $param = array('test_id' => $session['detail']['test_id'], "type" => $session['detail']['type']);
            $row = $this->speaking->get_next_test($param);
            if ($row['test_id'] > 0){
                $result['next_test'] = (int)$row['test_id'];
            }
            else{
                $result['next_test'] = -1;
            }
        }

        $this->output->set_output(json_encode($result));
    }
}