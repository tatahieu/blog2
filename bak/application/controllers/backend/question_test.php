<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question_test extends CI_Controller{
	private $error = array();
    function __construct(){
		parent::__construct();
		$this->lang->load('backend/question');
		$this->load->setData('title',$this->lang->line('qtest_title'));
	}
	public function index(){
        $limit = 20;
		$this->load->model('question_test_model','ques');
		if ($this->input->post('delete') && $this->input->post('cid'))
		{
            $row = $this->ques->delete();
			if ($row === false){
			     $data['error'] = 'Phải xóa các câu hỏi của phần test này trước';
			}
            if (!empty($row)){
				$this->load->helper('images');
                foreach ($row as $row){
                    if ($row['sound'] != ''){
                        files_delete('sound/'.$row['sound']);
                    }
				}
			}

		}
        $this->load->helper('form');
        /** FILTER **/
        $this->option[0] = $this->lang->line('ques_cate');
        $filter['data'][] = form_dropdown('cate',$this->category(),$this->input->get('cate'));
        $filter['data'][] = $this->lang->line('qtest_name').form_input('name',$this->input->get('name'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
		$data['row'] = $this->ques->lists($limit);
		/** PAGING **/
        $data['parent'] = ($this->input->get("parent")) ? 1 : 0;
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
		$this->load->layout('question/test_list',$data);
	}
	public function add(){
		$this->load->model('question_test_model','ques');

		$parent = ($this->input->get("parent")) ? $this->input->get("parent") : 0;
        if ($this->input->post('question_test_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
			    $this->load->helper('images');
                $sound = images_upload('sound',array(),'audio');
                $this->ques->insert($sound['success']);
                $this->session->set_userdata('success','add');
                if ($parent > 0){
                    redirect(site_url('question_test/add').'/?parent='.$parent);
                }
			    else{
                    redirect('question_test/add');
			    }
			}
		}

		$row = set_array('title','description','part',array('cate_id'=>0,'publish'=>1,'role' => array(0)));
		$data = $this->_form($row);
        $data['parent'] = $parent;
        $data['qselected'] = array();
		$this->load->layout('question/test_form',$data);
	}
    public function edit($id = 0){
        if ($id <= 0 OR !is_numeric($id)){
			show_404();
		}
		$this->load->model('question_test_model','ques');
		$row = $this->ques->detail(intval($id));
		if ($this->input->post('question_test_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
                if (isset($_FILES['sound'])){
                    $this->load->helper('images');
                    $sound = images_upload('sound',array(),'audio');
                    if ($sound != '' AND $row['sound'] != ''){
                        images_delete($row['sound'],'audio');
                    }
                }else{
                    $sound = '';
                }

				$this->ques->update($id,$sound['success']);
                if ($row['parent'] > 0){
                    redirect(site_url('question_test').'/?parent='.$row['parent']);
                }
				else{
				    redirect('question_test');
				}
			}
		}
        $this->load->model('group_model','group');
        $role = $this->group->get_group_by_test(intval($id));
        if (!empty($role)){
            foreach ($role as $role){
                $row['role'][] = $role['role_id'];
            }
        }
        else{
            $row['role'] = array(0);
        }
		if (empty($row)){
			show_404();
		}
		$data = $this->_form($row);
        $data['parent'] = ($row['parent'] > 0) ? $row['parent'] : 0;
        $data['qselected'] = $this->ques->get_question_selected($id);
		$this->load->layout('question/test_form',$data);
	}
    public function autocomplete(){
        $word = $this->input->get('word');
        if ($word == ""){
            return false;
        }
        $this->load->model('question_test_model','qtest');
        $row = $this->qtest->get_question($word,10);
        $out = json_encode($row);
        $this->output->set_output($out);
    }
	private function _select_category($data,$parent = 0,$level = ''){
		if (!empty($data[$parent])){
			foreach ($data[$parent] as $rows){
			$this->option[$rows['cate_id']] = $level.' '.$rows['name'];
			$this->_select_category($data,$rows['cate_id'],$level.'---');
			}
		}
	}
    public function category(){
        $this->load->model('question_cate_model','cate');
        $cate = $this->cate->lists();
		$select = array();
		foreach ($cate as $cate){
			$select[$cate['parent']][] = $cate;
		}
		$this->_select_category($select);
        return $this->option;
    }
	private function _form($row = array()){

        $static[] = js('jquery.autocomplete.js');
        $static[] = link_tag('jquery.autocomplete.css');
        $this->load->setArray("footer",$static);
		$this->load->helper('form');
		$category = $this->category();
        $this->load->model("group_model",'group');
        $r = $this->group->lists();
        $role[0] = 'Tất cả quyền hạn';
        foreach ($r as $r){
            $role[$r['id']] = $r['name'];
        }
        $roleselect = $row['role'];
        $data['role'] = form_multiselect('role_id[]',$role,$roleselect);
		$data['title'] = form_input('title',set_value('title',$row['title']));
		$data['category'] = form_dropdown('category',$category,$row['cate_id']);
        $data['part'] = form_dropdown('part',array(1=>'1 - photo',2 => '2 - Question-Response',3 => '3 - Conversation',4 => '4 - Talk',5 => '5 - Incomplete Sentences',6 => '6 - Text Completion',7 => '7 - Reading Comprehension 1', 8 => '8 - Writing'),$row['part']);
        $data['publish'] = form_checkbox('publish',1,$row['publish']);
        $data['question'] = form_input('question[]',set_value("question[]"),'class="autoc"');
        $data['sound'] = form_upload('sound','','size="50"');
        $data['description'] = form_detail('description',$row['description'],array("height"=>"200"));
		$data['submit'] = form_submit('question_test_submit',$this->lang->line('common_save'));
		return $data;
	}
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'title',
                 'label'   => $this->lang->line('ques_name'),
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}
}