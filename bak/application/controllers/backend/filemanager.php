<?php
class Filemanager extends CI_Controller {
	private $error = array();
	private $directory = '';
    private $uploadurl = '';
    private $type = '';
    private $userimage = 'userfiles/';
    public function __construct(){
	   parent::__construct();
       $this->lang->load('backend/filemanager');
       switch($this->input->get('type')){
            case 'file':
            case 'url':
            $this->directory = $this->config->item('path_upload').'files/';
            $this->uploadurl = UPLOAD_URL.'files/';
            $this->type = 'file';
            break;
            case 'flash':
            $this->directory = $this->config->item('path_upload').'flash/';
            $this->uploadurl = UPLOAD_URL.'files/';
            $this->type = 'flash';
            break;
            default:
            $this->directory = $this->config->item('path_upload').'images/'.$this->userimage;
            $this->uploadurl = UPLOAD_URL.'images/'.$this->userimage;
            $this->type = 'image';
		}

	}
	public function index() {
		$data['theme'] = $this->config->item('img').'filemanager/';
		if ($this->input->get('field')) {
			$data['field'] = $this->input->get('field');
		} else {
			$data['field'] = '';
		}
        $data['directory'] = $this->uploadurl;
        $data['type'] = $this->type;
        $fck = $this->input->get('CKEditorFuncNum');
        $data['formname'] = $this->input->get("CKEditor");
		if (is_string($fck)) {
			$data['fckeditor'] = $fck;
		} else {
			$data['fckeditor'] = false;
		}
		$this->load->view('common/filemanager',$data,FALSE);
	}
	public function directory() {
		$json = array();
		$this->load->helper('directory');
        $folder = $this->input->post('directory');
		if ($folder !== false) {
            if ($folder != ''){
                $folder = rtrim(str_replace('../', '', $folder),'/').'/';
            }
            $dir = rtrim($this->directory.$folder);
			$directories = directory_map($dir ,1,false,TRUE);
			if (!empty($directories)) {
                $i = 0;
				foreach ($directories as $directory) {
				    $realpath = $dir. rtrim($directory, '/');
                    if (is_dir($realpath)){
    					$json[$i]['data'] = basename($directory);
    					$json[$i]['attributes']['directory'] = $folder.$directory;
    					$children = directory_map($realpath,1,false,TRUE);
    					if ($children)  {
    						$json[$i]['children'] = ' ';
    					}
    					$i++;
                    }
				}
			}
		}

		$this->output->set_output(json_encode($json));
	}
    public function sortfile($a, $b){
        return $b['date'] - $a['date']; // moi den cu
    }
	public function files() {
		$json = array();
        $dir = str_replace('../', '', $this->input->post('directory'));
		if ($dir) {
			$directory = $this->directory . $dir;
		} else {
			$directory = $this->directory;
		}
        $path_icon = $this->config->item('theme').'images/filemanager/icon/';
		$type = $this->input->get('type');
        $conf = $this->config->item("uploadconf");

        switch ($type) {
            case 'flash':
            $allowed = $conf['flash']['allowed_types'];
            break;
            case 'file':
		    $allowed = $conf['files']['allowed_types'];
            break;
            default:
            $allowed = $conf['images']['allowed_types'];
		}
        $allowed = explode('|',$allowed);
        $this->load->helper('file');
        $files = get_dir_file_info($directory,TRUE);
        usort($files, array('Filemanager','sortfile'));
//        echo $this->directory;
//        echo $directory;
//         var_dump($files); die;
		//$files = glob(rtrim($directory, '/') . '/*');

		if ($files) {
            $j = 0;
			foreach ($files as $data) {
                $file = $data['name'];
				if (is_file($data['server_path'])) {
					$ext = end(explode(".",$file));
				} else {
					$ext = '';
				}
				if (in_array(strtolower($ext), $allowed) && strpos($file,'thumb_') === FALSE) {
					$size = $data['size'];

					$i = 0;

					$suffix = array(
						'B',
						'KB',
						'MB',
						'GB',
						'TB',
						'PB',
						'EB',
						'ZB',
						'YB'
					);

					while (($size / 1024) > 1) {
						$size = $size / 1024;
						$i++;
					}
                    $filepath = ($dir) ? $dir.'/'.$file : $file;
					$json[$j] = array(
						'file'     => $filepath,
						'filename' => basename($file),
						'size'     => round(substr($size, 0, strpos($size, '.') + 4), 2) . $suffix[$i],
					);
                    if ($type == 'image'){
                        $json[$j]['thumb'] = getimglink($this->userimage.$filepath,'size1');
                    }
                    else{
                        $json[$j]['thumb'] = $path_icon.str_replace('.','',$ext).'.png';
                    }
                    $j ++;
				}
			}
		}

		$this->output->set_output(json_encode($json));
	}

	public function create() {
		$json = array();
		$folder = $this->input->post('directory');
		if ($folder !== FALSE) {
            $name = $this->input->post('name');
			$name = str_replace(array('\'','"',' ','../'),'',$name);
            if (!preg_match('#([a-zA-Z0-9]+)#is', $name))
            {
                $json['error'] = $this->lang->line('fmag_error_valid_name');
            }
            if ($name != '') {
				$directory = rtrim($this->directory . str_replace('../', '', $folder), '/');

				if (!is_dir($directory)) {
					$json['error'] = $this->lang->line('fmag_error_directory');
				}
				$directory = $directory.'/'.$name;
				if (file_exists($directory)) {
					$json['error'] = $this->lang->line('fmag_error_exists');
				}
			} else {
				$json['error'] = $this->lang->line('fmag_error_name');
			}
		} else {
			$json['error'] = $this->lang->line('fmag_error_directory');
		}
		if (!isset($json['error'])) {
			mkdir($directory  , 0755);

			$json['success'] = $this->lang->line('fmag_text_create');
		}

		$this->output->set_output(json_encode($json));
	}

	public function delete() {
		$json = array();
        $path = $this->input->post('path');
        $path = explode(',',$path);
		if (!empty($path)) {
            $i = 0;
            foreach ($path as $p){
                $pp = str_replace('../', '', html_entity_decode($p, ENT_QUOTES, 'UTF-8'));
    			$pth = rtrim($this->directory . $pp, '/');
    			$realp[$i]['realpath'] = $pth;
                $realp[$i]['path'] = $pp;
                if (!file_exists($pth)) {
    				$json['error'] = $this->lang->line('fmag_error_select'); break;
    			}
    			if ($pth == rtrim($this->directory, '/')) {
    				$json['error'] = $this->lang->line('fmag_error_delete'); break;
    			}
                $i ++;
            }
		} else {
			$json['error'] = $this->lang->line('fmag_error_select');
		}
		if (!isset($json['error'])) {
            $this->load->helper("images");
            foreach ($realp as $realp){

                if (is_file($realp['realpath'])) {
                    $realpath = ($this->type == 'image') ? $this->userimage.$realp['path'] : $realp['path'];
				    images_delete($realpath,$this->type);
        		} elseif (is_dir($realp['realpath'])) {
        			$this->recursiveDelete($realp['realpath']);
        		}
            }
			$json['success'] = $this->lang->line('fmag_text_delete');
		}

		$this->output->set_output(json_encode($json));
	}

	protected function recursiveDelete($directory) {
		if (is_dir($directory)) {
			$handle = opendir($directory);
		}

		if (!$handle) {
			return false;
		}

		while (false !== ($file = readdir($handle))) {
			if ($file != '.' && $file != '..') {
				if (!is_dir($directory . '/' . $file)) {
					unlink($directory . '/' . $file);
				} else {
					$this->recursiveDelete($directory . '/' . $file);
				}
			}
		}

		closedir($handle);

		rmdir($directory);

		return true;
	}

	public function move() {
		$this->load->language('backend/filemanager');

		$json = array();
		$from = $this->input->post('from');
        $to = $this->input->post('to');
		if ($from != '') {
		    $from = explode(',',$from);
            foreach ($from as $from){
                $fr = ltrim($from,"/");
                if (strpos($to,$from) !== FALSE){
                    $json['error'] = $this->lang->line('fmag_access_denied_move'); break;
                }
    			$fr = rtrim(str_replace('../', '', $fr), '/');
                $rootfile[] = $fr;
                $fr = $this->directory . $fr;
    			if (!file_exists($fr)) {
    				$json['error'] = $this->lang->line('fmag_error_missing'); break;
    			}
    			if ($fr.'/' == $this->directory) {
    				$json['error'] = $this->lang->line('fmag_error_default'); break;
    			}
                if (file_exists($to . '/' . basename($fr))) {
				    $json['error'] = $this->lang->line('fmag_error_exists'); break;
                }
                $fromarr[] = $fr;

            }
			$to = rtrim($this->directory . str_replace('../', '', $to), '/');

			if (!file_exists($to)) {
				$json['error'] = $this->lang->line('fmag_error_move');
			}


		} else {
			$json['error'] = $this->lang->line('fmag_error_directory');
		}
		if (!isset($json['error'])) {
		    foreach ($fromarr as $fromarr){
                rename($fromarr, $to . '/' . basename($fromarr));
		    }
			// xoa cac file thumb
            $this->load->helper("images");
            foreach ($rootfile as $rootfile){
                images_delete($this->userimage.$rootfile);
            }

			$json['success'] = $this->lang->line('fmag_text_move');
		}

		$this->output->set_output(json_encode($json));
	}

	public function copy() {
		$json = array();
		$path = $this->input->post('path');
		$name = $this->input->post('name');

        if ($path !== false AND $name !== false) {
			$name = str_replace(array('\'','"',' ','../'),'',$name);
            if (!preg_match('#([a-zA-Z0-9]+)#is', $name))
            {
                $json['error'] = $this->lang->line('fmag_error_valid_name');
            }
            if ((strlen($name) < 3) || (strlen($name) > 255)) {
				$json['error'] = $this->lang->line('fmag_error_filename');
			}


			$old_name = rtrim($this->directory . str_replace('../', '', html_entity_decode($path, ENT_QUOTES, 'UTF-8')), '/');

			if (!file_exists($old_name) || $old_name == $this->directory) {
				$json['error'] = $this->lang->line('fmag_error_copy');
			}

			if (is_file($old_name)) {
				$ext = strrchr($old_name, '.');
			} else {
				$ext = '';
			}

			$new_name = dirname($old_name) . '/' . str_replace('../', '', html_entity_decode($name, ENT_QUOTES, 'UTF-8') . $ext);

			if (file_exists($new_name)) {
				$json['error'] = $this->lang->line('fmag_error_exists');
			}
		} else {
			$json['error'] = $this->lang->line('fmag_error_select');
		}
		if (!isset($json['error'])) {
			if (is_file($old_name)) {
				copy($old_name, $new_name);
			} else {
				$this->recursiveCopy($old_name, $new_name);
			}

			$json['success'] = $this->lang->line('fmag_text_copy');
		}

		$this->output->set_output(json_encode($json));
	}

	function recursiveCopy($source, $destination) {
		$directory = opendir($source);

		@mkdir($destination);

		while (false !== ($file = readdir($directory))) {
			if (($file != '.') && ($file != '..')) {
				if (is_dir($source . '/' . $file)) {
					$this->recursiveCopy($source . '/' . $file, $destination . '/' . $file);
				} else {
					copy($source . '/' . $file, $destination . '/' . $file);
				}
			}
		}

		closedir($directory);
	}

	public function folders() {
        $this->load->helper('directory');
        $out = '<option value="">'.$this->lang->line('fmag_root_folder').'</option>';
        $dir = directory_map($this->directory,0,false,true);
        $out .= $this->recursiveFolders($dir);
        $this->output->set_output($out);
	}

	protected function recursiveFolders($dir,$level = '',$level_val = '') {
        $out = '';
        foreach ($dir as $key => $val){
            if (is_array($val)){
                $out .= '<option value="'.$level.$key.'">'.$level_val.$key.'</option>';
                if (!empty($val))
                $out .= $this->recursiveFolders($val,$level.$key.'/',$level_val.'--');
            }
		}
        return $out;
	}

	public function rename() {
		$json = array();
        $path = $this->input->post('path');
        $name = $this->input->post('name');
        $name = str_replace(array('\'','"',' ','../'),'',$name);
        if (!preg_match('#([a-zA-Z0-9]+)#is', $name) || strpos($name,"thumb_") !== FALSE)
        {
            $json['error'] = $this->lang->line('fmag_error_valid_name');
        }
		if ($path !== false AND $name !== false) {
			if (strlen($name) > 100){
                $json['error'] = $this->lang->line('fmag_error_rename');
			}
			$thumb = rtrim( str_replace('../', '', html_entity_decode($path, ENT_QUOTES, 'UTF-8')), '/');
			$old_name = $this->directory.$thumb;
			if (!file_exists($old_name) || $old_name == $this->directory) {
				$json['error'] = $this->lang->line('fmag_error_name');
			}

			if (is_file($old_name)) {
				$ext = strrchr($old_name, '.');
			} else {
				$ext = '';
			}

			$new_name = dirname($old_name) . '/' . str_replace('../', '', html_entity_decode($this->input->post('name'), ENT_QUOTES, 'UTF-8') . $ext);

			if (file_exists($new_name)) {
				$json['error'] = $this->lang->line('fmag_error_exists');
			}
		}

		if (!isset($json['error'])) {
			rename($old_name, $new_name);
			$this->load->helper("images");
            images_delete($this->userimage.$thumb);
			$json['success'] = $this->lang->line('fmag_text_rename');
		}

		$this->output->set_output(json_encode($json));
	}

	public function upload() {

		$json = array();
		$dir = $this->input->post('directory');
        $type = $this->input->get('type');
		if ($dir !== false) {
		    $conf = $this->config->item("uploadconf");
            switch ($type) {
                case 'url':
                case 'file':
                    $config = $conf['files'];
                    break;
                case 'flash':
                    $config = $conf['flash'];
                    break;
                default:
                    $config = $conf['images'];
            }
            $config['upload_path'] = $this->directory.$dir;
            unset($config['file_name']);
    		$this->load->library('upload', $config);
            if ( ! $this->upload->do_multi_upload('image'))
    		{
    			$json['error'] = strip_tags($this->upload->display_errors());
    		}
		} else {
			$json['error'] = $this->lang->line('fmag_error_directory');
		}
		if (!isset($json['error'])) {
			$json['success'] = $this->lang->line('fmag_text_uploaded');
		}
		$this->output->set_output(json_encode($json));
	}
}
?>