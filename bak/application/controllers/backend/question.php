<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question extends CI_Controller{
    private $images_path = 'uploads/question_images/';
	private $error = array();
    function __construct(){
		parent::__construct();

		$this->lang->load('backend/question');
		$this->load->setData('title',$this->lang->line('ques_title'));
	}
	public function index(){
        $limit = 20;
		$this->load->model('question_model','ques');
		if ($this->input->post('delete') && $this->input->post('cid'))
		{
			$row = $this->ques->delete();
			if (!empty($row)){
				$this->load->helper('images');
                foreach ($row as $row){
					if ($row['images'] != ''){
                        images_delete($row['images']);
                    }
                    if ($row['sound'] != ''){
                        images_delete($row['sound'],'audio');
                    }
				}
			}
		}
        $this->load->helper('form');
        /** FILTER **/
        $this->option[0] = $this->lang->line('ques_cate');
        $filter['data'][] = form_dropdown('cate',$this->category(),$this->input->get('cate'));
        $filter['data'][] = $this->lang->line('ques_name').form_input('name',$this->input->get('name'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
		$data['row'] = $this->ques->lists($limit);
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
		$this->load->layout('question/list',$data);
	}
	public function add(){
		$this->load->model('question_model','ques');
		if ($this->input->post('question_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
                $this->load->helper('images');
                $img = images_upload('images');
                $sound = images_upload('sound',array(),'audio');
                $this->ques->insert($img['success'],$sound['success']);
                $this->session->set_userdata('success','add');
			    redirect('question/add');
			}
		}
		$row = set_array('title','detail','images','sound',array('publish' => 1,'cate_id'=>0,'answer'=> array(), 'date_up' => date('Y-m-d H:i:s')));
		$data = $this->_form($row);
		$this->load->layout('question/form',$data);
	}
    public function edit($id = 0){
        if ($id <= 0 OR !is_numeric($id)){
			show_404();
		}
		$this->load->model('question_model','ques');
		$row = $this->ques->detail(intval($id));
		if ($this->input->post('question_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
                $this->load->helper('images');
                if ($_FILES['images']){
                    $img = images_upload('images');
                    if ($img['success'] != '' AND $row['images'] != ''){
                        images_delete($row['images']);
                    }

				}
                if ($_FILES['sound']){
                    $sound = images_upload('sound',array(),'audio');
                    if ($sound['success'] != '' AND $row['sound'] != ''){
                        images_delete($row['sound'],'audio');
                    }
                }
				$this->ques->update(intval($id),$img['success'],$sound['success']);
				redirect('question');
			}
		}
		$cate = $this->ques->get_cate(intval($id));
		$row['cate_id'] = array();
		if (!empty($cate)){
			foreach ($cate as $cate){
				$row['cate_id'][] = $cate['cate_id'];
			}
		}
		if (empty($row)){
			show_404();
		}
        $row['answer'] = $this->ques->get_answer((int)$id);
		$data = $this->_form($row);
		$this->load->layout('question/form',$data);
	}
	private function _select_category($data,$parent = 0,$level = ''){
		if (!empty($data[$parent])){
			foreach ($data[$parent] as $rows){
			$this->option[$rows['cate_id']] = $level.' '.$rows['name'];
			$this->_select_category($data,$rows['cate_id'],$level.'---');
			}
		}
	}
    public function category(){
        $this->load->model('question_cate_model','cate');
        $cate = $this->cate->lists();
		$select = array();
		foreach ($cate as $cate){
			$select[$cate['parent']][] = $cate;
		}
		$this->_select_category($select);
        return $this->option;
    }
	private function _form($row = array()){
		$this->load->helper('form');
		$category = $this->category();
        $data['error_upload'] = $this->error;
		$data['title'] = form_textarea('title',set_value('title',$row['title']));
		$data['category'] = form_multiselect('category[]',$category,$row['cate_id'],'size="7" class="select_cate"');
        if (!empty($row['answer'])){
            $i = 1;
            foreach ($row['answer'] as $answer){
                $data['answer'][$i] = form_input('answer_'.$i,set_value('answer_'.$i,$answer['content']));
                $data['correct'][$i] = form_checkbox('correct_'.$i,1,$answer['correct']);
                $i++;
            }
        }
        else{
            $i = 1;
        }
        for ($j = 4; $j >= $i; $j --){
            $data['answer'][$j] = form_input('answer_'.$j,set_value('answer_'.$i,null));
            $data['correct'][$j] = form_checkbox('correct_'.$j,1,null);
        }
        $data['date_up'] = form_input('date_up',set_value('date_up',$row['date_up']));
		$data['image'] = form_upload('images','','size="50"');
		$data['sound'] = form_upload('sound','','size="50"');
        $data['publish'] = form_checkbox('publish',1,$row['publish']);
        $data['detail'] = form_detail('detail',$row['detail']);
		$data['submit'] = form_submit('question_submit',$this->lang->line('common_save'));
		return $data;
	}
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'title',
                 'label'   => $this->lang->line('ques_name'),
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}


    public function cate_index(){
		$this->load->model('question_cate_model','cate');
		if ($this->input->post('delete') && $this->input->post('cid'))
		{
			if ($this->cate->delete() === false){
				$data['error'] = $this->lang->line('ques_cate_delete_parent');
			}
		}
		$row = $this->cate->lists();
		$data['row'] = array();
		if (!empty($row)){
			foreach ($row as $row){
				$data['row'][$row['parent']][] = $row;
			}
		}
		$this->load->layout('question/cate_list',$data);
	}
	public function cate_add(){
		$this->load->model('question_cate_model','cate');
		if ($this->input->post('cate_submit')){
			$this->_cate_validation();
			if ($this->form_validation->run() == true)
			{
				$this->cate->insert();
                $this->session->set_userdata('success','add');
				redirect('question_cate/add');
			}
		}
		$row = set_array('name','relate_news','description',array('ordering' => 0,'parent' => 0,'cate_id'=>0));
		$data = $this->_cate_form($row);
		$this->load->layout('question/cate_form',$data);
	}
	public function cate_edit($id){
		$this->load->model('question_cate_model','cate');
		if ($this->input->post('cate_submit')){
			$this->_cate_validation();
			if ($this->form_validation->run() == true)
			{
				$this->cate->update(intval($id));
				redirect('question_cate');
			}
		}
		$row = $this->cate->detail(intval($id));
		if (empty($row)){
			show_404();
		}
		$data = $this->_cate_form($row);
		$this->load->layout('question/cate_form',$data);
	}
	private function _cate_select_parent($data,$parent = 0,$level = ''){
		if (!empty($data[$parent])){
			foreach ($data[$parent] as $rows){
			$this->option[$rows['cate_id']] = $level.' '.$rows['name'];
			$this->_cate_select_parent($data,$rows['cate_id'],$level.'---');
			}
		}
	}
    public function cate_parent($choose = 0){
        $row_parent = $this->cate->lists();
		$select = array();
		$this->option[0] = $this->lang->line('ques_cate_parent');
		foreach ($row_parent as $row_parent){
			if ($row_parent['cate_id'] != $choose)
			$select[$row_parent['parent']][] = $row_parent;
		}
		$this->_cate_select_parent($select);
        return $this->option;
    }
	private function _cate_form($row = array()){
		$parent = $this->cate_parent($row['cate_id']);
		$this->load->helper ('form');
		$data['cate_name'] = form_input('name',set_value('name',$row['name']));
        $data['relate_news'] = form_input('relate_news',set_value('relate_news',$row['relate_news']));
		$data['cate_ordering'] = form_input('ordering',set_value('ordering',$row['ordering']));
        $data['cate_description'] = form_textarea('description',set_value('description',$row['description']));
		$data['cate_parent'] = form_dropdown('parent',$parent,$row['parent'],set_select('parent',$row['parent']));
		$data['cate_submit'] = form_submit('cate_submit',$this->lang->line('common_save'));
		return $data;
	}
	// ok
	private function _cate_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => $this->lang->line('quest_cate_name'),
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'ordering',
                 'label'   => $this->lang->line('quest_cate_ordering'),
                 'rules'   => 'required|integer'
              ),
		);
  		$this->form_validation->set_rules($valid);
	}
    ////////////////////////////////////// QUESTION FULL/////////////////
    public function full_score($test_id) {
    	$this->load->model('question_full_model','qfull');
    	if ($this->input->post('export') == 1) {
    		$rows = $this->qfull->full_score(array('test_id' => $test_id,'limit' => 500, 'offset' => $offset));
    		$this->load->library('PHPExcel');
            $objPHPExcel = new PHPExcel();
            $i = 1; 
            $baseRow = 2;
            foreach($rows as $key => $row){   
                $count = $baseRow + $key;
                if($i == 1){
                    $objPHPExcel->getActiveSheet(0)
                    ->setCellValue('A'.$i, "Họ tên")
                    ->setCellValue('B'.$i, "Email")
                    ->setCellValue('C'.$i, "Điện thoại")
                    ->setCellValue('D'.$i, "Ngày sinh")
                    ->setCellValue('E'.$i, "Điểm read")
                    ->setCellValue('F'.$i, "Điểm list")
                    ->setCellValue('G'.$i, "Read đúng")
                    ->setCellValue('H'.$i, "List đúng")
                    ->setCellValue('I'.$i, "Tổng điểm")
                    ->setCellValue('J'.$i, "Thời gian bắt đầu")
                    ->setCellValue('K'.$i, "Thời gian kết thúc");
                }
                $objPHPExcel->getActiveSheet()->insertNewRowBefore($count,1);
                $objPHPExcel->getActiveSheet(0)
                    ->setCellValue('A'.$count, $row['fullname'])
                    ->setCellValue('B'.$count, $row['email']) 
                    ->setCellValue('C'.$count, $row['phone'])
                    ->setCellValue('D'.$count, $row['birthday'])
                    ->setCellValue('E'.$count, $row['read_score'])
                    ->setCellValue('F'.$count, $row['list_score'])
                    ->setCellValue('G'.$count, $row['list_correct'])
                    ->setCellValue('H'.$count, $row['read_correct'])
                    ->setCellValue('I'.$count, $row['total_score'])
                    ->setCellValue('J'.$count, date('H:i:s d/m/Y',$row['create_time']))
                    ->setCellValue('K'.$count, date('H:i:s d/m/Y',$row['update_time']));
                $i++;
            }
            
            $objPHPExcel->getActiveSheet()->setTitle('Test-Score-'.date('d-m-Y'));
            $objPHPExcel->setActiveSheetIndex(0);
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $filename = 'Test-Score-'.date('d-m-Y').'.xlsx';
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachement; filename="' . $filename . '"');
            $objWriter->save('php://output');
    	}
    	$limit = 20;
    	$page = ($page = $this->input->get('page') < 1) ? 1 : $page;
    	$offset = ($page - 1) * $limit;
		
		$data['row'] = $this->qfull->full_score(array('test_id' => $test_id,'limit' => $limit + 1, 'offset' => $offset));
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
		///////////////// FILTER ///////
		$this->load->helper('form');
        $this->option[0] = $this->lang->line('news_cate');
        //$filter['data'][] = form_dropdown('cate',$this->category(),$this->input->get('cate'));
        $filter['data'][] = 'Email: '.form_input('email',$this->input->get('email'));
        $filter['data'][] = 'Từ ngày: '.form_input('from_date',$this->input->get('from_date'),'id="filter_from_date"');
        $filter['data'][] = 'Đến ngày: '.form_input('to_date',$this->input->get('to_date'),'id="filter_to_date"');
        ///////////// STATIC ///////////
        $static[] = js('jquery-ui-1.10.0.custom.min.js');
        $static[] = link_tag('jquery-ui.css');
        $this->load->setArray("footer",$static);


        $data['filter'] = $this->load->view("common/data_filter",$filter);
        ///////////////////////////////////////////
		$this->load->layout('question/full_score',$data);
    }
    public function full_index(){
	    $limit = 20;
		$this->load->model('question_full_model','qfull');
		if ($this->input->post('delete') && $this->input->post('cid'))
		{
			$this->qfull->delete();
		}
        $data['show_type'] = $this->type;
		$data['row'] = $this->qfull->lists($limit);
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
		$this->load->layout('question/full_list',$data);
	}
	public function full_add(){
		$this->load->model('question_full_model','qfull');
		if ($this->input->post('qfull_submit')){
			$this->_full_validation();
			if ($this->form_validation->run() == true)
			{
				$this->qfull->insert();
                $this->session->set_userdata('success','add');
				redirect('question_full/add');
			}
		}
		$row = set_array('name','part1','part2','part3','type','part4','part5','part6','part7',array('time' => 120,'role' => array(0)));
		$data = $this->_full_form($row);
		$this->load->layout('question/full_form',$data);
	}
	public function full_edit($id){
		$this->load->model('question_full_model','qfull');
		if ($this->input->post('qfull_submit')){
			$this->_full_validation();
			if ($this->form_validation->run() == true)
			{
				$this->qfull->update(intval($id));
				redirect('question_full');
			}
		}
        $row = $this->qfull->detail(intval($id));
        $this->load->model('group_model','group');
		$role = $this->group->get_group_by_full(intval($id));
        if (!empty($role)){
            foreach ($role as $role){
                $row['role'][] = $role['role_id'];
            }
        }
        else{
            $row['role'] = array(0);
        }
		if (empty($row)){
			show_404();
		}
		$data = $this->_full_form($row);
		$this->load->layout('question/full_form',$data);
	}
	private function _full_form($row = array()){
	    $type = array("1" => 'Full',"2" => 'Mini',"3" => "Skill");
		$this->load->helper ('form');
        $this->load->model("group_model",'group');
        $r = $this->group->lists();
        $role[0] = 'Tất cả quyền hạn';
        foreach ($r as $r){
            $role[$r['id']] = $r['name'];
        }
        $roleselect = $row['role'];
		$data['name'] = form_input('name',set_value('name',$row['name']));
        $data['type'] = form_dropdown('type',$type,$row['type']);
        $data['part1'] = form_input('part1',set_value('part1',$row['part1']));
        $data['part2'] = form_input('part2',set_value('part2',$row['part2']));
        $data['part3'] = form_input('part3',set_value('part3',$row['part3']));
        $data['part4'] = form_input('part4',set_value('part4',$row['part4']));
        $data['part5'] = form_input('part5',set_value('part5',$row['part5']));
        $data['part6'] = form_input('part6',set_value('part6',$row['part6']));
        $data['part7'] = form_input('part7',set_value('part7',$row['part7']));
        $data['role'] = form_multiselect('role_id[]',$role,$roleselect);
        $data['time'] = form_input("time",set_value('time',$row['time']));
        $data['show_answer'] = form_checkbox('show_answer', 1, $row['show_answer']);
		$data['cate_submit'] = form_submit('qfull_submit',$this->lang->line('common_save'));
		return $data;
	}
	// ok
	private function _full_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => $this->lang->line('quest_cate_name'),
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}
}