<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Group extends CI_Controller{
	private $option = array();
	function __construct(){
		parent::__construct();
		$this->load->setData('title','Quản lý lớp học');
	}
	public function index(){
		$this->load->model('group_model','group');
		if ($this->input->post('delete') && $this->input->post('cid'))
		{
			if ($this->group->delete() === false){
				$data['error'] = 'Lỗi trong quá trình xóa';
			}
		}
		$data['row'] = $this->group->lists();

		$this->load->layout('group/list',$data);
	}
	public function add(){
		$this->load->model('group_model','group');
		if ($this->input->post('group_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->group->insert();
                $this->session->set_userdata('success','add');
				redirect('group/add');
			}
		}
		$row = set_array('name','username','password');
		$data = $this->_form($row);
		$this->load->layout('group/form',$data);
	}
	public function edit($id){
		$this->load->model('group_model','group');
		if ($this->input->post('group_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->group->update(intval($id));
				redirect('group');
			}
		}
		$row = $this->group->detail(intval($id));
		if (empty($row)){
			show_404();
		}
		$data = $this->_form($row);
		$this->load->layout('group/form',$data);
	}
	private function _form($row = array()){
		$this->load->helper ('form');
		$data['name'] = form_input('name',set_value('name',$row['name']));
		$data['username'] = form_input('username',set_value('username',$row['username']));
		$data['password'] = form_password('password','');
		$data['submit'] = form_submit('group_submit',$this->lang->line('common_save'));
		return $data;
	}
	// ok
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => 'Tên nhóm',
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'username',
                 'label'   => 'Tên đăng nhập',
                 'rules'   => 'required|alpha_numeric'
              ),
           array(
                 'field'   => 'password',
                 'label'   => 'Mật khẩu',
                 'rules'   => 'min_length[3]|max_length[40]|alpha_dash'
              ),
		);
  		$this->form_validation->set_rules($valid);
	}
} ?>