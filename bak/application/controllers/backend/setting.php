<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting extends CI_Controller{
	private $option = array();
	function __construct(){
		parent::__construct();
		$this->lang->load('backend/setting');
		$this->load->setData('title',$this->lang->line('setting_title'));
	}
    function index(){
        $this->load->model('system_model','setting');
        if ($this->input->post('setting_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
                $data = array(
                    'email_admin' => $this->input->post('email_admin'),
                    'email_host' => $this->input->post('email_host'),
                    'email_username' => $this->input->post('email_username'),
                    'email_password' => $this->input->post('email_password'),
                    'email_port' => (int)$this->input->post('email_port'),
                    'site_title' => $this->input->post('site_title'),
                    'metakey' => $this->input->post('metakey'),
                    'metadesc' => $this->input->post('metadesc'),
                    'images' => $this->input->post('images'),
                );

                $this->load->helper('images');
                $img = images_upload('images');
                if ($img['error'] == '' && $img['success'] != NULL){
                    $data['images'] = $img['success'];
                }
				$this->setting->update_setting($data,(int)$this->input->post('sid'));
			}
		}
        $row = $this->setting->get_setting();
        $this->load->helper('form');
        $data['site_title'] = form_input('site_title',set_value('site_title',$row['site_title']));
        $data['metakey'] = form_input('metakey',set_value('metakey',$row['metakey']));
        $data['metadesc'] = form_textarea(array('name'=>'metadesc','rows'=>"3",'cols'=>"60"),set_value('metadesc',$row['metadesc']));
        $data['email_admin'] = form_input('email_admin',set_value('email_admin',$row['email_admin']));
        $data['email_host'] = form_input('email_host',set_value('email_host',$row['email_host']));
        $data['email_username'] = form_input('email_username',set_value('email_username',$row['email_username']));
        $data['email_password'] = form_password('email_password',set_value('email_password',$row['email_password']));
        $data['email_port'] = form_input('email_port',set_value('email_port',$row['email_port']));
        $data['images'] = form_upload('images','','size="50"'); 
        $data['submit'] = form_submit('setting_submit',$this->lang->line('common_save'));
        $data['sid'] = form_hidden('sid',$row['id']);
        $data['row'] = $row;
        $this->load->layout('setting/edit',$data);
    }
    function _validation(){
        $this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'email_admin',
                 'label'   => $this->lang->line('setting_email_admin'),
                 'rules'   => 'valid_emails'
              ),
           array(
                 'field'   => 'site_title',
                 'label'   => $this->lang->line('setting_site_title'),
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'metakey',
                 'label'   => $this->lang->line('setting_metakey'),
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'metadesc',
                 'label'   => $this->lang->line('setting_metadesc'),
                 'rules'   => 'required'
              ),
		);
  		$this->form_validation->set_rules($valid);

    }

}