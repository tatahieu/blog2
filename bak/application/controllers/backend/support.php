<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Support extends CI_Controller{
	private $option = array();
	function __construct(){
		parent::__construct();
		$this->lang->load('backend/support');
		$this->load->setData('title',$this->lang->line('sup_title'));
	}
	public function index(){
		$this->load->model('support_model','support');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			$this->support->delete();
		}
		$data['row'] = $this->support->lists();
		$this->load->layout('support/list',$data);
	}
	public function add(){
		$this->load->model('support_model','support');
		if ($this->input->post('support_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->support->insert();
                $this->session->set_userdata('success','add');
				redirect('support/add');
			}
		}
		$row = set_array('job',array('ordering' => 0),'yahoo','skype','phone');
		$data = $this->_form($row);
		$this->load->layout('support/form',$data);
	}
	public function edit($id){
		if ($id <= 0 OR !is_numeric($id)){
			show_404();
		}
		$this->load->model('support_model','support');
		if ($this->input->post('support_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->support->update(intval($id));
                $this->session->set_userdata('success','edit');
				redirect('support');
			}
		}
		$row = $this->support->detail(intval($id));
		if (empty($row)){
			show_404();
		}
		$data = $this->_form($row);
		$this->load->layout('support/form',$data);
	}
	private function _form($row = array()){
		$this->load->helper ('form');
		$data['job'] = form_input('job',set_value('job',$row['job']));
		$data['yahoo'] = form_input('yahoo',set_value('yahoo',$row['yahoo']));
		$data['skype'] = form_input('skype',set_value('skype',$row['skype']));
		$data['phone'] = form_input('phone',set_value('phone',$row['phone']));
		$data['ordering'] = form_input('ordering',set_value('ordering',$row['ordering']));
		$data['submit'] = form_submit('support_submit',$this->lang->line('common_save'));
		return $data;
	}
	// ok
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'job',
                 'label'   => $this->lang->line('sup_job'),
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'ordering',
                 'label'   => $this->lang->line('doc_cate_ordering'),
                 'rules'   => 'integer'
              ),
		);
  		$this->form_validation->set_rules($valid);
	}
} ?>