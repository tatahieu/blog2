<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Block extends CI_Controller{
	private $_config = array();
    private $_block = array();
	private $_position = array();
    private $menu_option = array();
    private $_module;
    function __construct(){
		parent::__construct();
		$this->lang->load('backend/block');
		$this->load->setData('title',$this->lang->line('block_title'));
        $this->config->load('block');
        $blockconfig = $this->config->item("block");
        $this->_config = $blockconfig;
        foreach ($blockconfig as $key => $cf) {
            $block[$key] = $cf['name'];
            $position[$key] = $cf['position'];
        }
        $this->_block = $block;
        $this->_position = $position;
        unset($block,$position,$blockconfig) ;

	}
	public function index(){
        $this->load->model('block_model','block');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('block'))
		{
			$this->block->delete();
		}
        $this->load->helper('form');
        $filter['data'][] = $this->lang->line('block_module').form_dropdown('module',$this->_block,$this->input->get('module'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
		$data['rows'] = $this->block->lists();
		$this->load->layout('block/list',$data);
	}
	public function add($module = ''){
        // load model
        if (!array_key_exists($module,$this->_config)){

            return $this->load->layout('block/choosemodule',array('option' => $this->_block));
        }
        $this->_module = $module;
		$this->load->model('block_model','block');
        if ($this->input->post('block_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->block->insert($module);
                $this->session->set_userdata('success','add');
				redirect('block/add');
			}
		}
		$row = set_array('name',array('block_id'=>0,'ordering' => 0, 'item_nums' => 0,'module' => $module,"publish" => 1,"accesstype" => "all","access" => array()),'position','mid','style');
		$data = $this->_form($row);
		$this->load->layout('block/form',$data);
	}
    public function delete_item() {
		if ($this->input->post('cid') && $this->permission->check_permission_backend('block','delete',0))
		{
            $this->load->model('block_model','block');
			$this->block->delete();
            return 'true';
        }
        else{
            return 'false';
        }

    }
	public function edit($id=0){
        if ($id <= 0){
       	    show_404();
        }
		$this->load->model('block_model','block');
        $row = $this->block->detail(intval($id));
        $this->_module = $row['module'];
		if ($this->input->post('block_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->block->update(intval($id));
                $this->session->set_userdata('success','edit');
				redirect(base64_decode($this->input->get("refer")));
			}
		}

		if (empty($row)){
			show_404();
		}
        $access = $this->block->get_access_by_blockid($row['block_id']);
        $row['accesstype'] = 'all'; $dataaccess = array();
        foreach ($access as $as){
            if ($as['module'] != 'all'){
                $row['accesstype'] = 'module';
            }
            $dataaccess[] = $as['module'];
        }
        $row['access'] = $dataaccess;
		$data = $this->_form($row);

		$this->load->layout('block/form',$data);
	}
	private function _form($row = array()){
        // add static block
        $static[] = js('jquery-ui-1.10.0.custom.min.js');
        $static[] = link_tag('jquery-ui.css');
        $this->load->setArray("footer",$static);
 		$this->load->helper ('form');
        /////////////// menu list ////////////
        //$this->load->model("menu_model","menu");
        //$row_parent = $this->menu->lists();
        $config = $this->_config[$row['module']];
        $data['row'] = $row;
        $data['module'] = $config['name'];
        $data['module_key'] = $row['module'];
        $paramdata = array();

        if ($this->input->post("params")){
            $paramdata = $this->input->post("params");
        }
        else{
            if ($row['params']){
                $paramdata = json_decode($row['params'],TRUE);
            }
        }
        $data['paramdata'] = $paramdata;
        $paramdata['block'] = $row;
        // param common

        $data['ordering'] = form_input('ordering',set_value('ordering',$row['ordering']));
        $data['publish'] = form_checkbox('publish',1,$row['publish']);
        $data['name'] = form_input('name',set_value('name',$row['name']));
        $data['position'] = form_dropdown('position',$config['position'],$row['position']);
        $data['device'] = form_dropdown('device',array('0' => 'All device', 4 => 'pc', 1 => 'mobile'),$row['device']);
        // privaet param
        if ($config['params'])
        $data['params'] = $this->_get_param($config['params'],$paramdata);
        /// submit
        $data['submit'] = form_submit('block_submit',$this->lang->line('common_save'));
        ///// load module access //
        $data['access'] = form_dropdown("access",array("all" => "Tất cả các trang","module" => "Chọn module"),$row['accesstype'],'id="block_access"');
        $data['moduleaccess'] = form_multiselect('moduleaccess[]',access_module(),$row['access'],'size="15" class="select_cate"');

		return $data;
	}
    public function _check_module_position(){
        $param = $this->input->post('params');
        $ext = '';
        if (trim($param['temp']) != ''){
            $ext = '_'.$param['temp'];
        }
        $path = APPPATH.'views/frontend/block/'.$this->_module.$ext.'.php';
        if (!file_exists($path)){
            $this->form_validation->set_message('_check_module_position', $this->lang->line('block_error_position'));
            return false;
        }
        return true;
    }
    private function _get_param($arrs,$paramdata = array()){
        $i = 0;
        $paramname = $this->config->item("blockname");
        foreach ($arrs as $arr) {
            switch ($arr){
                case "id":
                    $param[] = array(
                        'name' => "ID",
                        'form' => form_input("suggest","",'id="autoc" autocomplete="off"').form_hidden("params[id]",$paramdata['id'],FALSE,'id="proid"')
                    );
                break;
                case "content":
                    $param[] = array(
                        'name' => "HTML",
                        'form' => form_detail("content",$paramdata['block']['content'])
                    );
                break;
                case "template":
                    $temp = $this->_config[$this->_module]['template'];
                    $param[] = array(
                        'name' => "Temp",
                        'form' => form_dropdown("params[template]",$temp,$paramdata["template"])
                    );
                break;
                case "thumb":
                    $size = $this->config->item("resize");
                    $thumb['size0'] = '0-0';
                    foreach ($size as $key => $size) {
                        $thumb[$key] = $size[0].'-'.$size[1];
                    }
                    $param[] = array(
                        'name' => $paramname[$arr],
                        'form' => form_dropdown("params[thumb]",$thumb,$paramdata["thumb"])
                    );
                break;
                case "title":
                    $checkbox = ($paramdata['title']) ? TRUE : FALSE;
                    $param[] = array(
                        'name' => $paramname[$arr],
                        'form' => form_checkbox("params[title]", 1 , $checkbox)
                    );
                break;
                default:
                    $param[] = array(
                        'name' => $paramname[$arr],
                        'form' => form_input("params[".$arr."]",set_value("params[".$arr."]",$paramdata[$arr]))
                    );
            }
            $i ++;
        }
        return $param;
    }
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => $this->lang->line('block_name'),
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'position',
                 'label'   => $this->lang->line('block_position'),
                 'rules'   => 'required'
              ),
            array(
                'field'   => 'module',
                'label'   => $this->lang->line('block_module'),
                'rules'   => 'callback__check_module_position'
            )

		);
  		$this->form_validation->set_rules($valid);
	}
} ?>