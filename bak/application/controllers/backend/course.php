<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Course extends CI_Controller{
	private $resize = array('width' => 130, 'height' => 130);
    private $images_path = 'uploads/news_images/';
	private $error = array();
    public $option = array();
    function __construct(){
		parent::__construct();
		$this->load->model('course_model','course');
        $this->load->setData('title','Quản lý bài học');
	}
    //////////////////////////////////////////////// WORD QUESTION //////////////////////////
	public function word_index(){
        $limit = 50;
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			$row = $this->course->word_delete();
			if (!empty($row)){
				$this->load->helper('images');
                foreach ($row as $row){
					if ($row['images'] != ''){
                        images_delete($row['images']);
                    }
				}
			}
		}
        $this->load->helper('form');
        /** FILTER **/
        $this->load->helper('form');
        $level = get_level();
        array_unshift($level,'Tất cả level');
        $filter['data'][] = form_dropdown('level',$level,$this->input->get('level'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
        /** End filter **/
		$data['row'] = $this->course->word_lists($limit);
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
		$this->load->layout('course/word_list',$data);
	}
	public function word_add(){
		if ($this->input->post('course_submit')){
			$this->_word_validation();
			if ($this->form_validation->run() == true)
			{
                $this->load->helper('images');
                $img = images_upload('images');
                if ($img['error'] != ''){
                    $this->error['upload'] = $img['error'];
                }else {
                    $this->course->word_insert($img['success']);
                    $this->session->set_userdata('success','add');
                    redirect('course/word_add');
                }
			}
		}
		$row = set_array('english','word','vietnamese','exam','phienam',array('topic' => 0,'level' => 0));
		$data = $this->_word_form($row);
		$this->load->layout('course/word_form',$data);
	}
    public function word_edit($id = 0){
        if ($id <= 0 OR !is_numeric($id)){
			show_404();
		}
		$row = $this->course->word_detail(intval($id));
		if ($this->input->post('course_submit')){
			$this->_word_validation();
			if ($this->form_validation->run() == true)
			{
                $this->load->helper('images');
                $img = images_upload('images');
                if ($img['error'] != ''){
                    $this->error['upload'] = $img['error'];
                }
                else{
                    if ($img['success'] != '' AND $row['images'] != ''){
                        images_delete($row['images']);
                    }
                    $this->course->word_update(intval($id),$img['success']);

				    redirect(base64_decode($this->input->get("refer")));
                }
			}
		}
		if (empty($row)){
			show_404();
		}
		$data = $this->_word_form($row);
        $data['row'] = $row;
		$this->load->layout('course/word_form',$data);
	}
	private function _word_form($row = array()){
		$this->load->helper('form');

        $this->load->model("topic_model","topic");
        $topic = $this->topic->lists();
        $t[0] = 'Chọn chủ đề';
        foreach ($topic as $topic){
            $t[$topic['topic_id']] = $topic['name'];
        }
        $level = get_level();
        $level[0] = 'Chọn level';
		$data['error_upload'] = $this->error['upload'];
		$data['english'] = form_input('english',set_value('english',$row['english']));
		$data['vietnamese'] = form_input('vietnamese',set_value('vietnamese',$row['vietnamese']));
		$data['word'] = form_input('word',set_value('word',$row['word']));
		$data['exam'] = form_textarea('exam',set_value('exam',$row['exam']));
        $data['phienam'] = form_input('phienam',set_value('phienam',$row['phienam']));
        $data['topic'] = form_dropdown('topic',$t,$row['topic_id']);
        $data['level'] = form_dropdown('level',$level,$row['level']);
		$data['images'] = form_upload('images','','size="50"');
		$data['submit'] = form_submit('course_submit',$this->lang->line('common_save'));
		return $data;
	}
	private function _word_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'english',
                 'label'   => 'Nghĩa tiếng anh',
                 'rules'   => 'required'
              ),
            array(
                 'field'   => 'vietnamese',
                 'label'   => 'Nghĩa tiếng việt',
                 'rules'   => 'required'
              ),
            array(
                 'field'   => 'word',
                 'label'   => 'Từ tiếng anh',
                 'rules'   => 'required'
              ),
            array(
                 'field'   => 'vietnamese',
                 'label'   => 'Nghĩa tiếng việt',
                 'rules'   => 'required'
              ),
		);
  		$this->form_validation->set_rules($valid);
	}
    ////////////////////////////////// COMPLETE ///////////////////////////
    public function com_index(){
        $limit = 50;
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','com_delete'))
		{
			$row = $this->course->com_delete();
			if (!empty($row)){
				$this->load->helper('images');
                foreach ($row as $row){
					if ($row['images'] != ''){
                        images_delete($row['images']);
                    }
                    if ($row['audio'] != ''){
                        images_delete($row['audio'],'files');
                    }
				}
			}
		}
        /** FILTER **/
        $this->load->helper('form');
        $level = get_level();
        array_unshift($level,'Tất cả level');
        $filter['data'][] = form_dropdown('level',$level,$this->input->get('level'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
        /** End filter **/
		$data['row'] = $this->course->com_lists($limit);
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
		$this->load->layout('course/com_list',$data);
	}
	public function com_add(){
		if ($this->input->post('course_submit')){
			$this->_com_validation();
			if ($this->form_validation->run() == true)
			{
                $this->load->helper('images');
                $img = images_upload('images');
                if ($img['error'] != ''){
                    $this->error['upload'] = $img['error'];
                }else {
                    $audio = images_upload("audio",array(),'files');
                    $this->course->com_insert($img['success'],$audio['success']);
                    $this->session->set_userdata('success','add');
                    redirect('course/com_add');
                }
			}
		}
		$row = set_array('question',array('topic' => 0,'level' => 0,'answer' => '[]'));
		$data = $this->_com_form($row);
		$this->load->layout('course/com_form',$data);
	}
    public function com_edit($id = 0){
        if ($id <= 0 OR !is_numeric($id)){
			show_404();
		}
		$row = $this->course->com_detail(intval($id));
		if ($this->input->post('course_submit')){
			$this->_com_validation();
			if ($this->form_validation->run() == true)
			{
                $this->load->helper('images');
                $img = images_upload('images');
                if ($img['error'] != ''){
                    $this->error['upload'] = $img['error'];
                }
                else{
                    $audio = images_upload('audio',array(),'files');
                    if ($img['success'] != '' AND $row['images'] != ''){
                        images_delete($row['images']);
                    }
                    if ($audio['success'] != '' AND $row['audio'] != ''){
                        images_delete($row['audio'],'files');
                    }
                    $this->course->com_update(intval($id),$img['success'],$audio['success']);
				    redirect(base64_decode($this->input->get("refer")));
                }
			}
		}
		if (empty($row)){
			show_404();
		}
		$data = $this->_com_form($row);
        $data['row'] = $row;
		$this->load->layout('course/com_form',$data);
	}
	private function _com_form($row = array()){
		$this->load->helper('form');
        $this->load->model("topic_model","topic");
        $topic = $this->topic->lists();
        $t[0] = 'Chọn chủ đề';
        foreach ($topic as $topic){
            $t[$topic['topic_id']] = $topic['name'];
        }
        $level = get_level();
        $level[0] = 'Chọn level';
		$data['error_upload'] = $this->error['upload'];
		$data['question'] = form_input('question',set_value('question',$row['question']));
		$data['answer'] = form_input('answer',set_value('answer',implode(',',json_decode($row['answer']))));
        $data['topic'] = form_dropdown('topic',$t,$row['topic_id']);
        $data['level'] = form_dropdown('level',$level,$row['level']);
		$data['images'] = form_upload('images','','size="50"');
        $data['audio'] = form_upload('audio','','size="50"');
		$data['submit'] = form_submit('course_submit',$this->lang->line('common_save'));
		return $data;
	}
	private function _com_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'question',
                 'label'   => 'Câu hỏi',
                 'rules'   => 'required'
              ),
            array(
                 'field'   => 'answer',
                 'label'   => 'Trả lời',
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}
}