<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Controller {
    private $user_data = array();
	public function __construct(){
		parent::__construct();
		$this->lang->load('backend/users');
		$this->load->setData('title',$this->lang->line('users_title'));
	}
    public function denied(){
        $this->load->setData('title','Không đủ quyền truy cập');
        $this->load->layout('users/denied',$data);
    }
	public function index(){
		$this->load->model('users_model','users');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			$this->users->delete();
		}
        $tmp = $this->input->get('tmp','nomal');
        if ($tmp == 'xls'){
        	$fromdate = convert_datetime($this->input->get('fromdate')); 
        	$todate = convert_datetime($this->input->get('todate'));
        	$type = $this->input->get("type");
        	// user register
            switch ($type) {
            	case 'register':
            		$this->db->select("user_id,fullname, username,email,phone,date_created,where_live,birthday");
		            $this->db->where("active",1);
		            $this->db->where("date_created > ",$fromdate);
		            $this->db->where("date_created < ",$todate);
		            $this->db->order_by("user_id","DESC");
		            $this->db->limit(5000);
		            $query = $this->db->get("member");
		            $rows = $query->result_array();

		            $this->load->library('PHPExcel');
		            $objPHPExcel = new PHPExcel();
	                $i = 1; 
	                $baseRow = 2;
	                foreach($rows as $key => $row){   
	                    $count = $baseRow + $key;
	                    if($i == 1){
	                        $objPHPExcel->getActiveSheet(0)
	                        ->setCellValue('A'.$i, "Họ tên")
	                        ->setCellValue('B'.$i, "Username")
	                        ->setCellValue('C'.$i, "Email")
	                        ->setCellValue('D'.$i, "Giới tính")
	                        ->setCellValue('E'.$i, "Số điện thoại")
	                        ->setCellValue('F'.$i, "Ngày đăng ký")
	                        ->setCellValue('G'.$i, "Ngày sinh")
	                        ->setCellValue('H'.$i, "Địa điểm");
	                    }
	                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($count,1);
	                    $objPHPExcel->getActiveSheet(0)
	                        ->setCellValue('A'.$count, $row['fullname'])
	                        ->setCellValue('B'.$count, $row['username'])
	                        ->setCellValue('C'.$count, $row['email'])
	                        ->setCellValue('D'.$count, $row['sex'])
	                        ->setCellValue('E'.$count, $row['phone'])
	                        ->setCellValue('F'.$count, date('d/m/Y',$row['date_created']))
	                        ->setCellValue('G'.$count, $row['birthday'])
	                        ->setCellValue('H'.$count, $row['where_live']);
	                    $i++;
	                }
	                
	                $objPHPExcel->getActiveSheet()->setTitle('Export-User-'.date('d-m-Y'));
	                $objPHPExcel->setActiveSheetIndex(0);
	                
	                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	                //$objWriter->save($filename);
	                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	                header('Content-Disposition: attachement; filename="' . $filename . '"');
	                $objWriter->save('php://output');

            		break;
            	case 'test':
            		$this->db->select("s.type, s.total_score,fullname, u.username,u.email,u.sex,u.phone,u.where_live,u.birthday,u.date_created,u.birthday,u.where_live");
		            $this->db->where("date_test > ",$fromdate);
		            $this->db->where("date_test < ",$todate);
		            $this->db->order_by("date_test","DESC");
		            $this->db->join('member as u','u.user_id = s.user_id');
		            $this->db->limit(5000);
		            $query = $this->db->get("question_score as s");
		            $rows = $query->result_array();

		            $this->load->library('PHPExcel');
		            $objPHPExcel = new PHPExcel();
	                $i = 1; 
	                $baseRow = 2;
	                foreach($rows as $key => $row){   
	                    $count = $baseRow + $key;
	                    if($i == 1){
	                        $objPHPExcel->getActiveSheet(0)
	                        ->setCellValue('A'.$i, "Họ tên")
	                        ->setCellValue('B'.$i, "Username")
	                        ->setCellValue('C'.$i, "Email")
	                        ->setCellValue('D'.$i, "Giới tính")
	                        ->setCellValue('E'.$i, "Số điện thoại")
	                        ->setCellValue('F'.$i, "Ngày đăng ký")
	                        ->setCellValue('G'.$i, "Ngày sinh")
	                        ->setCellValue('H'.$i, "Địa điểm")
	                        ->setCellValue('I'.$i, "ĐIểm test");
	                    }
	                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($count,1);
	                    $objPHPExcel->getActiveSheet(0)
	                        ->setCellValue('A'.$count, $row['fullname'])
	                        ->setCellValue('B'.$count, $row['username'])
	                        ->setCellValue('C'.$count, $row['email'])
	                        ->setCellValue('D'.$count, $row['sex'])
	                        ->setCellValue('E'.$count, $row['phone'])
	                        ->setCellValue('F'.$count, date('d/m/Y',$row['date_created']))
	                        ->setCellValue('G'.$count, $row['birthday'])
	                        ->setCellValue('H'.$count, $row['where_live'])
	                        ->setCellValue('I'.$count, $row['total_score']);
	                    $i++;
	                }
	                
	                $objPHPExcel->getActiveSheet()->setTitle('Export-User-'.date('d-m-Y'));
	                $objPHPExcel->setActiveSheetIndex(0);
	                
	                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	                //$objWriter->save($filename);
	                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	                header('Content-Disposition: attachement; filename="' . $filename . '"');
	                $objWriter->save('php://output');
            	default:
            		# code...
            		break;
            }
            
            exit();
        }
        $static[] = js('jquery-ui-1.10.0.custom.min.js');
        $static[] = js('jquery-ui-timepicker-addon.js');
        $static[] = link_tag('jquery-ui.css');
        $static[] = link_tag('jquery-ui-timepicker-addon.css');
        $this->load->setArray("footer",$static);
		$limit = 20;
		$data['row'] = $this->users->lists($limit);
        /////////// FILTER ///////////////
        $this->load->helper('form');
        $this->option[0] = $this->lang->line('news_cate');
        //$filter['data'][] = form_dropdown('cate',$this->category(),$this->input->get('cate'));
        $filter['data'][] = $this->lang->line('users_username').form_input('username',$this->input->get('username'));
        $filter['data'][] = $this->lang->line('users_email').form_input('email',$this->input->get('email'));
        $filter['data'][] = $this->lang->line('users_active').form_dropdown('active',array(0 => $this->lang->line('users_all'),1 => $this->lang->line('users_active'), 2 => $this->lang->line('users_unactive') ),$this->input->get('active'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
        ////////////////////////////////////
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
        $this->load->layout('users/list',$data);
	}
	public function edit($id = 0){
		$this->load->model('users_model','users');
		if ($this->input->post('users_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->users->update(intval($id));
                $this->session->set_userdata('success','edit');
                redirect(base64_decode($this->input->get("refer")));
			}
		}
		$this->load->helper('form');

		$row = $this->users->role_lists();

		$option = array();
		$option[0] = $this->lang->line('users_select_roles');
		if (!empty($row)){
			foreach ($row as $row){
				$option[$row['roles_id']] = $row['name'];
			}
		}
		$row = $this->users->detail(intval($id));
		if (empty($row)){
			show_404();
		}
		$data['username'] = form_input('username',set_value('username',$row['username']));
		$data['password'] = form_password('password');
		$data['roles'] = form_dropdown('roles',$option,$row['roles_id']);
		$data['active'] = form_checkbox('active',1,$row['active']);
		$data['email'] = form_input('email',set_value('email',$row['email']));
		$data['fullname'] = form_input('fullname',set_value('fullname',$row['fullname']));
		$data['sex'] = form_dropdown('sex',array(1 => $this->lang->line('users_male'),0 => $this->lang->line('users_female')),$row['sex'],set_select('sex',$row['sex']));
		$data['yahoo'] = form_input('yahoo',set_value('yahoo',$row['yahoo']));
		$data['skype'] = form_input('skype',set_value('skype',$row['skype']));

		$data['date_reg'] = sql_to_date($row['date_reg']);
		$data['submit'] = form_submit('users_submit',$this->lang->line('common_save'));
		$data['hidden'][] = form_hidden('cusername',$row['username']);
		$data['hidden'][] = form_hidden('cemail',$row['email']);
		$this->load->layout('users/form',$data);
	}
	public function login(){
		if ($this->session->userdata('username') && $this->session->userdata('userid')){
			redirect(site_url());
		}
		if ($this->input->post('log_submit')){

			$this->_login_validation();

			if ($this->form_validation->run() == true)
			{
			    $row = $this->user_data;
			    $this->permission->set_user_session($row);
				redirect();
			}
		}
		$this->load->helper('captcha');
		$this->load->helper('form');

        // SET CAPTCHA //
        $capcha = get_captcha('captcha_backend');

        $data['captcha'] = $capcha['image'];
		$data['input_captcha'] = form_input('log_captcha','','class="login_captcha"');
        // FORM LOGIN
		$data['username'] = form_input('log_username',set_value('log_username'),'maxlength = "100" class="login_user"');
		$data['password'] = form_password('log_password',set_value('log_password'),'maxlength = "100" class="login_pass"');
		$data['submit'] = form_submit('log_submit',$this->lang->line('users_login_submit'));
		$this->load->view('users/login',$data,FALSE);
	}
    public function logout(){
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('userid');
        $this->session->unset_userdata('roles');
        $this->session->unset_userdata('permission');
        delete_cookie(USER_REMEMBER_PASSWORD);
        redirect('users/login');
    }
    public function profile(){
        $this->load->setData('title',$this->lang->line('users_profile'));
        $this->load->layout('users/profile');
    }
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'username',
                 'label'   => $this->lang->line('users_username'),
                 'rules'   => 'required|min_length[3]|max_length[40]|alpha_dash|callback__user_exist'
              ),
           array(
                 'field'   => 'password',
                 'label'   => $this->lang->line('users_password'),
                 'rules'   => 'min_length[3]|max_length[40]|alpha_dash'
              ),
           array(
                 'field'   => 'email',
                 'label'   => $this->lang->line('users_email'),
                 'rules'   => 'required|valid_emails|callback__email_exist'
              ),
           array(
                 'field'   => 'fullname',
                 'label'   => $this->lang->line('users_fullname'),
                 'rules'   => 'required|min_length[3]'
              )
  		);
  		$this->form_validation->set_rules($valid);
	}
	function _user_exist($str){
		if ($this->input->post('cusername') != $str){
			$this->form_validation->set_message('_user_exist',$this->lang->line('users_username_exist'));
			return $this->users->check_exist('username',$str);
		}
		else{
			return true;
		}
	}
	function _email_exist($str){
		if ($this->input->post('cemail') != $str){
			$this->form_validation->set_message('_email_exist',$this->lang->line('users_email_exist'));
			return $this->users->check_exist('email',$str);
		}
		else{
			return true;
		}
	}
	private function _login_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'log_username',
                 'label'   => $this->lang->line('users_username'),
                 'rules'   => 'required|min_length[3]|max_length[40]'
              ),
           array(
                 'field'   => 'log_password',
                 'label'   => $this->lang->line('users_password'),
                 'rules'   => 'required|min_length[3]|max_length[40]'
              ),
           array(
                 'field'   => 'log_captcha',
                 'label'   => $this->lang->line('users_captcha'),
                 'rules'   => 'required|matches_str['.$this->session->userdata('captcha_backend').']|callback__user_check'
              )
  		);
  		$this->form_validation->set_rules($valid);
	}
	function _user_check(){
		$this->load->model('users_model');
		$row = $this->users_model->check_login();
		if (empty($row)){
			$this->form_validation->set_message('_user_check',$this->lang->line('users_error_check'));
			return false;
		}
        else{
            $this->user_data = $row;
            return true;
        }
	}
    ///////////////////////////////////// ROLE //////////////////////////////
    // list role
    public function role_index(){
		$this->load->model('users_model','roles');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','role_delete'))
		{
			$this->roles->role_delete();
		}
		$data['row'] = $this->roles->role_lists();
		$this->load->layout('users/roles_list',$data);
	}
    // add role
	public function role_add(){
		$this->load->model('users_model','roles');
		if ($this->input->post('roles_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->roles->role_insert();
                $this->session->set_userdata('success','add');
				redirect('users/role_add');
			}
		}
		$row = set_array('name','permission');
		$data = $this->_role_form($row);
		$this->load->layout('users/roles_form',$data);
	}
    // edit role
	public function role_edit($id = 0){
		$this->load->model('users_model','roles');
		if ($this->input->post('roles_submit')){
			$this->_role_validation();
			if ($this->form_validation->run() == true)
			{
				$this->roles->role_update(intval($id));
				redirect('users/role_index');
			}
		}
		$row = $this->roles->role_detail(intval($id));
		if (empty($row)){
			show_404();
		}
		$data = $this->_role_form($row);
		$this->load->layout('users/roles_form',$data);
	}
    // role valid form
	public function _role_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => $this->lang->line('users_roles_name'),
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}
    // role form
	public function _role_form($row = array()){
		$this->load->helper('form');

		$data['roles_module'] = $this->permission->backend();
		$data['roles_name'] = form_input('name',set_value('name',$row['name']));
		$data['roles_permission'] = unserialize($row['permission']);
		$data['roles_submit'] = form_submit('roles_submit',$this->lang->line('common_save'));
        //var_dump("<pre>",$data['roles_permission']); die;
		return $data;
	}
}
?>