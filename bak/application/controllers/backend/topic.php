<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Topic extends CI_Controller{
	private $resize = array('width' => 130, 'height' => 130);
    private $images_path = 'uploads/news_images/';
	private $error = array();
    public $option = array();
    function __construct(){
		parent::__construct();
		$this->load->setData('title','Quản lý topic');
	}
	public function index(){
		$this->load->model('topic_model','topic');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			$row = $this->topic->delete();
		}
        /** FILTER **/
        $this->load->helper('form');
        $level = get_level();
        array_unshift($level,'Tất cả level');
        $filter['data'][] = form_dropdown('level',$level,$this->input->get('level'));
        $filter['data'][] = form_dropdown('type',array('0' => 'Tất cả', 1=>'Word', 2 => 'Complete'),$this->input->get('type'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
        /*** FITLER ***/
		$data['row'] = $this->topic->lists();
		/** PAGING **/
		$this->load->layout('topic/list',$data);
	}
	public function add(){
		$this->load->model('topic_model','topic');
		if ($this->input->post('topic_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
                $this->load->helper('images');
                $this->topic->insert();
                $this->session->set_userdata('success','add');
                redirect('topic/add');
			}
		}
		$row = set_array('name','type',array('level' => 0));
		$data = $this->_form($row);
		$this->load->layout('topic/form',$data);
	}
    public function edit($id = 0){
        if ($id <= 0 OR !is_numeric($id)){
			show_404();
		}
		$this->load->model('topic_model','topic');
		if ($this->input->post('topic_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
                $this->topic->update(intval($id));
			    redirect('topic');
			}
		}
        $row = $this->topic->detail(intval($id));
		if (empty($row)){
			show_404();
		}
		$data = $this->_form($row);
        $data['row'] = $row;
		$this->load->layout('topic/form',$data);
	}
	private function _form($row = array()){
		$this->load->helper('form');
		$data['name'] = form_input('name',set_value('name',$row['name']));
        // chon loai
        $data['type'] = form_dropdown('type',array(1 => 'Word', 2 => 'Complete'),$row['type']);
        // chon level
        $level = get_level();
        array_unshift($level,'Tất cả các level');
        $data['level'] = form_dropdown('level',$level,$row['level']);
        $data['submit'] = form_submit('topic_submit',$this->lang->line('common_save'));
		return $data;
	}
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => 'Tên',
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}
}