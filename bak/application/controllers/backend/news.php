<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends CI_Controller{
	private $error = array();
    public $option = array();
    function __construct(){
		parent::__construct();


		$this->lang->load('backend/news');
		$this->load->setData('title',$this->lang->line('news_title'));

	}
	public function index(){
        $limit = 50;

		$this->load->model('news_model','news');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			$row = $this->news->delete();
			if (!empty($row)){
				$this->load->helper('images');
                foreach ($row as $row){
					if ($row['images'] != ''){
                        images_delete($row['images']);
                    }
				}
			}
            $this->session->set_userdata('success','delete');
		}
        $this->load->helper('form');
        /** FILTER **/
        $this->option[0] = $this->lang->line('news_cate');
        $filter['data'][] = form_dropdown('cate',$this->cate_parent(),$this->input->get('cate'));
        $filter['data'][] = $this->lang->line('news_name').form_input('name',$this->input->get('name'));
        $filter['data'][] = $this->lang->line('news_special').form_checkbox('special',1,$this->input->get('special'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
		$data['row'] = $this->news->lists($limit);
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);

		$this->load->layout('news/list',$data);
	}
	public function add(){

        // load model
		$this->load->model('news_model','news');
		if ($this->input->post('news_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{

                $this->load->helper('images');
                $img = images_upload('images');
                if ($img['error'] != ''){
                    $this->error['upload'] = $img['error'];
                }else {
                    //for ($i = 1; $i <= 40; $i ++)
                    $news_id = $this->news->insert($img['success']);
                    $this->session->set_userdata('success','add');
                    redirect('news/edit/'.$news_id);
                }
			}
		}
		$row = set_array('title','description','detail','images',array('cate_id'=>0,'original_cate' => 0,'publish' =>  0, 'date_up' => time(),'tags' => array()),'special','cate_name');
		$data = $this->_form($row);
		$this->load->layout('news/form',$data);
	}
    public function edit($id = 0){
        $id = intval($id);
        if ($id <= 0){
			show_404();
		}
        //load model

		$this->load->model('news_model','news');
		$row = $this->news->detail(intval($id));
		if (empty($row)){
			show_404();
		}

        if ($this->input->post('news_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{

                $this->load->helper('images');
                $img = images_upload('images');
                if ($img['error'] != ''){
                    $this->error['upload'] = $img['error'];
                }
                else{
                    if ($img['success'] != '' AND $row['images'] != ''){
                        images_delete($row['images']);
                    }
                    $this->news->update($id,$img['success']);
                    $this->session->set_userdata('success','edit');
				    redirect(base64_decode($this->input->get("refer")));
                }
			}
		}
        // get category
		$cate = $this->news->get_cate($id);
		$row['cate_id'] = array();

        if (!empty($cate)){
			foreach ($cate as $cate){
				$row['cate_id'][] = $cate['cate_id'];
			}
		}
        $this->load->model('group_model','group');
		$role = $this->group->get_group_by_news(intval($id));
        if (!empty($role)){
            foreach ($role as $role){
                $row['role'][] = $role['role_id'];
            }
        }
        else{
            $row['role'] = array(0);
        }
        // get all tag
        $tags = $this->news->get_tags($id);
        if (!empty($tags)){
            $row['tags'] = $tags;
        }

		$data = $this->_form($row);
        $data['row'] = $row;
		$this->load->layout('news/form',$data);
	}
    public function suggest_tag(){
        $name = $this->input->get("term");
        $this->load->model('news_model','news');
        $rows = $this->news->suggest_tag($name);
        $result = array();
        foreach ($rows as $row){
            $result[] = '{"id":"","label":"'.$row['name'].'","value":"'.$row['name'].'"}';
        }
        $this->output->set_output( "[".implode(',', $result)."]" );
    }
	private function _form($row = array()){
        // add static
        $static[] = js('jquery-ui-1.10.0.custom.min.js');
        $static[] = js('jquery-ui-timepicker-addon.js');
        $static[] = js('jquery.autoGrowInput.js');
        $static[] = js('jquery.tagedit.js');
        $static[] = link_tag('jquery-ui.css');
        $static[] = link_tag('jquery.tagedit.css');
        $static[] = link_tag('jquery-ui-timepicker-addon.css');
        $this->load->setArray("footer",$static);
        // load form
		$this->load->helper('form');
        $this->load->model("group_model",'group');
        $r = $this->group->lists();
        $role[0] = 'Tất cả các quyền hạn';
        foreach ($r as $r){
            $role[$r['id']] = $r['name'];
        }
        $roleselect = $row['role'];
        $data['role'] = form_multiselect('role_id[]',$role,$roleselect);
		$category = $this->cate_parent();
		$data['error_upload'] = $this->error['upload'];
		$data['title'] = form_input('title',set_value('title',$row['title']));
        $data['seo_title'] = form_input('seo_title',set_value('seo_title',$row['seo_title']));
        $data['seo_desc'] = form_input('seo_desc',set_value('seo_desc',$row['seo_desc']));
		$data['publish'] = form_checkbox('publish',1,$row['publish']);
		$data['special'] = form_checkbox('special',1,$row['special']);
		$data['category'] = form_multiselect('category[]',$category,$row['cate_id'],'size="7" class="select_cate"');
        $data['original_value'] = form_input("original_value",set_value('original_value',$row['cate_name']));
        $data['original_cate'] = form_hidden("original_cate",set_value('original_cate',$row['original_cate']));
        $data['date_up'] = form_input('date_up',set_value('date_up',convert_datetime($row['date_up'],2)));
		$data['image'] = form_upload('images','','size="50"');
        if (empty($row['tags'])){
            $data['tags'][] = form_input('tags[]',set_value('tags',''),'class="tag"');
        }
        else{
            foreach ($row['tags'] as $tag){
                $data['tags'][] = form_input('tags[]',set_value('tags',$tag['name']),'class="tag"');
            }
        }

		$data['description'] = form_textarea('description',set_value('description',$row['description']),' cols = "50" rows ="4"');
        $data['detail'] = form_detail('detail',html_entity_decode($row['detail']));
		$data['submit'] = form_submit('news_submit',$this->lang->line('common_save'));
		return $data;
	}
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'title',
                 'label'   => $this->lang->line('news_name'),
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}

    ////////////////////////////////// CATEGORY /////////////////////////////////
    public function cate_index(){
		$this->load->model('news_model','cate');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','cate_delete'))
		{
			if ($this->cate->cate_delete() === false){
				$data['error'] = $this->lang->line('news_cate_delete_parent');
			}
            else{
                $this->session->set_userdata('success','delete');
            }
		}
		$row = $this->cate->cate_lists();
		$data['row'] = array();
		if (!empty($row)){
			foreach ($row as $row){
				$data['row'][$row['parent']][] = $row;
			}
		}
		$this->load->layout('news/cate_list',$data);
	}
	public function cate_add(){
		$this->load->model('news_model','cate');
		if ($this->input->post('cate_submit')){
			$this->_cate_validation();
			if ($this->form_validation->run() == true)
			{
				$this->cate->cate_insert();
                $this->session->set_userdata('success','add');
				redirect('news/cate_add');
			}
		}
		$row = set_array('name','metakey','metadesc',array('ordering' => 0,'parent' => 0,'cate_id'=>0));
		$data = $this->_cate_form($row);
		$this->load->layout('news/cate_form',$data);
	}
	public function cate_edit($id){
		$this->load->model('news_model','cate');
		if ($this->input->post('cate_submit')){
			$this->_cate_validation();
			if ($this->form_validation->run() == true)
			{
				$this->cate->cate_update(intval($id));
                $this->session->set_userdata('success','edit');
				redirect('news/cate_index');
			}
		}
		$row = $this->cate->cate_detail(intval($id));
		if (empty($row)){
			show_404();
		}
		$data = $this->_cate_form($row);
		$this->load->layout('news/cate_form',$data);
	}
	private function _cate_select_parent($data,$parent = 0,$level = ''){
		if (!empty($data[$parent])){
			foreach ($data[$parent] as $rows){
			$this->option[$rows['cate_id']] = $level.' '.$rows['name'];
			$this->_cate_select_parent($data,$rows['cate_id'],$level.'---');
			}
		}
	}
    public function cate_parent($choose = 0){
        $this->load->model('news_model','cate');
        $row_parent = $this->cate->cate_lists();
		$select = array();
		$this->option[0] = $this->lang->line('news_cate_select_parent');
		foreach ($row_parent as $row_parent){
			if ($row_parent['cate_id'] != $choose)
			$select[$row_parent['parent']][] = $row_parent;
		}
		$this->_cate_select_parent($select);
        return $this->option;
    }
	private function _cate_form($row = array()){
		$parent = $this->cate_parent($row['cate_id']);
		$this->load->helper ('form');
		$data['cate_name'] = form_input('name',set_value('name',$row['name']));
		$data['cate_ordering'] = form_input('ordering',set_value('ordering',$row['ordering']));

        $data['cate_metakey'] = form_input('metakey',set_value('metakey',$row['metakey']));
        $data['cate_desc'] = form_textarea('metadesc',set_value('metadesc',$row['metadesc']));
        $data['cate_style'] = form_dropdown('style',array(1 => 'Nomal', 2 => "Thumb"),$row['style']);
		$data['cate_parent'] = form_dropdown('parent',$parent,$row['parent'],$row['parent']);
		$data['cate_submit'] = form_submit('cate_submit',$this->lang->line('common_save'));
		return $data;
	}
	// ok
	private function _cate_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => $this->lang->line('news_cate_name'),
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'ordering',
                 'label'   => $this->lang->line('news_cate_ordering'),
                 'rules'   => 'required|integer'
              ),
		);
  		$this->form_validation->set_rules($valid);
	}
}