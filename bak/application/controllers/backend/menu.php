<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu extends CI_Controller{
	private $option = array();
	private $position = array();
    function __construct(){
		parent::__construct();
		$this->lang->load('backend/menu');
		$this->load->setData('title',$this->lang->line('menu_title'));
        $this->config->load('block');
        $blockconfig = $this->config->item("block");
        $this->position = $blockconfig['menu']['position'];
	}
	public function index(){
		$this->load->model('menu_model','menu');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			if ($this->menu->delete() === false){
				$data['error'] = $this->lang->line('menu_error_del_parent');
			}
		}
        $this->load->helper('form');
        $filter['data'][] = $this->lang->line('menu_position').form_dropdown('position',$this->position,$this->input->get('position'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);

		$row = $this->menu->lists();
		$data['row'] = array();
		if (!empty($row)){
			foreach ($row as $row){
				$data['row'][$row['parent']][] = $row;
			}
		}
		$this->load->layout('menu/list',$data);
	}
	public function add(){
		$this->load->model('menu_model','menu');
        if ($this->input->post('menu_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->menu->insert();
                $this->session->set_userdata('success','add');
				redirect('menu/add');
			}
		}
		$row = set_array('name','item_mod','item_id','link',array('menu_id'=>0,'ordering' => 0,'parent' => 0,'position'=>'top','target' => '_parent'));
		$data = $this->_form($row);
		$this->load->layout('menu/form',$data);
	}
	public function edit($id=0){
        if ($id <= 0){
       	    show_404();
        }
        // load model
		$this->load->model('menu_model','menu');
		if ($this->input->post('menu_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->menu->update(intval($id));
                $this->session->set_userdata('success','edit');
				redirect(base64_decode($this->input->get("refer")));
			}
		}
		$row = $this->menu->detail(intval($id));
		if (empty($row)){
			show_404();
		}

		$data = $this->_form($row);
        $data['row'] = $row;
		$this->load->layout('menu/form',$data);
	}
	private function _select_parent($data,$parent = 0,$level = ''){
		if (!empty($data[$parent])){
			foreach ($data[$parent] as $rows){
			$this->option[$rows['position']][$rows['menu_id']] = $level.' '.$rows['name'];
			$this->_select_parent($data,$rows['menu_id'],$level.'---');
			}
		}
	}
	private function _form($row = array()){
        // add static
        $static[] = js('module/menu.js');
        $static[] = js('jquery-ui-1.10.0.custom.min.js');
        $static[] = link_tag('jquery-ui.css');
        $this->load->setArray("footer",$static);
        //////
        $opt_target = array(
            '_parent' => $this->lang->line('menu_target_parent'),
            '_blank' => $this->lang->line('menu_target_blank')
            );
        $opt_mod = array(
            'home' => $this->lang->line('common_home'),
            'news_cate' => $this->lang->line('common_mod_news_cate'),
            'product_cate' => $this->lang->line('common_mod_product_cate'),
            'contact' => $this->lang->line('common_mod_contact'),
            'advertise_cate' => $this->lang->line('common_mod_ads_cate'),
            //'product' => $this->lang->line('common_mod_product'),
            'news' => $this->lang->line('common_mod_news'),
            'link' => $this->lang->line('common_mod_link')
        );
        $row_parent = $this->menu->lists();
		$select = array();
		$this->option[0] = $this->lang->line('menu_parent');
		foreach ($row_parent as $row_parent){
			if ($row_parent['menu_id'] != $row['menu_id'])
			$select[$row_parent['parent']][] = $row_parent;
		}
        $data['paramold'] = json_encode(array('module' => $row['item_mod'], 'id' => (int)$row['item_id'], 'link' => $row['link']));
		$this->_select_parent($select);
		$this->load->helper ('form');
        $data['paramdefault'] = json_encode(array('module' => $row['item_mod'], 'id' => (int)$row['item_id'], 'link' => $row['link']));
		$data['module'] = form_dropdown('module',$opt_mod,$row['item_mod'],'id="menu_module"');
        $data['item_id'] = form_hidden("item_id",set_value('item_id',$row['item_id']),false,'id="item_id"');
        $data['getname'] = form_input('getname',set_value('getname'),'class="menu_getname" id="autoc"');
        $data['name'] = form_input('name',set_value('name',$row['name']));
		$data['ordering'] = form_input('ordering',set_value('ordering',$row['ordering']));
        $data['link'] = form_input('link',set_value('link',$row['link']),'id = "menu_link"');
		$data['position'] = form_dropdown('position',$this->position,$row['position']);
        $data['target'] = form_dropdown('target',$opt_target,$row['target']);
        $data['parent'] = form_dropdown('parent',$this->option,$row['parent']);
		$data['submit'] = form_submit('menu_submit',$this->lang->line('common_save'));
		return $data;
	}
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => $this->lang->line('menu_name'),
                 'rules'   => 'required'
              ),
           /*array(
                 'field'   => 'link',
                 'label'   => $this->lang->line('menu_link'),
                 'rules'   => 'is_spaces'
              ),*/
            array(
                 'field'   => 'ordering',
                 'label'   => $this->lang->line('menu_ordering'),
                 'rules'   => 'integer'
              ),
            array(
                 'field'   => 'position',
                 'label'   => $this->lang->line('menu_position'),
                 'rules'   => 'callback__position_check'
              )
		);
  		$this->form_validation->set_rules($valid);
	}
    public function _position_check($str){
        $id_parent = $this->input->post('parent');
        if ($id_parent == 0){
            return true;
        }
        else{
            $row = $this->menu->detail($id_parent);
            if ($row['position'] == $str){
                return true;
            }
            else{
                $this->form_validation->set_message('_position_check', $this->lang->line('menu_error_position'));
                return false;
            }
        }
    }
    public function autocomplete(){
        $module = $this->input->get('module');
        $word = $this->input->get('term');
        if ($module == "" OR $word == ""){
            return false;
        }
        $this->load->model('menu_model','menu');
        $row = $this->menu->getlink($module,$word);
        $out = json_encode($row);
        $this->output->set_output($out);
    }
} ?>