<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Advertise extends CI_Controller{
	private $error = array();
    private $option = array();
	function __construct(){
		parent::__construct();
		$this->lang->load('backend/advertise');
		$this->load->setData('title',$this->lang->line('adv_title'));
	}
	public function index(){
		$limit = 20;
		$this->load->model('advertise_model','adv');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			$row = $this->adv->delete();
			if (!empty($row)){
				$this->load->helper('images');
                foreach ($row as $row){
					if ($row['images'] != ''){
                        images_delete($row['images']);
                    }
				}
			}
		}
        /** FILTER **/
        $this->load->helper('form');
        $this->option[0] = $this->lang->line('adv_cate');
        $filter['data'][] = form_dropdown('cate',$this->cate_parent(),$this->input->get('cate'));
        $filter['data'][] = $this->lang->line('adv_name').form_input('name',$this->input->get('name'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
        /** PAGING **/
        $data['row'] = $this->adv->lists($limit);
        $config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
        /** DATA **/
        unset($data['row'][$limit]);
		$this->load->layout('advertise/list',$data);
	}
	public function add(){
		$this->load->model('advertise_model','adv');
		if ($this->input->post('adv_submit')){
            $rq = (!$_FILES['images']['tmp_name']) ? array('field'   => 'images', 'label'   => $this->lang->line('adv_image'), 'rules'   => 'required') : array();
            $this->_validation($rq);
			if ($this->form_validation->run() == true)
			{
                $this->load->helper('images');
                $img = images_upload('images');
                if ($img['error'] != ''){
                    $this->error['upload'] = $img['error'];
                }else {
                    $this->adv->insert($img['success']);
                    $this->session->set_userdata('success','add');
                    redirect('advertise/add');
                }
			}
		}
		$row = set_array('name','images',array('ordering'=>0,'publish' => 1,'cate_id' => 0),'link');
		$data = $this->_form($row);
		$this->load->layout('advertise/form',$data);
	}
	public function edit($id = 0){
		if ($id <= 0 OR !is_numeric($id)){
			show_404();
		}
		$this->load->model('advertise_model','adv');
		$row = $this->adv->detail(intval($id));
        if (empty($row)){
			show_404();
		}
		if ($this->input->post('adv_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
			    $this->load->helper('images');
                $img = images_upload('images');
                if ($img['error'] != ''){
                    $this->error['upload'] = $img['error'];
                }
                else{
                    if ($img['success'] != '' AND $row['images'] != ''){
                        images_delete($row['images']);
                    }
                    $this->adv->update(intval($id),$img['success']);
                    $this->session->set_userdata('success','edit');
				    redirect(base64_decode($this->input->get("refer")));
                }
			}
		}
		$data = $this->_form($row);
		$this->load->layout('advertise/form',$data);
	}
    /*
	private function _select_category($data,$parent = 0,$level = ''){
		if (!empty($data[$parent])){
			foreach ($data[$parent] as $rows){
			$this->option[$rows['cate_id']] = $level.' '.$rows['name'];
			$this->_select_category($data,$rows['cate_id'],$level.'---');
			}
		}
	}
    public function category(){
        $this->load->model('advertise_cate_model','cate');
        $cate = $this->cate->lists();
		$select = array();
		foreach ($cate as $cate){
			$select[$cate['parent']][] = $cate;
		}
		$this->_select_category($select);
        return $this->option;
    } */
	private function _form($row = array()){
		$this->load->helper('form');
        $cate = $this->cate_parent();
		$data['error_upload'] = $this->error['upload'];
		$data['name'] = form_input('name',set_value('name',$row['name']));
		$data['link'] = form_input('link',set_value('link',$row['link']));
		$data['ordering'] = form_input('ordering',set_value('ordering',$row['ordering']));
		$data['cate'] = form_dropdown('cate_id',$cate,$row['cate_id']);
		$data['publish'] = form_checkbox('publish',1,$row['publish']);
		$data['image'] = form_upload('images','','size="50"');
		$data['submit'] = form_submit('adv_submit',$this->lang->line('common_save'));
		return $data;
	}
	private function _validation($ext = array()){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => $this->lang->line('adv_name'),
                 'rules'   => 'required'
              ),
           	array(
                 'field'   => 'link',
                 'label'   => $this->lang->line('adv_link'),
                 'rules'   => 'required'
              ),
			array(
                 'field'   => 'ordering',
                 'label'   => $this->lang->line('adv_ordering'),
                 'rules'   => 'integer'
              ),
            array(
                 'field'   => 'cate_id',
                 'label'   => $this->lang->line('adv_cate'),
                 'rules'   => 'is_natural_no_zero'
              ),
		);
        if (!empty($ext)){
            array_push($valid,$ext);
        }
  		$this->form_validation->set_rules($valid);
	}
    //////////////////////////////// CATEGORY //////////////////////////////////
    public function cate_index(){
		$this->load->model('advertise_model','cate');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','cate_delete'))
		{
			if ($this->cate->cate_delete() === false){
				$data['error'] = $this->lang->line('adv_cate_delete_parent');
			}
		}
		$row = $this->cate->cate_lists();
		$data['row'] = array();
		if (!empty($row)){
			foreach ($row as $row){
				$data['row'][$row['parent']][] = $row;
			}
		}
		$this->load->layout('advertise/cate_list',$data);
	}
	public function cate_add(){
		$this->load->model('advertise_model','cate');
		if ($this->input->post('cate_submit')){
			$this->_cate_validation();
			if ($this->form_validation->run() == true)
			{
				$this->cate->cate_insert();
                $this->session->set_userdata('success','add');
				redirect('advertise/cate_add');
			}
		}
		$row = set_array('name',array('parent' => 0,'cate_id'=>0,'ordering' => 0));
		$data = $this->_cate_form($row);
		$this->load->layout('advertise/cate_form',$data);
	}
	public function cate_edit($id){
		$this->load->model('advertise_model','cate');
		if ($this->input->post('cate_submit')){
			$this->_cate_validation();
			if ($this->form_validation->run() == true)
			{
				$this->cate->cate_update(intval($id));
				redirect('advertise/cate_index');
			}
		}
		$row = $this->cate->cate_detail(intval($id));
		if (empty($row)){
			show_404();
		}
		$data = $this->_cate_form($row);
		$this->load->layout('advertise/cate_form',$data);
	}
	private function _cate_select_parent($data,$parent = 0,$level = ''){
		if (!empty($data[$parent])){
			foreach ($data[$parent] as $rows){
			$this->option[$rows['cate_id']] = $level.' '.$rows['name'];
			$this->_cate_select_parent($data,$rows['cate_id'],$level.'---');
			}
		}
	}
    public function cate_parent($choose = 0,$require = 1){
        $this->load->model('advertise_model','cate');
        $row_parent = $this->cate->cate_lists();
		$select = array();
        if ($require)
		$this->option[0] = $this->lang->line('adv_cate_select_parent');
		foreach ($row_parent as $row_parent){
			if ($row_parent['cate_id'] != $choose)
			$select[$row_parent['parent']][] = $row_parent;
		}
		$this->_cate_select_parent($select);
        return $this->option;
    }
	private function _cate_form($row = array()){
		$parent = $this->cate_parent($row['cate_id']);
		$this->load->helper ('form');
		$data['cate_name'] = form_input('name',set_value('name',$row['name']));
		$data['cate_ordering'] = form_input('ordering',set_value('ordering',$row['ordering']));
        $data['cate_parent'] = form_dropdown('parent',$parent,$row['parent'],set_select('parent',$row['parent']));
		$data['cate_submit'] = form_submit('cate_submit',$this->lang->line('common_save'));
		return $data;
	}
	// ok
	private function _cate_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => $this->lang->line('adv_cate_name'),
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}
}