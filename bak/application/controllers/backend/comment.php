<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Comment extends CI_Controller{
    function __construct(){
		parent::__construct();
		$this->load->setData('title','Quản lý comment');
	}
	public function index(){
        $limit = 20;
		$this->load->model('comment_model','comment');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			$row = $this->comment->delete();
		}
        $this->load->helper('form');
        /** FILTER **/
        $status = ($this->input->get("status")) ? $this->input->get("status") : 3;
        $filter['data'][] = form_dropdown('type',array(0=> "Tất cả bình luận",1 => "tin tức", 2 => "sản phẩm"),$this->input->get("type"));
        $filter['data'][] = form_dropdown('status',array(3=> "Tất cả tình trạng",1 => "Đã duyệt", 0 => "Chưa duyệt"),$status);
        $data['filter'] = $this->load->view("common/data_filter",$filter);
        /** fill data **/
		$data['row'] = $this->comment->lists($limit);
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
		$this->load->layout('comment/list',$data);
	}
    public function edit(){

		$this->load->model('comment_model','comment');
        $st = (int) $this->input->post("status");
        $st = ($st <= 0) ? 1 : 0;
        $id = (int) $this->input->post("id");
        if ($id <= 0){
			$this->output->set_output("false");
		}
		$this->comment->update($id,$st);

        $out = temp_status($st);
        $this->output->set_output($out);
	}
}