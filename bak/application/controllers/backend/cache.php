<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cache extends CI_Controller{
    private $path = '';
    function __construct(){
		parent::__construct();
        $this->lang->load('backend/mini');
		$this->load->setData('title',$this->lang->line('cache_title'));
        $path = APPPATH.'cache/';
        $this->path = rtrim($path, DIRECTORY_SEPARATOR);
	}
	public function index(){
        if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
        {
            $fd = $this->input->post('cid');
            if (!empty($fd)){
                $this->load->helper('file');
                foreach ($fd as $fd){
                    delete_files($this->path.$fd,TRUE,2);
                }
                $this->session->set_userdata('success','edit');
            }

		}
        $this->load->helper('directory');
        $data['dir'] = directory_map(APPPATH.'cache',2,FALSE,TRUE);
        $this->load->layout('mini/cache',$data);
    }
}