<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question_full extends CI_Controller{
	private $option = array();
    protected $type = array("1" => 'Full',"2" => 'Mini',"3" => "Skill");
    function __construct(){
		parent::__construct();
		$this->lang->load('backend/question');
		$this->load->setData('title','Quản lý Full test');
	}
	public function index(){
	    $limit = 20;
		$this->load->model('question_full_model','qfull');
		if ($this->input->post('delete') && $this->input->post('cid'))
		{
			$this->qfull->delete();
		}
        $data['show_type'] = $this->type;
		$data['row'] = $this->qfull->lists($limit);
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
		$this->load->layout('question/full_list',$data);
	}
	public function add(){
		$this->load->model('question_full_model','qfull');
		if ($this->input->post('qfull_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->qfull->insert();
                $this->session->set_userdata('success','add');
				redirect('question_full/add');
			}
		}
		$row = set_array('name','part1','part2','part3','type','part4','part5','part6','part7',array('time' => 120,'role' => array(0)));
		$data = $this->_form($row);
		$this->load->layout('question/full_form',$data);
	}
	public function edit($id){
		$this->load->model('question_full_model','qfull');
		if ($this->input->post('qfull_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{
				$this->qfull->update(intval($id));
				redirect('question_full');
			}
		}
        $row = $this->qfull->detail(intval($id));
        $this->load->model('group_model','group');
		$role = $this->group->get_group_by_full(intval($id));
        if (!empty($role)){
            foreach ($role as $role){
                $row['role'][] = $role['role_id'];
            }
        }
        else{
            $row['role'] = array(0);
        }
		if (empty($row)){
			show_404();
		}
		$data = $this->_form($row);
		$this->load->layout('question/full_form',$data);
	}
	private function _form($row = array()){
		$this->load->helper ('form');
        $this->load->model("group_model",'group');
        $r = $this->group->lists();
        $role[0] = 'Tất cả quyền hạn';
        foreach ($r as $r){
            $role[$r['id']] = $r['name'];
        }
        $roleselect = $row['role'];
		$data['name'] = form_input('name',set_value('name',$row['name']));
        $data['type'] = form_dropdown('type',$this->type,$row['type']);
        $data['part1'] = form_input('part1',set_value('part1',$row['part1']));
        $data['part2'] = form_input('part2',set_value('part2',$row['part2']));
        $data['part3'] = form_input('part3',set_value('part3',$row['part3']));
        $data['part4'] = form_input('part4',set_value('part4',$row['part4']));
        $data['part5'] = form_input('part5',set_value('part5',$row['part5']));
        $data['part6'] = form_input('part6',set_value('part6',$row['part6']));
        $data['part7'] = form_input('part7',set_value('part7',$row['part7']));
        $data['role'] = form_multiselect('role_id[]',$role,$roleselect);
        $data['time'] = form_input("time",set_value('time',$row['time']));
		$data['cate_submit'] = form_submit('qfull_submit',$this->lang->line('common_save'));
		return $data;
	}
	// ok
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => $this->lang->line('quest_cate_name'),
                 'rules'   => 'required'
              ),
           /*array(
                 'field'   => 'part1',
                 'label'   => 'part 1',
                 'rules'   => 'required|integer'
              ),
           array(
                 'field'   => 'part2',
                 'label'   => 'part 2',
                 'rules'   => 'required|integer'
              ),
           array(
                 'field'   => 'part3',
                 'label'   => 'part 3',
                 'rules'   => 'required|integer'
              ),
           array(
                 'field'   => 'part4',
                 'label'   => 'part 4',
                 'rules'   => 'required|integer'
              ),
           array(
                 'field'   => 'part5',
                 'label'   => 'part 5',
                 'rules'   => 'required|integer'
              ),
           array(
                 'field'   => 'part6',
                 'label'   => 'part 6',
                 'rules'   => 'required|integer'
              ),
           array(
                 'field'   => 'part7',
                 'label'   => 'part 7',
                 'rules'   => 'required|integer'
              ),
           array(
                 'field'   => 'part7',
                 'label'   => 'part 7',
                 'rules'   => 'required'
              ),*/
		);
  		$this->form_validation->set_rules($valid);
	}
} ?>