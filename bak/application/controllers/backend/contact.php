<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact extends CI_Controller{
    function __construct(){
		parent::__construct();
		$this->lang->load('backend/contact');
		$this->load->setData('title',$this->lang->line('contact_title'));
	}
	public function index(){
		$limit = 20;
        $script = array(
                link_tag('jquery_simpledialog.css'),
                js('jquery.simpledialog.0.1.js')
                );
        $this->load->setArray('script',$script);
        $this->load->model('contact_model','contact');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			$this->contact->delete();
		}
        $this->load->helper('form');
        $array = array(
                ''  => $this->lang->line('contact_status'),
                '0' => $this->lang->line('contact_status_unread'),
                '1' => $this->lang->line('contact_status_read')
                );
        $filter['data'][] = form_dropdown('status',$array,$this->input->get('status'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);
        $data['row'] = $this->contact->lists($limit);
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);
		$this->load->layout('contact/list',$data);
	}
    public function edit($id=0,$st=0){
        if ($id <= 0){
            exit();
        }
        if ($st == 1){
            $st = 0;
        }
        else{
            $st = 1;
        }
        $this->load->model('contact_model','contact');
        $this->contact->update_status($id,$st);
        $out = temp_status($st,array('id'=> 'status_'.$id,'onclick' => 'change_status('.$id.','.$st.')'));
        $this->output->set_output($out);
    }
}