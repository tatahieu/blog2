<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Writing extends CI_Controller{
    function __construct(){
		parent::__construct();
		$this->load->setData('title','Quản lý bài viết');
	}
    public function point(){
        $limit = 20;
		$this->load->model('writing_model','writing');
        /** FILTER **/
        $this->load->helper('form');
        $filter['data'][] = 'Thành viên: '.form_input('username',$this->input->get('username'));
        $filter['data'][] = 'Mã bài test: '.form_input('testid',$this->input->get('testid'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);

		$data['row'] = $this->writing->lists_point($limit);
        //var_dump('<pre>',$data['row']); die;
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);

		$this->load->layout('writing/point_list',$data);
    }
    public function point_detail(){
        $point = (int) $this->input->post("point");
        $answer_id = (int) $this->input->post("answer_id");
        $content = strip_tags($this->input->post("comment"));
        $arr['result'] = 'success';
        if ($answer_id < 1) {
            $arr['result'] = 'error';
            $arr['data'] = 'Không tìm thấy bài viết bạn chấm';
        }
        if ($point < 1) {
            $arr['result'] = 'error';
            $arr['data'] = 'Điểm không được chấm bằng 0';
        }
        if ($arr['result'] == 'success'){
            $arr['data'] = 'Đã lưu thông tin';
            $this->load->model('writing_model','writing');
            $this->writing->update_point_teacher($answer_id,$point,$content);
        }
        $this->output->set_output(json_encode($arr));
    }
	public function index(){
        $limit = 20;
		$this->load->model('writing_model','writing');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','delete'))
		{
			$row = $this->writing->delete();
			if (!empty($row)){
				$this->load->helper('images');
                foreach ($row as $row){
					if ($row['images'] != ''){
                        images_delete($row['images']);
                    }
				}
			}
            $this->session->set_userdata('success','delete');
		}
        $this->load->model("topic_model","topic");
        $data['type'] = get_writing_type();
        $data['level'] = get_level();
        $topic = $this->topic->lists();
        foreach ($topic as $topic){
            $data['topic'][$topic['topic_id']] = $topic['name'];
        }
        $this->load->helper('form');
        /** FILTER **/
        $this->load->helper('form');
        $level = $data['level'];
        array_unshift($level,'Tất cả');
        $filter['data'][] = 'Level: '.form_dropdown('level',$level,$this->input->get('level'));
        $data['filter'] = $this->load->view("common/data_filter",$filter);

		$data['row'] = $this->writing->lists($limit);
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);

		$this->load->layout('writing/list',$data);
	}
	public function add(){
        // load model
		$this->load->model('writing_model','writing');
		if ($this->input->post('writing_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{

                $this->load->helper('images');
                $img = images_upload('images');
                $param['images'] = ($img['success'] != '') ? $img['success'] : '';

                $this->writing->insert($param);
                $this->session->set_userdata('success','add');
                redirect('writing/add');
			}
		}
		$row = set_array('question','answer','images','sound',array('show_practice' => 1));
		$data = $this->_form($row);
		$this->load->layout('writing/form',$data);
	}
    public function edit($id = 0){
        $id = intval($id);
        if ($id <= 0){
			show_404();
		}
        //load model
		$this->load->model('writing_model','writing');
        $this->load->helper('images');
		$row = $this->writing->detail(intval($id));
        // delete images
        if ($this->input->post("delete_media")){
            switch ($this->input->post("delete_media")){
                case 'images':
                    images_delete($row['images']);
                    $this->db->set("images","");
                break;
            }
            $this->db->where("writing_id",$id);
            $this->db->update('writing_question');
            return $this->output->set_output(json_encode(array("status" => "success")));
		}
		if (empty($row)){
			show_404();
		}
        if ($this->input->post('writing_submit')){
			$this->_validation();
			if ($this->form_validation->run() == true)
			{


                // add media
                $img = images_upload('images');
                if ($img['success'] != '' AND $row['images'] != ''){
                    images_delete($row['images']);
                }
                $this->writing->update($id,array('images' => $img['success']));
                $this->session->set_userdata('success','edit');
			    redirect(base64_decode($this->input->get("refer")));
			}
		}

		$data = $this->_form($row);

        $data['row'] = $row;
		$this->load->layout('writing/form',$data);
	}

	private function _form($row = array()){
        // load form
		$this->load->helper('form');
        $this->load->model("topic_model","topic");
        $topic = $this->topic->lists();
        $t[0] = 'Chọn chủ đề';
        foreach ($topic as $topic){
            $t[$topic['topic_id']] = $topic['name'];
        }
        $level = get_level();

		$data['error_upload'] = $this->error['upload'];
        $data['title'] = form_input('title',set_value('title',$row['title']));
		$data['question'] = form_detail('question',$row['question'],array("height" => "200"));
		$data['images'] = form_upload('images','','size="50"');
        $data['answer'] = form_detail('answer',$row['answer'],array("height" => "200"));
        $data['level'] = form_dropdown('level',get_level(),$row['level']);
        $data['topic'] = form_dropdown('topic',$t,$row['topic']);
        $data['show_practice'] = form_checkbox('show_practice',1,$row['show_practice']);

		$data['submit'] = form_submit('writing_submit',$this->lang->line('common_save'));
		return $data;
	}
	private function _validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'title',
                 'label'   => 'Tiêu đề',
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}

    //////////////////////////////////////// TEST ///////////////////
    public function test_index(){
        $limit = 20;
		$this->load->model('writing_model','writing');
		if ($this->input->post('delete') && $this->input->post('cid') && $this->permission->check_permission_backend('','test_delete'))
		{
			$row = $this->writing->test_delete();
            $this->session->set_userdata('success','delete');
		}
        /** FILTER **/
        $this->load->helper('form');
        $level = get_level();
        array_unshift($level,'Tất cả');
        $pass = get_password();
        $password[null] = 'Tất cả';
        $password[0] = 'Không có password';
        foreach ($pass as $key => $pass) {
            $password[$key] = $pass['name'];
        }
        $passselect = ($this->input->get('password')) ? (int) $this->input->get("password") : null;
        $filter['data'][] = 'Level: '.form_dropdown('level',$level,$this->input->get('level'));
        $filter['data'][] = 'Part: '.form_dropdown('part',array(''=>'Tất cả',0 => 0,1=>1,2=>2,3=>3,4=>4,5=>5,6=>6),$this->input->get('part'));
        $filter['data'][] = 'Password: '.form_dropdown('password',$password,$passselect);
        $data['filter'] = $this->load->view("common/data_filter",$filter);
		$data['row'] = $this->writing->test_lists($limit);
		/** PAGING **/
		$config['total_rows'] = count($data['row']);
  		$config['per_page'] = $limit;
  		$this->load->library('paging',$config);
  		$data['paging'] = $this->paging->create_links();
		unset($data['row'][$limit]);

		$this->load->layout('writing/test_list',$data);
    }
    public function test_add(){
        // load model
		$this->load->model('writing_model','writing');
		if ($this->input->post('test_submit')){
			$this->_test_validation();
			if ($this->form_validation->run() == true)
			{

                $this->writing->test_insert();
                $this->session->set_userdata('success','add');
                redirect('writing/test_add');
			}
		}
		$row = set_array('name',array('role' => array(0)));
		$data = $this->_test_form($row);
		$this->load->layout('writing/test_form',$data);
    }
    public function test_edit($id){
        $id = intval($id);
        if ($id <= 0){
			show_404();
		}
        //load model
		$this->load->model('writing_model','writing');
		$row = $this->writing->test_detail(intval($id));
		if (empty($row)){
			show_404();
		}
        //////////// role edit ///////////
		$role = $this->writing->get_role_by_testid($id);
        if (!empty($role)){
            foreach ($role as $role){
                $row['role'][] = $role['role_id'];
            }
        }
        else{
            $row['role'] = array(0);
        }
        ////////// end role edit //////////
        if ($this->input->post('test_submit')){
			$this->_test_validation();
			if ($this->form_validation->run() == true)
			{
                $this->writing->test_update($id);
                $this->session->set_userdata('success','edit');
			    redirect(base64_decode($this->input->get("refer")));
			}
		}
		$data = $this->_test_form($row);
		$this->load->layout('writing/test_form',$data);
    }
    private function _test_form($row = array()){
        // load form
        $limit_per_cate = 30;
		$this->load->helper('form');
        // lay nhung loai part
        $part = get_writing_type();
        $select[0] = 'Fulltest - 0';
        foreach ($part as $keys => $value){
            $select[$keys] = $value . ' - '. $keys;
        }
        /////////////////// GROUP /////////////
        $this->load->model("group_model",'group');
        $r = $this->group->lists();
        $role[0] = 'Tất cả quyền hạn';
        foreach ($r as $r){
            $role[$r['id']] = $r['name'];
        }
        $roleselect = $row['role'];
        $data['role'] = form_multiselect('role_id[]',$role,$roleselect);
        //////////////// END GROUP ///////////
        $password = get_password();
        $arrpass[0] = 'Không đặt password';
        foreach ($password as $key => $pass){
            $arrpass[$key] = $pass['name'];
        }
        $data['password'] = form_dropdown('password',$arrpass,(int)$row['password']);
        $data['fulllevel'] = get_level();
        $data['jstype'] = json_encode($this->writing->type_to_question);
        $data['suggest'] = $this->writing->get_latest_question($limit_per_cate);
        $data['level'] = form_dropdown('level',get_level(),$row['level']);
		$data['error_upload'] = $this->error['upload'];
		$data['name'] = form_input('name',set_value('name',$row['name']));
		//// part 1

        for ($i = 1; $i <= 8; $i++){
            $data['question_'.$i] = form_input('question_'.$i,set_value('question_'.$i,$row['question_'.$i]));
        }
        $data['type'] = form_dropdown('type',$select,$row['type'],'onchange="change_type_test(this.value)" id="writing_type"');
		$data['submit'] = form_submit('test_submit',$this->lang->line('common_save'));
		return $data;
    }
    private function _test_validation(){
		$this->load->library('form_validation');
		$valid = array(
			array(
                 'field'   => 'name',
                 'label'   => 'Tên',
                 'rules'   => 'required'
              )
		);
  		$this->form_validation->set_rules($valid);
	}
}