<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question_cate_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($level = NULL){
		if (!is_null($level)){
			$this->db->where('parent',$level);
		}
		$this->db->order_by('ordering');
		$this->db->where('parent >=',0);
		$query = $this->db->get('question_cate');
		return $query->result_array();
    }
    public function detail($id){
    	$this->db->where('cate_id',$id);
    	$query = $this->db->get('question_cate',1);
    	return $query->row_array();
    }
    private function _input(){
		$input = array(
			'name' => $this->input->post('name'),
			'ordering' => intval($this->input->post('ordering')),
			'parent' => $this->input->post('parent'),
            'description' => $this->input->post('description'),
            'relate_news' => (int)$this->input->post("relate_news")
		);
		return $input;
    }
    public function insert(){
    	$input = $this->_input();
		$this->db->insert('question_cate',$input);
    }
    public function update($id){
    	$input = $this->_input();
		$this->db->where('cate_id',$id);
		$this->db->update('question_cate',$input);
    }
    public function delete(){
		$id = $this->input->post('cid');
		if ($this->_check_delete($id) === true){
			$this->db->where_in('cate_id',$id);
			$this->db->delete('question_cate');
			/** XOA DOC_TO_CATE **/
			$this->db->where_in('cate_id',$id);
			$this->db->delete('question_to_cate');
		}
		else{
			return false;
		}
    }
    private function _check_delete($id){
    	$this->db->select('cate_id');
		$this->db->where_in('parent',$id);
    	$this->db->where_not_in('cate_id',$id);
    	$query = $this->db->get('question_cate',1);
		$row = $query->row_array();
    	if (empty($row)) return true;
		else return false;
    }
}