<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Topic_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($limit = 10){
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if ($array['level']){
                $this->db->where_in('level',array($array['level'],0));
            }
            if ($array['type']){
                $this->db->where('type',$array['type']);
            }
        }
        $this->db->order_by('topic_id DESC');
		$query = $this->db->get('topic');
		return $query->result_array();
    }
    public function detail($id,$select = null){
		$this->db->where('topic_id',$id);
    	$query = $this->db->get('topic',1);
    	return $query->row_array();
    }
    public function insert(){
    	$input = $this->_input();
		$input['creation_time'] = time();
    	$this->db->insert('topic',$input);
    }
    public function update($id){
		$input = $this->_input();
		$this->db->where('topic_id',$id);
		$this->db->update('topic',$input);
    }
    public function delete(){
    	/** Liet ke danh sach file va anh can xoa **/
		$id = $this->input->post('cid');
 		/** xoa news **/
		$this->db->where_in('topic_id',$id);
		$this->db->delete('topic');
    }
    private function _input(){
		$input = array(
			'name' => $this->input->post('name'),
            'alias' => set_alias_link($this->input->post('name')),
            'type' => (int) $this->input->post('type'),
            'level' => (int) $this->input->post('level')
		);
		return $input;
    }
}