<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($limit = 10){
		$offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if (array_key_exists('cate',$array) AND $array['cate'] > 0){
                $this->db->join('news_to_cate','news_to_cate.news_id = news.news_id');
                $this->db->where('news_to_cate.cate_id',$array['cate']);
            }
            if (array_key_exists('name',$array)){
                $this->db->like('title',$array['name']);
            }
            if (array_key_exists('special',$array)){
                $this->db->where('special',1);
            }
        }
        $this->db->order_by('date_up DESC, news_id DESC');
		$this->db->select('news.news_id,title,date_up,special,hit,publish');
		$query = $this->db->get('news',$limit+1,$offset);
		return $query->result_array();
    }
    public function detail($id,$select = null){
    	$this->db->select("n.*,c.name as cate_name");
        $this->db->join("news_cate as c","n.original_cate = c.cate_id","LEFT");
		$this->db->where('n.news_id',$id);
    	$query = $this->db->get('news as n',1);
    	return $query->row_array();
    }
    public function get_cate($id){
    	$this->db->where('news_id',$id);
    	$query = $this->db->get('news_to_cate');
    	return $query->result_array();
    }
    public function insert($img){
    	$input = $this->_input();
		$input2 = array(
			'images' => $img,
            'user_id' => $this->session->userdata('userid'),
            'date_create' => time()
			);
		$input = array_merge($input,$input2);
        $role = $this->input->post("role_id");

        $role = ($role) ? $role : array(0);
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }

    	$this->db->insert('news',$input);
    	$news_id = (int)$this->db->insert_id();
        // update share url
        $share_url = '/'.set_alias_link($input['title']).'-nd'.$news_id;
        if ($input['original_cate'] > 0) {
            $catedetail = $this->cate_detail($input['original_cate']);
            if ($catedetail['style'] == 2) {
                $share_url = '/video/detail/'.set_alias_link($input['title']).'-'.$news_id.'.html';
            }
        }
        $this->db->set("share_url",$share_url);
        $this->db->where("news_id",$news_id);
        $this->db->update('news');
        // insert category
        $cate = $this->input->post('category');
    	if (is_array($cate)){
    	    $cate = array_filter($cate);
    	    if (!in_array($input['original_cate'],$cate)){
                array_push($cate,$input['original_cate']);
    	    }
	    	foreach ($cate as $cate){
   	            if ($cate > 0 && $news_id > 0){
					$input_cate[] = array(
						'cate_id' => $cate,
						'news_id' => $news_id
					);
                }
            }
            if (!empty($input_cate)){
            $this->db->insert_batch('news_to_cate',$input_cate);
            }
	    }
        // insert roles
        if ($sum > 0 and $news_id > 0){
            foreach ($role as $role){
    			if ($role > 0){
                    $r[] = array(
        				'news_id' => $news_id,
        				'role_id' => $role
        			);
                }
            }
            if (!empty($r)){
                $this->db->insert_batch('news_to_role',$r);
            }
        }
        // insert tags
        $tags = $this->input->post("tags");
        if (!empty($tags)){
            $this->_news_to_tag($tags,$news_id);
        }
        return $news_id;
    }
    public function update($id,$img = ''){
		$input = $this->_input();
		if ($img != ''){
			$input['images'] = $img;
		}
        ////// update share_url ////////
        $share_url = '/'.set_alias_link($input['title']).'-nd'.$id;
        if ($input['original_cate'] > 0) {
            $catedetail = $this->cate_detail($input['original_cate']);
            if ($catedetail['style'] == 2) {
                $share_url = '/video/detail/'.set_alias_link($input['title']).'-'.$id.'.html';
            }
        }
        $input['share_url'] = $share_url;
        /// update role for class
        $role = $this->input->post("role_id");
        $role = ($role) ? $role : array(0);
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }
        /// end update detail ////

		$this->db->where('news_id',$id);
		$this->db->update('news',$input);
		/** Xoa news_cate **/
		$this->db->where('news_id',$id);
		$this->db->delete('news_to_cate');
		/** Cap nhat thong tin moi **/
 		$cate = $this->input->post('category');
    	if (is_array($cate)){
            $cate = array_filter($cate);
    	    if (!in_array($this->input->post("original_cate"),$cate)){
                array_push($cate,$this->input->post("original_cate"));
    	    }
	    	foreach ($cate as $cate){
   	            if ($cate > 0 && $id > 0){
					$input_cate[] = array(
						'cate_id' => $cate,
						'news_id' => $id
					);
                }
            }
            if (!empty($input_cate)){
            $this->db->insert_batch('news_to_cate',$input_cate);
            }
	    }
        // edit menu
        $this->db->where("item_mod","news");
        $this->db->where("item_id",$id);
        $query = $this->db->get("menus");
        $row = $query->result_array();
        foreach ($row as $row){
            $this->db->set("link",$input['share_url']);
            $this->db->where("menu_id",$row['menu_id']);
            $this->db->update("menus");
        }
        // edit role
        $this->db->where("news_id",$id);
        $this->db->delete("news_to_role");

        if ($sum > 0){
            foreach ($role as $role){
    			if ($role > 0){
                    $r[] = array(
        				'news_id' => $id,
        				'role_id' => $role
        			);
                }
            }
            if (!empty($r)){
                $this->db->insert_batch('news_to_role',$r);
            }
        }
        // edit tags
        $tags = $this->input->post("tags");
        if (!empty($tags)){
            $this->_news_to_tag($tags,$id,1);
        }
    }
    /**
     * type = 0: add, type = 1: edit
     */
    private function _news_to_tag($tags,$news_id,$type = 0){
        if ($type == 1){
            $this->db->where("news_id",$news_id);
            $this->db->delete('news_to_tags');
        }
        if (!$tags) return false;
        // trim al element
        $checkexit = array();
        foreach ($tags as $tag){
            $tag = mb_strtolower(trim($tag),'utf8');
            if (in_array($tag,$checkexit)){
                continue;
            }
            else{
                $checkexit[] = $tag;
            }
            $this->db->select("tag_id");
            $this->db->where("name",$tag);
            $query = $this->db->get("tags");
            $rows = $query->row_array();
            // neu chua co tag nao
            if (empty($rows)){
                $input = array(
                    'name' => $tag,
                    'alias' => set_alias_link($tag)
                );
                $this->db->insert("tags",$input);
                $input_tag[] = array('tag_id' => (int)$this->db->insert_id(), 'news_id' => $news_id);
            }
            else{
                $input_tag[] = array('tag_id' => (int)$rows['tag_id'], 'news_id' => $news_id);
            }
        }
        if (!empty($input_tag)){

            $this->db->insert_batch('news_to_tags',$input_tag);
        }
    }
    public function suggest_tag($name){
        $this->db->select("name");
        $this->db->like("name",$name);
        $query = $this->db->get("tags",10);
        return $query->result_array();
    }
    public function get_tags($id){
        $this->db->select("t.name");
        $this->db->where('n.news_id',$id);
        $this->db->join("tags as t",'t.tag_id = n.tag_id');
        $query = $this->db->get("news_to_tags as n");
        return $query->result_array();
    }
    public function delete(){
    	/** Liet ke danh sach file va anh can xoa **/
		$id = $this->input->post('cid');
    	$this->db->select('images');
		$this->db->where_in('news_id',$id);
		$query = $this->db->get('news');
		$row = $query->result_array();
 		/** xoa news **/
		$this->db->where_in('news_id',$id);
		$this->db->delete('news');
		/** xoa news_to_cat **/
		$this->db->where_in('news_id',$id);
		$this->db->delete('news_to_cate');
        /** xoa news_to_tag **/
        $this->db->where_in('news_id',$id);
		$this->db->delete('news_to_tags');
		return $row;
    }
    private function _input(){
		$input = array(
			'title' => $this->input->post('title'),
			'detail' => $this->input->post('detail'),
            'special' => intval($this->input->post('special')),
			'publish' => intval($this->input->post('publish')),
            'original_cate' => intval($this->input->post("original_cate")),
            'date_up' => convert_datetime($this->input->post('date_up')),
			'description' => $this->input->post('description'),

            'seo_title' => $this->input->post('seo_title'),
            'seo_desc' => $this->input->post('seo_desc')
		);
		return $input;
    }
    //////////////////////////////////// CATEGORY /////////////////////////
    public function cate_lists($level = NULL){
		if (!is_null($level)){
			$this->db->where('parent',$level);
		}
		$this->db->order_by('ordering');
		$this->db->where('parent >=',0);
		$query = $this->db->get('news_cate');
		return $query->result_array();
    }
    public function cate_detail($id){
    	$this->db->where('cate_id',$id);
    	$query = $this->db->get('news_cate',1);
    	return $query->row_array();
    }
    public function cate_insert(){
    	$input = $this->_cate_input();
		$this->db->insert('news_cate',$input);
        $cateid = (int)$this->db->insert_id();
        // update share_url
        $this->db->where("cate_id",$cateid);
        if ($input['style'] == 2){
            $this->db->set("share_url",'/video/cate/'.set_alias_link($input['name']).'-'.$cateid.'.html');
        }
        else{
            $this->db->set("share_url",'/'.trim($input['alias'],'/').'-nl'.$cateid);
        }

        $this->db->update("news_cate");
    }
    public function cate_update($id){
    	$input = $this->_cate_input();
        if ($input['style'] == 2){
            $this->db->set("share_url",'/video/cate/'.set_alias_link($input['name']).'-'.$id.'.html');
        }
        else{
            $input['share_url'] = '/'.trim($input['alias'],'/').'-nl'.$id;
        }
		$this->db->where('cate_id',$id);
		$this->db->update('news_cate',$input);
        // edit menu
        $this->db->where("item_mod","news_cate");
        $this->db->where("item_id",$id);
        $query = $this->db->get("menus");
        $row = $query->result_array();
        $param = array('id' => $id, 'alias' => $input['alias'], 'module' => 'news_cate');
        foreach ($row as $row){
            $this->db->set("link",set_url($param));
            $this->db->where("menu_id",$row['menu_id']);
            $this->db->update("menus");
        }
    }
    private function _cate_input(){
		$input = array(
			'name' => $this->input->post('name'),
			'ordering' => intval($this->input->post('ordering')),
			'parent' => $this->input->post('parent'),
            'metakey' => $this->input->post("metakey"),
            'metadesc' => $this->input->post("metadesc"),
            'alias' => set_alias_link($this->input->post('name')),
            'style' => (int) $this->input->post("style")
		);
		return $input;
    }
    public function cate_delete(){
		$id = $this->input->post('cid');
		if ($this->_cate_check_delete($id) === true){
			$this->db->where_in('cate_id',$id);
			$this->db->delete('news_cate');
			/** XOA DOC_TO_CATE **/
			$this->db->where_in('cate_id',$id);
			$this->db->delete('news_to_cate');
		}
		else{
			return false;
		}
    }
    private function _cate_check_delete($id){
    	$this->db->select('cate_id');
		$this->db->where_in('parent',$id);
    	$this->db->where_not_in('cate_id',$id);
    	$query = $this->db->get('news_cate',1);
		$row = $query->row_array();
    	if (empty($row)) return true;
		else return false;
    }
}