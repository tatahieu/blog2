<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Block_model extends CI_Model {
    //public $permission = array();
	function __construct()
    {
        parent::__construct();
    }
    public function lists(){
		$this->db->order_by('position,ordering');
		if ($this->input->get('module')){
            $this->db->where('module',$this->input->get('module'));
		}
		$query = $this->db->get('block');
		return $query->result_array();
    }
    public function detail($id){
    	$this->db->where('block_id',$id);
    	$query = $this->db->get('block',1);
    	return $query->row_array();
    }
    private function _input(){
        $param = ($this->input->post("params")) ? json_encode($this->input->post("params")) : json_encode(array());
		$input = array(
			'name' => $this->input->post('name'),
			'ordering' => intval($this->input->post('ordering')),
			'position' => $this->input->post('position'),
            'content' => $this->input->post("content"),
            'publish' => intval($this->input->post("publish")),
            'device'  => (int) $this->input->post("device"),
            'params' => $param
		);
		return $input;
    }
    public function insert($module){
    	$input = $this->_input();
        $input['module'] = $module;
		$this->db->insert('block',$input);
        $blockid = (int)$this->db->insert_id();
        // insert to access
        $this->_update_block_to_mod($blockid,'add');
    }
    public function update($id){
    	$input = $this->_input();
		$this->db->where('block_id',$id);
		$this->db->update('block',$input);
        // update to access
        $this->_update_block_to_mod($id,'update');
    }
    public function delete(){
		$id = $this->input->post('cid');
	    $this->db->where_in('block_id',$id);
        $this->db->delete('block');
        // delete to access
        $this->_update_block_to_mod($id,'delete');

    }
    private function _update_block_to_mod($blockid,$type = 'add'){
        if ($type == 'update' || $type == 'delete'){
            (is_array($blockid)) ? $this->db->where_in("block_id",$blockid) : $this->db->where("block_id",$blockid);
            $this->db->delete("block_to_module");
        }
        if ($type == 'update' || $type == 'add'){
            if ($this->input->post("access") == 'all'){
                $input[] = array("block_id" => $blockid,"module" => "all");
            }
            else{
                $access = $this->input->post("moduleaccess");
                if (!empty($access)){
                    foreach ($access as $access){
                        $input[] = array("block_id" => $blockid,"module" => $access);
                    }
                }
                else{
                    $input[] = array("block_id" => $blockid,"module" => "all");
                }
            }
            $this->db->insert_batch("block_to_module",$input);
        }
    }
    public function get_access_by_blockid($blockid){
        $this->db->where("block_id",$blockid);
        $query = $this->db->get("block_to_module");
        return $query->result_array();
    }
    //// block for admin (menu)
    public function get_all_block(){
        $this->load->model('system_model','system');
        //$array = $this->session->userdata('permission');
        //$this->permission = $array;

        $out = '<ul id="common_menu">';
        $menu = array(
                'menu' => array(
                            "name" => $this->lang->line('common_mod_menu'),
                            "items" => array('add','index')
                            ),
                'news' => array(
                            "name" => $this->lang->line('common_mod_news'),
                            "items" => array('add','index',
                                            array(
                                                'name' => $this->lang->line('common_add').' '.$this->lang->line('common_mod_news_cate'),
                                                'action' => 'cate_add'
                                                ),
                                            array(
                                                'name' => $this->lang->line('common_index').' '.$this->lang->line('common_mod_news_cate'),
                                                'action' => 'cate_index'
                                                ),
                                        )
                            ),
                'question' => array(
                            "name" => "Câu hỏi",
                            "items" => array('add','index',
                                    array(
                                        'name' => 'Thêm bài test',
                                        'class' => 'question_test',
                                        'action' => 'add'
                                    ),
                                    array(
                                        'name' => 'Danh sách bài test',
                                        'class' => 'question_test',
                                        'action' => 'index'
                                    ),
                                    array(
                                        'name' => 'Thêm nhóm câu hỏi',
                                        'action' => 'cate_add'
                                    ),
                                    array(
                                        'name' => 'DS nhóm câu hỏi',
                                        'action' => 'cate_index'
                                    ),
                                    array(
                                        'name' => 'Thêm fulltest',
                                        'action' => 'full_add'
                                    ),
                                    array(
                                        'name' => 'Danh sách fulltest',
                                        'action' => 'full_index'
                                    ),


                            )
                ),
                'topic' => array(
                            "name" => 'Chủ đề',
                            "items" => array('add','index')
                            ),
                'course' => array(
                            "name" => 'Bài test',
                            "items" => array(
                                            array(
                                                'name' => 'Thêm từ',
                                                'action' => 'word_add'
                                                ),
                                            array(
                                                'name' => 'Danh sách từ',
                                                'action' => 'word_index'
                                                ),
                                            array(
                                                'name' => 'Thêm bài điền từ',
                                                'action' => 'com_add'
                                                ),
                                            array(
                                                'name' => 'Danh sách câu điên từ',
                                                'action' => 'com_index'
                                                )
                                        )
                            ),
                'speaking'  => array(
                            "name" => 'Kỹ năng nói',
                            "items" => array('add','index',
                                        array(
                                            'name' => 'Thêm bài test',
                                            'action' => 'test_add'
                                            ),
                                        array(
                                            'name' => 'Danh sách bài test',
                                            'action' => 'test_index'
                                            ),
                                        array(
                                            'name' => 'Chấm điểm',
                                            'action' => 'point'
                                            )
                                        )
                 ),
                 'writing'  => array(
                            "name" => 'Kỹ năng viết',
                            "items" => array('add','index',
                                        array(
                                            'name' => 'Thêm bài test',
                                            'action' => 'test_add'
                                            ),
                                        array(
                                            'name' => 'Danh sách bài test',
                                            'action' => 'test_index'
                                            ),
                                        array(
                                            'name' => 'Chấm điểm',
                                            'action' => 'point'
                                            )

                                        )
                 ),
                'advertise' => array(
                            "name" => $this->lang->line('common_mod_advertise'),
                            "items" => array('add','index',
                                            array(
                                                'name' => $this->lang->line('common_add').' '.$this->lang->line('common_mod_adv_cate'),
                                                'action' => 'cate_add'
                                                ),
                                            array(
                                                'name' => $this->lang->line('common_index').' '.$this->lang->line('common_mod_adv_cate'),
                                                'action' => 'cate_index'
                                                )
                                        )
                            ),
                'block' => array(
                            "name" => $this->lang->line('common_mod_block'),
                            "items" => array('add','index')
                            ),
                'support' => array(
                            "name" => $this->lang->line('common_mod_support'),
                            "items" => array('add','index')
                            ),
                'contact' => array(
                            "name" => $this->lang->line('common_mod_contact'),
                            'items' => array('index')
                            ),
                'users' => array(
                            "name" => $this->lang->line('common_mod_users'),
                            "items" => array('index',
                                            array(
                                                'name' => $this->lang->line('common_add').' '.$this->lang->line('common_mod_users_role'),
                                                'action' => 'role_add'
                                                ),
                                            array(
                                                'name' => $this->lang->line('common_index').' '.$this->lang->line('common_mod_users_role'),
                                                'action' => 'role_index'
                                                )
                                        )
                            ),
                'group' => array(
                            "name" => 'Lớp học',
                            "items" => array("add","index")
                            ),
                'setting' => array(
                            "name" => $this->lang->line('common_mod_system'),
                            'items' => array(
                                            array(
                                                'name' => $this->lang->line('common_mod_setting'),
                                                'action' => 'index'
                                                )
                                        )
                            ),
            );
        $out .= $this->_render_menu($menu);
        $out .= '</ul>';
        $this->load->setData('left_menu',$out);
        $this->load_static();
        $this->load->setArray("script",$this->success($this->session->userdata('success')));
	}
    private function _render_menu($arr){
        //var_dump($this->permission); die;
        $out = '<ul id="common_menu">';
        foreach ($arr as $key => $value){
                $i = 1;
                // menu level 1
                $it = '';
                foreach ($value['items'] as $item){

                    $class = (is_array($item) && $item['class']) ?  $item['class'] : $key;
                    $action = (is_array($item) && $item['action']) ? $item['action'] : $item;
                    if ($this->permission->check_permission_backend($class,$action)){

                        $link = (is_array($item) && $item['link']) ? $item['link'] : $class.'/'.$action;
                        if (is_array($item)){
                            $it .= '<div class="submenu"><a href="'.site_url($link).'">'.$item['name'].'</a></div>';
                        }
                        else {
                            $it .= '<div class="submenu"><a href="'.site_url($link).'">'.$this->lang->line('common_'.$item).' '.$value['name'].'</a></div>';
                        }
                    }
                    $i ++;
                }
                if ($i > 1){
                    $out .= '<li id="menu_'.$key.'">';
                    $out .= '<div class="left_menu">'.$value['name'].'</div>';
                    $out .= $it;
                    $out .= '</li>';
                }
        }
        $out .= '</ul>';
        return $out;
    }
    private function load_static(){
        $data[] = link_tag('tqn_portal.css');
		$data[] = '<script>
           var $base_url = \''.base_url().'\';
           var $site_url = \''.site_url().'/\';
           var $api_image_detail = \''.UPLOAD_URL.'images/userfiles/\';
           </script>';
        $data[] = js('jquery-1.8.3.min.js');
        $this->load->setArray('header',$data);
        $script[] = js('libraries.js');
        $script[] = js('jquery.quicksearch.js');
        $this->load->setArray("footer",$script);
    }
    private function success($type){
        $this->session->unset_userdata('success');
        $txt = '<script>var action_success = \''.$type.'\'</script>';
        $this->load->setArray("header",$txt);

    }
}