<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question_full_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($limit){
		$offset = 0;
        $array = $this->input->get(NULL);
        if (array_key_exists('page',$array)){
            $offset = ($array['page'] - 1) * $limit;
        }
        $this->db->order_by('full_id','DESC');
		$query = $this->db->get('question_full',$limit+1,$offset);
		return $query->result_array();
    }
    public function detail($id){
    	$this->db->where('full_id',$id);
    	$query = $this->db->get('question_full',1);
    	return $query->row_array();
    }
    private function _input(){
		$input = array(
			'name' => $this->input->post('name'),
			'part1' => (int) $this->input->post("part1"),
            'part2' => (int) $this->input->post("part2"),
            'part3' => (int) $this->input->post("part3"),
            'part4' => (int) $this->input->post("part4"),
            'part5' => (int) $this->input->post("part5"),
            'part6' => (int) $this->input->post("part6"),
            'part7' => (int) $this->input->post("part7"),
            'time' => (float) $this->input->post("time"),
            'type' => (int) $this->input->post("type"),
            'show_answer' => (int) $this->input->post("show_answer")
		);
		return $input;
    }
    public function insert(){
	    $input = $this->_input();
        // insert fulltest_to_roles
        $role = $this->input->post("role_id");
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }
		$this->db->insert('question_full',$input);
        $full_id = $this->db->insert_id();
        if ($sum > 0 and $full_id > 0){
            foreach ($role as $role){
    			if ($role > 0){
                    $r[] = array(
        				'full_id' => $full_id,
        				'role_id' => $role
        			);
                }
            }
            if (!empty($r)){
                $this->db->insert_batch('fulltest_to_role',$r);
            }
        }
    }
    public function update($id){
    	$input = $this->_input();
		$role = $this->input->post("role_id");
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }
        else{
            $input['isclass'] = 0;
        }


        $this->db->where('full_id',$id);
		$this->db->update('question_full',$input);
        // edit fulltest_To_roles
        $this->db->where("full_id",$id);
        $this->db->delete("fulltest_to_role");

        if ($sum > 0){
            foreach ($role as $role){
    			if ($role > 0){
                    $r[] = array(
        				'full_id' => $id,
        				'role_id' => $role
        			);
                }
            }
            if (!empty($r)){
                $this->db->insert_batch('fulltest_to_role',$r);
            }
        }
    }
    public function delete(){
		$id = $this->input->post('cid');
		$this->db->where_in('full_id',$id);
		$this->db->delete('question_full');

        $this->db->where_in("full_id",$id);
        $this->db->delete("fulltest_to_role");
    }
    public function full_score($params){
        $array = $this->input->get(NULL);
        $params = array_merge(array('limit' => 10, 'offset' => 0),$params);
        $this->db->join('member as m','m.user_id = q.user_id');
        $this->db->where('test_id',$params['test_id']);
        if (!empty($array)){
            if ($array['email']){
                $this->db->where('m.email',$array['email']);
            }
            if ($array['from_date']){
                $this->db->where('q.create_time >',convert_datetime($array['from_date'],3));
            }
            if ($array['to_date']){
                $this->db->where('q.create_time <',convert_datetime($array['to_date'],4));
            }
        }
        $this->db->order_by('score_id','DESC');
        $query = $this->db->get('question_score as q',$params['limit'],$params['offset']);
        return $query->result_array();
    }
}