<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Group_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists(){
		$this->db->order_by('id','desc');
		$query = $this->db->get('class');
		return $query->result_array();
    }
    public function get_group_by_full($id){
        $this->db->where("full_id",$id);
        $query = $this->db->get("fulltest_to_role");
        return $query->result_array();
    }
    public function get_group_by_test($id){
        $this->db->where("test_id",$id);
        $query = $this->db->get("test_to_role");
        return $query->result_array();
    }
    public function get_group_by_news($id){
        $this->db->where("news_id",$id);
        $query = $this->db->get("news_to_role");
        return $query->result_array();
    }
    public function detail($id){
    	$this->db->where('id',$id);
    	$query = $this->db->get('class');
    	return $query->row_array();
    }
    private function _input(){
		$input = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),

		);
		return $input;
    }
    public function insert(){
    	$input = $this->_input();
        $input['password'] = base64_encode($this->input->post('password'));
		$this->db->insert('class',$input);
    }
    public function update($id){
    	$input = $this->_input();
        if ($this->input->post('password')){
            $input['password'] = base64_encode($this->input->post('password'));
        }
		$this->db->where('id',$id);
		$this->db->update('class',$input);
    }
    public function delete(){
		$id = $this->input->post('cid');
		$this->db->where_in('id',$id);
		$this->db->delete('class');
    }
}