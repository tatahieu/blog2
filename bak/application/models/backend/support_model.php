<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Support_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists(){
		$this->db->order_by('ordering');
		$query = $this->db->get('support');
		return $query->result_array();
    }
    public function detail($id){
    	$this->db->where('sup_id',$id);
    	$query = $this->db->get('support');
    	return $query->row_array();
    }
    private function _input(){
		$input = array(
			'ordering' => intval($this->input->post('ordering')),
			'job' => $this->input->post('job'),
			'yahoo' => $this->input->post('yahoo'),
			'skype' => $this->input->post('skype'),
			'phone' => $this->input->post('phone')
		);
		return $input;
    }
    public function insert(){
    	$input = $this->_input();
		$this->db->insert('support',$input);
    }
    public function update($id){
    	$input = $this->_input();
		$this->db->where('sup_id',$id);
		$this->db->update('support',$input);
    }
    public function delete(){
		$id = $this->input->post('cid');
		$this->db->where_in('sup_id',$id);
		$this->db->delete('support');
    }
}