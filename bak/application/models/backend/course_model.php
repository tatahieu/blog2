<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Course_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    /////////////////////////// WORD //////////////////////////
    public function word_lists($limit = 10){
		$offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if ($array['level']){
                $this->db->where_in('c.level',array($array['level'],0));
            }
        }
        $this->db->order_by('word_id DESC');
        $this->db->join("topic as t","t.topic_id = c.topic_id","LEFT");
		$this->db->select("c.word_id,c.word,c.level,t.name as topic_name");
		$query = $this->db->get('course_word as c',$limit+1,$offset);
		return $query->result_array();
    }
    public function word_detail($id,$select = null){
    	$this->db->select("*");
		$this->db->where('word_id',$id);
    	$query = $this->db->get('course_word',1);
    	return $query->row_array();
    }
    public function word_insert($img){
    	$input = $this->_word_input();
		$input2 = array(
			'images' => $img,
            'user_id' => $this->session->userdata('userid'),
            'date_up' => time()
			);
		$input = array_merge($input,$input2);
    	$this->db->insert('course_word',$input);
    }
    public function word_update($id,$img = ''){
		$input = $this->_word_input();
		if ($img != ''){
			$input['images'] = $img;
		}
		$this->db->where('word_id',$id);
		$this->db->update('course_word',$input);
    }
    public function word_delete(){
    	/** Liet ke danh sach file va anh can xoa **/
		$id = $this->input->post('cid');
    	$this->db->select('images');
		$this->db->where_in('word_id',$id);
		$query = $this->db->get('course_word');
		$row = $query->result_array();
 		/** xoa word **/
		$this->db->where_in('word_id',$id);
		$this->db->delete('course_word');
		return $row;
    }
    private function _word_input(){
		$input = array(
			'english' => $this->input->post('english'),
			'vietnamese' => $this->input->post('vietnamese'),
            'word' => $this->input->post('word'),
			'exam' => $this->input->post('exam'),
            'phienam' => $this->input->post("phienam"),
            'level' => (int) $this->input->post("level"),
            'topic_id' => (int) $this->input->post("topic"),
		);
		return $input;
    }
    /////////////////////////// WORD //////////////////////////
    public function com_lists($limit = 10){
		$offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if ($array['level']){
                $this->db->where_in('c.level',array($array['level'],0));
            }
        }
        $this->db->order_by('com_id DESC');
        $this->db->join("topic as t","t.topic_id = c.topic_id","LEFT");
		$this->db->select("c.com_id,c.question,c.level,c.topic_id,images,audio,t.name as topic_name");
		$query = $this->db->get('course_complete as c',$limit+1,$offset);
		return $query->result_array();
    }
    public function com_detail($id,$select = null){
    	$this->db->select("*");
		$this->db->where('com_id',$id);
    	$query = $this->db->get('course_complete',1);
    	return $query->row_array();
    }
    public function com_insert($img = '',$audio = ''){
    	$input = $this->_com_input();
		$input2 = array(
			'images' => $img,
            'audio' => $audio,
            'user_id' => $this->session->userdata('userid'),
            'date_up' => time()
			);
		$input = array_merge($input,$input2);
    	$this->db->insert('course_complete',$input);
    }
    public function com_update($id,$img = '',$audio = ''){
		$input = $this->_com_input();
		if ($img != ''){
			$input['images'] = $img;
		}
        if ($audio != ''){
            $input['audio'] = $audio;
        }
		$this->db->where('com_id',$id);
		$this->db->update('course_complete',$input);
    }
    public function com_delete(){
    	/** Liet ke danh sach file va anh can xoa **/
		$id = $this->input->post('cid');
    	$this->db->select('images,audio');
		$this->db->where_in('com_id',$id);
		$query = $this->db->get('course_complete');
		$row = $query->result_array();
 		/** xoa word **/
		$this->db->where_in('com_id',$id);
		$this->db->delete('course_complete');
		return $row;
    }
    private function _com_input(){
		$input = array(
			'question' => $this->input->post('question'),
			'answer' => json_encode(explode(',',$this->input->post('answer'))),
            'level' => (int) $this->input->post("level"),
            'topic_id' => (int) $this->input->post("topic"),
		);
		return $input;
    }
}