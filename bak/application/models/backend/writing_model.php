<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Writing_model extends CI_Model {
    public $type_to_question = array(
                                1 => array(1,2,3,4,5),
                                2 => array(6,7),
                                3 => array(8)
                            );
	function __construct()
    {
        parent::__construct();
    }
    public function lists_point($limit = 10){
        $offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
        }
        $this->db->select("u.*,t.name as test_name,m.username");
        $this->db->join("writing_test as t","t.test_id = u.test_id");
        $this->db->join("member as m","m.user_id = u.user_id");
        $this->db->group_by("u.time,u.test_id");
        $this->db->order_by("time DESC");
        if ($array['username']){
            $this->db->where("m.username",$array['username']);
        }
        if ($array['testid']){
            $this->db->where("u.test_id",(int)$array['testid']);
        }
        $query = $this->db->get("writing_question_to_user as u",$limit + 1,$offset);
        return $query->result_array();
    }
    public function update_point_teacher($answer_id,$point,$comment = ''){
        $this->db->set("point_teacher",$point);
        $this->db->set("comment_teacher",$comment);
        $this->db->where("answer_id",$answer_id);
        $this->db->update("writing_question_to_user");
        // luu log
        $input = array(
            'user_id'   => $this->session->userdata("userid"),
            'action'    => 2,
            'time'      => time(),
            'param'    => json_encode(array("answer_id" => $answer_id,"point" => $point))
        );
        $this->db->insert("member_logs",$input);
    }
    public function lists($limit = 10){
		$offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if (array_key_exists('topic',$array)){
                $this->db->where('topic',$array['topic']);
            }
            if (array_key_exists('level',$array)){
                $this->db->where('level',$array['level']);
            }
            if (array_key_exists('topic',$array)){
                $this->db->where('topic',$array['topic']);
            }
        }
        $this->db->order_by('writing_id','DESC');
		$query = $this->db->get('writing_question',$limit+1,$offset);
		return $query->result_array();
    }
    public function detail($id,$select = null){
    	$this->db->where('writing_id',$id);
    	$query = $this->db->get('writing_question',1);
    	return $query->row_array();
    }
    public function insert($param){
    	$input = $this->_input();
		$input2 = array(
			'images'    => $param['images'],
            'user_id'   => $this->session->userdata('userid'),
            'date_up'   => time()
			);
		$input = array_merge($input,$input2);
    	$this->db->insert('writing_question',$input);
    	$quest_id = (int)$this->db->insert_id();
        // update share url
        return $quest_id;
    }
    public function update($id,$param){
		$input = $this->_input();
		if ($param['images'] != ''){
			$input['images'] = $param['images'];
		}
		$this->db->where('writing_id',$id);
		$this->db->update('writing_question',$input);
    }

    public function delete(){
    	/** Liet ke danh sach file va anh can xoa **/
		$id = $this->input->post('cid');
    	$this->db->select('images');
		$this->db->where_in('writing_id',$id);
		$query = $this->db->get('writing_question');
		$row = $query->result_array();
 		/** xoa writing **/
		$this->db->where_in('writing_id',$id);
		$this->db->delete('writing_question');
        return $row;
    }
    private function _input(){
		$input = array(
			'title' => $this->input->post('title'),
			'question' => $this->input->post('question'),
            'answer'  => $this->input->post('answer'),
            'topic' => intval($this->input->post('topic')),
            'level' => intval($this->input->post('level')),
		);
		return $input;
    }

    ///////////// test ////////
    public function get_latest_question($limit){
        $return = array();
        $this->db->select("writing_id,title,level");
        $this->db->order_by('writing_id','DESC');
        $query = $this->db->get("writing_question",$limit);
        return $query->result_array();


    }
    public function test_detail($id){
        $this->db->select();
        $this->db->where("test_id",$id);
        $query = $this->db->get("writing_test",1);
        return $query->row_array();
    }
    public function test_insert(){
        $input = $this->_test_input();
        $input['user_id'] = $this->session->userdata("userid");
        $input['date_up'] = time();
        ///////////// add role ////////
        $role = $this->input->post("role_id");
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }
        ///// end add role //////
        $this->db->insert("writing_test",$input);
        $testid = (int)$this->db->insert_id();
        $level = get_code_by_id($input['level']);
        $level = ($level) ? $level : 'fulltest';
        if ($testid > 0){
            /// add to group /////////
            if ($sum > 0){
                foreach ($role as $role){
        			if ($role > 0){
                        $r[] = array(
            				'writing_test_id' => $testid,
            				'role_id' => $role
            			);
                    }
                }
                if (!empty($r)){
                    $this->db->insert_batch('writing_test_to_role',$r);
                }
            }
            /////// update share url /////
            $shareurl = '/writing/'.$level.'/'.set_alias_link($input['name']).'-'.$testid.'.html';
            $this->db->set("share_url",$shareurl);
            $this->db->where("test_id",$testid);
            $this->db->update("writing_test");
        }
    }
    public function test_update($id){
        $input = $this->_test_input();
        ////////////// group ////////
        $role = $this->input->post("role_id");
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }
        else{
            $input['isclass'] = 0;
        }
        /////////////// level /////////
        $level = get_code_by_id($input['level']);
        $level = ($level) ? $level : 'fulltest';
        $input['share_url'] = '/writing/'.$level.'/'.set_alias_link($input['name']).'-'.$id.'.html';
        $this->db->where("test_id",$id);
        $this->db->update("writing_test",$input);
        // edit writing to test role
        $this->db->where("writing_test_id",$id);
        $this->db->delete("writing_test_to_role");
        if ($sum > 0){
            foreach ($role as $role){
    			if ($role > 0){
                    $r[] = array(
        				'writing_test_id' => $id,
        				'role_id' => $role
        			);
                }
            }
            if (!empty($r)){
                $this->db->insert_batch('writing_test_to_role',$r);
            }
        }
    }
    public function test_lists($limit){
        $offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if ($array['level']){
                $this->db->where("level",(int)$array['level']);
            }
            if (is_numeric($array['part'])){
                $this->db->where("type",(int)$array['part']);
            }
            if (is_numeric($array['password'])){
                $this->db->where('password',$array['password']);
            }
        }
        $this->db->order_by('test_id','DESC');
		$query = $this->db->get('writing_test',$limit+1,$offset);
		return $query->result_array();
    }
    public function test_delete(){
		$id = $this->input->post('cid');
		$this->db->where_in('test_id',$id);
		$this->db->delete('writing_test');
        return $row;
    }
    private function _test_input(){
        $type = (int)$this->input->post("type");
        $filter = $this->type_to_question;

        if (!array_key_exists($type,$filter)){
            for ($i = 1; $i <= 8; $i ++){
                $input['question_'.$i] = (int)$this->input->post("question_".$i);
                if (!$input['question_'.$i]){
                    $isnotfulltest = 1;
                }
            }
        }
        else{
            $filter = $filter[$type];
            for ($i = 1; $i <= 8; $i ++){
                if (in_array($i,$filter)){
                    $input['question_'.$i] = (int)$this->input->post("question_".$i);
                }
                else{
                    $input['question_'.$i] = null;
                }
                if (!$input['question_'.$i]){
                    $isnotfulltest = 1;
                }
            }
        }

        $input['level'] = (int) $this->input->post("level");
        $input['name'] = $this->input->post("name");
        $input['type'] = $type;
        $input['password'] = (int) $this->input->post("password");
        return $input;
    }
    // get thong tin cac quyen lop hoc hien dang co
    public function get_role_by_testid($id){
        $this->db->where("writing_test_id",$id);
        $query = $this->db->get("writing_test_to_role");
        return $query->result_array();
    }
}