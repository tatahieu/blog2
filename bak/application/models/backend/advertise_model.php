<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Advertise_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($limit = 10){
        $offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if (array_key_exists('cate',$array) AND $array['cate'] > 0){
                $this->db->where('a.cate_id',$array['cate']);
            }
            if (array_key_exists('name',$array)){
                $this->db->like('a.name',$array['name']);
            }
        }
        $this->db->select('a.*,c.name as cate_name');
		$this->db->order_by('a.cate_id,ordering');
        $this->db->join('advertise_cate as c','c.cate_id = a.cate_id','LEFT');
		$query = $this->db->get('advertise as a',$limit+1,$offset);
		return $query->result_array();
    }
    public function detail($id){
		$this->db->where('adv_id',$id);
    	$query = $this->db->get('advertise',1);
    	return $query->row_array();
    }
    public function insert($img){
    	$input = $this->_input();
		$input['images'] = $img;
        $input['date_up'] = time();
    	$this->db->insert('advertise',$input);
    }
    public function update($id,$img = null){
		$input = $this->_input();
		if ($img != ''){
		  $input['images'] = $img;
		}
		$this->db->where('adv_id',$id);
		$this->db->update('advertise',$input);
    }
    public function delete(){
    	/** Liet ke danh sach anh can xoa **/
		$id = $this->input->post('cid');
    	$this->db->select('images');
		$this->db->where_in('adv_id',$id);
		$query = $this->db->get('advertise');
		$row = $query->result_array();
 		/** xoa adv **/
		$this->db->where_in('adv_id',$id);
		$this->db->delete('advertise');
		return $row;
    }
    private function _input(){
		$input = array(
			'name' => $this->input->post('name'),
			'link' => $this->input->post('link'),
			'cate_id' => $this->input->post('cate_id'),
			'ordering' => intval($this->input->post('ordering')),
			'publish' => intval($this->input->post('publish'))
		);
		return $input;
    }
    ////////////////////////////////// CATEGORY /////////////////////////////////
    public function cate_lists($level = NULL){
		if (!is_null($level)){
			$this->db->where('parent',$level);
		}
		$this->db->order_by('ordering,cate_id DESC');
		$this->db->where('parent >=',0);
		$query = $this->db->get('advertise_cate');
		return $query->result_array();
    }
    public function cate_detail($id){
    	$this->db->where('cate_id',$id);
    	$query = $this->db->get('advertise_cate',1);
    	return $query->row_array();
    }
    private function _cate_input(){
		$input = array(
			'name' => $this->input->post('name'),
			'ordering' => (int)$this->input->post('ordering'),
			'parent' => $this->input->post('parent'),
            'alias' => set_alias_link($this->input->post("name"))
		);
		return $input;
    }
    public function cate_insert(){
    	$input = $this->_cate_input();
		$this->db->insert('advertise_cate',$input);

        // update share_url
        $cateid = (int)$this->db->insert_id();
        $this->db->where("cate_id",$cateid);
        $this->db->set("share_url",'/'.trim($input['alias'],'/').'-ads'.$cateid);
        $this->db->update("advertise_cate");
    }
    public function cate_update($id){
    	$input = $this->_cate_input();
        // add input share_url
        $input['share_url'] = '/'.trim($input['alias'],'/').'-ads'.$id;
		$this->db->where('cate_id',$id);
		$this->db->update('advertise_cate',$input);
    }
    public function cate_delete(){
		$id = $this->input->post('cid');
		if ($this->_cate_check_delete($id) === true){
			$this->db->where_in('cate_id',$id);
			$this->db->delete('advertise_cate');
		}
		else{
			return false;
		}
    }
    private function _cate_check_delete($id){
    	$this->db->select('cate_id');
		$this->db->where_in('parent',$id);
    	$this->db->where_not_in('cate_id',$id);
    	$query = $this->db->get('advertise_cate',1);
		$row = $query->row_array();
    	if (!empty($row)){
    	   return false;
    	}
        $this->db->select("adv_id");
        $this->db->where_in('cate_id',$id);
       	$query = $this->db->get('advertise',1);
		$row = $query->row_array();
        if (!empty($row)){
            return false;
        }
        return true;
    }
}