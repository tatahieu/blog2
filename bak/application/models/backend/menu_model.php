<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($level = NULL){
		if (!is_null($level)){
			$this->db->where('parent',$level);
		}
        else{
            $this->db->where('parent >=',0);
        }
		$this->db->order_by('position,ordering');
		if ($this->input->get('position')){
            $this->db->where('position',$this->input->get('position'));
		}
		$query = $this->db->get('menus');
		return $query->result_array();
    }
    public function detail($id){
    	$this->db->where('menu_id',$id);
    	$query = $this->db->get('menus',1);
    	return $query->row_array();
    }
    private function _input(){
        $link = trim($this->input->post('link'));
        $link = (strpos($link,'://')) ? $link : '/'.trim($link,'/');
		$input = array(
			'name' => $this->input->post('name'),
			'ordering' => intval($this->input->post('ordering')),
			'parent' => $this->input->post('parent'),
            'target' => $this->input->post('target'),
            'position' => $this->input->post('position'),
            'link'  => $link,
            'item_id' => (int) $this->input->post('item_id'),
            'item_mod' => $this->input->post("module")
		);
        return $input;
    }
    public function insert(){
    	$input = $this->_input();
		$this->db->insert('menus',$input);
    }
    public function update($id){
    	$input = $this->_input();
		$this->db->where('menu_id',$id);
		$this->db->update('menus',$input);
    }
    public function delete(){
		$id = $this->input->post('cid');
		if ($this->_check_delete($id) === true){
		  $this->db->where_in('menu_id',$id);
		  $this->db->delete('menus');
          return true;
		}
        else{
            return false;
        }
    }
    public function getlink($module,$word){
        $type = (is_numeric($word)) ? 1 : 0;
        switch ($module){
            case 'news':
            $this->db->select('title as label,news_id as id,share_url');
            $this->db->from('news');
            ($type) ? $this->db->where("news_id",$word) : $this->db->like('title',$word,'left');
            $this->db->where('publish',1);
            $this->db->order_by('news_id','DESC');
            break;
            case 'product':
            $this->db->select('pro_id as id,title as label,share_url');
            $this->db->from('product');
            ($type) ? $this->db->where("pro_id",$word) : $this->db->like('title',$word);
            $this->db->where('publish',1);
            $this->db->order_by('pro_id','DESC');
            break;
            case 'news_cate':
            $this->db->select('cate_id as id,name as label,share_url');
            $this->db->from('news_cate');
            ($type) ? $this->db->where("cate_id",$word) : $this->db->like('name',$word);
            break;
            case 'product_cate':
            $this->db->select('cate_id as id,name as label,share_url');
            $this->db->from('product_cate');
            ($type) ? $this->db->where("cate_id",$word) : $this->db->like('name',$word);
            break;
            case 'advertise_cate':
            $this->db->select('cate_id as id,name as label,share_url');
            $this->db->from('advertise_cate');
            ($type) ? $this->db->where("cate_id",$word) : $this->db->like('name',$word);
            break;
            default:
            return false;
        }
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result_array();
    }
    private function _check_delete($id){
    	$this->db->select('menu_id');
		$this->db->where_in('parent',$id);
    	$this->db->where_not_in('menu_id',$id);
    	$query = $this->db->get('menus',1);
		$row = $query->row_array();
    	if (empty($row)) return true;
		else return false;
    }
}