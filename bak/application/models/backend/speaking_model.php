<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Speaking_model extends CI_Model {
    public $type_to_question = array(
                                    1 => array(1,2),
                                    2 => array(3),
                                    3 => array(4,5,6),
                                    4 => array(7,8,9),
                                    5 => array(10),
                                    6 => array(11),
                                );
	function __construct()
    {
        parent::__construct();
    }
    public function lists_point($limit = 10){
        $offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
        }
        $this->db->select("u.*,t.name as test_name,m.username");
        $this->db->join("speaking_test as t","t.test_id = u.test_id");
        $this->db->join("member as m","m.user_id = u.user_id");
        if ($array['username']){
            $this->db->where("m.username",$array['username']);
        }
        if ($array['testid']){
            $this->db->where("u.test_id",(int)$array['testid']);
        }
        $this->db->group_by("u.time,u.test_id");
        $this->db->order_by("time DESC");
        $query = $this->db->get("speaking_question_to_user as u",$limit + 1,$offset);
        return $query->result_array();
    }
    public function update_point_teacher($record_id,$point,$comment){
        $this->db->set("point_teacher",$point);
        $this->db->set("comment_teacher",$comment);
        $this->db->where("record_id",$record_id);
        $this->db->update("speaking_question_to_user");
        // luu log
        $input = array(
            'user_id'   => $this->session->userdata("userid"),
            'action'    => 1,
            'time'      => time(),
            'param'    => json_encode(array("record_id" => $record_id,"point" => $point))
        );
        $this->db->insert("member_logs",$input);
    }

    public function lists($limit = 10){
		$offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if (array_key_exists('level',$array)){
                $this->db->where('level',$array['level']);
            }

            if (array_key_exists('topic',$array)){
                $this->db->where('topic',$array['topic']);
            }
        }
        $this->db->order_by('speaking_id','DESC');
		$query = $this->db->get('speaking_question',$limit+1,$offset);
		return $query->result_array();
    }
    public function detail($id,$select = null){
    	$this->db->where('speaking_id',$id);
    	$query = $this->db->get('speaking_question',1);
    	return $query->row_array();
    }
    public function insert($param){
    	$input = $this->_input();
		$input2 = array(
			'images'    => $param['images'],
            'sound'     => $param['sound'],
            'answer_sound' => $param['answer_sound'],
            'user_id'   => $this->session->userdata('userid'),
            'date_up'   => time()
			);
		$input = array_merge($input,$input2);
    	$this->db->insert('speaking_question',$input);
    	$news_id = (int)$this->db->insert_id();
        // update share url
        return $news_id;
    }
    public function update($id,$param){
		$input = $this->_input();
		if ($param['images'] != ''){
			$input['images'] = $param['images'];
		}
        if ($param['sound'] != ''){
            $input['sound'] = $param['sound'];
        }
        if ($param['answer_sound'] != ''){
            $input['answer_sound'] = $param['answer_sound'];
        }
		$this->db->where('speaking_id',$id);
		$this->db->update('speaking_question',$input);
    }

    public function delete(){
    	/** Liet ke danh sach file va anh can xoa **/
		$id = $this->input->post('cid');
    	$this->db->select('images,sound,answer_sound');
		$this->db->where_in('speaking_id',$id);
		$query = $this->db->get('speaking_question');
		$row = $query->result_array();
 		/** xoa speaking **/
		$this->db->where_in('speaking_id',$id);
		$this->db->delete('speaking_question');
        return $row;
    }
    private function _input(){
		$input = array(
			'title' => $this->input->post('title'),
			'content' => $this->input->post('content'),
            'topic' => intval($this->input->post('topic')),
            'level' => intval($this->input->post('level')),
            'prepare_time' => intval($this->input->post('prepare_time')),
            'response_time' => intval($this->input->post('response_time')),
            'answer_text' => $this->input->post('answer_text')
		);
		return $input;
    }
    ///////////// question ////////
    public function get_latest_question($limit = 10){
        $return = array();
        $this->db->select("speaking_id,title,level");
        $this->db->order_by('speaking_id','DESC');
        $query = $this->db->get("speaking_question",$limit);
        return $query->result_array();


    }
    public function test_detail($id){
        $this->db->select();
        $this->db->where("test_id",$id);
        $query = $this->db->get("speaking_test",1);
        return $query->row_array();
    }
    public function test_insert(){
        $input = $this->_test_input();
        $input['user_id'] = $this->session->userdata("userid");
        $input['date_up'] = time();
        ///////////// add role ////////
        $role = $this->input->post("role_id");
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }
        ///// end add role //////
        $this->db->insert("speaking_test",$input);
        $testid = (int)$this->db->insert_id();
        $level = get_code_by_id($input['level']);
        $level = ($level) ? $level : 'fulltest';
        if ($testid > 0){
            /// add to group
            if ($sum > 0){
                foreach ($role as $role){
        			if ($role > 0){
                        $r[] = array(
            				'speaking_test_id' => $testid,
            				'role_id' => $role
            			);
                    }
                }
                if (!empty($r)){
                    $this->db->insert_batch('speaking_test_to_role',$r);
                }
            }
            /// update share url
            $shareurl = '/speaking/'.$level.'/'.set_alias_link($input['name']).'-'.$testid.'.html';
            $this->db->set("share_url",$shareurl);
            $this->db->where("test_id",$testid);
            $this->db->update("speaking_test");
        }
    }
    public function test_update($id){
        $input = $this->_test_input();
        ////////////// group ////////
        $role = $this->input->post("role_id");
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }
        else{
            $input['isclass'] = 0;
        }
        /////////////// level /////////
        $level = get_code_by_id($input['level']);
        $level = ($level) ? $level : 'fulltest';
        $input['share_url'] = '/speaking/'.$level.'/'.set_alias_link($input['name']).'-'.$id.'.html';
        $this->db->where("test_id",$id);
        $this->db->update("speaking_test",$input);
        // edit speaking to test role
        $this->db->where("speaking_test_id",$id);
        $this->db->delete("speaking_test_to_role");
        if ($sum > 0){
            foreach ($role as $role){
    			if ($role > 0){
                    $r[] = array(
        				'speaking_test_id' => $id,
        				'role_id' => $role
        			);
                }
            }
            if (!empty($r)){
                $this->db->insert_batch('speaking_test_to_role',$r);
            }
        }
    }
    public function test_lists($limit){
        $offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if ($array['level']){
                $this->db->where("level",(int)$array['level']);
            }
            if (is_numeric($array['part'])){
                $this->db->where("type",(int)$array['part']);
            }
            if (is_numeric($array['password'])){
                $this->db->where('password',$array['password']);
            }
        }
        $this->db->order_by('test_id','DESC');
		$query = $this->db->get('speaking_test',$limit+1,$offset);
		return $query->result_array();
    }
    public function test_delete(){
		$id = $this->input->post('cid');
		$this->db->where_in('test_id',$id);
		$this->db->delete('speaking_test');
        return $row;
    }
    private function _test_input(){
        $type = (int)$this->input->post("type");
        $filter = $this->type_to_question;

        if (!array_key_exists($type,$filter)){
            for ($i = 1; $i <= 11; $i ++){
                $input['question_'.$i] = (int)$this->input->post("question_".$i);
            }
        }
        else{
            $filter = $filter[$type];
            for ($i = 1; $i <= 11; $i ++){
                if (in_array($i,$filter)){
                    $input['question_'.$i] = (int)$this->input->post("question_".$i);
                }
                else{
                    $input['question_'.$i] = null;
                }
            }
        }
        $input['level'] = (int) $this->input->post("level");
        $input['name'] = $this->input->post("name");
        $input['type'] = $type;
        $input['password'] = (int) $this->input->post("password");
        return $input;
    }
    // get thong tin cac quyen lop hoc hien dang co
    public function get_role_by_testid($id){
        $this->db->where("speaking_test_id",$id);
        $query = $this->db->get("speaking_test_to_role");
        return $query->result_array();
    }
}