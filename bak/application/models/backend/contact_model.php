<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($limit = 10){
        $array = $this->input->get(NULL); $offset = 0;
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1)*$limit;
            }
            if (array_key_exists('status',$array)){
                $this->db->where('status',$array['status']);
            }
        }
		$this->db->order_by('status,contact_id DESC');
		$query = $this->db->get('contact',$limit+1,$offset);
		return $query->result_array();
    }
    public function delete(){
    	$id = $this->input->post('cid');
    	$this->db->where_in('contact_id',$id);
		$this->db->delete('contact');
    }
    public function update_status($id,$st){
        $this->db->where('contact_id',$id);
        $this->db->set('status',$st);
        $this->db->update('contact');
    }
}