<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model {
	public $paging = '';
	function __construct()
    {
		parent::__construct();
    }
    function update($id){
		$input = array(
			'username' => $this->input->post('username'),
			'roles_id' => intval($this->input->post('roles')),
			'active' => intval($this->input->post('active')),
			'email' => $this->input->post('email'),
			'fullname' => $this->input->post('fullname'),
			'sex' => intval($this->input->post('sex')),
			'yahoo' => $this->input->post('yahoo'),
			'skype' => $this->input->post('skype')
		);
		if ($this->input->post('password')){
			$input['password'] = md5($this->input->post('username').$this->input->post('password'));
		}
		$this->db->where('user_id',$id);
		$this->db->update('member',$input);
	}
	function lists($limit = 10){
		$array = $this->input->get(NULL);
        $offset = 0;
        if ($array) {
    		if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1)*$limit;
    		}
    		if ($array['username']){
                $this->db->where('username',$array['username']);
            }
            if ($array['email']){
                $this->db->where('email',$array['email']);
            }
            if (array_key_exists('active',$array) && $array['active'] > 1){
                $active = ($array['active'] == 1) ? 1 : 0;
                $this->db->where('active',$active);
            }
        }
        $this->db->from('member');
		$this->db->join('member_roles','member_roles.roles_id = member.roles_id','left');
        $this->db->order_by('user_id','DESC');
		$this->db->select('user_id,username,email,fullname,active,name');
		$this->db->limit($limit+1,$offset);
		$query = $this->db->get();
		return $query->result_array();
	}
	function detail($id){
		$this->db->where('user_id',$id);
		$query = $this->db->get('member',1);
		return $query->row_array();
	}
	function delete(){
		$id = $this->input->post('cid');
		$this->db->where_in('user_id',$id);
		$this->db->delete('member');
	}
 	function check_exist($field,$value){
    	$this->db->where($field,$value);
    	$count = $this->db->count_all_results('member');
    	if ($count > 0){
    		return false;
    	}
    	else{
    		return true;
    	}
    }
	function check_login(){
		if ($this->input->post('log_username') && $this->input->post('log_password')){
    		$this->db->join('member_roles','member_roles.roles_id = member.roles_id',"LEFT");
    		$this->db->select('user_id,password,username,fullname,where_live,phone,birthday,member_roles.name,member_roles.permission');
    		$this->db->where('username',$this->input->post('log_username'));
            $this->db->where('password',md5($this->input->post('log_username').$this->input->post('log_password')));
    		$this->db->where('active',1);
	    	$this->db->limit(1);
    		$query =  $this->db->get('member');
    		return $query->row_array();
    	}
    	return;
	}
    ////////////////////// ROLE /////////////////////////////
    function role_detail($id){
		$this->db->where('roles_id',$id);
		$query = $this->db->get('member_roles',1);
		return $query->row_array();
	}
	function role_lists(){
		$this->db->select('roles_id,name');
		$query = $this->db->get('member_roles');
		return $query->result_array();
	}
	function role_insert(){
		$input = $this->_role_input();
		$this->db->insert('member_roles',$input);
	}
	function role_update($id){
		$input = $this->_role_input();
		$this->db->where('roles_id',$id);
		$this->db->update('member_roles',$input);
	}
	function role_delete(){
		$id = $this->input->post('cid');
		$this->db->where_in('roles_id',$id);
		$this->db->delete('member_roles');
	}
	function _role_input(){
		$input = array(
			'name' => $this->input->post('name'),
			'permission' => serialize($this->input->post('permission'))
		);
		return $input;
	}
}