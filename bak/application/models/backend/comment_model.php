<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Comment_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($limit = 20){
		$offset = 0;
        $array = $this->input->get(NULL);
        if (array_key_exists('page',$array)){
            $offset = ($array['page'] - 1) * $limit;
        }
        if (array_key_exists('type',$array) AND $array['type'] > 0){
            $this->db->where("type",$array['type']);
        }
        if (array_key_exists('status',$array) AND $array['status'] < 3){
            $this->db->where("status",$array['status']);
        }
        $this->db->order_by('status,comment_id DESC');
		$this->db->select('*');
		$query = $this->db->get('comment',$limit+1,$offset);
		return $query->result_array();
    }

    public function update($id,$status){
        $this->db->set("status",$status);
		$this->db->where("comment_id",$id);
        $this->db->update("comment");
    }
    public function delete(){
		$id = $this->input->post('cid');
		$this->db->where_in('comment_id',$id);
		$this->db->delete('comment');
    }
}