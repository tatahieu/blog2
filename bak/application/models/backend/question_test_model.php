<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question_test_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($limit = 10){
		$offset = 0;
        $array = $this->input->get(NULL);
        if ($array){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if (array_key_exists('name',$array)){
                $this->db->like('title',$array['name']);
            }
            if (array_key_exists('cate',$array) AND $array['cate'] > 0){
                $this->db->where('question_test.cate_id',$array['cate']);
            }
            if (array_key_exists('parent',$array) AND $array['parent'] > 0){
                $this->db->where("question_test.parent",$array['parent']);
                $this->db->select("sound");
            }
            else{
                $this->db->where("question_test.parent",0);
            }
        }
        $this->db->join('question_cate','question_test.cate_id = question_cate.cate_id','LEFT');
        $this->db->order_by('qtest_id DESC');
		$this->db->select('qtest_id,title,publish,part,question_test.description,name');
		$query = $this->db->get('question_test',$limit+1,$offset);
		return $query->result_array();
    }
    public function detail($id,$select = null){
    	if (is_null($select)){
			$select = 'title,publish,cate_id,part,sound,parent,description';
		}
		$this->db->select($select);
		$this->db->where('qtest_id',$id);
    	$query = $this->db->get('question_test',1);
    	return $query->row_array();
    }
    public function insert($sound){
    	// insert test
        $input = $this->_input();

        $role = $this->input->post("role_id");
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }


		$input['date_up'] = date('Y-m-d H:i:s');
    	if ($this->input->get('parent')){
    	   $input['sound'] = $sound;
    	   $input['parent'] = (int)$this->input->get('parent');
           $input['cate_id'] = 0;
           $input['part'] = 0;
    	}
        $this->db->insert('question_test',$input);

        // insert question to test
        $qt = $this->input->post('question');
        $qt = array_filter(array_unique($qt));
        $tid = $this->db->insert_id();
        if (is_array($qt) AND !empty($qt) AND $tid > 0){
            foreach ($qt as $qt){
                if ($qt > 0){
                    $this->db->set("test_id",$tid);
                    $this->db->where("question_id",$qt);
                    $this->db->update("question");
                }
            }
    	}
        if ($sum > 0 and $tid > 0){
            foreach ($role as $role){
    			if ($role > 0){
                    $r[] = array(
        				'test_id' => $tid,
        				'role_id' => $role
        			);
                }
            }
            if (!empty($r)){
                $this->db->insert_batch('test_to_role',$r);
            }
        }
    }
    public function update($id,$sound = ''){
		$input = $this->_input();
        $role = $this->input->post("role_id");
        $sum = array_sum($role);
        if ($sum > 0){
            $input['isclass'] = 1;
        }
        else{
            $input['isclass'] = 0;
        }

        if ($sound != ''){
            $input['sound'] = $sound;
        }
		$this->db->where('qtest_id',$id);
		$this->db->update('question_test',$input);
        // insert lai
        $qt = $this->input->post('question');
        $qt = array_filter(array_unique($qt));
        $tid = $id;
        if (is_array($qt) AND !empty($qt) AND $tid > 0){
            $this->db->set("test_id",0);
            $this->db->where("test_id",$tid);
            $this->db->update("question");
            foreach ($qt as $qt){
                if ($qt > 0){
                    $this->db->set("test_id",$tid);
                    $this->db->where("question_id",$qt);
                    $this->db->update("question");
                }
            }
    	}
        $this->db->where("test_id",$id);
        $this->db->delete("test_to_role");
        if ($sum > 0){
            foreach ($role as $role){
    			if ($role > 0){
                    $r[] = array(
        				'test_id' => $id,
        				'role_id' => $role
        			);
                }
            }
            if (!empty($r)){
                $this->db->insert_batch('test_to_role',$r);
            }
        }
    }
    public function delete(){
        $id = $this->input->post('cid');
        $valid1 = $this->check_question_delete($id);
        if (!empty($valid1)){
            return false;
        }
        $valid2 = $this->check_child_test($id);
        if (!empty($valid2)){
            return false;
        }
       	$this->db->select('sound');
		$this->db->where_in('qtest_id',$id);
		$query = $this->db->get('question_test');
		$row = $query->result_array();
        // xoa question_test
		$this->db->where_in('qtest_id',$id);
		$this->db->delete('question_test');
        // xoa test_to_role

        $this->db->where_in('test_id',$id);
        $this->db->delete("test_to_role");
        //
        return $row;
    }
    public function check_question_delete($test_id){
        $this->db->select("question_id");
        $this->db->where_in("test_id",$test_id);
        $query = $this->db->get("question",1);
        return $query->row_array();
    }
    public function check_child_test($test_id){
        $this->db->select("qtest_id");
        $this->db->where_in("parent",$test_id);
        $query = $this->db->get("question_test",1);
        return $query->row_array();
    }
    public function get_question($keyword,$limit){
        $this->db->select("question_id,title");
        $this->db->where("publish",1);
        $this->db->like('title',$keyword);
        $this->db->where("test_id",0);
        $this->db->limit($limit);
        $query = $this->db->get('question');
        return $query->result_array();
    }
    public function get_question_selected($test_id){
        $this->db->select("question_id,title");
        $this->db->where("test_id",$test_id);
        $query = $this->db->get('question');
        return $query->result_array();
    }
    private function _input(){
		$input = array(
            'title' => $this->input->post('title'),
			'description' => $this->input->post('description'),
            'publish'   => (int)$this->input->post('publish'),
            'cate_id' => (int)$this->input->post('category'),
            'part' => (int) $this->input->post('part')
		);
		return $input;
    }
}