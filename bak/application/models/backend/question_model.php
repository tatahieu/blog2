<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function lists($limit = 10){
		$offset = 0;
        $array = $this->input->get(NULL);
        if (!empty($array)){
            if (array_key_exists('page',$array)){
                $offset = ($array['page'] - 1) * $limit;
            }
            if (array_key_exists('cate',$array) AND $array['cate'] > 0){
                $this->db->join('question_to_cate as c','q.question_id = c.question_id');
                $this->db->where('c.cate_id',$array['cate']);
            }
            if (array_key_exists('name',$array)){
                $this->db->like('title',$array['name']);
            }
        }
        $this->db->order_by('q.question_id DESC');
		$this->db->select('q.question_id,title,publish,test_id,images,sound');
		$query = $this->db->get('question as q',$limit+1,$offset);
		return $query->result_array();
    }
    public function detail($id,$select = null){
    	if (is_null($select)){
			$select = 'title,detail,date_up,publish,images,sound';
		}
		$this->db->select($select);
		$this->db->where('question_id',$id);
    	$query = $this->db->get('question',1);
    	return $query->row_array();
    }
    public function get_cate($id){
    	$this->db->select('cate_id');
    	$this->db->where('question_id',$id);
    	$query = $this->db->get('question_to_cate');
    	return $query->result_array();
    }
    public function insert($img,$sound){
    	$input = $this->_input();
		$input2 = array(
			'images' => $img,
            'sound' => $sound,
            'user_id' => $this->session->userdata('userid')
			);
		$input = array_merge($input,$input2);
    	$this->db->insert('question',$input);
    	/** INSERT CATE **/
        $cate = $this->input->post('category');
        $question_id = $this->db->insert_id();
    	if (is_array($cate) AND !empty($cate)){
			foreach ($cate as $cate){
    			$input_cate[] = array(
    				'cate_id' => (int)$cate,
    				'question_id' => $question_id
    			);
            }
            $this->db->insert_batch('question_to_cate',$input_cate);
    	}
        /** INSERT ANSWER **/
        for ($i = 1; $i < 5; $i ++){
            $content = $this->input->post('answer_'.$i);
            if ($content != ''){
                $input_answer[] = array(
                    'content' => $content,
                    'question_id' => $question_id,
                    'correct' => $this->input->post('correct_'.$i),
                    'result' => $i
                );

            }
        }
        if (isset($input_answer) and !empty($input_answer)){
            $this->db->insert_batch('question_answer',$input_answer);
        }
    }
    public function update($id,$img = '',$sound = ''){
		$input = $this->_input();
		if ($img != ''){
			$input['images'] = $img;
		}
        if ($sound != ''){
            $input['sound'] = $sound;
        }
		$this->db->where('question_id',$id);
		$this->db->update('question',$input);
		/** Xoa cate **/
		$this->db->where('question_id',$id);
		$this->db->delete('question_to_cate');
		/** Cap nhat thong tin moi **/
 		$cate = $this->input->post('category');
    	if (is_array($cate) AND !empty($cate)){
   			foreach ($cate as $cate){
   				$input_cate[] = array(
			   		'cate_id' => $cate,
			   		'question_id' => $id
			   	);
   			}
   			$this->db->insert_batch('question_to_cate',$input_cate);
   		}
        /** Xoa answer **/
        $this->db->where('question_id',$id);
        $this->db->delete('question_answer');
        /** cap nhat lai answer **/
        for ($i = 1; $i < 5; $i ++){
            $content = $this->input->post('answer_'.$i);
            if ($content != ''){
                $input_answer[] = array(
                    'content' => $content,
                    'question_id' => $id,
                    'correct' => $this->input->post('correct_'.$i),
                    'result' => $i
                );

            }
        }
        if (isset($input_answer) and !empty($input_answer)){
            $this->db->insert_batch('question_answer',$input_answer);
        }
    }
    public function delete(){
    	/** Liet ke danh sach file va anh can xoa **/
		$id = $this->input->post('cid');
    	$this->db->select('images,sound');
		$this->db->where_in('question_id',$id);
		$query = $this->db->get('question');
		$row = $query->result_array();
 		/** xoa question **/
		$this->db->where_in('question_id',$id);
		$this->db->delete('question');
		/** xoa question_to_cat **/
		$this->db->where_in('question_id',$id);
		$this->db->delete('question_to_cate');
        /** xoa answer**/
        $this->db->where_in('question_id',$id);
        $this->db->delete('question_answer');
		return $row;
    }
    public function get_answer($id){
        //$this->db->select();
        $this->db->where('question_id',$id);
        $this->db->order_by('result');
        $query = $this->db->get('question_answer');
        return $query->result_array();
    }
    private function _input(){
		$input = array(
            'title' => $this->input->post('title'),
			'detail' => $this->input->post('detail'),
            'publish'   => (int)$this->input->post('publish'),
            'date_up' => $this->input->post('date_up')
		);
		return $input;
    }
}