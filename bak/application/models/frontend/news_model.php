<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model {

	public $cate_name = '';

    public $cate_id = 0;

    function __construct()

    {

        parent::__construct();

    }

    public function list_total_rule1($params){

        $params_default = array("category_id" => 0, "exclude" => array());

        $params = array_merge($params_default,$params);

        // caching

       

            $this->db->join('news_to_cate as t','t.news_id = n.news_id');

            $this->db->where('t.cate_id = '.$params['category_id']);

            $this->db->where('n.publish',1);

            $this->db->where("isclass",0);

            $data = $this->db->count_all_results('news as n');

       
        return $data;

    }

    /**

     * @author: namtq

     * @desc: Danh sach tin theo cate co liston

     * @param: array("category_id" => 0, "limit" => 10, "page" => 1, "exclute" => array()));

     */

    public function lists_by_cate_rule1($params){

        $params_default = array("category_id" => 0, "limit" => 10, "page" => 1, "exclude" => array());

        $params = array_merge($params_default,$params);

        // caching



            $offset = ($params['page'] - 1)*$params['limit'];

            $this->db->select('n.news_id,n.title,n.images,description,n.share_url');

            $this->db->where('n.publish',1);

            $this->db->where("t.cate_id",$params['category_id']);

            if (!empty($params['exclude'])){

                $this->db->where_not_in("n.news_id",$params['exclude']);

            }

            $this->db->where("isclass",0);

            $this->db->order_by('date_up','DESC');

            $this->db->join('news_to_cate as t','t.news_id = n.news_id');

            $query = $this->db->get('news as n',$params['limit'],$offset);

            $data = $query->result_array();



        return $data;

    }

    /**

     * @author: namtq

     * @desc: tong tin theo original cate

     * @param: array("category_id" => 0, "limit" => 10, "page" => 1);

     */

    public function lists_total_rule2($params){

        $params_default = array("category_id" => 0);

        $params = array_merge($params_default,$params);



       

            $this->db->where('n.publish',1);

            if ($params['category_id'] > 0){

                $this->db->where('original_cate = '.$params['category_id']);

            }

            $data = $this->db->count_all_results("news");


        return $data;

    }

    /**

     * @author: namtq

     * @desc: Danh sach tin theo original cate

     * @param: array("category_id" => 0, "limit" => 10, "page" => 1);

     */

    public function lists_by_cate_rule2($params){

        $params_default = array("category_id" => 0, "limit" => 10, "page" => 1);

        $params = array_merge($params_default,$params);

        $offset = ($params['page'] - 1)*$params['limit'];



    
            $this->db->select('news_id,title,images,description,share_url');

            $this->db->where('publish',1);

            if (is_array($params['category_id'])){

                $this->db->where_in('original_cate',$params['category_id']);

            }

            elseif ($params['category_id'] > 0){

                $this->db->where('original_cate = '.$params['category_id']);

            }

            $this->db->order_by('date_up','DESC');

            $query = $this->db->get("news",$params['limit'],$offset);

    		$data = $query->result_array();

         
        return $data;

    }

    public function detail($id,$option = array()){

        // caching

        //$keycache = 'news/detail/'.$id;

        //if (!$data = $this->cache->get($keycache)) {

        	$this->db->select('title,description,detail,share_url,hit,original_cate,seo_title,seo_desc,news_id,images');

            if ($option['preview'] == 1) {

                $this->db->where('publish',0);

            }

            else {

                $this->db->where('publish',1);

            }

            $this->db->where('n.news_id',$id);

        	$query = $this->db->get('news as n',1);

        	$data = $query->row_array();

            /* if (!$option) {

                $this->cache->save($keycache,$data);

            }*/

        //}

        return $data;

    }

    public function special($limit){

        $keycache = 'news/special/'.$limit;

        if (!$data = $this->cache->get($keycache)) {

            $this->db->select('news_id,title,description,images');

            $this->db->where('publish',1);

            $this->db->where('special',1);

            $this->db->order_by('date_up','DESC');

            $query = $this->db->get('news',$limit);

            $data = $query->result_array();

            $this->cache->save($keycache,$data);

        }

        return $data;

    }

    public function relate($id = 0,$cate_id = '',$limit = 10){

        $this->db->where("n.original_cate",$cate_id);

        $this->db->where('n.news_id <',$id);

        $this->db->where('n.publish',1);

		$this->db->from('news as n');

        $this->db->limit($limit);

        $this->db->order_by('date_up','DESC');

		$this->db->select('n.news_id,n.title,date_up,share_url');

        $query = $this->db->get();

		return $query->result_array();

    }

    public function latest($id = 0,$cate_id = '',$limit = 10){

        $this->db->where("n.original_cate",$cate_id);

        $this->db->where('n.news_id >',$id);

        $this->db->where('n.publish',1);

		$this->db->from('news as n');

        $this->db->limit($limit);

        $this->db->order_by('date_up','DESC');

		$this->db->select('n.news_id,n.title,date_up,share_url');

        $query = $this->db->get();

		return $query->result_array();

    }

    /*

    public function get_cate($level = null){

        if ($level != 'null'){

            $this->db->where("parent",$level);

        }

    	$this->db->select('name,alias,parent');

        $this->db->where("cate_code",1);

        $this->db->order_by('ordering');

    	$query = $this->db->get('news_cate');

    	return $query->result_array();

    }*/

    public function get_cate_detail($cateid){

        $this->db->select('name,share_url,metakey,metadesc,style,cate_id,parent');

        $this->db->where('cate_id',$cateid);

        $query = $this->db->get('news_cate');

        return $query->row_array();

    }

    public function search_count($keyword){

        $this->db->where('publish',1);

        $this->db->like('title',$keyword);

        return $this->db->count_all_results('news');

    }

    public function search($keyword,$page = 1,$limit = 10){

        $offset = ($page - 1)*$limit;

        $this->db->select('news_id,title,description,images,date_up,share_url');

        $this->db->where('publish',1);

        $this->db->like('title',$keyword);

        $this->db->order_by('date_up','DESC');

        $this->db->limit($limit,$offset);

        $query = $this->db->get('news');

        return $query->result_array();



    }

    public function count_hit($id){

        $this->db->where('news_id',$id);

        $this->db->set('hit','hit + 1',FALSE);

        $this->db->update('news');

    }

    /** TAGS **/

    public function get_tags($news_id){

        $this->db->select("n.tag_id,n.name as tag_name");

        $this->db->where("t.news_id",$news_id);

        $this->db->join("tags as n","n.tag_id = t.tag_id");

        $query = $this->db->get("news_to_tags as t");

        return $query->result_array();

    }

    public function tag_total($tagid){

        $this->db->where("tag_id",$tagid);

        return $this->db->count_all_results("news_to_tags");

    }

    public function tags_list($tagid,$limit,$page){

        $offset = ($page - 1)*$limit;

        $this->db->select('n.news_id,n.title,n.images,n.description,n.share_url,tg.name as tag_name');

        $this->db->where("t.tag_id",$tagid);

        $this->db->order_by('n.date_up','DESC');

        $this->db->join("news as n","n.news_id = t.news_id");

        $this->db->join("tags as tg","tg.tag_id = t.tag_id");



        $query = $this->db->get("news_to_tags as t",$limit,$offset);

		return $query->result_array();

    }

    /** LIST FOR MEMBER CLASSS **/

    public function list_for_roles($role,$cate = 0,$limit = 10,$page = 1){

        $offset = ($page - 1) * $limit;

        if ($cate > 0){

            $this->db->join("news_to_cate as c","c.news_id = r.news_id");

            $this->db->where("c.cate_id",$cate);

        }

        $this->db->distinct();

        $this->db->join("news as n","n.news_id = r.news_id");

        $this->db->select("r.news_id,n.title,n.description,n.images,n.share_url");

        $this->db->where("publish",1);

        $this->db->where("r.role_id",$role);

        $this->db->order_by("r.news_id","DESC");

        $query = $this->db->get("news_to_role as r",$limit,$offset);

        return $query->result_array();

    }

    public function count_list_for_roles($role,$cate = 0){



        if ($cate > 0){

            $this->db->join("news_to_cate as c","c.news_id = r.news_id");

            $this->db->where("c.cate_id",$cate);

        }

        $this->db->join("news as n","n.news_id = r.news_id");

        $this->db->where("publish",1);

        $this->db->where("r.role_id",$role);

        return $this->db->count_all_results("news_to_role as r");

    }

    public function check_news_role($news_id,$role){

        $this->db->where("news_id",$news_id);

        $this->db->where("role_id",$role);

        return $this->db->count_all_results("news_to_role");

    }

    /**

     * lay cac cate news

     */

    public function get_list_cate($param = array()){

        if ($param['style']){

            $this->db->where("style",$param['style']);

        }

        if ($param['parent'] == 1){

            $this->db->where("parent >",0);

        }

        $query = $this->db->get("news_cate");

        return $query->result_array();

    }

    public function get_latest_id($param = array()){

        $this->db->select("news_id");

        $this->db->where("original_cate",$param['cate_id']);

        $query = $this->db->get("news",1);

        return $query->row_array();

    }

}