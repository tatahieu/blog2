<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model {
	function __construct()
    {
		parent::__construct();
    }
/** =================================== LOGIN ========================================= **/
   	public function check_login($param = array()){
   	    $param['password'] = ($param['password']) ? $param['password'] : md5($this->input->post('log_username').$this->input->post('log_password'));
        $param['username'] = ($param['username']) ? $param['username'] : $this->input->post('log_username');
		$this->db->join('member_roles','member_roles.roles_id = member.roles_id',"LEFT");
		$this->db->select('user_id,username,password,fullname,where_live,phone,birthday, member_roles.name,member_roles.permission');
		$this->db->where('username',$param['username']);
		$this->db->where('password',$param['password']);
		$this->db->where('active',1);
    	$this->db->limit(1);
		$query =  $this->db->get('member');
		return $query->row_array();
	}
    // kiem tra user da dang nhap lop hoc chua
    public function check_class_login($username,$password){
        $this->db->where("username",$username);
        $this->db->where("password",base64_encode($password));
        $query = $this->db->get("class",1);
        return $query->row_array();
    }
/** ======================================= COMMON =============================== **/
    function check_email_exist($email = ''){
        $this->db->where("email",$email);
        return $this->db->count_all_results("member");
    }
    function check_username_exist($username){
        $this->db->where("username",$username);
        return $this->db->count_all_results("member");
    }
    public function check_exist($username = '',$email = ''){
        if ($username == '' && $email == ''){
            return false;
        }
        //$this->db->select('user_id');
        $this->db->where('username',$username);
        $this->db->or_where('email',$email);
        $this->db->limit('1');
        $query = $this->db->get('member');
        return $query->row_array();
    }
/** ================================== FORGOT PASS ================================ **/
    public function forgot_create_code($username){
        // xoa code cu
        $this->db->where("username",$username);
        $this->db->where("type",2);
        $this->db->delete("member_active");
        // tao code moi
        $code = random_string('md5');
        $active = array(
            'username' => $username,
            'created' => date('Y-m-d H:i:s'),
            'code' => $code,
            'type' => 2
        );
        $this->db->insert('member_active',$active);
        return $code;
    }
    public function check_email_forgot($email){
        $this->db->select("username");
        $this->db->where("email",$email);
        $query = $this->db->get("member",1);
        return $query->row_array();
    }
    public function check_forgot_code($username,$code){
        $this->db->where("username",$username);
        $this->db->where("type",2);
        $this->db->where("code",$code);
        $count = $this->db->count_all_results("member_active");
        if ($count > 0) return true; else return false;
    }
    public function set_new_password($username,$password){
        $this->db->set("password",md5($username.$password));
        $this->db->set("active",1);
        $this->db->where("username",$username);
        $this->db->update("member");
        // xoa active
        $this->db->where("username",$username);
        $this->db->delete("member_active");
    }
/** ===================================== PROFILE ==================================== **/
    public function detail($id){
		$this->db->where('user_id',$id);
		$this->db->where('active',1);
		$query = $this->db->get('member',1);
		return $query->row_array();
	}
    public function insert_from_social($input) {
        $userProfile = $this->check_exist('',$input['email']);
        // neu user da ton tai
        if ($userProfile) {
            return $userProfile;
        }
        // insert user
        else {
            $this->db->insert('member',$input);
            $userid = (int)$this->db->insert_id();
            return $this->detail($userid);
        }
    }
    public function register(){
        $input = array(
            'fullname' => $this->input->post('fullname'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('username').$this->input->post('password')),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            //'yahoo' => $this->input->post('yahoo'),
            // 'skype' => $this->input->post('skype'),
            'active' => 1,
            'roles_id' => 0,
            'sex' => intval($this->input->post('sex')),
            'date_reg' => date('Y-m-d'),
            'date_created' => time(),
            'send_mail' => (int) $this->input->post('send_mail'),

            'where_live' => (int) $this->input->post("where_live"),
            'birthday' => (int) $this->input->post("birth_year").'-'.(int) $this->input->post("birth_month").'-'.(int) $this->input->post("birth_date")
        );
        $this->db->insert('member',$input);
        $this->load->helper('string');
        $code = random_string('md5');
        $active = array(
            'username' => $this->input->post('username'),
            'created' => date('Y-m-d H:i:s'),
            'code' => $code,
            'type' => 1
        );
        $this->db->insert('member_active',$active);
        return $code;
    }
    public function update_profile($input = array()){
        if (!$input) {
            $input = array(
                'fullname' => $this->input->post('fullname'),
                'yahoo' => $this->input->post('yahoo'),
                'skype' => $this->input->post('skype'),
                'sex' => intval($this->input->post('sex')),
                'phone' => $this->input->post('phone'),
                'where_live' => (int) $this->input->post("where_live"),
                'birthday' => (int) $this->input->post("birth_year").'-'.(int) $this->input->post("birth_month").'-'.(int) $this->input->post("birth_date")
            );
            if ($this->input->post('password')){
                $input['password'] = md5($this->session->userdata('username').$this->input->post('password'));
            }
        }
        $this->db->where('user_id',$this->session->userdata('userid'));
        $this->db->update('member',$input);
    }
/** ============================================= ACTIVE ================================ **/
    public function active ($username,$code){
        $this->db->where("username",$username);
        $this->db->where("type",1);
        $this->db->where("code",$code);
        $count = $this->db->count_all_results("member_active");
        // neu ton tai key
        if ($count > 0){
            $this->db->set('active',1);
            $this->db->where('username',$username);
            $this->db->update('member');
            // xoa key active cu
            $this->db->where('username',$username);
            $this->db->delete('member_active');
            // neu key active trong vong 2 ngay
            return true;
        }
        return false;
    }
    public function check_email_active($email){
        $this->db->select("username");
        $this->db->where("email",$email);
        $this->db->where("active",0);
        $query = $this->db->get("member",1);
        return $query->row_array();
    }
    public function reactive_code($username){
        $this->db->where("username",$username);
        $this->db->where("type",1);
        $this->db->delete("member_active");
        $code = random_string('md5');
        $active = array(
            'username' => $username,
            'created' => date('Y-m-d H:i:s'),
            'code' => $code,
            'type' => 1
        );
        $this->db->insert('member_active',$active);
        return $code;
    }
    /** ================================== USER AREA ================================ **/
    // bai writing cua 1 member da test
    public function get_user_speaking($param){
        $offset = ($param['page'] - 1)*$param['limit'];
        $this->db->select("u.*,t.name as test_name");
        $this->db->where("u.user_id",$param['user_id']);
        $this->db->join("speaking_test as t","t.test_id = u.test_id");
        $this->db->group_by("u.time,u.test_id");
        $this->db->order_by("time","DESC");
        $query = $this->db->get("speaking_question_to_user as u",$param['limit'] + 1,$offset);
        return $query->result_array();
    }
    // bai writing cua 1 member da test
    public function get_user_writing($param){
        $offset = ($param['page'] - 1)*$param['limit'];
        $this->db->select("u.*,t.name as test_name");
        $this->db->where("u.user_id",$param['user_id']);
        $this->db->join("writing_test as t","t.test_id = u.test_id");
        $this->db->group_by("u.time,u.test_id");
        $this->db->order_by("time","DESC");
        $query = $this->db->get("writing_question_to_user as u",$param['limit'] + 1,$offset);
        return $query->result_array();
    }
    public function checkclass($param){

    }
}