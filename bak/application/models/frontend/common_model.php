<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function set_setting($id = 1){
		$this->db->where('id',$id);
        $query = $this->db->get('setting');
        return $query->row_array();
    }
}