<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Course_model extends CI_Model {
	public $cate_name = '';
    public $cate_id = 0;
    function __construct()
    {
        parent::__construct();
    }
    public function get_word_list($param){
        switch($param['type']){
            case 'random':
            $this->db->order_by('word_id', 'RANDOM');
            break;
        }
        if ($param['topic_id'] > 0) {
            $this->db->where("topic_id",$param['topic_id']);
        }
        $this->db->where("level",$param['level']);
        $this->db->select("word_id,images,english,vietnamese,exam,word,phienam");
        $query = $this->db->get("course_word",$param['limit'],$param['offset']);
        return $query->result_array();
    }
    public function get_word_complete($param){
        switch($param['type']){
            case 'random':
            $this->db->order_by('com_id', 'RANDOM');
            break;
            default:
            $this->db->order_by("com_id","DESC");
        }
        $this->db->where("topic_id",$param['topic_id']);
        $this->db->where("level",$param['level']);
        $this->db->select("com_id,audio,images,question,answer");
        $query = $this->db->get("course_complete",$param['limit'],$param['offset']);
        return $query->result_array();
    }
    public function get_topic_by_alias($alias){
        $this->db->where("alias",$alias);
        $query = $this->db->get("topic");
        return $query->row_array();
    }
    /**
     * @desc: lay toan bo thong tin topic
     */
    public function get_all_topic($param = array()){
        $this->db->where_in("level",array(0,$param['level']));
        $this->db->where("type",$param['type']);
        $query = $this->db->get("topic",50);
        return $query->result_array();
    }
}