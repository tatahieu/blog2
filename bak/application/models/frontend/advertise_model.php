<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Advertise_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    // get album
    public function get_cate_child($cateid,$limit,$page){
        if ($limit > 0 and $page >= 1){
            $offset = ($page - 1) * $limit;
		}
        $this->db->select("c.name,c.cate_id,c.alias,a.images");
        $this->db->join("advertise as a","a.cate_id = c.cate_id",'LEFT');
        $this->db->group_by("c.cate_id");
        $this->db->order_by("c.ordering");
        $this->db->limit($limit,$offset);
        $this->db->where("c.parent",$cateid);
        $query = $this->db->get("advertise_cate as c",$limit,$offset);
        return $query->result_array();
    }
    function count_cate_child($cateid){
        //$this->db->select("c.cate_id");
        //$this->db->join("advertise as a","a.cate_id = c.cate_id");
        //$this->db->group_by("c.cate_id");
        $this->db->where("c.parent",$cateid);
        return $this->db->count_all_results("advertise_cate as c");
    }
    // get list pic
    public function lists($cate_id,$limit = 0,$page = 1){
		if ($limit > 0 and $page >= 1){
            $offset = ($page - 1) * $limit;
            $this->db->limit($limit,$offset);
		}
        $this->db->select('adv_id,name,link,images');
        $this->db->where('publish',1);
        $this->db->where('cate_id',$cate_id);
		$this->db->order_by('ordering,adv_id DESC');
		$query = $this->db->get('advertise');
		return $query->result_array();
    }
    public function lists_count($cate_id){
        $this->db->where('publish',1);
        $this->db->where('cate_id',$cate_id);
		return $this->db->count_all_results('advertise');
    }
    public function get_cate_name($cate_id){
        $this->db->select('name');
        $this->db->where('cate_id',$cate_id);
        $query = $this->db->get('advertise_cate',1);
        return $query->row_array();
    }
}