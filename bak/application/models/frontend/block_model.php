<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Block_model extends CI_Model {
	protected $_position = array();
    protected $_menu = array();
    function __construct()
    {
        parent::__construct();
    }
    public function get_all_block(){
        $mod = $this->config->item("mod_access");
        $mod = $mod['name'];

        $keycache = 'common/block/'.DEVICE_ENV.'/'.$mod;
        if (!$blocks = $this->cache->get($keycache)) {

            if ($mod){
                $this->db->where_in("m.module",array("all",$mod));
            }
            else{
                $this->db->where_in("m.module","all");
            }
            $this->db->where_in("b.device",array(0,DEVICE_ENV));
            $this->db->order_by('b.ordering');
            $this->db->where("b.publish",1);
            $this->db->group_by("m.block_id");
            $this->db->join("block as b","b.block_id = m.block_id");
            $query = $this->db->get('block_to_module as m');
            /// get all block position
            $blocks = $query->result_array();
            $this->cache->save($keycache,$blocks);
        }
        // get all data
        foreach ($blocks as $block){
            $param = @json_decode($block['params'],TRUE);
            $data = array();
            $data['block'] = $block;
            $data['params'] = $param;
            switch ($block['module']){
                case 'news_cate':
                    $data['rows'] = $this->news_cate($param['id'],$param['nums_item']);
                break;
                case 'news':
                    $data['rows'] = $this->news($param['id'],1);
                break;
                case 'advertise_cate':
                    $data['rows'] = $this->advertise_cate($param['id'],$param['nums_item']);
                break;
                case 'static':
                    $data['rows'] = $block;
                break;
                case 'support':
                    $limit = 20;
                    $data['rows'] = $this->support(20);
                break;
                case 'menu':
                    $data['rows'] = $this->menu($block['position']);
                break;
                case 'breadcrumb':
                    $data['rows'] = $this->config->item("breadcrumb");
                break;
                default:
                    $data['rows'] = array();
            }
            if (!empty($data['rows'])){
                $file = ($param['temp']) ? $block['module'].'_'.$param['temp'] : $block['module'];
                $this->load->setArray($block['position'],$this->load->view('block/'.$file,$data));
            }
        }
        $this->addstatic();
    }
    private function news($id){
        if ($id <= 0){return array();}
        $this->db->select("title,description,images");
        $this->db->where("news_id",$id);
        $query = $this->db->get("news",1);
		return $query->row_array();
    }
    private function news_cate($cate,$limit = 10){
        $this->load->model('news_model','news');
        return $this->news->lists_by_cate_rule1(array('category_id' => $cate,'limit' => $limit));
    }
    private function news_detail($id){
        if ($id <= 0){return array();}
        $this->db->select("title,images,description,detail");
        $this->db->where("news_id",$id);
        $query = $this->db->get("news");
        return $query->row_array();
    }
    private function advertise_cate($cate_id,$limit = 0){
        if ($cate_id <= 0){return array();}
        $keycache = 'advertise/'.$cate_id.'/'.$limit;
        if (!$data = $this->cache->get($keycache)) {
            if ($limit > 0){
                $this->db->limit($limit);
            }
            $this->db->where('cate_id',(int)$cate_id);
            $this->db->order_by('ordering');
            $query = $this->db->get('advertise');
            $data = $query->result_array();
            $this->cache->save($keycache,$data);
        }
        return $data;
    }
    private function support($limit = 0){
        if ($limit > 0){
            $this->db->limit($limit);
        }
		$this->db->order_by('ordering');
		$query = $this->db->get('support');
		return $query->result_array();
    }
    private function menu($position = ''){
        if ($this->_menu){
            return $this->_menu[$position];
        }
        $keycache = 'common/menu';
        if (!$arrMenu = $this->cache->get($keycache)) {

            $this->db->order_by('parent,ordering');
            $query = $this->db->get('menus');
            $rows = $query->result_array();
            $selection = $this->config->item("menu_select");
            foreach ($rows as $row){
                if ($row['item_mod'] == $selection['item_mod'] && $row['item_id'] == $selection['item_id']){
                    $row['selected'] = 1;
                }
                // menu cap 1
                if ($row['parent'] == 0){
                    $data[$row['position']][$row['menu_id']] = $row;
                }
                else{
                    if (array_key_exists($row['parent'],$data[$row['position']])){
                        $data[$row['position']][$row['parent']]['child'][$row['menu_id']] = $row;
                        if ($row['selected'] == 1) {
                            // set select for parent
                            $data[$row['position']][$row['parent']]['selected'] = 1;
                        }
                        $check[$row['menu_id']] = $row['parent'];
                    }
                    else {
                        $recheck[] = $row;
                    }
                }
            }
            if ($recheck) {
                foreach ($recheck as $recheck) {
                    if ($check[$recheck['parent']]){
                        $data[$recheck['position']][$check[$recheck['parent']]]['child'][$recheck['parent']]['child'][] = $recheck;
                        if ($recheck['selected'] == 1){
                            $data[$recheck['position']][$check[$recheck['parent']]]['child'][$recheck['parent']]['selected'] = 1;
                            $data[$recheck['position']][$check[$recheck['parent']]]['selected'] = 1;
                        }
                    }
                    else {
                        $data[$recheck['position']]['sub'] = $recheck;
                    }
                }
            }
            $this->_menu = $data;
            $arrMenu = $data;
            $this->cache->save($keycache,$arrMenu);
        }
        return $arrMenu[$position];
    }
    // default static
    private function addstatic(){
        
        
        if (DEVICE_ENV == 1) {
            $data[] = link_tag('bootstrap.min.css');
            $data[] = link_tag('m_tqn_portal.css');
        }
        else {
            $data[] = link_tag('tqn_portal.css');

        }
        $data[] = '<script>
           var $base_url = \''.base_url().'\';
           var $site_url = \''.site_url().'\';
           var $api_image_detail = \''.UPLOAD_URL.'images/userfiles/\';
           var $image = \''.$this->config->item('img').'\';
           var $theme = \''.$this->config->item('theme').'\';
           var $themeorg = \''.$this->config->item('themeorg').'\';
           var UPLOAD_URL = \''.UPLOAD_URL.'\';
           var DEVICE_ENV = \''.DEVICE_ENV.'\';
           </script>';
        $data[] = js('jquery-1.9.0.min.js');
        $data[] = js('libraries.js');

        $data[] = link_tag('jquery.fancybox.css');
        

        $this->load->setArray('common',$data,TRUE);
        //////////////////////////////////
        $script[] = '<div id="message_alert"></div>';
        $script[] = js('jquery.fancybox.pack.js');
        $script[] = js('common.js?v=2');
        if (DEVICE_ENV == 1) {
            $script[] = js('mobile.js');
        }
        else {
            $script[] = js('pc.js');
        }
        //$script[] = js('jquery.jqplugin.1.0.2.min.js');
        
        
        $script[] = js('jquery-ui-1.10.0.custom.min.js');
        $script[] = link_tag('ui-lightness/jquery-ui-1.10.0.custom.min.css');
        $this->load->setArray('script',$script);
    }
}