<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Speaking_model extends CI_Model {
	protected $_position = array();
    function __construct()
    {
        parent::__construct();
    }
    public function test_lists($param){
        $offset = (int)($param['page'] - 1) * $param['limit'];
        $this->db->select("t.*");
        $this->db->order_by("test_id","DESC");
        //var_dump($param['type']); die;
        if ($param['type'] === 'fullpart'){ // lay cac bai khong thuoc
            $this->db->where("type >",0);
        }
        elseif (is_numeric($param['type']) && $param['type'] >= 0) {
            $this->db->where("type",$param['type']);
        }
        if ($param['level'] > 0){
            $this->db->where("level" , $param['level']);
        }
        if ($param['role'] > 0){
            $this->db->where("isclass",1);
            $this->db->join("speaking_test_to_role as r",'r.speaking_test_id = t.test_id');
            $this->db->where("r.role_id",$param['role']);
        }
        else {
            $this->db->where("isclass",0);
        }
        if ($param['password'] > 0){
            $this->db->where("password",$param['password']);
        }
        else{
            $this->db->where("password",0);
        }
        $query = $this->db->get("speaking_test as t",$param['limit'],$offset);
        return $query->result_array();
    }
    public function get_test_by_role($param){

    }
    public function get_answer($question_id){
        $this->db->where_in("speaking_id",$question_id);
        $query = $this->db->get("speaking_question",11);
        return $query->result_array();
    }
    // chi tiet bai test
    public function get_test_detail($id){
        $this->db->where("test_id",$id);
        $query = $this->db->get("speaking_test");
        return $query->row_array();
    }
    // Danh sach bai test
    public function get_question_by_test($param){
        $offset = ($param['page'] - 1) * $param['limit'];
        $this->db->select("");
        $this->db->where("type",$param['type']);
        $query = $this->db->get("speaking_test",$param['limit'],$offset);
        return $query->result_array();
    }
    // chi tiet tung cau hoi
    public function detail_question($id){
        $this->db->where("speaking_id",$id);
        $query = $this->db->get("speaking_question",1);
        return $query->row_array();
    }
    // lay bai test tiep theo
    public function get_next_test($param = array()){
        $this->db->select("test_id");
        $this->db->where("test_id <",$param['test_id']);
        $this->db->where("type",$param['type']);
        $this->db->order_by("test_id","DESC");
        $query = $this->db->get("speaking_test",1);
        return $query->row_array();
    }
    // lay loi ghi am theo thoi gian
    public function get_user_answer($param){
        $this->db->where("user_id",$param['user_id']);
        $this->db->where("time",$param['time']);
        $this->db->where("test_id",$param['test_id']);
        $query = $this->db->get("speaking_question_to_user");
        return $query->result_array();
    }
    // lay chi tiet tung cau tra loi
    public function get_answer_by_user($param = array()){
        $this->db->where("user_id",$param['user_id']);
        $this->db->where("speaking_id",$param['speaking_id']);
        $query = $this->db->get("speaking_question_to_user",$param['limit']);
        return $query->result_array();
    }
    // lay 1 mang cau hoi
    public function get_question_by_arr($param = array()){
        $this->db->select("sp.speaking_id,sp.images,sp.content,sp.sound,sp.title,u.sound as user_sound,u.comment_teacher,u.point_teacher,u.record_id");
        $this->db->where_in("sp.speaking_id",$param['speaking_id']);
        $this->db->join("speaking_question_to_user as u","u.speaking_id = sp.speaking_id AND u.time=".$param['time']." AND u.user_id=".$param['user_id'],"LEFT");
        $query = $this->db->get("speaking_question as sp",11);
        return $query->result_array();
    }
    // lay cau tra loi moi nhat cua thanh vien
    public function get_answer_by_testid($param){
        $this->db->where("user_id",$param['user_id']);
        $this->db->where("test_id",$param['test_id']);
        if ($param['time']){
            $this->db->where("time",$param['time']);
        }
        $this->db->order_by("record_id","DESC");
        $query = $this->db->get("speaking_question_to_user",$param['limit']);
        return $query->result_array();
    }
    /**
     * @param: type (1: tra loi tung cau hoi)
     */
    function insert_user_answer($param){

        $this->db->insert('speaking_question_to_user',$param);
    }
}