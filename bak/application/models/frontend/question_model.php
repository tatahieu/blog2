<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Question_model extends CI_Model {
	public $cate_name = '';
    public $cate_id = 0;
    function __construct()
    {
        parent::__construct();
    }
    function get_cate_info($cate_id){
        $this->db->select("name,description");
        $this->db->where("cate_id",$cate_id);
        $query = $this->db->get("question_cate",1);
        return $query->row_array();
    }
    function get_cate_test($cate_id){
        $this->db->select("qtest_id,title");
        $this->db->where("publish",1);
        $this->db->where("parent",0);
        $this->db->where("isclass",0);
        $this->db->where("cate_id",$cate_id);
        $this->db->order_by("qtest_id","desc");
        $query = $this->db->get('question_test');
        return $query->result_array();
    }

    function get_answer($qid){
        $this->db->select("question_id,content,result");
        $this->db->where_in("question_id",$qid);
        $this->db->order_by("result");
        $query = $this->db->get("question_answer");
        return $query->result_array();
    }
    function get_test_info($test_id){
        $this->db->select("t.title,c.name,t.isclass,c.relate_news,t.description,t.cate_id,t.part");
        $this->db->join("question_cate as c","c.cate_id = t.cate_id","LEFT");
        $this->db->where("qtest_id",$test_id);
        $query = $this->db->get('question_test as t',1);
        return $query->row_array();
    }
    function get_relate_news($cate_id){
        $this->db->select("n.title,n.news_id,c.cate_id");
        $this->db->join("news_to_cate as c","c.news_id = n.news_id");
        $this->db->where("c.cate_id",$cate_id);
        $this->db->order_by("n.news_id","DESC");
        $query = $this->db->get("news as n",6);
        return $query->result_array();
    }
    function save_question_score($score_id, $input){
        $input['update_time'] = time();
        $this->db->where('score_id',$score_id);
        $this->db->update("question_score",$input);
        return $this->db->affected_rows();
    }
    function random_by_test($test_id,$limit = 0){
        if ($limit > 0){
            $offset = ($this->input->get("q")) ? ((int) $this->input->get("q") - 1) * $limit : '' ;
            $this->db->limit($limit+1, $offset);
        }
        $this->db->select("question_id,images,sound,detail,title,test_id");
        $this->db->where("publish",1);

        $this->db->where_in("test_id",$test_id);
        $this->db->order_by('question_id','DESC');
        $query = $this->db->get("question");
        $rows = $query->result_array();
        if (is_array($test_id)) {
            $result = array();
            foreach ($rows as $key => $row) {
                $result[$row['test_id']][] = $row;
            }
        }
        else {
            $result = $rows;
        }
        return $result;
    }
    function get_relate_test ($cate_id,$test_id,$role=0){
        $this->db->select("q.qtest_id,q.title");
        $this->db->where("q.cate_id",$cate_id);
        $this->db->where("q.qtest_id !=",$test_id);
        $this->db->where("q.parent",0);
        if ($role){
            //echo 'testok';
            $this->db->where("isclass",1);
            $this->db->join("test_to_role as r","r.test_id = q.qtest_id");
            $this->db->where("r.role_id = ".$this->session->userdata("class_id"));
        }
        else{
            $this->db->where("isclass",0);
        }
        $this->db->order_by("q.qtest_id","DESC");
        $query = $this->db->get("question_test as q");
        return $query->result_array();
    }
    function get_test_child($test_id,$limit = 0){
        if ($limit > 0){
            $offset = ($this->input->get("q")) ? ((int) $this->input->get("q") - 1) : '' ;
            $this->db->limit($limit + 1,$offset);
        }
        $this->db->select("qtest_id,title,description,sound");
        $this->db->where("parent",$test_id);
        $this->db->where("publish",1);
        $this->db->order_by("qtest_id","DESC");
        $query = $this->db->get("question_test");
        return $query->result_array();
    }
    function total_full_question_by_class($params){
        $this->db->where("publish",1);
        $this->db->where("parent",0);
        $this->db->where("isclass",1);
        $this->db->join("test_to_role as r",'r.test_id = t.qtest_id');
        $this->db->where("r.role_id",$params['role_id']);
        return $this->db->count_all_results('question_test as t');
    }
    function get_full_question_by_class($params = array()){
        $params_default = array("page" => 1,'limit' => 20);
        $params = array_merge($params_default,$params);
        $offset = ($params['page'] - 1) * $params['limit'];
        $this->db->select("qtest_id,title");
        $this->db->where("publish",1);
        $this->db->where("parent",0);
        $this->db->where("isclass",1);
        $this->db->join("test_to_role as r",'r.test_id = t.qtest_id');
        $this->db->where("r.role_id",$params['role_id']);

        $this->db->order_by("qtest_id","desc");
        $query = $this->db->get('question_test as t',$params['limit'],$offset);
        return $query->result_array();
    }
    function get_testing_result($qid){
        $this->db->select("question_id,result");
        $this->db->where("correct",1);
        $this->db->where_in("question_id",$qid);
        $query = $this->db->get("question_answer");
        return $query->result_array();
    }
    function get_all_by_class($role_id){
        $this->db->select("r.full_id,q.name");
        $this->db->where("r.role_id",$role_id);
        $this->db->join("question_full as q","q.full_id = r.full_id");
        $this->db->order_by("r.full_id","Desc");
        $query = $this->db->get("fulltest_to_role as r");
        return $query->result_array();
    }
    function get_all_full($type = 1,$part=0){
        $this->db->select("q.full_id,q.name");
        if ($type > 0){
            $this->db->where("type",$type);
        }
        if ($type == 3 AND $part != 0){
            $part = "part".$part;
            $this->db->where($part.' >',0);
        }
        $this->db->distinct("q.full_id");
        //$this->db->join("fulltest_to_role as r","r.full_id = q.full_id","LEFT");
        //$this->db->where("r.role_id",null);
        $this->db->where("isclass",0);
        $this->db->order_by("full_id","DESC");
        $query = $this->db->get("question_full as q");
        return $query->result_array();
    }
    function get_all_detail($id){
        $this->db->where("full_id",$id);
        $query = $this->db->get("question_full",1);
        return $query->row_array();
    }
    function get_role_question_full($id){
        $this->db->select("role_id");
        $this->db->where("full_id",$id);
        $query = $this->db->get("fulltest_to_role");
        return $query->result_array();
    }
    function get_top_scrore($limit){
        $this->db->limit($limit);
        $this->db->select("m.username, s.*");
        $this->db->join("member as m",'m.user_id = s.user_id');
        $this->db->order_by("total_score","DESC");
        $query = $this->db->get('question_score as s');
        return $query->result_array();
    }
    function check_question_by_role($testid,$role){
        $this->db->where("test_id",$testid);
        $this->db->where("role_id",$role);
        return $this->db->count_all_results('test_to_role');
    }
    function insert_user_score($params = array()){
        $this->db->select('update_time');
        $this->db->get('question_full');
        $this->db->where('test_id',$params['test_id']);
        $this->db->where('user_id',$params['user_id']);
        $this->db->order_by('score_id','DESC');
        $query = $this->db->get('question_score');
        $row = $query->row_array();
        $limit_time = time() - 10*60;
        if ($row && $row['update_time'] > $limit_time) {
            return FALSE;
        }
        $input = array(
            'test_id' => $params['test_id'],
            'user_id' => $params['user_id'],
            'list_score' => 0,
            'read_score' => 0,
            'total_score' => 0,
            'type' => $params['type'],
            'create_time' => time(),
            'update_time' => time()
        );
        $this->db->insert('question_score',$input);
        return (int)$this->db->insert_id();
    }
}