<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Writing_model extends CI_Model {
	protected $_position = array();
    function __construct()
    {
        parent::__construct();
    }
    public function test_lists($param){
        $offset = (int)($param['page'] - 1) * $param['limit'];
        $this->db->select("t.*");
        $this->db->order_by("test_id","DESC");
        if ($param['type'] === 'fullpart'){ // lay cac bai khong thuoc
            $this->db->where("type >",0);
        }
        elseif (is_numeric($param['type']) && $param['type'] >= 0) {
            $this->db->where("type",$param['type']);
        }
        if ($param['level'] > 0){
            $this->db->where("level" , $param['level']);
        }
        if ($param['role'] > 0){
            $this->db->where("isclass",1);
            $this->db->join("writing_test_to_role as r",'r.writing_test_id = t.test_id');
            $this->db->where("r.role_id",$param['role']);
        }
        else {
            $this->db->where("isclass",0);
        }
        if ($param['password'] > 0){
            $this->db->where("password",$param['password']);
        }
        else{
            $this->db->where("password",0);
        }
        $query = $this->db->get("writing_test as t",$param['limit'],$offset);
        return $query->result_array();
    }
    // chi tiet bai test
    public function get_test_detail($id){
        $this->db->where("test_id",$id);
        $query = $this->db->get("writing_test");
        return $query->row_array();
    }
    // Danh sach bai test
    public function get_question_by_test($param){
        $offset = ($param['page'] - 1) * $param['limit'];
        $this->db->select("");
        $this->db->where("type",$param['type']);
        $query = $this->db->get("writing_test",$param['limit'],$offset);
        return $query->result_array();
    }
    // chi tiet tung cau hoi
    public function detail_question($arrid){
        $this->db->where_in("writing_id",$arrid);
        $query = $this->db->get("writing_question");
        return $query->result_array();
    }
    // lay bai test tiep theo
    public function get_next_test($param = array()){
        $this->db->select("test_id");
        $this->db->where("test_id <",$param['test_id']);
        $this->db->where("type",$param['type']);
        $this->db->order_by("test_id","DESC");
        $query = $this->db->get("writing_test",1);
        return $query->row_array();
    }
    // lay cau tra loi
    public function get_answer($question_id = array()){
        $this->db->select("");
        $this->db->where_in("writing_id",$question_id);
        $query = $this->db->get("writing_question");
        return $query->result_array();
    }
    // lay thanh vien tra loi moi nhat
    public function get_answer_by_test($param){
        $this->db->select("*");
        $this->db->where("user_id",$param['user_id']);
        $this->db->where("test_id",$param['test_id']);
        if ($param['time']) {
            $this->db->where("time",$param['time']);
        }
        $this->db->order_by("answer_id","DESC");
        $query = $this->db->get("writing_question_to_user",$param['limit']);
        return $query->result_array();
    }
    public function get_question_by_arr($param){
        $this->db->select("sp.writing_id,sp.images,sp.title,sp.question,u.content,u.comment_teacher,u.point_teacher,u.answer_id");
        $this->db->where_in("sp.writing_id",$param['writing_id']);
        $this->db->join("writing_question_to_user as u","u.writing_id = sp.writing_id AND u.time=".$param['time']." AND u.user_id=".$param['user_id'],"LEFT");
        $query = $this->db->get("writing_question as sp",11);
        return $query->result_array();
    }
    /**
     * @param: type (1: tra loi tung cau hoi)
     */
    function insert_user_answer($param){

        $this->db->insert_batch('writing_question_to_user',$param);
    }
}