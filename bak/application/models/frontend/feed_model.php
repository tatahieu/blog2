<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Feed_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function reg_by_class(){
		$input = array(
            'email' => $this->input->post("email"),
            'mobile' => $this->input->post("mobile"),
            'fullname' => $this->input->post("fullname"),
            'class' => $this->input->post("class"),
            'type' => $this->input->post("type"),
            'date' => date('Y-m-d H:i:s')
        );
        $this->db->insert("feed_email_class",$input);
    }
    public function check_email_exist($email,$type){
        $this->db->where("email",$email);
        $this->db->where("type",$type);
        return $this->db->count_all_results("feed_email_class");
    }
    public function list_member_total($type){
        $this->db->join("class as c","c.id = f.class");
        $this->db->where('type',$type);
        return $this->db->count_all_results("feed_email_class as f");
    }
    public function list_member_reged($type,$limit = 10, $page = 1){
        $offset = ($page - 1) * $limit ;

        $this->db->select('f.fullname,f.date,c.name');
        $this->db->join("class as c","c.id = f.class");
        $this->db->where('type',$type);
        //$this->db->where('active',1);
        $query = $this->db->get("feed_email_class as f",$limit,$offset);
        return $query->result_array();
    }
    public function class_list(){
		$this->db->order_by('id','desc');
		$query = $this->db->get('class');
		return $query->result_array();
    }
}