function str_to_uri(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    // remove accents, swap ñ for n, etc
    var from = "àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ";
    var to = "aaaaaaaaaaaaaaaaaeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyyd";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }
    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/\+/g, '')// collapse dashes
            .replace('---','-')
            .replace('--','-');
    return str;
}
function createCookie(name,value,time) {
	if (time) {
		var date = new Date();
		date.setTime(date.getTime()+(time*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function delCookie(name) {
     createCookie(name,"",-1);
}

function update_status(url,id){
    var val = $("#status_"+id).find("img").attr("rel");
    $.post($site_url+ url,{ status: val, id: id}, function(data) {
        $("#status_"+id).html(data);
    });
}
function redirect($url){
    window.location.href = $url;
}
$(document).ready(function() {
    var parent = readCookie("parent_menu_admin");
    if (parent != null){
        $("#"+parent+" .submenu").show();
        $("#"+parent).attr('rel',1);
    }
    $("#common_menu .left_menu").bind("click",function(){
        if ($(this).attr('rel') == 1){
            $(this).parent().find('.submenu').hide();
            $(this).attr('rel',0);
        }
        else{
            $(this).parent().find('.submenu').show();
            $(this).attr('rel',1);
        }
    });
    $(".submenu").bind("click",function(){
        var id = $(this).parent().attr("id");
        createCookie("parent_menu_admin",id,3600);
    });
    ///////////////// Success noties /////////
    if (typeof(action_success) != 'undefined'){
        switch (action_success){
            case 'add': 
                $(".right_colum").find(".title_panel").after('<div class="action_success">Thêm dữ liệu thành công</div>').fadeIn(500,function(){
                    setTimeout(function(){
                        $(".right_colum").find(".action_success").fadeOut(2000);
                    },2000);
                 });
            break;
            case 'edit':
                $(".right_colum").find(".title_panel").after('<div class="action_success">Sửa dữ liệu thành công</div>').fadeIn(500,function(){
                    setTimeout(function(){
                        $(".right_colum").find(".action_success").fadeOut(2000);
                    },2000);
                 });
            break;
            case 'delete':
                $(".right_colum").find(".title_panel").after('<div class="action_success">Xóa dữ liệu thành công</div>').fadeIn(500,function(){
                    setTimeout(function(){
                        $(".right_colum").find(".action_success").fadeOut(2000);
                    },2000);
                 });
            break;
        }
    }
});