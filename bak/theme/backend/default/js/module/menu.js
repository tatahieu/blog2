var menu = {
    defaultparam: '',
    run: function(){
        //console.log(this.position);
        var self = this;
        var modid = $("#menu_module");
        // preload
        var mod = modid.val();
        this.module_change(mod);
        modid.change(function(){
             var mod = $(this).val();
             self.module_change(mod);
        });
        this.suggest();
    },
    setlink: function(mod) {

    },
    // change box moddule
    module_change: function(mod) {
        // show and hide autocomplete
        switch (mod){
            case 'home':
                $("#autoc").hide();
                //$("#menu_link").val("").hide();
            break;
            case 'link':
                $("#autoc").hide();
                $("#menu_link").val("").show();
            break;
            case 'contact':
                $("#autoc").hide();
                $("#menu_link").val("contact").hide();
            break;
            case 'news_cate':
            case 'product_cate':
            case 'news':
            case 'product':
            case 'advertise_cate':
                $("#autoc,#menu_link").val("").show();
            break;
            default:
                $("#autoc").hide();
        }
        $("#item_id").val(0);
        if (mod == this.defaultparam.module){
            $("#item_id").val(this.defaultparam.id);
            $("#menu_link").val(this.defaultparam.link);
        }
    },
    suggest: function() {
        self = this;
        var cache = {};
		$( "#autoc" ).autocomplete({
			minLength: 2,
            focus: function( event, ui ) {
				$(this).val( ui.item.value + ' - '+ ui.item.id );
				return false;
			},
            select: function( event, ui ) {
                mod = $("#menu_module").val();
				$(this).val( ui.item.value + ' - '+ ui.item.id );
                // set for link
				switch (mod){
                    default:
                    var link = ui.item.share_url;
				}
                $("#item_id").val(ui.item.id);
                $("#menu_link").val(link);
                return false;
			},
            change: function (event, ui) {
                if (!ui.item) {
                     $(this).val('');
                     $("#menu_link").val('');
                }
            },
			source: function( request, response ) {
			    mod = $("#menu_module").val();
			    request.module = mod;

				var term = request.term;
                var key = term;

				if ( key in cache ) {
					response( cache[ key ] );
					return;
				}
				$.getJSON( $site_url + "menu/autocomplete", request, function( data, status, xhr ) {
					cache[key] = data;
					response( data );
				});
			}
		});
    }
}