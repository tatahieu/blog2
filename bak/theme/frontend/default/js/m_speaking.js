var speaking = {
    question_audio: '',
    pharse: 1,
    param: [],
    speaking_run: function(){
        // nap truoc record player
        if (typeof (FLASH_DETECT) != 'undefined') {
            Recorder.initialize({
                swfSrc: $themeorg + "js/record/recorder.swf"
            });
        }
        else {
            alert("Máy bạn cần cài flash player mới có thể ghi âm được");
        }
        $("#first_question").bind("click",function(){
            $(this).remove();
        });
        $("#speaking").fadeIn(500,function(){
            speaking.userguide();
        });
        $("#speaking_exit").bind("click",function(){
            speaking.exit();
        });
        //////// keeplife after 2minute /////////
         setInterval(function(){
            $.get( "/users/keeplife", function( data ) {
            });
         },120000);
    },
    userguide: function(){
        var img = $("#speaking_userguide").find(".flash_support");
        // xem co bao nhieu cai anh trong day
        var size = img.size();
        // cau hoi binh thuong
        if (size <= 0) {
            $("#continue_button").remove();
            this.start();
        }
        else{
            var first = img.first().show();
            var audio = first.attr("data-sound");
            if (audio != ''){
                this.playuserguide(audio);
            }
            $("#continue_button").show(0,function(){
                //first.show();
                $(this).bind("click",function(){
                    first.remove();
                    speaking.userguide();
                })
            });
        }
    },
    start: function(){
        $("#question_show").show();
        $("#speaking_userguide").hide();
        var number = this.param.question_number;
        if (number == 7) {
            this.param.prepare_time = 30;
        }
        // cau 7 thoi gian chuan bi --> doc cau hoi --> tra loi
        this.pharse = (this.param.prepare_time > 0) ? 1 : 2
        // running
        if (this.question_audio != '' && number != 7){
            this.playsound(this.question_audio);
        }
        else{
            this.phraseaction();
        }
        $( "#response_count_down,#prepare_count_down" ).draggable({ cursor: "move"});
    },
    playuserguide: function(url) {
        $("#jplayer_speaking_userguide").jPlayer("setMedia", {
            mp3: url
        }).jPlayer("play");
        $("#jplayer_speaking_userguide").jPlayer({
            ready: function () {
                $(this).jPlayer("setMedia", {
                    mp3: url
                }).jPlayer("play");
            },
            swfPath: $themeorg + "js/jplayer",
            wmode: "window",
            supplied: "mp3",
            ended: function(){
                $("#continue_button").click();
            }
        });
    },
    // play audio gioi thieu + cau hoi
    playsound: function(url) {
        $("#jplayer_speaking_userguide").jPlayer("destroy");
        $("#jplayer_speaking").jPlayer("stop");
        $("#jplayer_speaking").jPlayer("setMedia", {
            mp3: url
        }).jPlayer("play");
        $("#jplayer_speaking").jPlayer({
            ready: function () {
                $(this).jPlayer("setMedia", {
                    mp3: url
                }).jPlayer("play");
            },
            swfPath: $themeorg + "js/jplayer",
            wmode: "window",
            supplied: "mp3",
            ended: function(){
                speaking.phraseaction();
            }
        });
    },
    phraseaction: function(){
        //console.log(speaking.pharse);
        switch (speaking.pharse) {
            // doc cau chuan bi
            case 1:
                speaking.pharse = 2;
                speaking.playsound($theme+'audio/prepare.mp3');
            break;
            // thoi gian chuan bi
            case 2:
                speaking.pharse = 3;
                speaking.prepare_action();
            break;
            case 3:
            case 5:
                speaking.pharse = 6;
                speaking.response_action();

            break;
            // 4,5 danh cho question 7
            case 4:
                speaking.pharse = 5;
                speaking.playsound($theme+'audio/beep.mp3');
            break;
            default:
            return;
        }
    },
    // thoi gian chuan bi
    prepare_action: function(){
        // neu thoi gian chuan bi > 0 dem nguoc thoi gian
        var number = speaking.param.question_number;
        if (this.param.prepare_time > 0){
            // set thoi gian chay
            function liftOff() {
                if (number == 7 && speaking.pharse == 3){
                    speaking.pharse = 4;
                    speaking.playsound(speaking.question_audio);
                    $('#prepare_count_down').hide().countdown('destroy');
                }
                else{
                    if (number == 3 || number == 10 || number == 11){
                    var audiofile = 'begin_speaking_now.mp3';
                    }
                    else if (number == 4 || number == 5 || number == 6 || number == 7 || number == 8 || number == 9) {
                        var audiofile = 'beep.mp3';
                    }
                    else{
                        var audiofile = 'response.mp3';
                    }
                    speaking.playsound($theme+'audio/' + audiofile);
                    $('#prepare_count_down').hide().countdown('destroy');
                }
            }
            shortly = new Date();
            shortly.setSeconds(shortly.getSeconds() + this.param.prepare_time);
            $('#prepare_count_down').fadeIn(500,function(){
               $(this).countdown({until: shortly,onExpiry: liftOff,compact: true,
    description: ''});
            });
        }
        else{
            if (number == 4 || number == 5 || number == 6 || number == 7 || number == 8 || number == 9){
                speaking.playsound($theme+'audio/beep.mp3');
            }
            else{
                speaking.playsound($theme+'audio/response.mp3');
            }
            // tra loi

        }
    },
    // ghi am
    response_action: function(){
        Recorder.record({
          start: function(){
                $('#response_allow_down').hide().countdown('destroy');
                function liftOfftime() {
                    Recorder.stop();
                    $("#dialog-message").html('<p>File ghi âm của bạn đang được tải lên hệ thống.</p><br><div id="progressbar"><div class="progress-label">Uploading...</div></div>');
                    ////////// PROGRESSS /////
                        var progressbar = $( "#progressbar" ),
                        progressLabel = $( ".progress-label" );
                        progressbar.progressbar({
                            value: false,
                            change: function() {
                                progressLabel.text( progressbar.progressbar( "value" ) + "%" );
                            }/*,
                            complete: function() {
                                progressLabel.text( "Complete!" );
                            }*/
                        });
                        speaking.progress();
                    ////
                    $( "#dialog-message" ).dialog()
                    speaking.record_upload();
                    $('#response_count_down').hide().countdown('destroy');
                }
                shortly2 = new Date();
                shortly2.setSeconds(shortly2.getSeconds() + speaking.param.response_time);
                $('#response_count_down').fadeIn(500,function(){
                    $(this).countdown({until: shortly2,onExpiry: liftOfftime,compact: true,description: ''});
                });
          }
        });
        function liftOfftime() {
            location.reload(); // reload sau 5s
        }
        shortly2 = new Date();
        shortly2.setSeconds(shortly2.getSeconds() + 5);
        $('#response_allow_down').fadeIn(500,function(){
            $(this).countdown({until: shortly2,onExpiry: liftOfftime,compact: true,description: ''});
        });
    },
    // tai file len mang
    record_upload: function() {
        method: "PUT"
        param = this.param,
        Recorder.upload({
          url:        $base_url + 'speaking/sendrecord',
          audioParam: "recordfile",
          params: { // Additional parameters (needs to be a flat object)
              "speaking_id" : param.speaking_id
          },
          progress: function (){
            alert("ok");
          },
          success: function(responseText){
            var result = $.parseJSON(responseText);
            if (result.error != ''){
                $("#dialog-message").html("<p>"+result.error+"</p>");
                 $( "#dialog-message" ).dialog({
                    modal: true,
                    open: function( event, ui ) {
                        setTimeout(function() {
                            redirect(param.link_exit);
                        }, 4000);
                    }
                 });
            }
            else{
                if (!result.next_test) // next question
                {
                    $("#dialog-message").html("<p>Hệ thống đã lưu câu trả lời. Hệ thống sẽ chuyển sang câu hỏi tiếp theo sau vài giây. Nếu không muốn đợi click nút OK</p>");

                     $( "#dialog-message" ).dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                location.reload();
                            }
                        },
                        open: function( event, ui ) {
                            setTimeout(function() {
                                location.reload(); // reload sau 10s
                            }, 10000);
                        }
                     });
                }
                else{
                    $("#dialog-message").html("<p>Lựa chọn</p>");
                     $( "#dialog-message" ).dialog({
                        modal: true,
                        buttons: {
                            'Exit': function() {
                                redirect(param.link_exit);
                            },
                            'Model answer': function() {
                                redirect("/speaking/answer/"+speaking.param.test_id);
                            }

                        },
                        open: function( event, ui ) {
                            setTimeout(function() {
                                redirect(param.link_exit);
                            }, 10000);
                        }
                     });

                }

            }
          }
        });
    },
    preview_mp3: function(url){
        this.stop_preview();
        $("#jplayer_mp3").jPlayer("setMedia", {
            mp3: url
        }).jPlayer("play");
        $("#jplayer_mp3").jPlayer({
            ready: function () {
                $(this).jPlayer("setMedia", {
                    mp3: url
                }).jPlayer("play");

            },

            swfPath: $themeorg + "js/jplayer",
            wmode: "window",
            supplied: "mp3",
            play : function(){
                $("#preview_playing").show();
            },
            ended: function(){
                $("#preview_playing").hide();
            }
        });
    },
    preview_wav: function (url){
        this.stop_preview();
        $("#jplayer_wav").jPlayer("setMedia", {
            wav: url
        }).jPlayer("play");
        $("#jplayer_wav").jPlayer({
            ready: function () {
                $(this).jPlayer("setMedia", {
                    wav: url
                }).jPlayer("play");
            },
            swfPath: $themeorg + "js/jplayer",
            wmode: "window",
            supplied: "wav",
            play: function(){
                $("#preview_playing").show();
            },
            ended: function(){
                $("#preview_playing").hide();
            }
        });
    },
    stop_preview: function (){
        $("#preview_playing").hide();
        $(".player_preview").jPlayer("stop");
    },
    point_form: function(id){
        id = parseInt(id);
        if (id <= 0) return false;
        var default_point = $("#point_view_"+id).text();
        var default_comment = $("#point_comment_"+id).text();
        $("#point_teacher_form").html('<table width="100%"><tr><td width="60px">Điểm:</td><td><input id="teacher_point" type="text" name="point" value="'+default_point+'"></td></tr><tr><td>Nhận xét:</td><td><textarea style="width: 100%; height: 80px" id="teacher_comment">'+default_comment+'</textarea></td></tr></table>');
        $("#point_teacher_form" ).dialog({
            modal: true,
            buttons: {
                'Chấm điểm': function() {
                    var point = parseInt($("#teacher_point").val());
                    var teacher_comment = $("#teacher_comment").val();
                    $.post($base_url + 'admin/speaking/point_detail', {point: point, comment: teacher_comment, record_id: id}, function(data) {
                        alert(data.data);
                        if (data.result == 'success'){
                            $("#point_view_"+id).text(point);
                            $("#point_comment_"+id).text(teacher_comment);
                            $("#point_teacher_form" ).dialog("close");
                        }
                    },"JSON");
                },
                'Đóng': function(){
                    $("#point_teacher_form" ).dialog("close");
                }
            },
         });
    },
    progress: function () {
        var time = (speaking.param.response_time + 7) * 10;
        var val = $( "#progressbar" ).progressbar( "value" ) || 0;
        $( "#progressbar" ).progressbar( "value", val + 1 );
        if ( val < 97 ) {
            setTimeout(function () {
                 speaking.progress();
             }, time);
        }
    },
    exit: function(){
         $("#dialog-message").html("<p>If you log out, the system will NOT save your unfinished answers on file. Are you sure to log out? </p>");
         if (speaking.param.type == 0) {
             $( "#dialog-message" ).dialog({
                modal: true,
                buttons: {
                    Ok: function() {
                        redirect(speaking.param.link_exit);
                    },
                    Cancel: function(){
                        $("#dialog-message" ).dialog("close");
                    }
                },
                open: function( event, ui ) {
                    setTimeout(function() {
                        $("#dialog-message" ).dialog("close");
                    }, 15000);
                }
             });
         }
         else {
             $("#dialog-message").html("<p>If you log out, the system will NOT save your unfinished answer on file. Are you sure to log out? </p>");
             $( "#dialog-message" ).dialog({
                modal: true,
                buttons: {
                    Ok: function() {
                        redirect(speaking.param.link_exit);
                    },
                    Cancel: function(){
                        $("#dialog-message" ).dialog("close");
                    },
                    "Model Answer": function(){
                        redirect("/speaking/answer/" + speaking.param.test_id);
                    }
                },
                open: function( event, ui ) {
                    setTimeout(function() {
                        $("#dialog-message" ).dialog("close");
                    }, 15000);

                 }
            });
        }
    }
}
function next_question(){
    var max_page = $("#change_question").attr("max-page");
    var current_page = parseInt($("#change_question").attr("current-page"));
    var page = (current_page == max_page) ? 1 : current_page + 1;
    showquestion(current_page,page);
}
function prev_question(){
    var max_page = $("#change_question").attr("max-page")
    var current_page = parseInt($("#change_question").attr("current-page"));
    var page = (current_page == 1) ? max_page : current_page - 1;
    showquestion(current_page,page);
}
function showquestion(curpage, page){
    // tat tieng
    speaking.stop_preview();
    // chuyen tab
    $("#speaking_tab_"+curpage).hide();
    $("#speaking_tab_"+page).show();
    $("#change_question").attr("current-page",page);
}
function change_answer(id){
    // add style selected
    $(".answer_number").removeClass("selected");
    $("#answer_number_"+id).addClass("selected");
    //show content
    $(".speaking_tab").hide();
    $("#speaking_tab_"+id).fadeIn(300);
}
function show_tab_script($number){
    if ($(".answer_txt_"+$number).is(':visible')){
        $(".answer_txt_"+$number).fadeOut(500);
    }
    else {

        $(".answer_txt_"+$number).fadeIn(500);
    }
}
function show_teacher_comment(id){
    var value = $("#point_comment_"+id).text();
    $("#comment_teacher_view").html("<p>"+value+"</p>");
    $("#comment_teacher_view" ).dialog({
        modal: true
    });
}