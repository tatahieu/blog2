function check_user_exist(){
    var username = $("#users_register input[name=username]").val();
    $("#users_register input[name=username]").bind("focus",function(){
         $("#check_username").html("check");
         $("#check_username").attr("rel",0);
    });
    if (username.length <= 3 || username.length > 40){
        $("#check_username").html('<image src="'+$image+'no-tick.png">');
        return false;
    }
    if ($("#check_username").attr("rel") != "1"){
        $.post($base_url+'users/ajax_check_username',{ username:username }, function(data) {
            if (data == "1"){
                $("#check_username").html('<image src="'+$image+'tick.png">');

            }
            else{
                $("#check_username").html('<image src="'+$image+'no-tick.png">');
            }
            $("#check_username").attr("rel",1);
        });
    }
}
function check_email_exist(){
    var email = $("#users_register input[name=email]").val();
    $("#users_register input[name=email]").bind("focus",function(){
         $("#check_email").html("check");
         $("#check_email").attr("rel",0);
    });
    if (email.length <= 3 || email.length > 40){
        $("#check_email").html('<image src="'+$image+'no-tick.png">');
        return false;
    }
    if ($("#check_email").attr("rel") != "1"){
        $.post($base_url+'users/ajax_check_email',{ email:email }, function(data) {
            if (data == "1"){
                $("#check_email").html('<image src="'+$image+'tick.png">');

            }
            else{
                $("#check_email").html('<image src="'+$image+'no-tick.png">');
            }
            $("#check_email").attr("rel",1);
        });
    }
}