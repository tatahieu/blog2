var course = {
    result: '',
    currentword : 1,
    currenttype : 1,
    total: 0,
    currenttypeid: 0,
    retypequestion: [],
    word: function(){
        var $total = this.total;
        // play first load
        this.word_change(1,1);
        // title click
        $("#word_title").find("li").bind("click",function(){
              $("#word_title").find("li").removeClass("active");
              $(this).addClass("active");
              var rel = $(this).attr("rel");
              $(".course_word_item").hide();
              $("#"+rel).fadeIn(500);
              if (rel == 'retype'){
                    course.retyperefresh();
              }
              else if (rel == 'word_drop'){
                    course.droprefresh();
              }
              else{
                    course.word_change(1);
              }
        });
        // word list
        $("#word_list .left").find("img").bind("click",function(){
            var rel = $(this).attr("rel");
            course.word_change(rel);
        });
        $("#word_list").find(".btn-next").bind("click",function(){
            var rel = (course.currentword == $total) ? 1 : (parseInt(course.currentword) + 1);
            course.word_change(rel)
        });
        $("#word_list").find(".btn-prev").bind("click",function(){
            var rel = (course.currentword == 1) ? $total : (parseInt(course.currentword) - 1);
            course.word_change(rel)
        });
        $("#word_list .flip-card, #word_list .item").bind("click",function(){
            var rel = course.currentword;
            var divshow = $("#word_list").find(".data"+rel).find(".translate");
            if (divshow.is(':hidden')){
                divshow.hide().fadeIn();
                $("#word_list").find(".data"+rel).find(".image").hide();
            }
            else{
                divshow.hide();
                $("#word_list").find(".data"+rel).find(".image").hide().fadeIn();
            }
        });
        // nghe lai tu
        $("#word_list .speaker").bind("click",function(){
             var rel = course.currentword;
             course.word_change(rel,1);
        });
        // drop text
        $("#loadResultDrop").bind("click",function(){
            if ($(this).attr("rel") == 1){
                return false;
            }
            course.word_result();
            $(this).attr("rel",1);
        });
        $("#loadCheckDrop").bind("click",function(){
            if ($(this).attr("rel") == 1) return false;
            course.word_check();
            $("#loadResultDrop").attr("rel",1);
            $(this).attr("rel",1);

        });
        // refresh drop
        $("#droprefresh").bind("click",function(){
            course.droprefresh();
        });

        // retype
        $("#retype_next").bind("click",function () {
            course.retypenext();
        });
        $("#retype_prev").bind("click",function () {
            course.retypeprev();
        });
        $("#retype .listen-again").bind("click",function(){
            course.retypeshow(0,'nghelai');
        });
        // help button
        $("#retype .show-answer").bind("click",function(){
            course.retypehelp();
        });
        // go dap an roi enter
        $('#retype .textbox').bind('keypress', function(e) {
        	if(e.keyCode==13){
        		course.retypeanswer(this.value);
        	}
        });
        if (typeof (FLASH_DETECT) != 'undefined') {
            this.word_record();
        }
        this.drop();
    },
    word_record: function(){
        Recorder.initialize({
            swfSrc: $themeorg + "js/record/recorder.swf"
        });
        $("#word_list").find(".record").bind("click",function(){
            course.record_recording();
        });
    },
    record_recording: function(){
        Recorder.record({
          start: function(){
            course.record_stop_rule_2();
            $("#word_list").find(".record").addClass("record_active");
            $("#word_list").find(".stop").addClass("stop_active");
            $("#word_list").find(".play").addClass("play_active");
            $("#word_list").find(".stop_active").bind("click",function(){
                course.record_stop();
            });
            $("#word_list").find(".play_active").bind("click",function(){
                course.record_play();
            });
          },
          progress: function(milliseconds){
            // cho thu am duoi 1 phut
            if (milliseconds > 60 * 1000){
                course.record_stop();
            }
          }
        });
    },
    record_play: function() {
        this.record_stop();
        Recorder.play({
          progress: function(milliseconds){
          }
        });
        $("#word_list").find(".stop").addClass("stop_active");
        $("#word_list").find(".play").removeClass("play_active");
        $("#word_list").find(".stop_active").bind("click",function(){
            course.record_stop_rule_2();
        })
    },
    record_stop_rule_2: function(){
        Recorder.clearBindings("play");
        $("#word_list").find(".play").addClass("play_active");
    },
    record_stop: function() {
        Recorder.stop();
        $("#word_list").find(".stop").removeClass("stop_active");
        $("#word_list").find(".record").removeClass("record_active");
    },
    record_upload: function() {
        Recorder.upload({
          url:        $base_url + 'course/senddatarecord',
          audioParam: "recordfile",
          success: function(){
            alert("your file was uploaded!");
          }
        });
    },
    //////////////////utility//////////
    word_change: function(rel,notload){
        if (!notload){
            $("#word_list .left").find("img").removeClass("active");
            $("#word_list .thumb"+rel).addClass("active");
            $("#word_list .right").find(".item").hide();
            $("#word_list .right").find(".data"+rel).hide().fadeIn();
            this.currentword = rel;
        }
        var txt = $("#word_list").find(".data"+rel).find(".img-caption").text();
        this.playsound($base_url + "course/getgoogle/"+txt);
    },
    playsound: function(url) {
        $("#jplayer_word").jPlayer("setMedia", {
            mp3: url
        }).jPlayer("play");
        $("#jplayer_word").jPlayer({
            ready: function () {
                $(this).jPlayer("setMedia", {
                    mp3: url
                }).jPlayer("play");
            },
            swfPath: $themeorg + "js/jplayer",
            wmode: "window",
            supplied: "mp3"
        });
    },
    drag: function (){
        $("#word_drop .word_item").draggable({
            revert: "invalid",
            helper: "clone",
            cursor: "move",
            zIndex: 10
        });
    },
    drop: function (){
        this.drag();
        $("#word_drop .droppable").droppable({
            drop: function (event, ui) {
                var $draggable = $(ui.draggable);
                // neu da co drop tu truoc thi tra ra
                if ($(this).find(".word_item").size() > 0){
                    $(this).find(".word_item").appendTo("#word_drop .left");
                }
                $(this).append('<div class="word_item">' + $draggable.text()+ '</div>');
                $draggable.remove();
                course.drag();
            },
            hoverClass: "hover"
        });
        $("#word_drop .left").droppable({
            accept : "#word_drop .right .word_item",
            drop: function (event, ui) {
                var $draggable = $(ui.draggable);
                $(this).append('<div class="word_item">' + $draggable.text()+ '</div>');
                $draggable.remove();
                course.drag();
            }
        });

    },
    word_result: function (){
        $("#word_drop .word_drop").each(function(){
            $(this).prepend('<div class="result-background"></div>');
            var id = $(this).attr("id");
            var value = $(this).find(".word_item").text().trim();
            // ket qua dung
            if (course.result[id] == value){
                $(this).prepend('<div class="result_img"><img class="correct-icon" src="' + $theme + 'images/icons/flashcard-correct-shadow.png" alt="correct"></div>');
            }
            else{
                $(this).prepend('<div class="result_img"><img class="incorrect-icon" src="' + $theme + 'images/icons/flashcard-incorrect-shadow.png" alt="incorrect"></div>')
            }
        });
        //destroy drop
        this.dropdestroy();
    },
    word_check: function(){
        $("#word_drop .word_drop").each(function(){
            var id = $(this).attr("id");
            $(this).find(".word_item").remove();
            $(this).append('<div class="word_item">' + course.result[id]+ '</div>');
        });
        //destroy drop
        this.dropdestroy();
    },
    dropdestroy: function(){
        //$("#word_drop .word_item").draggable( "destroy" );
        $("#word_drop .left, #word_drop .droppable" ).droppable( "destroy" );
    },
    droprefresh: function() {
        // clear all div
        $("#word_drop").find(".word_item").remove();
        $("#word_drop .word_drop").find("div").remove();
        // render new word left
        $.each(this.result,function(index,value){
            $("#word_drop .left").prepend('<div class="word_item">' + value + '</div>');
        });
        $("#loadResultDrop,#loadCheckDrop").attr("rel",0);
        this.drop();
    },
    // button next and prev retype
    retypenext: function() {
        var current = this.currenttype;
        if (current == this.total) return false;
        this.retypeshow(current + 1);
    },
    retypeprev: function() {
        var current = this.currenttype;
        if (current == 1) return false;
        this.retypeshow(current - 1);
    },
    retyperefresh: function(){
        this.retypeshow(1);
        $("#retype .correct,#retype .incorrect").find(".number").text(0);
    },
    retypeshow: function(rel,type){
        if (type == 'nghelai'){
            var rel = this.currenttype;
        }
        else{
            $("#retype .img-main").hide();
            $("#retype").find(".item"+rel).fadeIn(250);
        }
        var $id = $("#retype").find(".item"+rel).attr("id");
        $id = $id.replace("type_","");
        this.currenttypeid = $id;
        // play sound
        var txt = this.result['pic_'+$id];
        this.playsound($base_url + "course/getgoogle/"+txt);
        // con lai bao nhieu cau hoi
        $("#retype .remain").find(".number").text(this.total - rel);
        // an nut goi y
        $("#retype").find(".answer_help").hide().empty();
        this.currenttype = rel;
    },
    retypehelp: function(){
        if (this.currenttypeid <= 0) return false;
        var txt = this.result['pic_'+this.currenttypeid];
        $("#retype").find(".answer_help").text(txt).fadeIn(250);
    },
    retypeanswer: function(value){
        if (this.currenttypeid <= 0 || value == '' || value == "undefined") return false;
        var txt = this.result['pic_'+this.currenttypeid];
        //console.log(this.retypequestion);
        // kiem tra ket qua
        if (txt == value.trim()){
            // tang diem tra loi dung
            if( this.retypequestion.indexOf( this.currenttypeid ) < 0 ){
                this.retypequestion.push(this.currenttypeid);
                var score = $("#retype .correct").find(".number").text();
                $("#retype .correct").find(".number").text(parseInt(score) + 1);
            }
            $("#retype").find(".answer_result").html('<img class="type_correct" src="'+$theme+'images/icons/flashcard-correct-shadow.png">').fadeIn(500,function(){
                setTimeout(function(){
                    $("#retype .answer_result").fadeOut().empty();
                    course.retypenext();
                    $("#retype").find(".textbox").val("");

                },1000);
            });
        }
        else{
            // tang diem tra loi sai
            if( this.retypequestion.indexOf( this.currenttypeid ) < 0 ){
                this.retypequestion.push(this.currenttypeid);
                var score = $("#retype .incorrect").find(".number").text();
                $("#retype .incorrect").find(".number").text(parseInt(score) + 1);
            }
            $("#retype").find(".answer_result").html('<img class="type_incorrect" src="'+$theme+'images/icons/flashcard-incorrect-shadow.png">').fadeIn(1500,function(){
                setTimeout(function(){
                    $("#retype .answer_result").fadeOut().empty();
                    $("#retype").find(".textbox").val("");
                },1000);
            });
        }
    }
}
var course_complete = {
    currentindex: 1,
    total: 1,
    answer: '',
    isresult: 0,
    run: function() {
        this.showquestion(this.currentindex);
        $("#course_complete .inactive-button").bind("click",function(){
            var rel = $(this).text();
            course_complete.showquestion(rel);
        });
        $("#complete_next").bind("click",function(){
            course_complete.showquestion(parseInt(course_complete.currentindex) + 1);
        });
        $("#complete_prev").bind("click",function(){
            course_complete.showquestion(parseInt(course_complete.currentindex) - 1);
        });
        $("#complete_result").bind("click",function(){
            var flag = 1;
            $(".wccontent").find("input").each(function(){
                if ($(this).val() == '') {
                    flag = 0;
                    if (confirm("Vẫn còn chỗ trống chưa điền. Bạn có chắc muốn xem đáp án không")) {
                        flag = 1;
                    }
                    return false;
                }
            });
            if (flag == 1) {
                course_complete.showresult();
            }
        });
        $("#complete_check").bind("click",function(){
            course_complete.showcheck();
        });

    },
    showquestion: function(index){
        if (index > this.total || index < 1) return false;
        this.currentindex = index;
        if (this.total > 1){
            $("#course_complete").find(".inactive-button").removeClass("selected");
            $("#course_complete").find(".page_"+index).addClass("selected");
            $("#course_complete").find(".item").hide();
            $("#course_complete").find(".ques_"+index).show();
        }
        $("#course_complete .ques_"+index).show(0,function(){
            var url  = UPLOAD_URL + 'files/' + $(this).find(".audio").text();
            //$("#jquery_jplayer_1").jPlayer("clearMedia");;
            course_complete.player(url);
        })
    },
    showresult: function(){
        this.isresult = 1;
        $("#course_complete").find(".item").each(function(index){
            var id = $(this).attr("id");
            id = id.replace("course_com_","");
            $(this).find("input").each(function(index){
                $(this).val(course_complete.answer[id][index]);
            });
        });
    },
    showcheck: function(){
        if (this.isresult == 1) return false;
        var trueresult = 0;
        var falseresult = 0;
        $("#course_complete").find(".item").each(function(index){
            var id = $(this).attr("id");
            id = id.replace("course_com_","");

            $(this).find("input").each(function(index){
                // ket qua dung
                if ($(this).val().trim() == course_complete.answer[id][index].trim()){
                    $(this).after('<img src="'+$theme+'images/icons/true-false-tick-green-24.png" class="false-icon" style="margin: -2px 2px 0px 1px; box-shadow: none; border: medium none; width: 18px; height: 18px;">');
                    trueresult ++;
                }
                else{
                    $(this).after('<img src="'+$theme+'images/icons/true-false-cross-red-24.png" class="false-icon" style="margin: -2px 2px 0px 1px; box-shadow: none; border: medium none; width: 18px; height: 18px;">');
                    falseresult ++;
                }
            });
        });
        this.isresult = 1;
        alert("Trả lời đúng: "+trueresult+" \n Trả lời sai: " + falseresult);
    },
    player: function(url){
        //console.log(url);
        $("#jquery_jplayer_1").jPlayer("setMedia", {
            mp3: url
        });
        mediaplayer.params = {
            url: {mp3: url},
        };
        mediaplayer.mediaconfig = {
    		supplied: 'mp3',
        };
        mediaplayer.run();
    }
}