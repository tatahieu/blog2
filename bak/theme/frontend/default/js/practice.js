function send_answer_training(as){
    // kiem tra xem da tra loi het chua

    var form = $('#testing_form');
    var total = $("#testing_form input[type=radio]:checked").size();
    if (total != as){
        alert("Bạn chưa trả lời hết các câu hỏi");
        return false;
    }
    $(".testing_submit").remove();
    $.ajax({
        type: "POST",
        url: form.attr( 'action' ),
        data: form.serialize(),
        success: function( data ) {
            if (data == 'error'){
                alert("Lỗi dữ liệu đầu vào");
                return false;
            }
            var json = $.parseJSON(data);
            // hien ket qua
            $(".score span").text(json.result);
            if (json.answer){
                $.each(json.answer,function (i,item){
                    $("#testing_answer_"+i+" input[value="+item+"]").parent().find(".answer_status").html('<img src="'+$theme+'images/icon_true.jpg">');
                    if (parseInt($("#testing_answer_"+i+" input:checked").val()) != parseInt(item)){
                        $("#testing_answer_"+i+" input:checked").parent().find(".answer_status").html('<img src="'+$theme+'images/icon_false.jpg">');
                    }
                    $(".answer_content").show();
                    $("#test_random .div_text").addClass("result");
                });
            }
        }
    });
}
function tabscript(as){
    var form = $('#testing_form');
    var total = $("#testing_form input[type=radio]:checked").size();
    if (total != as){
        alert("Bạn chưa trả lời hết các câu hỏi");
        return false;
    }
    $.ajax({
        type: "POST",
        url: form.attr( 'action' ),
        data: form.serialize(),
        success: function( data ) {
            if (data == 'error'){
                alert("Lỗi dữ liệu đầu vào");
                return false;
            }
            var json = $.parseJSON(data);
            // hien ket qua
            $(".score span").text(json.result);
            if (json.answer){
                $.each(json.answer,function (i,item){

                    if (parseInt($("#testing_answer_"+i+" input:checked").val()) != parseInt(item)){
                        $("#testing_answer_"+i+" input:checked").parent().find(".answer_status").html('<img src="'+$theme+'images/icon_false.jpg">');
                    }
                    else{
                        $("#testing_answer_"+i+" input[value="+item+"]").parent().find(".answer_status").html('<img src="'+$theme+'images/icon_true.jpg">');
                    }
                });
            }
        }
    });
}
function alias_answer(){
    $(".full_answer").removeClass("ansselected");
    var $i = 0;
    $(".alias_answer:visible").each(function(i){
        $("#answer_"+($(this).attr("rel"))).addClass("ansselected");
        $i ++;
    });
    return $i;
}
function fulltest_next(){
    var current_view = parseInt($(".current_view").text());
    var next = current_view + 1;
    var total = $(".alias_answer:visible").size();
    $("#question_"+(current_view - total + 1)).parent().hide();
    $("#question_"+next).parent().show();
    $(".waiting").hide();
    $("#question_"+next).show();
    $i = alias_answer();
    next = next + $i - 1;
    $(".current_view").text(next);
    if ($("#question_"+(next+1)).length <= 0){
        $("#button_next_full").hide();
    }
    if (next != 1){
    $("#button_back_full").show();}
    ///////// SCROLL TO ANSWER /////////////
    $('html,body').animate({
      scrollTop: $('#fulltest_form').offset().top
    }, 200);
    $('#fulltest_form').find(".result").animate({
       scrollTop: 0
    }, function(){
        var post_parent = $(this).offset().top;
        var post = $('#answer_'+ next).offset().top;
        $(this).scrollTop(post - post_parent);
    });
    //////////////// UNSET SOUND ////////////
    stop_audio_html5();
    
    //playsound(next,current_view,'Question '+next);
}
function fulltest_back(crv){
    var $count = $(".alias_answer:visible").size();
    var $current_view = parseInt($(".current_view").text());
    var crv = $current_view - $count;
    $(".current_view").text(crv);
    //////
    $("#question_"+(crv + 1)).hide();
    $("#question_"+(crv + 1)).parent().hide();
    var id = "question_"+crv;
    if ($("#question_"+crv).length == 0){
        id = $(".alias_answer[rel="+crv+"]").parent().attr("id");
    }
    $("#"+id).parent().show();
    $(".waiting").hide();
    $("#"+id).show();
    alias_answer();
    $("#button_next_full").show();
    if (crv == 1){
        $("#button_back_full").hide();
    }
    ///////// SCROLL TO ANSWER /////////////
    $('html,body').animate({
      scrollTop: $('#fulltest_form').offset().top
    }, 200);
    $('#fulltest_form').find(".result").animate({
       scrollTop: 0
    }, function(){
        var post_parent = $(this).offset().top;
        var post = $('#answer_'+ crv).offset().top;
        $(this).scrollTop(post - post_parent);
    });
    //////////////// UNSET SOUND ////////////
    stop_audio_html5();
    
    //playsound(crv,$current_view,'Question '+crv);
}
function stop_audio_html5() {
    var audioClass = document.getElementsByClassName("html5_audio");
    if (audioClass.length > 0) {
        $.each( audioClass, function( index ) {
            audioClass[index].pause();
        });
    }
}
function playsound(id,idstop,name){
    $("#jplayer_"+idstop).jPlayer("stop");
    var url = $("#media_player_"+id).attr("data-url");
    if ($("#jplayer_"+id).length <= 0) {
       $("#media_player_"+id).html('<div id ="jplayer_'+id+'"></div><div id="jp_container_1" class="jp-audio"><div class="jp-type-single"><div class="jp-gui jp-interface"><ul class="jp-controls"><li><a href="javascript:;"class="jp-play"tabindex="1">play</a></li><li><a href="javascript:;"class="jp-pause"tabindex="1">pause</a></li><li><a href="javascript:;"class="jp-stop"tabindex="1">stop</a></li><li><a href="javascript:;"class="jp-mute"tabindex="1"title="mute">mute</a></li><li><a href="javascript:;"class="jp-unmute"tabindex="1"title="unmute">unmute</a></li><li><a href="javascript:;"class="jp-volume-max"tabindex="1"title="max volume">max volume</a></li></ul><div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div><div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div><div class="jp-time-holder"><div class="jp-current-time"></div><div class="jp-duration"></div><ul class="jp-toggles"><li><a href="javascript:;"class="jp-repeat"tabindex="1"title="repeat">repeat</a></li><li><a href="javascript:;"class="jp-repeat-off"tabindex="1"title="repeat off">repeat off</a></li></ul></div></div><div class="jp-title"><ul><li>'+name+'</li></ul></div><div class="jp-no-solution"><span>Update Required</span>To play the media you will need to either update your browser to a recent version or update your<a href="http://get.adobe.com/flashplayer/"target="_blank">Flash plugin</a>.</div></div></div>');
    }
    $("#jplayer_"+id).jPlayer("setMedia", {
        mp3: url
    }).jPlayer("play");
    $("#jplayer_"+id).jPlayer({
        ready: function () {
            $(this).jPlayer("setMedia", {
                mp3: url
            }).jPlayer("play");
        },
        solution:"flash,html",
        swfPath: $theme + "js/jplayer"
    });
}
function send_answer_fulltest(){
    $("#submit_fulltest").hide();
    var form = $('#fulltest_form');
    $.ajax({
        type: "POST",
        url: form.attr( 'action' ),
        data: form.serialize(),
        dataType: 'json',
        success: function( json ) {
            if (json.result == 'error'){
                return showdialog("Kết quả",json.message);
            }
            $("#show_part").show();
            // hien ket qua

            $(".score span").text(json.result);
            $(".score").show();
            $(".waiting").show();
            $(".result_hide").show();
            $("#displayCounter").remove();
            $("#show_count_down").remove();
            $("#button_back_full").remove();
            $("#button_next_full").remove();
            if (json.answer){
                var k = 1;
                $.each(json.answer,function (i,item){
                    //$(".testing_answer_"+i+" input[value="+item+"]").parent().find(".answer_status").html('<img src="'+$theme+'images/icon_true.jpg">');
                    $(".qanswer_"+i+"_"+item).find(".answer_status").html('<img src="'+$theme+'images/icon_true.jpg">');
                    $(".anwser_hide_"+i+"_"+item).find(".answer_status").html('<img src="'+$theme+'images/icon_true.jpg">');
                    var my_answer = parseInt($(".testing_answer_"+i+" input:checked").val());
                    if (my_answer != parseInt(item)){
                        $(".testing_answer_"+i+" input:checked").parent().find(".answer_status").html('<img src="'+$theme+'images/icon_false.jpg">');
                        $(".anwser_hide_"+i+"_"+ my_answer).find(".answer_status").html('<img src="'+$theme+'images/icon_false.jpg">');
                    }
                    //var mediaurl = $("#media_player_" + item).attr("data-url");
                    //$("#media_player_" + k).html('<object width="200" height="20" data="'+$image+'player.swf" type="application/x-shockwave-flash"><param value="'+$image+'player.swf" name="movie"><param value="mp3='+mediaurl+'&amp;showvolume=1&amp;showinfo=1&amp;showloading=always&amp;showstop=1&amp;autoload=1" name="FlashVars"></object>');
                    //k ++;
                });
            }
            // skill test
            if (json.type == 3){
                showdialog("Kết quả","Bạn trả lời được "+json.result+"/"+json.total+" câu");

            }
            else{
                showdialog("Kết quả","<p>Bạn được :<br><span style=\"font-weight: bold; font-size: 15px;\">"+ parseInt(json.score_list) +"</span> điểm phần list<br><span style=\"font-weight: bold; font-size: 15px;\">" + parseInt(json.score_read) + "</span> điểm phần read<br>Tổng số: <span style=\"font-weight: bold; font-size: 15px; color: red;\">"+(json.score_list + json.score_read) + "</span> điểm<p><p>" + json.save_score +"</p>");
            }

            $(".answer_content").show();
            $(".full_answer").addClass("ansselected");
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR,textStatus,errorThrown);
        }
    });
}
function send_answer_writing(){
    $(".show_answer").show();
}
function show_part(id){
    $(".media .full").hide();
    $("#part_"+id).show();
    $("#part_"+id+" div").show();
    $('html,body').animate({
      scrollTop: $('#fulltest_form').offset().top
    }, 200);
}
function showdialog(title,msg){
    $("#dialog-message").html(msg);
    $( "#dialog-message" ).dialog({
        modal: true,
        buttons: {
            'Xem đáp án': function() {
                $(this).dialog("close");
            },
            'Thoát' : function() {
                window.history.back();
            }
        },
        title: title
    });
    $(".ui-dialog").css("z-index",9999);
    $(".ui-widget-overlay").css("z-index",9998);
}