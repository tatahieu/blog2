$(document).ready(function(){
    $("#header").find(".menu").bind("click",function(){
        if ($('#menutop').is(':visible')) {
            $("#menutop").hide();
            $(this).removeClass("menu_active");
            //clearTimeout(timeoutmenu);
        }
        else {
            $(this).addClass("menu_active");
            $("#menutop").show(400);
        }
    });
    $("#footer").find(".menu").bind("click",function(){
        $('html, body').animate({scrollTop:0},500);
        $("#header").find(".menu").addClass("menu_active");
        $("#menutop").show(400);
    });
    // show menu left
    $(".menuchild").parent().bind("click",function() {
        $.fancybox.open( $(this).html(), {width: '100%',autoSize: false,wrapCSS: 'menu_popup'});
    }).find(".link1").attr("href","javascript:void(0)");
    // show member menu
    $("#header").find(".member").bind("click",function(){
        $(this).find(".member_extend").toggle();
    });
    // show main menu
    $("#menu_drop_icon").bind("click",function(){
        $(this).hide();
        $("#menuleft").find(".ulmenu1").fadeIn();
    })
})