function quick_login(){
    $( "#mini_login" ).dialog({
        modal: true,
        buttons: {
            'Đăng nhập': function() {
                $.post($base_url+'users/login_ajax', $('#quick_login').serialize(),function(data){
                    if (data.success){
                        $( "#mini_login" ).dialog("close");
                        message (data.success);
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }
                    else{
                        $("#quick_login_error").html(data.error);
                    }
                },'JSON');
            },
            'Quên mật khẩu': function() {
                $( "#mini_login" ).dialog("close");
                user_forgot_pass();
            }
        }
     });
}
function user_forgot_pass(){
    $( "#user_forgot" ).dialog({
        modal: true,
        buttons: {
            'Gửi mật khẩu': function() {
                var emailuser = $("#users_forgot_email").val();
                $.post($base_url+'users/forgotajax', {email: emailuser},function(data){
                    if (data.success){
                        $( "#user_forgot" ).dialog("close");
                        message (data.success);
                    }
                    else{
                        $("#user_forgot_error").html(data.error);
                    }
                },'JSON');
            }
        }
     });
}
function message(text){
    $("#message_alert").html("<p>"+text+"</p>").dialog({
        modal: true,
        buttons: {
            'Ok': function() {
                $("#message_alert").dialog("close");
            }
        },
        open: function( event, ui ) {
            setTimeout(function() {
                $("#message_alert").dialog("close");
            }, 3000);
        }

     });
}