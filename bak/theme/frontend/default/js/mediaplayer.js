var mediaplayer = {
    params: {
        url: '',
        autoplay: 0,
    },
    skin: 'mac',
    id: 'mediaplayer',
    object_id: 'jquery_jplayer_1',

    run: function (){
        if ($("#"+this.id).html().length <= 1){
            this.html();
        }
        this.script();
    },
    mediaconfig: '',
    fullscreen: 0,
    html: function(){
        this.template();
        var html =
            ////////////// OBJECT render ///
            '<div id="'+this.object_id+'"></div>' +
            ///////////// HTML player ////////
            '<div id="jp_container_1" class="player-container"><div class="jp-jplayer jp-jplayer-video" id="jplayer"></div>' +
			//////////// onpentag Interface -->
			'<div class="jp-interface" id="jp-interface">' +

            /////////Controls////////
            '<div class="jp-controls"><a href="#" class="jp-play jp-button" tabindex="1"></a><a href="#" class="jp-pause jp-button" tabindex="1"></a><span class="divider"></span></div>' +

			//////// Progress Bar //////////
			'<div class="jp-progress-container"><div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"><div class="bullet"><div class="jp-current-time"></div></div></div></div></div><span class="divider"></span></div>' +

			////Volume Bar////
			'<div class="jp-volume-bar-container"><a href="#" class="jp-mute" tabindex="1"></a><a href="#" class="jp-unmute" tabindex="1"></a><div class="jp-volume-bar"><div class="jp-volume-bar-value"><div class="bullet"></div></div></div></div>';
            //// Fullscreen ////////
            if (this.fullscreen){
                html += '<span class="divider"></span><div class="jp-full-screen-container"><a href="#" class="jp-full-screen jp-button" tabindex="1"></a><a href="#" class="jp-restore-screen jp-button" tabindex="1"></a></div>';
            }
            /// close tag interface /////////
			html += '</div></div>';
        $("#"+this.id).append(html);
    },
    template: function(){
        //$("head").append('<link href="' + $theme + 'js/jplayer/skin/' + this.skin+ '/style.css" media="screen" rel="stylesheet" type="text/css"/>');
    },
    script: function(){
        var url = this.params.url;

        $("#"+this.object_id).jPlayer({
    		ready: function(){
    			$(this).jPlayer("setMedia", url);
    		},

            currentTime: '.jp-current-time',
            swfPath:  $theme + "js/jplayer",
            solution: "html, flash",
            volume: '0.5',
    		/*
            size: {
    		    width: this.size.width,
    		    height: this.size.height
    		},

            //backgroundColor: "#000000",

    		cssSelectorAncestor: mediaplayer.cssSelector,
    		supplied: mediaplayer.supplied,

    		 */
    	},mediaplayer.mediaconfig);
    	$('.jp-progress-container').hover(function(){
    		var current_time = $(this).find('.jp-current-time');
    		current_time.stop().show().animate({opacity: 1}, 300);
    	}, function(){
    		var current_time = $(this).find('.jp-current-time');
    		current_time.stop().animate({opacity: 0}, 150, function(){ jQuery(this).hide(); });
    	});
    }
}