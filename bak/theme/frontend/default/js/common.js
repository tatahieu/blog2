/** HAVE TO REQUIRE **/
function createCookie(name,value,time) {
	if (time) {
		var date = new Date();
		date.setTime(date.getTime()+(time*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function delCookie(name) {
     createCookie(name,"",-1);
}
function resizeImage(img,nsize)
{
    var nWidth=img.width;
    var    nHeight=img.height;
    nScale = nWidth/nHeight;
    //Check:
    if (nScale > 1) {
        nWidth = nsize;
        nHeight = (nWidth*img.height)/img.width;
        //fix vertical-align:
        nMargin = (nWidth - nHeight)/2;
        //Set style to image:
        img.width = nWidth;
        img.height = nHeight;
        img.style.margin = nMargin + 'px 0px';
        }
    else {
        //Return size:
        nHeight = nsize;
        nWidth = (nHeight*img.width)/img.height;
        //fix align:
        nMargin = (nHeight - nWidth)/2;
        //Set style to image:
        img.width = nWidth;
        img.height = nHeight;
        img.style.margin = '0px ' + nMargin + 'px';
    }
}

////////////// REDIRECT URL //////////
function redirect($url){
    window.location.href = $url;
}
/** UTIL **/
$(document).ready(function() {
    // UP TO TOP
    $("#support_online").bind("click",function() {
        //$(this).find(".fb-comments").empty();
        $("#support_facebook").fadeIn();
    })
    $("#support_facebook").find(".support_close").bind("click",function() {
        $("#support_facebook").hide();
    })
    $('.backtop').click(function() {
        $('html, body').animate({scrollTop:0},500);
    });
    if (typeof(MISS_PROFILE) != 'undefined' && !jQuery.isEmptyObject(MISS_PROFILE)) {
        var html = '<div  id="update_missing" title="Nhập thông tin còn thiếu">\
            <form method="POST" action="" id="update_missing_form">\
                <div id="update_missing_error"></div>\
                <div class="desc">Điền đầy đủ thông tin để Ms Hoa Toeic chăm sóc bạn tốt hơn</div>\
                <table width="100%">';
                if (typeof(MISS_PROFILE.fullname) != 'undefined') {
                    html += '<tr>\
                        <td class="label">Họ tên:</td>\
                        <td><input id="fullname" type="text" name="fullname" value=""/></td>\
                    </tr>';
                }
                
                if (typeof(MISS_PROFILE.birthday) != 'undefined' ) {
                    var date_option = month_option = year_option =''; 
                    for (i = 1; i <= 31; i ++) {
                        date_option +='<option value="' + i + '">' + i + '</option>';
                    }
                    for (i = 1; i <= 12; i ++) {
                        month_option +='<option value="' + i + '">' + i + '</option>';
                    }
                    for (i = 1930; i <= 2006; i ++) {
                        year_option +='<option value="' + i + '">' + i + '</option>';
                    }
                    html += '<tr>\
                        <td class="label">Ngày sinh:</td>\
                        <td>\
                            Ngày: <select name="birth_date">' + date_option + '</select>\
                            Ngày: <select name="birth_month">' + month_option + '</select>\
                            Ngày: <select name="birth_year">' + year_option + '</select>\
                        </td>\
                    </tr>';
                }
                if (typeof(MISS_PROFILE.live) != 'undefined') {
                    html += '<tr>\
                        <td class="label">Nơi bạn sống:</td>\
                        <td>\
                            <select name="where_live">\
                                <option selected="selected" value="1">Hà Nội</option>\
                                <option value="2">Hồ Chí Minh</option>\
                                <option value="3">Đà Nẵng</option>\
                                <option value="0">Khác</option>\
                            </select>\
                        </td>\
                    </tr>';
                }
                if (typeof(MISS_PROFILE.phone) != 'undefined') {
                    html += '<tr>\
                        <td class="label">Số điện thoại:</td>\
                        <td><input type="text" name="phone" value=""/></td>\
                    </tr>';
                }
            html += '<tr><td colspan="2"><button id="update_miss_profile">Cập nhật thông tin</buton></td></tr>\
                </table>\
            </form>\
        </div>';
        $.fancybox.open(html);
        $("#update_missing_form").submit(function(e) {
            $.ajax({
                   type: "POST",
                   dataType: "json",
                   url: "/users/update_miss",
                   data: $("#update_missing_form").serialize(), // serializes the form's elements.
                   success: function(data)
                   {
                        console.log(data);
                       if(data.status == 'success') {
                            alert("Cập nhật thành công");
                            $.fancybox.close();
                       }
                       else {
                        alert(data.message);
                       }
                   },

            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    }

});
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// tweet
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
// google plus
(function() {
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
po.src = 'https://apis.google.com/js/plusone.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})(); 
function keep_alive_session(time) {
    $.ajax({
        type: "GET",
        url: '/users/keeplife'
    });
    setTimeout(keep_alive_session , 30000);
}
