/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.baseHref = $api_image_detail;
	config.filebrowserBrowseUrl = $site_url+'filemanager/?type=url';
	config.filebrowserImageBrowseUrl = $site_url+'filemanager/?type=image';
	config.filebrowserFlashBrowseUrl = $site_url+'filemanager/?type=flash';


    config.filebrowserUploadUrl = $site_url+'/filemanager/';
    config.filebrowserImageUploadUrl = $site_url+'/filemanager/';
	config.filebrowserFlashUploadUrl = $site_url+'/filemanager/';
    config.filebrowserWindowWidth = '800';
	config.filebrowserWindowHeight = '500';
    config.pasteFromWordNumberedHeadingToList = true;

    config.htmlEncodeOutput = false;
    config.pasteFromWordPromptCleanup = true;
    config.basicEntities = false;
    config.entities = false;
	// Define changes to default configuration here. For example:
	config.allowedContent = true;
	config.removeFormatAttributes = '';
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.removePlugins = 'elementspath';
    config.extraPlugins = 'youtube,audio';
   
    config.toolbar = 'Full';
 
	config.toolbar_Full =
	[
		{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
	        'HiddenField' ] },
		'/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
		'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','Youtube','Audio'] }
	];
	 
	config.toolbar_Basic =
	[
		['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','About']
	];
};
