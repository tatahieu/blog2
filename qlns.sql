-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 05, 2020 lúc 04:23 PM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `qlns`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bang_cap_nhan_vien`
--

CREATE TABLE `bang_cap_nhan_vien` (
  `ma_bang_cap` int(20) NOT NULL,
  `ma_nhan_vien` int(20) NOT NULL,
  `ma_chuyen_nganh` int(20) NOT NULL,
  `ten_bang_cap` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `loai_bang` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ten_truong` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nam_tot_nghiep` date NOT NULL,
  `hinh_thuc_dao_tao` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chuc_nang`
--

CREATE TABLE `chuc_nang` (
  `ma_chuc_nang` int(20) NOT NULL,
  `ten_chuc_nang` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chuc_vu`
--

CREATE TABLE `chuc_vu` (
  `ma_chuc_vu` int(20) NOT NULL,
  `ten_chuc_vu` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ten_quyen` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chuyen_nganh`
--

CREATE TABLE `chuyen_nganh` (
  `ma_chuyen_nganh` int(20) NOT NULL,
  `ten_chuyen_nganh` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cong_tac`
--

CREATE TABLE `cong_tac` (
  `ma_nhan_vien` int(20) NOT NULL,
  `ma_chuc_vu` int(20) NOT NULL,
  `ngay_bdau_ctac` date NOT NULL,
  `ngay_ket_thuc` date NOT NULL,
  `ten_cong_ty` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ghi_chu` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hop_dong`
--

CREATE TABLE `hop_dong` (
  `ma_hop_dong` int(20) NOT NULL,
  `loai_hop_dong` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hop_dong_nhan_vien`
--

CREATE TABLE `hop_dong_nhan_vien` (
  `ma_nhan_vien` int(20) NOT NULL,
  `ma_hop_dong` int(20) NOT NULL,
  `ngay_ky` date NOT NULL,
  `ngay_ket_thuc` date NOT NULL,
  `tinh_trang` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `kt_kl`
--

CREATE TABLE `kt_kl` (
  `ma_ly_do` int(20) NOT NULL,
  `ten_ly_do` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhan_vien`
--

CREATE TABLE `nhan_vien` (
  `ma_nhan_vien` int(20) NOT NULL,
  `ho_va_ten` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_sinh` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `que_quan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quoc_tich` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ton_giao` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `gioi_tinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `trinh_do` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `dien_thoai` int(10) NOT NULL,
  `email` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_vao_cong_ty` date NOT NULL,
  `cmtnd` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhan_vien_khen_thuong`
--

CREATE TABLE `nhan_vien_khen_thuong` (
  `ma_nhan_vien` int(20) NOT NULL,
  `ma_ly_do` int(20) NOT NULL,
  `ngay_kt_kl` date NOT NULL,
  `ghi_chu` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `phong_ban`
--

CREATE TABLE `phong_ban` (
  `ma_phong_ban` int(20) NOT NULL,
  `ten_phong_ban` int(20) NOT NULL,
  `sdt` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `quyen_truy_cap`
--

CREATE TABLE `quyen_truy_cap` (
  `ma_quyen` int(20) NOT NULL,
  `ten_quyen` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ma_chuc_nang` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `thay_doi`
--

CREATE TABLE `thay_doi` (
  `ma_nhan_vien` int(20) NOT NULL,
  `ma_phong_ban` int(20) NOT NULL,
  `ma_chuc_vu` int(20) NOT NULL,
  `ngay_chuyen` date NOT NULL,
  `noi_den` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ly_do_chuyen` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bang_cap_nhan_vien`
--
ALTER TABLE `bang_cap_nhan_vien`
  ADD PRIMARY KEY (`ma_bang_cap`);

--
-- Chỉ mục cho bảng `chuc_nang`
--
ALTER TABLE `chuc_nang`
  ADD PRIMARY KEY (`ma_chuc_nang`);

--
-- Chỉ mục cho bảng `chuc_vu`
--
ALTER TABLE `chuc_vu`
  ADD PRIMARY KEY (`ma_chuc_vu`);

--
-- Chỉ mục cho bảng `chuyen_nganh`
--
ALTER TABLE `chuyen_nganh`
  ADD PRIMARY KEY (`ma_chuyen_nganh`);

--
-- Chỉ mục cho bảng `cong_tac`
--
ALTER TABLE `cong_tac`
  ADD PRIMARY KEY (`ma_nhan_vien`);

--
-- Chỉ mục cho bảng `hop_dong`
--
ALTER TABLE `hop_dong`
  ADD PRIMARY KEY (`ma_hop_dong`);

--
-- Chỉ mục cho bảng `hop_dong_nhan_vien`
--
ALTER TABLE `hop_dong_nhan_vien`
  ADD PRIMARY KEY (`ma_nhan_vien`);

--
-- Chỉ mục cho bảng `kt_kl`
--
ALTER TABLE `kt_kl`
  ADD PRIMARY KEY (`ma_ly_do`);

--
-- Chỉ mục cho bảng `nhan_vien`
--
ALTER TABLE `nhan_vien`
  ADD PRIMARY KEY (`ma_nhan_vien`);

--
-- Chỉ mục cho bảng `nhan_vien_khen_thuong`
--
ALTER TABLE `nhan_vien_khen_thuong`
  ADD PRIMARY KEY (`ma_nhan_vien`);

--
-- Chỉ mục cho bảng `phong_ban`
--
ALTER TABLE `phong_ban`
  ADD PRIMARY KEY (`ma_phong_ban`);

--
-- Chỉ mục cho bảng `quyen_truy_cap`
--
ALTER TABLE `quyen_truy_cap`
  ADD PRIMARY KEY (`ma_quyen`);

--
-- Chỉ mục cho bảng `thay_doi`
--
ALTER TABLE `thay_doi`
  ADD PRIMARY KEY (`ma_nhan_vien`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bang_cap_nhan_vien`
--
ALTER TABLE `bang_cap_nhan_vien`
  MODIFY `ma_bang_cap` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `chuc_nang`
--
ALTER TABLE `chuc_nang`
  MODIFY `ma_chuc_nang` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `chuc_vu`
--
ALTER TABLE `chuc_vu`
  MODIFY `ma_chuc_vu` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `chuyen_nganh`
--
ALTER TABLE `chuyen_nganh`
  MODIFY `ma_chuyen_nganh` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `cong_tac`
--
ALTER TABLE `cong_tac`
  MODIFY `ma_nhan_vien` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `hop_dong`
--
ALTER TABLE `hop_dong`
  MODIFY `ma_hop_dong` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `hop_dong_nhan_vien`
--
ALTER TABLE `hop_dong_nhan_vien`
  MODIFY `ma_nhan_vien` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `kt_kl`
--
ALTER TABLE `kt_kl`
  MODIFY `ma_ly_do` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `nhan_vien`
--
ALTER TABLE `nhan_vien`
  MODIFY `ma_nhan_vien` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `nhan_vien_khen_thuong`
--
ALTER TABLE `nhan_vien_khen_thuong`
  MODIFY `ma_nhan_vien` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `phong_ban`
--
ALTER TABLE `phong_ban`
  MODIFY `ma_phong_ban` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `quyen_truy_cap`
--
ALTER TABLE `quyen_truy_cap`
  MODIFY `ma_quyen` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `thay_doi`
--
ALTER TABLE `thay_doi`
  MODIFY `ma_nhan_vien` int(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
